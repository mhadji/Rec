#ifndef PRFTZONEHANDLER_H 
#define PRFTZONEHANDLER_H 1

// Include files
#include "DetDesc/ValidDataObject.h"
#include "Kernel/DetectorSegment.h"
#include "PrKernel/PrHitZone.h"
#include "PrKernel/PrFTInfo.h"

/** @class PrFTZoneHandler PrFTZoneHandler.h
 * Handlers of zones, the object is stored in the detector event store as a condition
 * and each algorithms reads the object from there without calling the HitManagers (tools)
 * @author Renato Quagliani
 * @author Sebastien Ponce
 */

typedef std::vector<PrHitZone*> PrHitZones;

class PrFTZoneHandler : public ValidDataObject {

public: 

  /// Standard constructor
  PrFTZoneHandler( ) = default;
  
  void MakeZone( unsigned int n , DetectorSegment& seg , float xMin, float xMax, float yMin, float yMax){
    m_zones[n].setZone( n, seg, xMin, xMax, yMin, yMax);
  }
  const PrHitZone& zone( unsigned int n ) const{ 
    return m_zones[n];
  }

 private:

  //plain vector with indexing needed!
  std::array<PrHitZone,PrFTInfo::Numbers::NFTZones> m_zones;
};

#endif // PRFTZONEHANDLER_H
