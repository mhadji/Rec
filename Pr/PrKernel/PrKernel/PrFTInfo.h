#ifndef PRFTINFO_H 
#define PRFTINFO_H 1

#include <string>

/** Constant information of the detector
 *  @author Sebastien Ponce
 *  @date   2016-09-30
 */

namespace PrFTInfo {
  
  enum Numbers { NFTZones    = 24 , 
                 NFTXLayers  = 6 ,
                 NFTUVLayers = 6 };
  
  unsigned int nbZones();

  const std::string FTHitsLocation = "FT/FTHits";
  const std::string FTCondLocation = "Conditions/FT";
  const std::string FTZonesLocation = "Conditions/FT/FTZones";
  
}

#endif // PRFTINFO_H
