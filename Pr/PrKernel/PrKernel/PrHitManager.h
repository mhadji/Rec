// $Id: $
#ifndef PRKERNEL_PRHITMANAGER_H
#define PRKERNEL_PRHITMANAGER_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/IIncidentListener.h"
#include "PrKernel/PrHit.h"
#include "PrKernel/PrHitZone.h"

static const InterfaceID IID_PrHitManager ( "PrHitManager", 1, 0 );

/** @class PrHitManager PrHitManager.h PrKernel/PrHitManager.h
 *  Hit manager for the Hits in the SciFi
 *
 *  @author Olivier Callot
 *  @date   2012-03-13
 *  @author Thomas Nikodem
 *  @date   2016-04-26
 *  @author Renato Quagliani
 *  @data   2016-06-21
 */

typedef std::vector<PrHit>::iterator HitIterat;

class PrHitManager final : public GaudiTool, public IIncidentListener {
public:
  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_PrHitManager; }
  /// Standard constructor
  PrHitManager( const std::string& type,
                const std::string& name,
                const IInterface* parent);
  virtual ~PrHitManager( ); ///< Destructor
  StatusCode initialize() override;
  StatusCode finalize() override;
  virtual void buildGeometry() {};
  virtual void decodeData()    {};
  /// Clear the zones, and reset the hit pointer. Store the maximum size.
  void clearHits() {
    for(unsigned int i=0;i<m_hits.size();i++){
      if( LIKELY( m_hits[i].size() != 0)) m_hits[i].clear();
    }
    m_decodedData = false;
  };
  /// Handle the incident 'BeginEvent': Invalidate the decoding
  void handle ( const Incident& incident ) override {
    if ( IncidentType::BeginEvent == incident.type() ){
      this->clearHits();
      m_eventReady = false;
    }
  };

  template <typename ... Args>
  PrHit& addHitInZone( unsigned int lay, Args&& ...  args ){
    m_hits[lay].emplace_back( std::forward<Args>(args)... );
    return m_hits[lay].back();
  }
  void MakeZone( unsigned int n , DetectorSegment& seg , float xMin, float xMax, float yMin, float yMax){
    m_zones[n].setZone( n, seg, xMin, xMax, yMin, yMax);
    m_hits[n].reserve(500);
  }
  /// Access a zone. Creation is handled separately
  PrHitZone& zone( unsigned int n ){
    return m_zones[n];
  }

  /// Return the current number of zones
  unsigned int nbZones()   const { return Numbers::NFTZones;  }

  bool isEmpty(unsigned int lay) {return getIterator_End(lay) == getIterator_Begin(lay);}
  // Hit handling/ sorting etc...
  struct compX{
    inline bool operator()(      float  lv,        float  rv)  const{return lv < rv;}
    inline bool operator()(const PrHit& lhs,       float  rv)  const{return (*this)(lhs.x(),  rv);}
    inline bool operator()(      float  lv,  const PrHit& rhs) const{return (*this)(lv,       rhs.x());}
    inline bool operator()(const PrHit& lhs, const PrHit& rhs) const{return (*this)(lhs.x(),  rhs.x());}
    inline bool operator()(const PrHit* lhs,       float  rv)  const{return (*this)(lhs->x(), rv);}
    inline bool operator()(      float  lv,  const PrHit* rhs) const{return (*this)(lv,       rhs->x());}
    inline bool operator()(const PrHit* lhs, const PrHit* rhs) const{return (*this)(lhs->x(), rhs->x());}
  };
  struct compXreverse{
    inline bool operator()(      float  lv,        float  rv)  const{return lv > rv;}
    inline bool operator()(const PrHit& lhs,       float  rv)  const{return (*this)(lhs.x(),  rv);}
    inline bool operator()(      float  lv,  const PrHit& rhs) const{return (*this)(lv,       rhs.x());}
    inline bool operator()(const PrHit& lhs, const PrHit& rhs) const{return (*this)(lhs.x(),  rhs.x());}
    inline bool operator()(const PrHit* lhs,       float  rv)  const{return (*this)(lhs->x(), rv);}
    inline bool operator()(      float  lv,  const PrHit* rhs) const{return (*this)(lv,       rhs->x());}
    inline bool operator()(const PrHit* lhs, const PrHit* rhs) const{return (*this)(lhs->x(), rhs->x());}
  };

  template <class ForwardIt, class T>
  ForwardIt linear_lower_bound(ForwardIt first, ForwardIt last, T value){
    while (first != last && *first < value) ++first;
    return first;
  }

  template <class ForwardIt, class T, class Compare>
  ForwardIt linear_lower_bound(ForwardIt first, ForwardIt last, T value, Compare comp){
    while (first != last && comp(*first, value)) ++first;
    return first;
  }

  template <class ForwardIt, class T>
  ForwardIt linear_upper_bound(ForwardIt first, ForwardIt last, T value){
    while (first != last && !(value < *first)) ++first;
    return first;
  }
  template <class ForwardIt, class T, class Compare>
  ForwardIt linear_upper_bound(ForwardIt first, ForwardIt last, T value, Compare comp){
    while(first != last && !comp(value, *first)) ++first;
    return first;
  }

  //search lowerBound given a layer in log time
  HitIterat getIterator_lowerBound(unsigned int lay, float xMin){
    return std::lower_bound( getIterator_Begin(lay), getIterator_End(lay), xMin ,
                             [](const PrHit& a, const float testval) -> bool { return a.x() < testval;} );
  }

  //search upperBound given a layer in log timing
  HitIterat getIterator_upperBound(unsigned int lay, float xMax){
    return std::upper_bound( getIterator_Begin(lay), getIterator_End(lay), xMax ,
                             [](const float testval, const PrHit& a) -> bool { return a.x() > testval ; });
  }

  //search lowerBound from known position to another known position ( log time)
  HitIterat get_lowerBound_log(HitIterat& given_it, HitIterat& end, float xMin){
    return std::lower_bound(given_it, end, xMin, compX());
  }
  //search upperBound from a known position to another known position (log time)
  HitIterat get_upperBound_log(HitIterat& given_it, HitIterat& end,float xMax){
    return std::upper_bound( given_it, end, xMax, compX());
  }
  //search lowerBound from a known position to another known position (linear time)
  HitIterat get_lowerBound_lin(HitIterat& given_it, HitIterat&end, float xMin){
    return linear_lower_bound( given_it , end, xMin, compX());
  }
  //search upperBound from known position to another known position (linear time)
  HitIterat get_upperBound_lin(HitIterat& given_it, HitIterat& end, float xMax){
    return linear_upper_bound( given_it, end, xMax, compX());
  }
  //search with STL method a lowerBound in low/high range starting from high
  HitIterat get_lowerBound_log_reverse( HitIterat& low, HitIterat& high, float xMin){
    std::reverse_iterator<HitIterat> revBegin(high);
    std::reverse_iterator<HitIterat> revEnd(low);
    std::reverse_iterator<HitIterat> revLower = std::upper_bound( revBegin, revEnd, xMin, compXreverse());
    return revLower.base();
  }
  //search with STL method an upper bond in low/high range starting from high
  HitIterat get_upperBound_log_reverse( HitIterat& low, HitIterat& high, float xMax){
    std::reverse_iterator<HitIterat> revBegin(high);
    std::reverse_iterator<HitIterat> revEnd(low);
    std::reverse_iterator<HitIterat> revUpper = std::lower_bound( revBegin, revEnd, xMax, compXreverse());
    return revUpper.base();
  }

  //search linearly a lowerBound in low/high range starting from high
  HitIterat get_lowerBound_lin_reverse( HitIterat&low, HitIterat& high, float xMin){
    std::reverse_iterator<HitIterat> revBegin(high);
    std::reverse_iterator<HitIterat> revEnd(low);
    std::reverse_iterator<HitIterat> revLower = linear_upper_bound( revBegin, revEnd, xMin, compXreverse());
    return revLower.base();
  }

  //search linearly an upperBound in low/high range starting from high
  HitIterat get_upperBound_lin_reverse( HitIterat& low, HitIterat& high, float xMax){
    std::reverse_iterator<HitIterat> revBegin(high);
    std::reverse_iterator<HitIterat> revEnd(low);
    std::reverse_iterator<HitIterat> revUpper = linear_lower_bound( revBegin, revEnd, xMax, compXreverse());
    return revUpper.base();
  }
  HitIterat getIterator_Begin(unsigned int lay){
    return m_hits[lay].begin();
  }
  HitIterat getIterator_End(unsigned int lay){
    return m_hits[lay].end();
  }
  // Constants you need to change if  Sci-Fi number of layers is going to change.
  enum Numbers { NFTZones    = 24 ,
                 NFTXLayers  = 6 ,
                 NFTUVLayers = 6 };
protected:
  bool  m_geometryBuilt = false; ///< Flag to check if geometry was already built
  bool  m_decodedData   = false;   //don't decode data twice!
private:
  std::array< std::vector<PrHit> , Numbers::NFTZones> m_hits;
  //plain vector with indexing needed!
  std::array<PrHitZone,Numbers::NFTZones> m_zones;
  int              m_maxSize;
  bool             m_eventReady = true;
};
#endif // PRKERNEL_PRHITMANAGER_H
