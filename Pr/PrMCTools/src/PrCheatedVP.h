#ifndef PRCHEATEDVP_H 
#define PRCHEATEDVP_H 1

// Gaudi
#include "GaudiAlg/Transformer.h"
// Event/DigiEvent
#include "Event/VPCluster.h"
#include "PrFitParams/IPrFitTool.h"
#include "PrFitParams/PrFitTool.h"

/** @class PrCheatedVP PrCheatedVP.h
 *  Cheated pattern recognition for the upgraded VELO
 *
 *  @author Olivier Callot
 *  @date   2012-07-26
 */

template <bool useMCHits> 
class PrCheatedVPBase : public 
    Gaudi::Functional::Transformer<std::conditional_t<useMCHits,
        LHCb::Tracks(const LHCb::MCParticles&, const LHCb::MCHits&),
        LHCb::Tracks(const LHCb::MCParticles&)>>
{
public:
  /// Using Transfomer's constructor
  using PrCheatedVPBase::Transformer::Transformer;

  // Algorithm initialization
  StatusCode initialize() override;

protected:
  const IPrFitTool* m_ft = nullptr;

private:
  ToolHandle<const IPrFitTool> m_fitTool{"PrFitTool", this};
};

class PrCheatedVP final : public PrCheatedVPBase<false>
{
public:
  PrCheatedVP(const std::string& name, ISvcLocator* pSvcLocator);
  LHCb::Tracks operator() (const LHCb::MCParticles&) const override;
};

class PrCheatedVPMCHits final : public PrCheatedVPBase<true>
{
public:
  PrCheatedVPMCHits(const std::string& name, ISvcLocator* pSvcLocator);
  LHCb::Tracks operator() (const LHCb::MCParticles&, const LHCb::MCHits&) const override;
};



#endif 

