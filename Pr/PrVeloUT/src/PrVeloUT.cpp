// Include files

// local
#include "PrVeloUT.h"
//-----------------------------------------------------------------------------
// Implementation file for class : PrVeloUT
//
// 2007-05-08 : Mariusz Witek
// 2017-03-01: Christoph Hasse (adapt to future framework)
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PrVeloUT )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrVeloUT::PrVeloUT(const std::string& name,
                   ISvcLocator* pSvcLocator) :
Transformer(name, pSvcLocator,
            KeyValue{"InputTracksName", LHCb::TrackLocation::Velo} ,
            KeyValue{"OutputTracksName", LHCb::TrackLocation::VeloTT}){
}
//=============================================================================
// Initialization
//=============================================================================
StatusCode PrVeloUT::initialize() {
  auto sc = Transformer::initialize();
  if (sc.isFailure()) return sc;  // error printed already by GaudiAlgorithm

  m_veloUTTool = tool<ITracksFromTrackR>("PrVeloUTTool", this );

  if (m_doTiming) {
    m_timerTool = tool<ISequencerTimerTool>( "SequencerTimerTool" );
    m_timerTool->increaseIndent();
    m_veloUTTime = m_timerTool->addTimer( "Internal VeloUT Tracking" );
    m_timerTool->decreaseIndent();
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
LHCb::Tracks PrVeloUT::operator()(const LHCb::Tracks& inputTracks) const {
  if ( m_doTiming ) m_timerTool->start( m_veloUTTime );
  LHCb::Tracks outputTracks;
  outputTracks.reserve(200);
  std::vector<LHCb::Track*> tmpTracks;
  tmpTracks.reserve(5);
  counter("#seeds") += inputTracks.size();

  auto eventState = m_veloUTTool->createState();
  for(const auto& veloTr: inputTracks) {
    m_veloUTTool->tracksFromTrack(*veloTr, tmpTracks, eventState).ignore();
    if(!tmpTracks.empty()){
      LHCb::Track* outTrack = *tmpTracks.begin();
      tmpTracks.clear();
      outputTracks.insert(outTrack);
    }
  }
  counter("#tracks") += outputTracks.size();
  if ( m_doTiming ) m_timerTool->stop( m_veloUTTime );
  return outputTracks;
}
