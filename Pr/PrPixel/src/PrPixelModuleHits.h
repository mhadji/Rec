#ifndef PRPIXELMODULEHITS_H
#define PRPIXELMODULEHITS_H 1

// Local
#include "PrPixelHit.h"

// Range V3
#include <range/v3/algorithm.hpp>
#include <range/v3/view.hpp>

/** @class PrPixelModule PrPixelModule.h
 *  Class to hold hits for one VP module
 *
 *  @author Sebastien Ponce
 */
namespace {
template <std::size_t N>
struct GetN {
  template <typename T>
  auto operator()(T&& t) const ->
    decltype(std::get<N>(std::forward<T>(t))) {
      return std::get<N>(std::forward<T>(t));
  }
};
}

class PrPixelModuleHits final {

 public:
  bool empty() const { return m_empty; }
  float lastHitX() const { return m_lastHitX; }
  void setLastHitX(const float x) { m_lastHitX = x; }
  float firstHitX() const { return m_firstHitX; }
  void setFirstHitX(const float x) { m_firstHitX = x; }
  const PrPixelHits& hits() const { return m_hits; }
  PrPixelHits& hits() { return m_hits; }
  const std::vector<float>& xFractions() const { return m_xFractions; }
  const std::vector<float>& yFractions() const { return m_yFractions; }
  const std::vector<std::vector<LHCb::VPChannelID>>& channelIDs() const { return m_channelIDs; }

  /// Remove all stored hits.
  void reset() {
    m_hits.clear();
    m_empty = true;
  }

  /// Add a new (pointer to) hit.
  void addHit(PrPixelHit&& hit, float fx, float fy) {
    m_hits.emplace_back(hit);
    m_xFractions.emplace_back(fx);
    m_yFractions.emplace_back(fy);
    m_empty = false;
  }

  /// Add a new (pointer to) hit.
  void addHit(PrPixelHit&& hit, float fx, float fy, std::vector<LHCb::VPChannelID>&& channelIDs) {
    m_hits.emplace_back(std::move(hit));
    m_xFractions.emplace_back(fx);
    m_yFractions.emplace_back(fy);
    m_channelIDs.emplace_back(std::move(channelIDs));
    m_empty = false;
  }

  void sort() {
     const auto lower = PrPixelHit::LowerByX{};
     auto compare = [&lower](const PrPixelHit& h1, const PrPixelHit& h2) {
        return lower(h1, h2);
     };

     if (m_channelIDs.empty()) {
        auto zipped = ranges::view::zip(m_hits, m_xFractions, m_yFractions);
        ranges::sort(zipped, compare, GetN<0>{});
     } else {
        auto zipped = ranges::view::zip(m_hits, m_xFractions, m_yFractions, m_channelIDs);
        ranges::sort(zipped, compare, GetN<0>{});
     }
  }

 private:
  float m_lastHitX = -1;
  float m_firstHitX;
  /// Vector of pointers to hits
  PrPixelHits m_hits;
  std::vector<float> m_xFractions;
  std::vector<float> m_yFractions;
  std::vector<std::vector<LHCb::VPChannelID>> m_channelIDs;
  bool m_empty = true;
};
#endif  // PRPIXELMODULEHITS_H
