// LHCb
#include "Event/Track.h"
#include "Event/StateParameters.h"
#include "Kernel/VPConstants.h"
// Local
#include "PrPixelTracking.h"

DECLARE_COMPONENT( PrPixelTracking )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrPixelTracking::PrPixelTracking(const std::string &name,
                                 ISvcLocator *pSvcLocator) :
MultiTransformer(name , pSvcLocator,
                 KeyValue{"RawEventLocations",
                          Gaudi::Functional::concat_alternatives(LHCb::RawEventLocation::Velo,
                                                                 LHCb::RawEventLocation::Default,
                                                                 LHCb::RawEventLocation::Other)},
                 {KeyValue{"OutputTracksName", LHCb::TrackLocation::Velo},
                  KeyValue{"ClusterLocation", LHCb::VPClusterLocation::Default}}) {}


//=============================================================================
// Initialization
//=============================================================================
StatusCode PrPixelTracking::initialize() {

  StatusCode sc = MultiTransformer::initialize();
  if (sc.isFailure()) return sc;
  // Setup the hit manager.
  m_hitManager->setMaxClusterSize(m_maxClusterSize);
  m_hitManager->setTrigger(m_trigger);

  // Setup the debug tool.
  if ("" != m_debugToolName.value()) m_debugTool = tool<IPrDebugTool>(m_debugToolName.value());
  // Setup the timing measurement.
  if (m_doTiming) {
    m_timerTool = tool<ISequencerTimerTool>("SequencerTimerTool/Timer", this);
    m_timeTotal = m_timerTool->addTimer("Total");
    m_timerTool->increaseIndent();
    m_timePrepare = m_timerTool->addTimer("Prepare");
    m_timePairs = m_timerTool->addTimer("Find by pairs");
    m_timeFinal = m_timerTool->addTimer("Store tracks");
    m_timerTool->decreaseIndent();
  }

  // use the square of the scatter to avoid calls to sqrt()
  m_maxScatter.set(m_maxScatter.value()*m_maxScatter.value());

  // always set Histo top dir if we are debugging histos
#ifdef DEBUG_HISTO
    setHistoTopDir("VP/");
#endif

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
std::tuple<LHCb::Tracks, LHCb::VPClusters> PrPixelTracking::operator()(const LHCb::RawEvent& rawEvent) const {
  // Build hits from clusters and sort them by global x within each module.

  // start timing -- FIXME : not thead safe !
  if (m_doTiming) {
    m_timerTool->start(m_timeTotal);
    m_timerTool->start(m_timePrepare);
  }

  // create hits and clusters
  std::vector<PrPixelModuleHits> modulehits(m_hitManager->lastModule()+1);
  LHCb::VPClusters clusters;
  StatusCode sc = m_hitManager->process(rawEvent, modulehits, clusters, m_storecluster);
  if (sc.isFailure()) {
    throw GaudiException("Failure in getting hits!", "PrPixelTracking" , sc);
  }

  if (0 <= m_wantedKey) {
    info() << "--- Looking for track " << m_wantedKey << endmsg;
    for (unsigned int i = m_hitManager->firstModule(); i <= m_hitManager->lastModule(); ++i) {
      for (const auto& hit: modulehits[i].hits()) {
        if (matchKey(hit)) printHit(hit);
      }
    }
  }

  if (m_doTiming) m_timerTool->stop(m_timePrepare);

  // Allocate space for Tracks
  PrPixelTracks tracks;
  tracks.reserve(10000);

  // Search for tracks by finding a pair of hits, then extrapolating.
  if (m_doTiming) m_timerTool->start(m_timePairs);
  searchByPair(modulehits, tracks);
  if (m_doTiming) m_timerTool->stop(m_timePairs);

  // Convert temporary tracks to LHCb tracks.
  if (m_doTiming) m_timerTool->start(m_timeFinal);
  LHCb::Tracks outputTracks;
  makeLHCbTracks(tracks, outputTracks);
  if (m_doTiming) {
    m_timerTool->stop(m_timeFinal);
    m_timerTool->stop(m_timeTotal);
  }

#ifdef DEBUG_HISTO
    monitor(modulehits, outputTracks, clusters);
#endif

  return std::make_tuple(std::move(outputTracks), std::move(clusters));
}

//=============================================================================
// Extend track towards smaller z,
// on both sides of the detector as soon as one hit is missed.
//=============================================================================
void PrPixelTracking::extendTrack(const std::vector<PrPixelModuleHits>& modulehits,
                                  PrPixelTrack& track,
                                  const PrPixelHit& hit1,
                                  const PrPixelHit& hit2,
                                  bool debug) const {
  // Pointers for easy swapping
  const PrPixelHit* h1 = &hit1;
  const PrPixelHit* h2 = &hit2;
  // Initially scan every second module (stay on the same side).
  int step = 2;
  // Start two modules behind the last one.
  int next = h2->module() - step;
  // Count modules without hits found.
  unsigned int nbMissed = 0;
  while (next >= 0) {
    // Attempt to add new hits from this module with given tolerances
    const PrPixelHit *h3 = bestHit(modulehits[next], m_hitManager->module(next)->z(),
                                   m_extraTol, m_maxScatter, *h1, *h2, debug);
    if (h3) {
      track.addHit(h3);
      // Reset missed hit counter.
      nbMissed = 0;
      // Update the pair of hits to be used for extrapolating.
      h1 = h2;
      h2 = h3;
    } else {
      // No hits found.
      if (step == 2) {
        // Look on the other side.
        h3 = bestHit(modulehits[next+1], m_hitManager->module(next + 1)->z(),
                     m_extraTol, m_maxScatter, *h1, *h2, debug);
        if (!h3) {
          nbMissed += step;
        } else {
          track.addHit(h3);
          h1 = h2;
          h2 = h3;
        }
        // Switch to scanning every module (left and right).
        step = 1;
      } else {
        ++nbMissed;
      }
    }
    if (m_maxMissed < nbMissed) break;
    next -= step;
  }
}

//=========================================================================
//  Search starting with a pair of consecutive modules.
//=========================================================================
void PrPixelTracking::searchByPair(const std::vector<PrPixelModuleHits>& modulehits,
                                   PrPixelTracks& tracks) const {

  // Get the range of modules to start search on,
  // starting with the one at largest Z.
  const int lastModule = m_hitManager->lastModule();
  const int firstModule = m_hitManager->firstModule() + 4;
  for (int sens0 = lastModule; firstModule <= sens0; --sens0) {
    // Pick-up the "paired" module one station backwards
    const int sens1 = sens0 - 2;
    const float z0 = m_hitManager->module(sens0)->z();
    const float z1 = m_hitManager->module(sens1)->z();
    const float dz = z0 - z1;
    // Calculate the search window from the slope limits.
    const float dxMax = m_maxXSlope * fabs(dz);
    const float dyMax = m_maxYSlope * fabs(dz);
    // Loop over hits in the first module (larger Z) in the pair.
    const auto& module1hits = modulehits[sens1].hits();
    auto end1 = module1hits.cend();
    auto first1 = module1hits.cbegin();
    for (const auto& hit0: modulehits[sens0].hits()) {
      // Skip hits already assigned to tracks.
      if (hit0.isUsed()) continue;
      const float x0 = hit0.x();
      const float y0 = hit0.y();
      // Calculate x-pos. limits on the other module.
      const float xMin = x0 - dxMax;
      const float xMax = x0 + dxMax;
      if (UNLIKELY(m_debugTool != nullptr && matchKey(hit0))) {
        info() << format("s1%3d xMin%9.3f xMax%9.3f ", sens1, xMin, xMax);
        printHit(hit0, "St0");
      }
      // Loop over hits in the second module (smaller Z) in the pair.
      for (auto ith1 = first1; end1 != ith1; ith1++) {
        const float x1 = ith1->x();
        const float y1 = ith1->y();
        // Skip hits below the X-pos. limit.
        if (x1 < xMin) {
          first1 = ith1 + 1;
          continue;
        }
        // Stop search when above the X-pos. limit.
        if (x1 > xMax) break;
        // Skip hits already assigned to tracks.
        if (ith1->isUsed()) continue;
        // Skip hits out of Y-pos. limit.
        if (fabs(y1 - y0) > dyMax) continue;

        bool debug = false;
        if (UNLIKELY(m_debugTool != nullptr)) {
          if (matchKey(hit0) && matchKey(*ith1)) debug = true;
          if (debug) {
            info() << format("s1%3d dxRel %7.3f dyRel %7.3f    ", sens1,
                             (x1 - xMin) / (xMax - xMin),
                             fabs(ith1->y() - y0) / dyMax);
            printHit(*ith1);
          }
        }
        // Make a seed track out of these two hits.
        PrPixelTrack track{hit0, *ith1};
        // Extend the seed track towards smaller Z.
        extendTrack(modulehits, track, hit0, *ith1, debug);
        const unsigned int nHits = track.hits().size();
        if (nHits < 3) continue;

        // Final checks
        if (nHits == 3) {
          // In case of short tracks, all three hits should be unused.
          if (track.nbUnused() != 3) {
            if (UNLIKELY(debug)) {
              info() << "  -- reject, only " << track.nbUnused()
                     << " unused hits." << endmsg;
              printTrack(track);
            }
            continue;
          }
          // Fit the track and apply a cut on the chi2.
          track.fit();
          if (track.chi2() > m_maxChi2Short) {
            if (UNLIKELY(debug)) {
              info() << " -- reject, chi2 " << track.chi2() << " too high."
                     << endmsg;
              printTrack(track);
            }
            continue;
          }
        } else {
          if (track.nbUnused() < m_fractionUnused * nHits) {
            if (UNLIKELY(debug)) {
              info() << "  -- reject, only " << track.nbUnused() << "/"
                     << track.hits().size() << " hits are unused." << endmsg;
              printTrack(track);
            }
            continue;
          }
        }

        tracks.push_back(track);
        if (UNLIKELY(debug || msgLevel(MSG::DEBUG))) {
          info() << "=== Store track Nb " << tracks.size() << endmsg;
          printTrack(track);
        }
        if (nHits > 3) {
          track.tagUsedHits();
          break;
        }
      }
    }
  }
}

//=========================================================================
// Convert the local tracks to LHCb tracks
//=========================================================================
void PrPixelTracking::makeLHCbTracks(const PrPixelTracks& tracks,
                                     LHCb::Tracks& outputTracks) const {

  unsigned int key = 0;
  for (auto track: tracks) {
    // Skip 3-hit tracks with float-used hits.
    if (track.hits().size() == 3) {
      if (track.nbUnused() != 3) continue;
    } else {
      track.fit();
    }
    // Create a new LHCb track.
    LHCb::Track *newTrack = new LHCb::Track(key++);
    newTrack->setType(LHCb::Track::Velo);
    newTrack->setHistory(LHCb::Track::PatFastVelo);
    newTrack->setPatRecStatus(LHCb::Track::PatRecIDs);
    if (UNLIKELY(m_debugTool != nullptr)) {
      info() << "=== Store track Nb " << outputTracks.size() << "\tnhits "
             << track.hits().size() << endmsg;
      printTrack(track);
    }
    // Sort the hits back by decreasing z.
    std::sort(track.hits().begin(), track.hits().end(),
              PrPixelHit::DecreasingByZ());

    // Loop over the hits and add their LHCbIDs to the LHCb track.
    for (auto hit: track.hits()) {
      newTrack->addToLhcbIDs(hit->id());
    }
    // Decide if this is a forward or backward track.
    // Calculate z where the track passes closest to the beam.
    const float zBeam = track.zBeam();
    // Define backward as z closest to beam downstream of hits.
    const bool backward = zBeam > track.hits().front()->z();
    newTrack->setFlag(LHCb::Track::Backward, backward);

    // Get the state at zBeam from the straight line fit.
    LHCb::State state;
    state.setLocation(LHCb::State::ClosestToBeam);
    state.setState(track.state(zBeam));
    state.setCovariance(track.covariance(zBeam));

    // Parameters for kalmanfit scattering. calibrated on MC, shamelessly
    // hardcoded:
    const float tx = state.tx();
    const float ty = state.ty();
    const float scat2 = 1e-8 + 7e-6 * (tx * tx + ty * ty);

    // The logic is a bit messy in the following, so I hope we got all cases
    // right
    if (m_stateClosestToBeamKalmanFit ||
        m_addStateFirstLastMeasurementKalmanFit) {
      // Run a K-filter with scattering to improve IP resolution
      LHCb::State upstreamstate;
      track.fitKalman(upstreamstate, backward ? 1 : -1, scat2);
      // Add this state as state at first measurement if requested
      if (m_addStateFirstLastMeasurementKalmanFit) {
        upstreamstate.setLocation(LHCb::State::FirstMeasurement);
        newTrack->addToStates(upstreamstate);
      }
      // Transport the state to the closestToBeam position
      if (m_stateClosestToBeamKalmanFit) {
        upstreamstate.setLocation(LHCb::State::ClosestToBeam);
        upstreamstate.linearTransportTo(zBeam);
        newTrack->addToStates(upstreamstate);
      }
    }
    if (!m_stateClosestToBeamKalmanFit) {
      newTrack->addToStates(state);
    }

    // Set state at last measurement, if requested
    if ((!backward && m_stateEndVeloKalmanFit) ||
        m_addStateFirstLastMeasurementKalmanFit) {
      LHCb::State downstreamstate;
      track.fitKalman(downstreamstate, backward ? -1 : +1, scat2);
      if (m_addStateFirstLastMeasurementKalmanFit) {
        downstreamstate.setLocation(LHCb::State::LastMeasurement);
        newTrack->addToStates(downstreamstate);
      }
      if (m_stateEndVeloKalmanFit) {
        state = downstreamstate;
      }
    }

    // Add state at end of velo
    if (!backward) {
      state.setLocation(LHCb::State::EndVelo);
      state.linearTransportTo(StateParameters::ZEndVelo);
      newTrack->addToStates(state);
    }

    // Set the chi2/dof
    newTrack->setNDoF(2 * (track.hits().size() - 2));
    newTrack->setChi2PerDoF(track.chi2());
    // Add the LHCb track to the list.
    outputTracks.insert(newTrack);
  }

}

//=========================================================================
// Add hits from the specified module to the track
//=========================================================================
const PrPixelHit *PrPixelTracking::bestHit(const PrPixelModuleHits& modulehits,
                                           float module_z,
                                           const float xTol,
                                           const float maxScatter,
                                           const PrPixelHit& h1,
                                           const PrPixelHit& h2,
                                           bool debug) const {
  if (modulehits.empty()) return nullptr;
  const float x1 = h1.x();
  const float x2 = h2.x();
  const float y1 = h1.y();
  const float y2 = h2.y();
  const float z1 = h1.z();
  const float z2 = h2.z();
  const float td = 1.0 / (z2 - z1);
  const float txn = (x2 - x1);
  const float tx = txn * td;
  const float tyn = (y2 - y1);
  const float ty = tyn * td;
  // Extrapolate to the z-position of the module
  const float xGuess = x1 + tx * (module_z - z1);

  // If the first hit is already below this limit we can stop here.
  if (modulehits.lastHitX() < xGuess - xTol) return nullptr;
  if (modulehits.firstHitX() > xGuess + xTol) return nullptr;

  // Do a binary search through the hits.
  unsigned int hit_start(0);
  unsigned int step(modulehits.hits().size());
  const unsigned int module_nhits(step);
  const PrPixelHits &module_hits = modulehits.hits();
  while (2 < step) {  // quick skip of hits that are above the X-limit
    step /= 2;
    if ((module_hits[hit_start + step]).x() < xGuess - xTol) hit_start += step;
  }

  // Find the hit that matches best.
  unsigned int nFound = 0;
  float bestScatter = maxScatter;
  const PrPixelHit *bestHit = nullptr;
  for (unsigned int i = hit_start; i < module_nhits; ++i) {
    const auto& hit = module_hits[i];
    const float hit_z = hit.z();
    const float hit_x = hit.x();
    const float hit_y = hit.y();
    const float dz = hit_z - z1;
    const float xPred = x1 + tx * dz;
    const float yPred = y1 + ty * dz;
#ifdef DEBUG_HISTO
    plot((hit.x() - xPred) / xTol, "HitExtraErrPerTol",
         "Hit X extrapolation error / tolerance", -4.0, +4.0, 400);
#endif
    // If x-position is above prediction + tolerance, keep looking.
    if (hit_x + xTol < xPred) continue;
    // If x-position is below prediction - tolerance, stop the search.
    if (hit_x - xTol > xPred) break;
    const float dy = yPred - hit_y;
    // Skip hits outside the y-position tolerance.
    if (fabs(dy) > xTol) continue;
    const float scatterDenom = 1.0 / (hit_z - z2);
    const float dx = xPred - hit_x;
    const float scatterNum = (dx * dx) + (dy * dy);
    const float scatter = scatterNum * scatterDenom * scatterDenom;
    if (scatter < bestScatter) {
      bestHit = &hit;
      bestScatter = scatter;
    }
    if (scatter < maxScatter) ++nFound;
#ifdef DEBUG_HISTO
    plot(sqrt(scatter), "HitScatter", "hit scatter [rad]", 0.0, 0.5, 500);
    plot2D(dx, dy, "Hit_dXdY",
           "Difference between hit and prediction in x and y [mm]", -1, 1, -1,
           1, 500, 500);
#endif
  }
#ifdef DEBUG_HISTO
  plot(nFound, "HitExtraCount",
       "Number of hits within the extrapolation window with chi2 within limits",
       0.0, 10.0, 10);
#endif
  if (bestHit) {
#ifdef DEBUG_HISTO
    plot(sqrt(bestScatter), "HitBestScatter", "best hit scatter [rad]", 0.0,
         0.1, 100);
#endif
    if (UNLIKELY(debug)) printHitOnTrack(*bestHit, false);
  }
  return bestHit;
}

//=========================================================================
// Debug the content of a hit
//=========================================================================
void PrPixelTracking::printHit(const PrPixelHit& hit,
                               const std::string& title) const {
  info() << title;
  info() << format(" module%3d x%8.3f y%8.3f z%8.2f used%2d", hit.module(),
                   hit.x(), hit.y(), hit.z(), hit.isUsed());
  if (UNLIKELY(m_debugTool != nullptr)) {
    const LHCb::LHCbID& id = hit.id();
    info() << " MC: ";
    m_debugTool->printKey(info(), id);
    if (matchKey(hit)) info() << " ***";
  }
  info() << endmsg;
}

//=========================================================================
// Print all hits on a track.
//=========================================================================
void PrPixelTracking::printTrack(PrPixelTrack& track) const {
  for (const auto& hit: track.hits()) {
    printHit(*hit);
  }
}

//=========================================================================
// Print a hit on a track.
//=========================================================================
void PrPixelTracking::printHitOnTrack(const PrPixelHit& hit,
                                      const bool ifMatch) const {
  bool isMatching = matchKey(hit);
  isMatching = (isMatching && ifMatch) || (!isMatching && !ifMatch);
  if (isMatching) printHit(hit, "   ");
}


//=========================================================================
// countHits
//=========================================================================
std::tuple<unsigned int, unsigned int>
PrPixelTracking::countHits(const std::vector<PrPixelModuleHits>& modulehits) const {
  unsigned int n = 0;
  unsigned int nUsed = 0;
  for (const auto& modulehit: modulehits) {
    for (const auto& hit: modulehit.hits()) {
      ++n;
      if (hit.isUsed()) {
        ++nUsed;
      }
    }
  }
  return std::make_tuple(n, nUsed);
}

//=========================================================================
// monitor
//=========================================================================
#ifdef DEBUG_HISTO
StatusCode PrPixelTracking::monitor(const std::vector<PrPixelModuleHits>& modulehits,
                                    const LHCb::Tracks& tracks,
                                    const LHCb::VPClusters& clusters) const {
  const unsigned int firstModule = m_hitManager->firstModule();
  const unsigned int lastModule = m_hitManager->lastModule();
  for (unsigned int i = firstModule; i < lastModule; ++i) {
    for (const auto& hit : modulehits[i].hits()) {
      const float x = hit.x();
      const float y = hit.y();
      const float z = hit.z();
      const float r = sqrt(x * x + y * y);
      if (!hit.isUsed()) {
        plot3D(x, y, z, "UnusedHits3D", "Distribution of Unused Hits",
               -50., 50., -50., 50., -500., 800., 100, 100, 200);
        plot2D(r, z, "UnusedHitsRZ", "Distribution of Unused Hits",
               0., 60., -500., 800., 100, 100);
        plot2D(x, y, "UnusedHitsXY", "Distribution of Unused Hits",
               -50., 50., -50., 50., 100, 100);
      }
      plot3D(x, y, z, "Hits3D", "Distribution of Hits",
             -50., 50., -50., 50., -500., 800., 100, 100, 200);
      plot2D(r, z, "HitsRZ", "Distribution of Hits",
             0., 60., -500., 800., 100, 100);
      plot2D(x, y, "HitsXY", "Distribution of Hits",
             -50., 50., -50., 50., 100, 100);
    }
  }

  // count nb hits and nb used hits
  const std::tuple<unsigned int, unsigned int> nbhits = countHits(modulehits);
  plot(std::get<0>(nbhits), "HitsPerEvent", "Number of hits per event", 0.0, 8000.0, 80);
  if (std::get<0>(nbhits) > 0) {
    plot(100. * std::get<1>(nbhits) / std::get<0>(nbhits), "PercentUsedHitsPerEvent",
         "Percentage of hits assigned to tracks", 0.0, 100.0, 100);
  }

  unsigned int nFwd = 0;
  unsigned int nBwd = 0;
  for (const auto& track : tracks) {
    const bool bwd = track->checkFlag(LHCb::Track::Backward);
    const unsigned int nHitsPerTrack = track->lhcbIDs().size();
    const float chi2 = track->chi2PerDoF();
    const float eta = track->pseudoRapidity();
    const float phi = track->phi() / Gaudi::Units::degree;
    if (bwd) {
      ++nBwd;
      plot(nHitsPerTrack, "BwdHitsPerTrack",
           "Number of hits per backward track", 0.5, 40.5, 40);
      plot(chi2, "BwdChi2", "Chi2/DoF of backward tracks", 0., 10., 50);
      plot(eta, "BwdEta", "Pseudorapidity of backward tracks", 1., 6., 50);
      plot(phi, "BwdPhi", "Phi-angle of backward tracks", -180., 180., 60);
      plot2D(eta, nHitsPerTrack, "BwdHitsPerTrackVsEta",
             "Hits/track vs pseudorapidity of backward tracks",
             1., 6., 0.5, 15.5, 50, 15);
      plot2D(eta, chi2, "BwdChi2VsEta",
             "Chi2/DoF vs pseudorapidity of backward tracks",
             1., 6., 0., 10., 50, 20);
      plot2D(nHitsPerTrack, chi2, "BwdChi2VsHitsPerTrack",
             "Chi2/DoF vs hits/backward track", 0.5, 15.5, 0., 10., 15, 20);
    } else {
      ++nFwd;
      plot(nHitsPerTrack, "FwdHitsPerTrack",
           "Number of hits per forward track", 0.5, 40.5, 40);
      plot(chi2, "FwdChi2", "Chi2/DoF of forward tracks", 0., 10., 50);
      plot(eta, "FwdEta", "Pseudorapidity of forward tracks", 1., 6., 50);
      plot(phi, "FwdPhi", "Phi-angle of forward tracks", -180., 180., 60);
      plot2D(eta, nHitsPerTrack, "FwdHitsPerTrackVsEta",
             "Hits/track vs pseudorapidity of forward tracks",
             1., 6., 0.5, 15.5, 50, 15);
      plot2D(eta, chi2, "FwdChi2VsEta",
             "Chi2/DoF vs pseudorapidity of forward tracks",
             1., 6., 0., 10., 50, 20);
      plot2D(nHitsPerTrack, chi2, "FwdChi2VsHitsPerTrack",
             "Chi2/DoF vs hits/forward track", 0.5, 15.5, 0., 10., 15, 20);
    }
    // Calculate radius at first and last hit
    // (assume that hits are sorted by module)
    const LHCb::VPChannelID id0 = track->lhcbIDs().front().vpID();
    const LHCb::VPChannelID id1 = track->lhcbIDs().back().vpID();
    const LHCb::VPCluster* cluster0 = clusters.object(id0);
    if (!cluster0) continue;
    const LHCb::VPCluster* cluster1 = clusters.object(id1);
    if (!cluster1) continue;
    const float x0 = cluster0->x();
    const float y0 = cluster0->y();
    const float r0 = sqrt(x0 * x0 + y0 * y0);
    const float x1 = cluster1->x();
    const float y1 = cluster1->y();
    const float r1 = sqrt(x1 * x1 + y1 * y1);
    const float minR = r0 > r1 ? r1 : r0;
    const float maxR = r0 > r1 ? r0 : r1;
    plot(minR, "MinHitRadius", "Smallest hit radius [mm]", 0., 50., 100);
    plot(maxR, "MaxHitRadius", "Largest hit radius [mm]", 0., 50., 100);
  }
  plot(nFwd, "FwdTracksPerEvent", "Number of forward tracks per event",
       0., 400., 40);
  plot(nBwd, "BwdTracksPerEvent", "Number of backward tracks per event",
       0., 400., 40);
  return StatusCode::SUCCESS;
}
#endif
