#ifndef PRPIXELHITMANAGER_H
#define PRPIXELHITMANAGER_H 1

// Gaudi
#include "GaudiAlg/GaudiTool.h"

// LHCb
#include "Event/VPCluster.h"
// Kernel/LHCbKernel
#include "Kernel/VPConstants.h"
#include "Kernel/VPChannelID.h"
// Det/VPDet
#include "VPDet/DeVP.h"
// Local
#include "PrPixelHit.h"
#include "PrPixelModule.h"
#include "PrPixelModuleHits.h"
#include "PrPixelUtils.h"

namespace LHCb {
class RawEvent;
class RawBank;
}

static const InterfaceID IID_PrPixelHitManager("PrPixelHitManager", 1, 0);

/** @class PrPixelHitManager PrPixelHitManager.h
 *  Tool to handle the VP geometry and hits
 *
 *  @author Olivier Callot
 *  @author Kurt Rinnert
 *  @date   2012-01-05
 */

class PrPixelHitManager : public GaudiTool {

 public:
  // Return the interface ID
  static const InterfaceID &interfaceID() { return IID_PrPixelHitManager; }

  /// Standard constructor
  PrPixelHitManager(const std::string &type, const std::string &name,
                    const IInterface *parent);

  StatusCode initialize() override;

  /// Set maximum cluster size.
  void setMaxClusterSize(const unsigned int size) { m_maxClusterSize = size; }
  /// Set trigger flag
  void setTrigger(const bool triggerFlag) { m_trigger = triggerFlag; }

  unsigned int firstModule() const { return m_firstModule; }
  unsigned int lastModule() const { return m_lastModule; }
  PrPixelModule *module(const unsigned int n) const { return m_modules[n]; }

  /// Main method : extract hits, sort them and store them
  StatusCode process(const LHCb::RawEvent& rawEvent,
                     std::vector<PrPixelModuleHits>& modulehits,
                     LHCb::VPClusters& clusters,
                     bool storecluster) const;

 private:
  /// Cache Super Pixel patterns for isolated Super Pixel clustering.
  void cacheSPPatterns();

  /// Recompute the geometry in case of change
  StatusCode rebuildGeometry();

  /// Extract the hits form rawbanks
  bool buildHitsFromRawBank(const LHCb::RawEvent& rawEvent,
                            std::vector<PrPixelModuleHits>& modulehits,
                            PrPixelModuleHits& extraHits) const;

  /// Store trigger clusters.
  void storeTriggerClusters(const std::vector<PrPixelModuleHits>& modulehits,
                            LHCb::VPClusters& clusters) const;

  /// Store offline clusters.
  void storeOfflineClusters(const std::vector<PrPixelModuleHits>& modulehits,
                            const PrPixelModuleHits& extraHits,
                            LHCb::VPClusters& clusters) const;

 private:

  /// Detector element
  DeVP *m_vp;

  /// List of pointers to modules (which contain pointers to their hits)
  /// FIXME This is not thread safe on a change on geometry.
  /// FIXME These vectors should be stored as a derived condition
  std::vector<PrPixelModule*> m_modules;
  std::vector<PrPixelModule> m_module_pool;

  /// Indices of first and last module
  unsigned int m_firstModule;
  unsigned int m_lastModule;

  // quick test for message level
  bool m_isDebug;

  // SP pattern buffers for clustering, cached once.
  // There are 256 patterns and there can be at most two
  // distinct clusters in an SP.
  std::array<unsigned char,256> m_sp_patterns;
  std::array<unsigned char,256> m_sp_sizes;
  std::array<float,512> m_sp_fx;
  std::array<float,512> m_sp_fy;

  /// Maximum allowed cluster size (no effect when running on lite clusters).
  unsigned int m_maxClusterSize = 4;

  /// Are we running in the trigger?
  bool m_trigger = false;

  /// Cache of local to global transformations, 16 stride aligned.
  float m_ltg[16 * VP::NSensors]; // 16*208 = 16*number of sensors

  /// pointers to local x coordinates and pitches
  const double *m_local_x;
  const double *m_x_pitch;

  /// pixel size in y; this is constant for all pixels on a sensor
  float m_pixel_size;
};

#endif  // PRPIXELHITMANAGER_H
