// Include files 

// from Gaudi
#include "Event/State.h"

#include "Event/MCParticle.h"

#include "Event/ODIN.h"

#include "Event/MCTrackInfo.h"

#include <TFile.h>

// local
#include "CompareTracks.h"


//##################################################################################################
//
// Implementation file for class : CompareTracks
//
// 2017-11-08: Simon Stemmle
//
//##################################################################################################

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CompareTracks  )


//==================================================================================================
// Standard constructor, initializes variables
//==================================================================================================
CompareTracks::CompareTracks( const std::string& name,
                                        ISvcLocator* pSvcLocator) :
  Consumer(name, pSvcLocator, {
                KeyValue{"InputTracks1",  "Rec/Track/ForwardFastFitted_TMP"},
                KeyValue{"InputTracks2",  "Rec/Track/ForwardFastFitted"},
                KeyValue{"ODINLocation", LHCb::ODINLocation::Default},
                KeyValue{"LinkerLocation", Links::location("Rec/Track/ForwardFastFitted")} })
{
}

//==================================================================================================
// Main execution
//==================================================================================================
void CompareTracks::operator()(const LHCb::Tracks& tracks1, const LHCb::Tracks& tracks2,
                               const LHCb::ODIN& odin, const LHCb::LinksByKey& links) const {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;
 
  if(tracks1.empty() || tracks2.empty()){
   return;
  }
 
  //Create output tuple
  //Create a new file for every event in order to be threadsafe 
  TFile outputFile((m_FileName.toString()+"_"+std::to_string(odin.runNumber())+
                    "_"+std::to_string(odin.eventNumber())+".root").c_str(),"RECREATE");

  //create the varaibles to be filled
  tupleVars vars;
  
  //create the tree
  TTree tree("compare","compare");
  addBranches(tree, &vars);
    
  //Loop over tracks1
  for(auto const &track1 : tracks1){
    //Search for the respective track in tracks2
    auto i = std::find_if(std::begin(tracks2), std::end(tracks2),
                          [&](auto t){return t->key() == track1->key();});
    if(i==std::end(tracks2)){
      warning() << "No matching track in second container found" << endmsg;
      return;
    } 

    //compare the tracks and fill the tuple

    //1. Near the beam pipe

    //Get the z position of the state near the beam pipe
    double zBeam = 0;
    const LHCb::State *state1 = track1->stateAt(LHCb::State::Location::ClosestToBeam);
    const LHCb::State *state2 = (*i)->stateAt(LHCb::State::Location::ClosestToBeam);
    if(state1!=nullptr && state2!=nullptr) zBeam = 0.5*(state1->z()+state2->z());

    FillNtuple(*track1, *(*(i)), links, &vars, zBeam, 0, true);

    //2. ...
    
    //3. ...

    tree.Fill();
  }

  outputFile.Write();
  outputFile.Close();
  
  return;
}

//==================================================================================================
//  Add branches to the tuple that will be filled with the comparison information
//==================================================================================================
void CompareTracks::addBranches(TTree &tree, tupleVars *treeVars) const {
  
  //Set the branches
  tree.Branch("sF_sigmaxx_T"     , &(treeVars->m_sF_P[2][0 ])      , "sF_sigmaxx_T/D");
  tree.Branch("sF_sigmayy_T"     , &(treeVars->m_sF_P[2][2 ])      , "sF_sigmayy_T/D");
  tree.Branch("sF_sigmatxtx_T"   , &(treeVars->m_sF_P[2][5 ])      , "sF_sigmatxtx_T/D");
  tree.Branch("sF_sigmatyty_T"   , &(treeVars->m_sF_P[2][9 ])      , "sF_sigmatyty_T/D");
  tree.Branch("sF_sigmaqopqop_T" , &(treeVars->m_sF_P[2][14 ])     , "sF_sigmaqopqop_T/D");
  tree.Branch("sF_sigmaxy_T"     , &(treeVars->m_sF_P[2][1 ])      , "sF_sigmaxy_T/D");
  tree.Branch("sF_sigmaxtx_T"    , &(treeVars->m_sF_P[2][3 ])      , "sF_sigmaxtx_T/D");
  tree.Branch("sF_sigmaxty_T"    , &(treeVars->m_sF_P[2][6 ])      , "sF_sigmaxty_T/D");
  tree.Branch("sF_sigmaxqop_T"   , &(treeVars->m_sF_P[2][10 ])     , "sF_sigmaxqop_T/D");
  tree.Branch("sF_sigmaytx_T"    , &(treeVars->m_sF_P[2][4 ])      , "sF_sigmaytx_T/D");
  tree.Branch("sF_sigmayty_T"    , &(treeVars->m_sF_P[2][7 ])      , "sF_sigmayty_T/D");
  tree.Branch("sF_sigmayqop_T"   , &(treeVars->m_sF_P[2][11 ])     , "sF_sigmayqop_T/D");
  tree.Branch("sF_sigmatxty_T"   , &(treeVars->m_sF_P[2][8 ])      , "sF_sigmatxty_T/D");
  tree.Branch("sF_sigmatxqop_T"  , &(treeVars->m_sF_P[2][12 ])     , "sF_sigmatxqop_T/D");
  tree.Branch("sF_sigmatyqop_T"  , &(treeVars->m_sF_P[2][13 ])     , "sF_sigmatyqop_T/D");

  tree.Branch("sF_x_T"           , &(treeVars->m_sF_x[2][0])       , "sF_x_T/D");
  tree.Branch("sF_y_T"           , &(treeVars->m_sF_x[2][1])       , "sF_y_T/D");
  tree.Branch("sF_tx_T"          , &(treeVars->m_sF_x[2][2])       , "sF_tx_T/D");
  tree.Branch("sF_ty_T"          , &(treeVars->m_sF_x[2][3])       , "sF_ty_T/D");
  tree.Branch("sF_qop_T"         , &(treeVars->m_sF_x[2][4])       , "sF_qop_T/D");
  
  tree.Branch("sF_z_T"           , &(treeVars->m_sF_z[2])          , "sF_z_T/D");

  tree.Branch("sF_true_x_T"      , &(treeVars->m_sF_true_x[2][0])  , "sF_true_x_T/D");
  tree.Branch("sF_true_y_T"      , &(treeVars->m_sF_true_x[2][1])  , "sF_true_y_T/D");
  tree.Branch("sF_true_tx_T"     , &(treeVars->m_sF_true_x[2][2])  , "sF_true_tx_T/D");
  tree.Branch("sF_true_ty_T"     , &(treeVars->m_sF_true_x[2][3])  , "sF_true_ty_T/D");
  tree.Branch("sF_true_qop_T"    , &(treeVars->m_sF_true_x[2][4])  , "sF_true_qop_T/D");
  
  tree.Branch("sF_sigmaxx_V"     , &(treeVars->m_sF_P[0][0 ])      , "sF_sigmaxx_V/D");
  tree.Branch("sF_sigmayy_V"     , &(treeVars->m_sF_P[0][2 ])      , "sF_sigmayy_V/D");
  tree.Branch("sF_sigmatxtx_V"   , &(treeVars->m_sF_P[0][5 ])      , "sF_sigmatxtx_V/D");
  tree.Branch("sF_sigmatyty_V"   , &(treeVars->m_sF_P[0][9 ])      , "sF_sigmatyty_V/D");
  tree.Branch("sF_sigmaqopqop_V" , &(treeVars->m_sF_P[0][14 ])     , "sF_sigmaqopqop_V/D");
  tree.Branch("sF_sigmaxy_V"     , &(treeVars->m_sF_P[0][1 ])      , "sF_sigmaxy_V/D");
  tree.Branch("sF_sigmaxtx_V"    , &(treeVars->m_sF_P[0][3 ])      , "sF_sigmaxtx_V/D");
  tree.Branch("sF_sigmaxty_V"    , &(treeVars->m_sF_P[0][6 ])      , "sF_sigmaxty_V/D");
  tree.Branch("sF_sigmaxqop_V"   , &(treeVars->m_sF_P[0][10 ])     , "sF_sigmaxqop_V/D");
  tree.Branch("sF_sigmaytx_V"    , &(treeVars->m_sF_P[0][4 ])      , "sF_sigmaytx_V/D");
  tree.Branch("sF_sigmayty_V"    , &(treeVars->m_sF_P[0][7 ])      , "sF_sigmayty_V/D");
  tree.Branch("sF_sigmayqop_V"   , &(treeVars->m_sF_P[0][11 ])     , "sF_sigmayqop_V/D");
  tree.Branch("sF_sigmatxty_V"   , &(treeVars->m_sF_P[0][8 ])      , "sF_sigmatxty_V/D");
  tree.Branch("sF_sigmatxqop_V"  , &(treeVars->m_sF_P[0][12 ])     , "sF_sigmatxqop_V/D");
  tree.Branch("sF_sigmatyqop_V"  , &(treeVars->m_sF_P[0][13 ])     , "sF_sigmatyqop_V/D");

  tree.Branch("sF_x_V"           , &(treeVars->m_sF_x[0][0])       , "sF_x_V/D");
  tree.Branch("sF_y_V"           , &(treeVars->m_sF_x[0][1])       , "sF_y_V/D");
  tree.Branch("sF_tx_V"          , &(treeVars->m_sF_x[0][2])       , "sF_tx_V/D");
  tree.Branch("sF_ty_V"          , &(treeVars->m_sF_x[0][3])       , "sF_ty_V/D");
  tree.Branch("sF_qop_V"         , &(treeVars->m_sF_x[0][4])       , "sF_qop_V/D");
 
  tree.Branch("sF_z_V"           , &(treeVars->m_sF_z[0])          , "sF_z_V/D");

  tree.Branch("sF_true_x_V"      , &(treeVars->m_sF_true_x[0][0])  , "sF_true_x_V/D");
  tree.Branch("sF_true_y_V"      , &(treeVars->m_sF_true_x[0][1])  , "sF_true_y_V/D");
  tree.Branch("sF_true_tx_V"     , &(treeVars->m_sF_true_x[0][2])  , "sF_true_tx_V/D");
  tree.Branch("sF_true_ty_V"     , &(treeVars->m_sF_true_x[0][3])  , "sF_true_ty_V/D");
  tree.Branch("sF_true_qop_V"    , &(treeVars->m_sF_true_x[0][4])  , "sF_true_qop_V/D");
  
  tree.Branch("sF_sigmaxx"       , &(treeVars->m_sF_P[1][0 ])      , "sF_sigmaxx/D");
  tree.Branch("sF_sigmayy"       , &(treeVars->m_sF_P[1][2 ])      , "sF_sigmayy/D");
  tree.Branch("sF_sigmatxtx"     , &(treeVars->m_sF_P[1][5 ])      , "sF_sigmatxtx/D");
  tree.Branch("sF_sigmatyty"     , &(treeVars->m_sF_P[1][9 ])      , "sF_sigmatyty/D");
  tree.Branch("sF_sigmaqopqop"   , &(treeVars->m_sF_P[1][14 ])     , "sF_sigmaqopqop/D");
  tree.Branch("sF_sigmaxy"       , &(treeVars->m_sF_P[1][1 ])      , "sF_sigmaxy/D");
  tree.Branch("sF_sigmaxtx"      , &(treeVars->m_sF_P[1][3 ])      , "sF_sigmaxtx/D");
  tree.Branch("sF_sigmaxty"      , &(treeVars->m_sF_P[1][6 ])      , "sF_sigmaxty/D");
  tree.Branch("sF_sigmaxqop"     , &(treeVars->m_sF_P[1][10 ])     , "sF_sigmaxqop/D");
  tree.Branch("sF_sigmaytx"      , &(treeVars->m_sF_P[1][4 ])      , "sF_sigmaytx/D");
  tree.Branch("sF_sigmayty"      , &(treeVars->m_sF_P[1][7 ])      , "sF_sigmayty/D");
  tree.Branch("sF_sigmayqop"     , &(treeVars->m_sF_P[1][11 ])     , "sF_sigmayqop/D");
  tree.Branch("sF_sigmatxty"     , &(treeVars->m_sF_P[1][8 ])      , "sF_sigmatxty/D");
  tree.Branch("sF_sigmatxqop"    , &(treeVars->m_sF_P[1][12 ])     , "sF_sigmatxqop/D");
  tree.Branch("sF_sigmatyqop"    , &(treeVars->m_sF_P[1][13 ])     , "sF_sigmatyqop/D");

  tree.Branch("sF_x"             , &(treeVars->m_sF_x[1][0])       , "sF_x/D");
  tree.Branch("sF_y"             , &(treeVars->m_sF_x[1][1])       , "sF_y/D");
  tree.Branch("sF_tx"            , &(treeVars->m_sF_x[1][2])       , "sF_tx/D");
  tree.Branch("sF_ty"            , &(treeVars->m_sF_x[1][3])       , "sF_ty/D");
  tree.Branch("sF_qop"           , &(treeVars->m_sF_x[1][4])       , "sF_qop/D");
  
  tree.Branch("sF_z"             , &(treeVars->m_sF_z[1])          , "sF_z/D");
  tree.Branch("sF_chi2"          , &(treeVars->m_sF_chi2)          ,"sF_chi2/D");
  tree.Branch("sF_ndof"          , &(treeVars->m_sF_ndof)          ,"sF_ndof/D");

  tree.Branch("sF_true_x"        , &(treeVars->m_sF_true_x[1][0])  , "sF_true_x/D");
  tree.Branch("sF_true_y"        , &(treeVars->m_sF_true_x[1][1])  , "sF_true_y/D");
  tree.Branch("sF_true_tx"       , &(treeVars->m_sF_true_x[1][2])  , "sF_true_tx/D");
  tree.Branch("sF_true_ty"       , &(treeVars->m_sF_true_x[1][3])  , "sF_true_ty/D");
  tree.Branch("sF_true_qop"      , &(treeVars->m_sF_true_x[1][4])  , "sF_true_qop/D");
  
  tree.Branch("dF_sigmaxx_T"     , &(treeVars->m_dF_P[2][0 ])      , "dF_sigmaxx_T/D");
  tree.Branch("dF_sigmayy_T"     , &(treeVars->m_dF_P[2][2 ])      , "dF_sigmayy_T/D");
  tree.Branch("dF_sigmatxtx_T"   , &(treeVars->m_dF_P[2][5 ])      , "dF_sigmatxtx_T/D");
  tree.Branch("dF_sigmatyty_T"   , &(treeVars->m_dF_P[2][9 ])      , "dF_sigmatyty_T/D");
  tree.Branch("dF_sigmaqopqop_T" , &(treeVars->m_dF_P[2][14 ])     , "dF_sigmaqopqop_T/D");
  tree.Branch("dF_sigmaxy_T"     , &(treeVars->m_dF_P[2][1 ])      , "dF_sigmaxy_T/D");
  tree.Branch("dF_sigmaxtx_T"    , &(treeVars->m_dF_P[2][3 ])      , "dF_sigmaxtx_T/D");
  tree.Branch("dF_sigmaxty_T"    , &(treeVars->m_dF_P[2][6 ])      , "dF_sigmaxty_T/D");
  tree.Branch("dF_sigmaxqop_T"   , &(treeVars->m_dF_P[2][10 ])     , "dF_sigmaxqop_T/D");
  tree.Branch("dF_sigmaytx_T"    , &(treeVars->m_dF_P[2][4 ])      , "dF_sigmaytx_T/D");
  tree.Branch("dF_sigmayty_T"    , &(treeVars->m_dF_P[2][7 ])      , "dF_sigmayty_T/D");
  tree.Branch("dF_sigmayqop_T"   , &(treeVars->m_dF_P[2][11 ])     , "dF_sigmayqop_T/D");
  tree.Branch("dF_sigmatxty_T"   , &(treeVars->m_dF_P[2][8 ])      , "dF_sigmatxty_T/D");
  tree.Branch("dF_sigmatxqop_T"  , &(treeVars->m_dF_P[2][12 ])     , "dF_sigmatxqop_T/D");
  tree.Branch("dF_sigmatyqop_T"  , &(treeVars->m_dF_P[2][13 ])     , "dF_sigmatyqop_T/D");

  tree.Branch("dF_x_T"           , &(treeVars->m_dF_x[2][0])       , "dF_x_T/D");
  tree.Branch("dF_y_T"           , &(treeVars->m_dF_x[2][1])       , "dF_y_T/D");
  tree.Branch("dF_tx_T"          , &(treeVars->m_dF_x[2][2])       , "dF_tx_T/D");
  tree.Branch("dF_ty_T"          , &(treeVars->m_dF_x[2][3])       , "dF_ty_T/D");
  tree.Branch("dF_qop_T"         , &(treeVars->m_dF_x[2][4])       , "dF_qop_T/D");
  
  tree.Branch("dF_z_T"           , &(treeVars->m_dF_z[2])          , "dF_z_T/D");

  tree.Branch("dF_true_x_T"      , &(treeVars->m_dF_true_x[2][0])  , "dF_true_x_T/D");
  tree.Branch("dF_true_y_T"      , &(treeVars->m_dF_true_x[2][1])  , "dF_true_y_T/D");
  tree.Branch("dF_true_tx_T"     , &(treeVars->m_dF_true_x[2][2])  , "dF_true_tx_T/D");
  tree.Branch("dF_true_ty_T"     , &(treeVars->m_dF_true_x[2][3])  , "dF_true_ty_T/D");
  tree.Branch("dF_true_qop_T"    , &(treeVars->m_dF_true_x[2][4])  , "dF_true_qop_T/D");
  
  tree.Branch("dF_sigmaxx_V"     , &(treeVars->m_dF_P[0][0 ])      , "dF_sigmaxx_V/D");
  tree.Branch("dF_sigmayy_V"     , &(treeVars->m_dF_P[0][2 ])      , "dF_sigmayy_V/D");
  tree.Branch("dF_sigmatxtx_V"   , &(treeVars->m_dF_P[0][5 ])      , "dF_sigmatxtx_V/D");
  tree.Branch("dF_sigmatyty_V"   , &(treeVars->m_dF_P[0][9 ])      , "dF_sigmatyty_V/D");
  tree.Branch("dF_sigmaqopqop_V" , &(treeVars->m_dF_P[0][14 ])     , "dF_sigmaqopqop_V/D");
  tree.Branch("dF_sigmaxy_V"     , &(treeVars->m_dF_P[0][1 ])      , "dF_sigmaxy_V/D");
  tree.Branch("dF_sigmaxtx_V"    , &(treeVars->m_dF_P[0][3 ])      , "dF_sigmaxtx_V/D");
  tree.Branch("dF_sigmaxty_V"    , &(treeVars->m_dF_P[0][6 ])      , "dF_sigmaxty_V/D");
  tree.Branch("dF_sigmaxqop_V"   , &(treeVars->m_dF_P[0][10 ])     , "dF_sigmaxqop_V/D");
  tree.Branch("dF_sigmaytx_V"    , &(treeVars->m_dF_P[0][4 ])      , "dF_sigmaytx_V/D");
  tree.Branch("dF_sigmayty_V"    , &(treeVars->m_dF_P[0][7 ])      , "dF_sigmayty_V/D");
  tree.Branch("dF_sigmayqop_V"   , &(treeVars->m_dF_P[0][11 ])     , "dF_sigmayqop_V/D");
  tree.Branch("dF_sigmatxty_V"   , &(treeVars->m_dF_P[0][8 ])      , "dF_sigmatxty_V/D");
  tree.Branch("dF_sigmatxqop_V"  , &(treeVars->m_dF_P[0][12 ])     , "dF_sigmatxqop_V/D");
  tree.Branch("dF_sigmatyqop_V"  , &(treeVars->m_dF_P[0][13 ])     , "dF_sigmatyqop_V/D");

  tree.Branch("dF_x_V"           , &(treeVars->m_dF_x[0][0])       , "dF_x_V/D");
  tree.Branch("dF_y_V"           , &(treeVars->m_dF_x[0][1])       , "dF_y_V/D");
  tree.Branch("dF_tx_V"          , &(treeVars->m_dF_x[0][2])       , "dF_tx_V/D");
  tree.Branch("dF_ty_V"          , &(treeVars->m_dF_x[0][3])       , "dF_ty_V/D");
  tree.Branch("dF_qop_V"         , &(treeVars->m_dF_x[0][4])       , "dF_qop_V/D");
 
  tree.Branch("dF_z_V"           , &(treeVars->m_dF_z[0])          , "dF_z_V/D");

  tree.Branch("dF_true_x_V"      , &(treeVars->m_dF_true_x[0][0])  , "dF_true_x_V/D");
  tree.Branch("dF_true_y_V"      , &(treeVars->m_dF_true_x[0][1])  , "dF_true_y_V/D");
  tree.Branch("dF_true_tx_V"     , &(treeVars->m_dF_true_x[0][2])  , "dF_true_tx_V/D");
  tree.Branch("dF_true_ty_V"     , &(treeVars->m_dF_true_x[0][3])  , "dF_true_ty_V/D");
  tree.Branch("dF_true_qop_V"    , &(treeVars->m_dF_true_x[0][4])  , "dF_true_qop_V/D");
  
  tree.Branch("dF_sigmaxx"       , &(treeVars->m_dF_P[1][0 ])      , "dF_sigmaxx/D");
  tree.Branch("dF_sigmayy"       , &(treeVars->m_dF_P[1][2 ])      , "dF_sigmayy/D");
  tree.Branch("dF_sigmatxtx"     , &(treeVars->m_dF_P[1][5 ])      , "dF_sigmatxtx/D");
  tree.Branch("dF_sigmatyty"     , &(treeVars->m_dF_P[1][9 ])      , "dF_sigmatyty/D");
  tree.Branch("dF_sigmaqopqop"   , &(treeVars->m_dF_P[1][14 ])     , "dF_sigmaqopqop/D");
  tree.Branch("dF_sigmaxy"       , &(treeVars->m_dF_P[1][1 ])      , "dF_sigmaxy/D");
  tree.Branch("dF_sigmaxtx"      , &(treeVars->m_dF_P[1][3 ])      , "dF_sigmaxtx/D");
  tree.Branch("dF_sigmaxty"      , &(treeVars->m_dF_P[1][6 ])      , "dF_sigmaxty/D");
  tree.Branch("dF_sigmaxqop"     , &(treeVars->m_dF_P[1][10 ])     , "dF_sigmaxqop/D");
  tree.Branch("dF_sigmaytx"      , &(treeVars->m_dF_P[1][4 ])      , "dF_sigmaytx/D");
  tree.Branch("dF_sigmayty"      , &(treeVars->m_dF_P[1][7 ])      , "dF_sigmayty/D");
  tree.Branch("dF_sigmayqop"     , &(treeVars->m_dF_P[1][11 ])     , "dF_sigmayqop/D");
  tree.Branch("dF_sigmatxty"     , &(treeVars->m_dF_P[1][8 ])      , "dF_sigmatxty/D");
  tree.Branch("dF_sigmatxqop"    , &(treeVars->m_dF_P[1][12 ])     , "dF_sigmatxqop/D");
  tree.Branch("dF_sigmatyqop"    , &(treeVars->m_dF_P[1][13 ])     , "dF_sigmatyqop/D");

  tree.Branch("dF_x"             , &(treeVars->m_dF_x[1][0])       , "dF_x/D");
  tree.Branch("dF_y"             , &(treeVars->m_dF_x[1][1])       , "dF_y/D");
  tree.Branch("dF_tx"            , &(treeVars->m_dF_x[1][2])       , "dF_tx/D");
  tree.Branch("dF_ty"            , &(treeVars->m_dF_x[1][3])       , "dF_ty/D");
  tree.Branch("dF_qop"           , &(treeVars->m_dF_x[1][4])       , "dF_qop/D");
  
  tree.Branch("dF_z"             , &(treeVars->m_dF_z[1])          , "dF_z/D");
  tree.Branch("dF_chi2"          , &(treeVars->m_dF_chi2)          ,"dF_chi2/D");
  tree.Branch("dF_ndof"          , &(treeVars->m_dF_ndof)          ,"dF_ndof/D");

  tree.Branch("dF_true_x"        , &(treeVars->m_dF_true_x[1][0])  , "dF_true_x/D");
  tree.Branch("dF_true_y"        , &(treeVars->m_dF_true_x[1][1])  , "dF_true_y/D");
  tree.Branch("dF_true_tx"       , &(treeVars->m_dF_true_x[1][2])  , "dF_true_tx/D");
  tree.Branch("dF_true_ty"       , &(treeVars->m_dF_true_x[1][3])  , "dF_true_ty/D");
  tree.Branch("dF_true_qop"      , &(treeVars->m_dF_true_x[1][4])  , "dF_true_qop/D");
  
  tree.Branch("true_qop_vertex"  , &(treeVars->m_true_qop_vertex)  , "true_qop_vertex/D");
  
  tree.Branch("MCstatus"         , &(treeVars->m_MC_status)        , "MCstatus/I");
}

//==================================================================================================
//  Get the states informations at position z and fill the tree variables
//==================================================================================================
void CompareTracks::FillNtuple(const LHCb::Track &track1, const LHCb::Track &track2,
                               const LHCb::LinksByKey& links, tupleVars *vars, double z,
                               int nPos, bool closeToVertex) const {
  vars->m_MC_status =  MatchesMC(track2, links);

  //Get the states closest to the desired z position
  LHCb::State *state1 = (track1.closestState(z)).clone();
  LHCb::State *state2 = (track2.closestState(z)).clone();

  //extrapolate to the exact z position
  StatusCode sc1 = m_extrapolator->propagate(*state1, z);
  StatusCode sc2 = m_extrapolator->propagate(*state2, z);

  //covariance matrices
  Gaudi::TrackSymMatrix covMat1;
  covMat1 = state1->covariance();  
  
  Gaudi::TrackSymMatrix covMat2;
  covMat2 = state2->covariance();  

  if(!sc1.isSuccess() || !sc2.isSuccess()) return;

  //Get the true position at this z position
  double trueX, trueY, trueTX, trueTY, trueQoP, trueQoP_Vertex;
  //Get the QoP at the vertex
  TrueState(z, trueX, trueY, trueTX, trueTY, trueQoP_Vertex, track1, links, true, closeToVertex);
  //Get the actual state at the z position
  TrueState(z, trueX, trueY, trueTX, trueTY, trueQoP, track1, links, false, closeToVertex);

  //Set the state variables
  vars->m_sF_x[nPos][0] = state1->x();
  vars->m_sF_x[nPos][1] = state1->y();
  vars->m_sF_x[nPos][2] = state1->tx();
  vars->m_sF_x[nPos][3] = state1->ty();
  vars->m_sF_x[nPos][4] = state1->qOverP();
  
  vars->m_sF_true_x[nPos][0] = trueX; 
  vars->m_sF_true_x[nPos][1] = trueY;
  vars->m_sF_true_x[nPos][2] = trueTX;
  vars->m_sF_true_x[nPos][3] = trueTY;
  vars->m_sF_true_x[nPos][4] = trueQoP;

  vars->m_dF_x[nPos][0] = state2->x();
  vars->m_dF_x[nPos][1] = state2->y();
  vars->m_dF_x[nPos][2] = state2->tx();
  vars->m_dF_x[nPos][3] = state2->ty();
  vars->m_dF_x[nPos][4] = state2->qOverP();
  
  vars->m_dF_true_x[nPos][0] = trueX; 
  vars->m_dF_true_x[nPos][1] = trueY;
  vars->m_dF_true_x[nPos][2] = trueTX;
  vars->m_dF_true_x[nPos][3] = trueTY;
  vars->m_dF_true_x[nPos][4] = trueQoP;

  vars->m_true_qop_vertex = trueQoP_Vertex;

  vars->m_sF_z[nPos] = z;

  vars->m_dF_z[nPos] = z;
  
  int k =0;
  for(int i=0;i<5;i++){
    for(int j=0;j<=i;j++){
      vars->m_sF_P[nPos][k] = covMat1(i,j); 
      vars->m_dF_P[nPos][k] = covMat2(i,j); 
      k++;
    }
  }

  //Set other track varaibles 
  vars->m_sF_ndof = track1.nDoF(); 
  vars->m_sF_chi2 = track1.chi2(); 
  
  vars->m_dF_ndof = track2.nDoF(); 
  vars->m_dF_chi2 = track2.chi2(); 
 
}

//==================================================================================================
// Check if a MC particle is linked to this track
//==================================================================================================
int CompareTracks::MatchesMC(const LHCb::Track &track, const LHCb::LinksByKey& links) const {
  InputLinks<ContainedObject, LHCb::MCParticle> TrackParticleLinks(links);
  //Look for an associated MC particle
  auto trackLinks = TrackParticleLinks.from(track.key());
  if (trackLinks.empty()) {
     debug() << "No links for track key " << track.key() << endmsg;
     return 0;
  }
  auto mcpart = trackLinks.begin()->to();
  if(!mcpart)return 0;
 
  //check quality of matching 
  MCTrackInfo trackInfo( evtSvc(), msgSvc() );
  if ( 0 == trackInfo.fullInfo( mcpart ) ) return 2;
  bool isLong  = trackInfo.hasVeloAndT( mcpart );
  isLong = isLong && ( abs( mcpart->particleID().pid() ) != 11 ); // and not electron
  if(!isLong) return 2;
  bool eta25       = (mcpart->momentum().Eta() > 1.8 && mcpart->momentum().Eta() < 5.3);
  if(!eta25) return 2;
  
  if(std::fabs(track.pseudoRapidity() -  mcpart->momentum().Eta()) >0.05) return 2;
  return 1;
}

//==================================================================================================
// Get true state at a given z position
//==================================================================================================
bool CompareTracks::TrueState(double zpos, double& trueX, double& trueY,
                              double& truetX, double& truetY, double& trueqop,
                              const LHCb::Track &track, const LHCb::LinksByKey& links,
                              bool initialQop, bool closeToVertex) const {
  InputLinks<ContainedObject, LHCb::MCParticle> TrackParticleLinks(links);
  //Look for an associated MC particle
  auto trackLinks = TrackParticleLinks.from(track.key());
  if (trackLinks.empty()) {
     debug() << "No links for track key " << track.key() << endmsg;
     return 0;
  }
  auto mcPart = trackLinks.begin()->to();
  if(!mcPart)return false;

  LHCb::State state;
  //create the true state from the MC hits using the ideal state creator
  if(!closeToVertex){
    StatusCode sc = m_idealStateCreator->createState( mcPart, zpos, state);
    if( !sc.isSuccess() ) error() << "No ideal state could be created" << endmsg;
  }
  //use the MCParticle information to get the true state near the vertex
  else{
    StatusCode sc = m_idealStateCreator->createStateVertex( mcPart, state);
    if( !sc.isSuccess() ) error() << "No ideal state could be created" << endmsg;
    m_extrapolator->propagate(state, zpos);
  }
  trueX  = state.x();
  trueY  = state.y();
  truetX = state.tx();
  truetY = state.ty();

  if(!initialQop)trueqop = state.qOverP();
  else trueqop = mcPart->particleID().threeCharge() * 1./3 * 1./mcPart->momentum().P();

  return true;
}
