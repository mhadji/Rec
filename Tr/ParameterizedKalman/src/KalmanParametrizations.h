#ifndef KALMANPARAMETRIZATIONS_H 
#define KALMANPARAMETRIZATIONS_H 1

// Include files
#include "Kernel/Trajectory.h"
#include "GaudiKernel/GenericMatrixTypes.h"
#include "GaudiKernel/GenericVectorTypes.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"

enum class Polarity { Up, Down };

/** @class KalmanParametrizations KalmanParametrizations.h
 *  Contains a set of extrapolation methods that are used in the ParameterizedKalmanFit 
 *  It uses parametrizations for the extrapolation through material and the magnetic field
 *  
 *
 *  @author Simon Stemmle
 *  @date   2017-10-26
 */

class KalmanParametrizations
{
public:
  /// default constructor
  KalmanParametrizations();

  /// Constructor for loading the correct parameters for the given magnet polarity
  KalmanParametrizations(Polarity polarity, bool useOneParameterSet=false);
  
  /// Extrapolate inside the VELO
  void ExtrapolateInV(double zFrom, double zTo, Gaudi::Vector5 &x, Gaudi::Matrix5x5 &F,
                      Gaudi::SymMatrix5x5 &Q) const;
  
  /// Extrapolate VELO <-> UT
  bool ExtrapolateVUT(double zFrom, double zTo, Gaudi::Vector5 &x, Gaudi::Matrix5x5 &F,
                      Gaudi::SymMatrix5x5 &Q) const;
  
  /// Extrapolate VELO <-> UT (traj)
  bool ExtrapolateVUT(double zFrom, const LHCb::Trajectory &traj, double &zTo,
                      Gaudi::Vector5 &x, Gaudi::Matrix5x5 &F, Gaudi::SymMatrix5x5 &Q) const;

  /// Get noise for VELO <- UT
  void GetNoiseVUTBackw(double zFrom, double zTo, const Gaudi::Vector5 &x,
                        Gaudi::SymMatrix5x5 &Q) const;
  
  /// Predict UT <-> UT (traj)
  void ExtrapolateInUT(double zFrom, int nLayer,  const LHCb::Trajectory &traj,
                       double &zTo, Gaudi::Vector5 &x, Gaudi::Matrix5x5 &F,
                        Gaudi::SymMatrix5x5 &Q) const;
  
  /// Predict UT <-> UT
  void ExtrapolateInUT(double zFrom, int nLayer, double zTo, Gaudi::Vector5 &x,
                       Gaudi::Matrix5x5 &F, Gaudi::SymMatrix5x5 &Q) const;
  
  /// Extrapolate UT (fixed z) -> T (fixed z) 
  void ExtrapolateUTT(Gaudi::Vector5 &x, Gaudi::Matrix5x5 &F, Gaudi::SymMatrix5x5 &Q) const;
  /// actual parametrization for the step UT (fixed z) -> T (fixed z)
  int extrapUTT(double& x, double& y, double& tx, double& ty, double qop, double* der_tx,
                double* der_ty, double* der_qop) const;

  /// Get noise for UT (fixed z) <- T (fixed z) 
  void GetNoiseUTTBackw(const Gaudi::Vector5 &x, Gaudi::SymMatrix5x5 &Q) const;
  
  /// Extrapolate T <-> T (traj)
  void ExtrapolateInT(double zFrom, int nLayer,  const LHCb::Trajectory &traj,
                      double &zTo, Gaudi::Vector5 &x, Gaudi::Matrix5x5 &F,
                      Gaudi::SymMatrix5x5 &Q) const;
  
  /// Extrapolate T <-> T
  void ExtrapolateInT(double zFrom, int nLayer, double zTo, double DzDy, double DzDty,
                      Gaudi::Vector5 &x, Gaudi::Matrix5x5 &F, Gaudi::SymMatrix5x5 &Q) const;
  
  /// Extrapolate T(fixed z=7783) <-> first T layer (traj)
  void ExtrapolateTFT(double zFrom,  const LHCb::Trajectory &traj,double &zTo,
                      Gaudi::Vector5 &x, Gaudi::Matrix5x5 &F, Gaudi::SymMatrix5x5 &Q) const;
  
  /// Extrapolate T(fixed z=7783) <-> first T layer
  void ExtrapolateTFT(double zFrom, double zTo, Gaudi::Vector5 &x, Gaudi::Matrix5x5 &F,
                      Gaudi::SymMatrix5x5 &Q) const;

  /// Update the parameters (if needed) for the given magnet polarity
  void UpdateParameters(Polarity polarity, bool useOneParameterSet=false);

private:
  ///  Read extrapolation parameters from file
  bool read_params(std::string file, std::vector<std::vector<double>> &params, int numParams,
                   int numParamSets);

  /// This switches all parameters that linearly/cubicly/... depend on q/p
  template<std::size_t SIZE>
  void SwitchParamsForPolarity(std::vector<std::vector<double>> &params,
                               const std::array<unsigned int, SIZE> list);
 
  ///  set hard coded parameters fro UTT extrapolation 
  void setUTTParameters(Polarity polarity);

  //Tracks the magnet polarity, the current parameters correspond to.
  bool   paramsLoaded = false;
  Polarity m_Polarity = Polarity::Up;

  //###########################
  //parameter vectors 
  //###########################
  
  //Parameters that change sign under polarity flip
  const std::array<unsigned int,1> flip_Par_predictV    = {4};
  const std::array<unsigned int,4> flip_Par_predictVUT  = {0,8,9,10};
  const std::array<unsigned int,4> flip_Par_predictUT   = {5,6,7,10};
  const std::array<unsigned int,8> flip_Par_predictUTTF = {0,2,3,5,6,8,9,11};
  const std::array<unsigned int,2> flip_Par_predictTFT  = {5,6};
  const std::array<unsigned int,3> flip_Par_predictT    = {5,6,7};

  bool m_qop_flip = false;

  //predict params
  std::vector<std::vector<double>> Par_predictV;
  std::vector<std::vector<double>> Par_predictVUT;
  std::vector<std::vector<double>> Par_predictUT;
  std::vector<std::vector<double>> Par_predictUTTF;
  std::vector<std::vector<double>> Par_predictTFT;
  std::vector<std::vector<double>> Par_predictT;

  //parameters for pierres method
  //they are initialized in the constructor depending on the magnet polarity
  std::array<double,4> parx0;
  std::array<double,4> parx1;
  std::array<double,4> partx0;
  std::array<double,4> partx1;
  std::array<std::array<double,25>,25> C0x_00;
  std::array<std::array<double,25>,25> C0x_10;
  std::array<std::array<double,25>,25> C0x_01;
  std::array<std::array<double,25>,25> C0y_00;
  std::array<std::array<double,25>,25> C0y_10;
  std::array<std::array<double,25>,25> C0y_01;
  std::array<std::array<double,25>,25> C0tx_00;
  std::array<std::array<double,25>,25> C0tx_10;
  std::array<std::array<double,25>,25> C0tx_01;
  std::array<std::array<double,25>,25> C0ty_00;
  std::array<std::array<double,25>,25> C0ty_10;
  std::array<std::array<double,25>,25> C0ty_01;
  std::array<std::array<double,25>,25> C1x_00;
  std::array<std::array<double,25>,25> C1x_10;
  std::array<std::array<double,25>,25> C1x_01;
  std::array<std::array<double,25>,25> C1y_00;
  std::array<std::array<double,25>,25> C1y_10;
  std::array<std::array<double,25>,25> C1y_01;
  std::array<std::array<double,25>,25> C1tx_00;
  std::array<std::array<double,25>,25> C1tx_10;
  std::array<std::array<double,25>,25> C1tx_01;
  std::array<std::array<double,25>,25> C1ty_00;
  std::array<std::array<double,25>,25> C1ty_10;
  std::array<std::array<double,25>,25> C1ty_01;
  std::array<std::array<double,25>,25> C2x_00;
  std::array<std::array<double,25>,25> C2x_10;
  std::array<std::array<double,25>,25> C2x_01;
  std::array<std::array<double,25>,25> C2y_00;
  std::array<std::array<double,25>,25> C2y_10;
  std::array<std::array<double,25>,25> C2y_01;
  std::array<std::array<double,25>,25> C2tx_00;
  std::array<std::array<double,25>,25> C2tx_10;
  std::array<std::array<double,25>,25> C2tx_01;
  std::array<std::array<double,25>,25> C2ty_00;
  std::array<std::array<double,25>,25> C2ty_10;
  std::array<std::array<double,25>,25> C2ty_01;
  std::array<std::array<double,25>,25> C3x_00;
  std::array<std::array<double,25>,25> C3x_10;
  std::array<std::array<double,25>,25> C3x_01;
  std::array<std::array<double,25>,25> C3y_00;
  std::array<std::array<double,25>,25> C3y_10;
  std::array<std::array<double,25>,25> C3y_01;
  std::array<std::array<double,25>,25> C3tx_00;
  std::array<std::array<double,25>,25> C3tx_10;
  std::array<std::array<double,25>,25> C3tx_01;
  std::array<std::array<double,25>,25> C3ty_00;
  std::array<std::array<double,25>,25> C3ty_10;
  std::array<std::array<double,25>,25> C3ty_01;
  
  double alpha;
  double beta;
  double ZINI;
  double ZFIN;
  int NBINX;
  int NBINY;
  double PMIN;
  double TXY_MAX;

};
#endif
