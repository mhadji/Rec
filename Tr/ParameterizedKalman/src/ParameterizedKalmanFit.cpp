// Include files

#include "Event/State.h"

#include "Event/ODIN.h"

#include "LHCbMath/Similarity.h"

#include "Kernel/Trajectory.h"

// local
#include "ParameterizedKalmanFit.h"

//########################################################################
//
// Implementation file for class : ParameterizedKalmanFit
//
// 2017-10-26: Simon Stemmle
//
//########################################################################

using namespace ParKalman;

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( ParameterizedKalmanFit  )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
ParameterizedKalmanFit::ParameterizedKalmanFit( const std::string& name,
                                        ISvcLocator* pSvcLocator) :
  Transformer(name, pSvcLocator,
                KeyValue{"InputName",  "Rec/Track/ForwardFast"},
                KeyValue{"OutputName", "Rec/Track/Best2"})
{
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode ParameterizedKalmanFit::initialize() {
  StatusCode sc = Transformer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  m_measProviderT.retrieve();
  m_measProviderV.retrieve();
  m_measProviderUT.retrieve();

  m_magFieldSvc       = svc<ILHCbMagnetSvc>( "MagneticFieldSvc", true );

  //cache information for the smoother of outliers should be removed
  m_do_smoother = m_MaxNoutlier>0;

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
LHCb::Tracks ParameterizedKalmanFit::operator()(const LHCb::Tracks& input) const {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;
  //============================================================
  //== Main processing: fit Tracks
  //============================================================

  //output tracks
  LHCb::Tracks result;

  if(input.empty()){
    return result;
  }

  //fit status
  StatusCode sc;

  //struct that contains the intermediate track information
  trackInfo tI;
  tI.m_do_smoother = m_do_smoother;

  //select the respective extrapolator
  if(m_magFieldSvc->signedRelativeCurrent()>0)
    tI.m_extr = &m_ParExtrUp;
  else
    tI.m_extr = &m_ParExtrDown;

  //Loop over the tracks and fit them
  for (auto const &trackIn : input) {
    // Create a new track keeping the same key
    LHCb::Track& track = *(trackIn->cloneWithKey());

    tI.m_track = &track;

    sc = fit(tI);

    if(sc.isSuccess()) result.insert(&track);
  }

  return result;
}


//=============================================================================
//  Perform the fit
//=============================================================================
StatusCode ParameterizedKalmanFit::fit(trackInfo &tI) const {
  //load hit information
  LoadHits(tI, m_measProviderV, m_measProviderUT, m_measProviderT, m_UseUT, m_UseT);

  //current state:
  //current z position
  double lastz =-1.;
  //state vector x: x,y,tx,ty,qop
  Gaudi::Vector5 x;
  Gaudi::SymMatrix5x5 C;

  //best state closest to the beam
  double zBest=0;
  Gaudi::Vector5 xBest;
  Gaudi::SymMatrix5x5 CBest;
  //the according best chi2
  double chi2Best=0;


  //#####################################
  //do a forward and a backward iteration
  //#####################################

  //create the seed state at the first VELO hit
  CreateVeloSeedState(0, x, C, lastz, tI);

  //reset chi2 for the forward filtering
  tI.m_chi2=0;
  tI.m_chi2V=0;

  //start by updating with the first measurment
  UpdateState(1, 0, x, C, lastz, tI);

  //forward filtering
  for(int nhit=1; nhit<tI.m_NHitsTotal; nhit++){
    if(!PredictState(1, nhit, x, C, lastz, tI)) return StatusCode::FAILURE;
    UpdateState(1, nhit, x, C, lastz, tI);
  }

  //first best momentum estimate
  tI.m_BestMomEst = x[4];
  //take the chi2 from the forward filtering
  chi2Best = tI.m_chi2;

  //reset covariance matrix to represent "no information"
  C(0,0)=400;
  C(0,1)=0;
  C(0,2)=0;
  C(0,3)=0;
  C(0,4)=0;
  C(1,1)=400;
  C(1,2)=0;
  C(1,3)=0;
  C(1,4)=0;
  C(2,2)=0.01;
  C(2,3)=0;
  C(2,4)=0;
  C(3,3)=0.01;
  C(3,4)=0;
  C(4,4)=25*C(4,4);//TODO check this

  tI.m_chi2=0;
  tI.m_chi2T=0;

  //backward filtering
  //start with an update using the information of the last hit
  UpdateState(-1, tI.m_NHitsTotal-1, x, C, lastz, tI);
  for(int nhit=tI.m_NHitsTotal-2; nhit>=0; nhit--){
    if(!PredictState(-1, nhit, x, C, lastz, tI)) return StatusCode::FAILURE;
    UpdateState(-1, nhit, x, C, lastz, tI);
  }

  //we have now a first best state
  xBest = x;
  CBest = C;
  zBest = lastz;

  //use the momentum estimate from the backward filtering (might be better)
  if(m_UseBackwardEstiamte) xBest[4] = tI.m_BestMomEst;

  //##############################################
  //do outlier removal if requested and necessary
  //##############################################

  bool ForwardUpToDate = false;

  for(int i=0; i<m_MaxNoutlier; i++){
    //start by running the smoother (average forward and backward)
    tI.m_chi2=0;
    for(int nhit=0; nhit<tI.m_NHitsTotal; nhit++){
      AverageState(nhit, tI);
    }
    //now check if there are outliers and remove the worst one
    if(DoOutlierRemoval(tI)){
      //use the state after an backward propagation as the best state
      if(!m_UseBackwardEstiamte){
        //do a backward iteration

        //set the state at the last hit
        lastz=tI.m_StateZPos[tI.m_NHitsTotal-1];
        x = tI.m_StateForwardUpdated[tI.m_NHitsTotal-1];
        //reset covariance matrix to represent "no information"
        C(0,0)=400;
        C(0,1)=0;
        C(0,2)=0;
        C(0,3)=0;
        C(0,4)=0;
        C(1,1)=400;
        C(1,2)=0;
        C(1,3)=0;
        C(1,4)=0;
        C(2,2)=0.01;
        C(2,3)=0;
        C(2,4)=0;
        C(3,3)=0.01;
        C(3,4)=0;
        C(4,4)=25*C(4,4);//TODO check this

        tI.m_chi2=0;
        tI.m_chi2T=0;

        //backward filtering
        //start with an update using the information of the last hit
        UpdateState(-1, tI.m_NHitsTotal-1, x, C, lastz, tI);
        for(int nhit=tI.m_NHitsTotal-2; nhit>=0; nhit--){
          if(!PredictState(-1, nhit, x, C, lastz, tI)) return StatusCode::FAILURE;
          UpdateState(-1, nhit, x, C, lastz, tI);
        }

        //we have now a better best state
        xBest = x;
        CBest = C;
        zBest = lastz;
        chi2Best = tI.m_chi2;

        //do a forward iteration in case that a further outlier iteration is planned
        if(i<m_MaxNoutlier-1){
          //reset covariance matrix to represent "no information"
          C(0,0)=400;
          C(0,1)=0;
          C(0,2)=0;
          C(0,3)=0;
          C(0,4)=0;
          C(1,1)=400;
          C(1,2)=0;
          C(1,3)=0;
          C(1,4)=0;
          C(2,2)=0.01;
          C(2,3)=0;
          C(2,4)=0;
          C(3,3)=0.01;
          C(3,4)=0;
          C(4,4)=25*C(4,4);//TODO check this

          //reset chi2 for the forward filtering
          tI.m_chi2=0;
          tI.m_chi2V=0;

          //start by updating with the first measurment
          UpdateState(1, 0, x, C, lastz, tI);
          //forward filtering
          for(int nhit=1; nhit<tI.m_NHitsTotal; nhit++){
            if(!PredictState(1, nhit, x, C, lastz, tI)) return StatusCode::FAILURE;
            UpdateState(1, nhit, x, C, lastz, tI);
          }
        }
      }
      //use the momentum after an forward propagation in the best state
      else{
        ForwardUpToDate = true;

        //do a forward iteration
        //reset chi2 for the forward filtering
        tI.m_chi2=0;
        tI.m_chi2V=0;

        //set the state at the first hit
        lastz=tI.m_StateZPos[0];
        x = tI.m_StateForwardUpdated[0];
        //reset covariance matrix to represent "no information"
        C(0,0)=400;
        C(0,1)=0;
        C(0,2)=0;
        C(0,3)=0;
        C(0,4)=0;
        C(1,1)=400;
        C(1,2)=0;
        C(1,3)=0;
        C(1,4)=0;
        C(2,2)=0.01;
        C(2,3)=0;
        C(2,4)=0;
        C(3,3)=0.01;
        C(3,4)=0;
        C(4,4)=25*C(4,4);//TODO check this

        //start by updating with the first measurment
        UpdateState(1, 0, x, C, lastz, tI);

        //forward filtering
        for(int nhit=1; nhit<tI.m_NHitsTotal; nhit++){
          if(!PredictState(1, nhit, x, C, lastz, tI)) return StatusCode::FAILURE;
          UpdateState(1, nhit, x, C, lastz, tI);
        }

        tI.m_BestMomEst = x[4];
        chi2Best = tI.m_chi2;

        //reset covariance matrix to represent "no information"
        C(0,0)=400;
        C(0,1)=0;
        C(0,2)=0;
        C(0,3)=0;
        C(0,4)=0;
        C(1,1)=400;
        C(1,2)=0;
        C(1,3)=0;
        C(1,4)=0;
        C(2,2)=0.01;
        C(2,3)=0;
        C(2,4)=0;
        C(3,3)=0.01;
        C(3,4)=0;
        C(4,4)=25*C(4,4);//TODO check this

        tI.m_chi2=0;
        tI.m_chi2T=0;

        //backward filtering
        //start with an update using the information of the last hit
        UpdateState(-1, tI.m_NHitsTotal-1, x, C, lastz, tI);
        for(int nhit=tI.m_NHitsTotal-2; nhit>=0; nhit--){
          if(!PredictState(-1, nhit, x, C, lastz, tI)) return StatusCode::FAILURE;
          UpdateState(-1, nhit, x, C, lastz, tI);
        }

        //we have now a first best state
        xBest = x;
        xBest[4] = tI.m_BestMomEst;
        CBest = C;
        zBest = lastz;

      }
    }
    //No outlier found
    else{
     //tell that the last forward iteration is up to date
     ForwardUpToDate = true;
     break;
    }
  }

  //In case that there is no forward up to date information and
  //we have removed a Velo hit we do the velo fit again
  if(!ForwardUpToDate && tI.m_NdofV!=2*tI.m_NHitsV){
    //set state from first hit
    lastz=tI.m_StateZPos[0];
    x=tI.m_StateBackwardUpdated[0];
    //reset covariance matrix to represent "no information"
    C(0,0)=400;
    C(0,1)=0;
    C(0,2)=0;
    C(0,3)=0;
    C(0,4)=0;
    C(1,1)=400;
    C(1,2)=0;
    C(1,3)=0;
    C(1,4)=0;
    C(2,2)=0.01;
    C(2,3)=0;
    C(2,4)=0;
    C(3,3)=0.01;
    C(3,4)=0;
    C(4,4)=25*C(4,4);//TODO check this

    //reset chi2 for the forward filtering
    tI.m_chi2=0;
    tI.m_chi2V=0;

    //start by updating with the first measurment
    UpdateState(1, 0, x, C, lastz, tI);
    //forward filtering
    for(int nhit=1; nhit<tI.m_NHitsV; nhit++){
      if(!PredictState(1, nhit, x, C,lastz, tI)) return StatusCode::FAILURE;
      UpdateState(1, nhit, x, C, lastz, tI);
    }
  }

  //extrapolate to the vertex
  ExtrapolateToVertex(xBest, CBest, zBest, m_extrapolator_toPV);
  //and create a new LHCb::Track
  tI.m_chi2=chi2Best;
  addInfoToTrack(xBest, CBest, zBest, tI);

  return StatusCode::SUCCESS;
}

//////////////////////////////////////////
// General method for predicting to a hit
//////////////////////////////////////////
bool ParameterizedKalmanFit::PredictState(int forward, int nHit, Gaudi::Vector5 &x,
                                          Gaudi::SymMatrix5x5 &C, double &lastz,
                                          trackInfo &tI) const {
  //success flag
  bool Succes = true;

  //Choose the appropiate predicting method depending on the detector
  //forward_________________________________________________________
  if(forward>0){
    //Predict inside VELO
    if(nHit<tI.m_NHitsV) PredictStateV(nHit, x, C, lastz, tI);

    //Predict to first UT layer or directly to T
    else if(nHit==tI.m_NHitsV){
      //To first UT hit
      Succes = PredictStateVUT(x, C, lastz, tI);
      tI.m_PrevNUT=0;
      //predict further in UT if there is no hit
      while(tI.m_HasHitUT[tI.m_PrevNUT]==0 && Succes && tI.m_PrevNUT<3){
        tI.m_PrevNUT++;
        PredictStateUT(tI.m_PrevNUT, x, C, lastz, tI);
      }
      //In case there is no UT hit, extrapolate to T
      if(tI.m_NHitsUT==0 && Succes){
        PredictStateUTT(x, C, lastz, tI);
        tI.m_PrevNT=0;
        //predict further if there is no hit
        while(tI.m_HasHitT[tI.m_PrevNT]==0){
          tI.m_PrevNT++;
          PredictStateT(tI.m_PrevNT, x, C, lastz, tI);
        }
      }
    }
    //Predict inside UT
    else if(nHit<tI.m_NHitsV+tI.m_NHitsUT){
      //predict to next UT layer
      tI.m_PrevNUT++;
      PredictStateUT(tI.m_PrevNUT, x, C, lastz, tI);
      //predict further if there is no hit
      while(tI.m_HasHitUT[tI.m_PrevNUT]==0 && tI.m_PrevNUT<3){
        tI.m_PrevNUT++;
        PredictStateUT(tI.m_PrevNUT, x, C, lastz, tI);
      }
    }

    //Predict from UT to T
    else if(nHit==tI.m_NHitsV+tI.m_NHitsUT){
      //check if we are at last UT station layer
      while(tI.m_PrevNUT<3){
        tI.m_PrevNUT++;
        PredictStateUT(tI.m_PrevNUT, x, C, lastz, tI);
      }
      PredictStateUTT(x, C, lastz, tI);
      tI.m_PrevNT=0;
      //predict further if there is no hit
      while(tI.m_HasHitT[tI.m_PrevNT]==0){
        tI.m_PrevNT++;
        PredictStateT(tI.m_PrevNT, x, C, lastz, tI);
      }
    }

    //Predict inside T
    else if(nHit<tI.m_NHitsTotal){
      //predict to next T layer
      tI.m_PrevNT++;
      PredictStateT(tI.m_PrevNT, x, C, lastz, tI);
      //predict further if there is no hit
      while(tI.m_HasHitT[tI.m_PrevNT]==0 && tI.m_PrevNT<11){
        tI.m_PrevNT++;
        PredictStateT(tI.m_PrevNT, x, C, lastz, tI);
      }
    }
  }
  //forward end_____________________________________________________

  //backwards_______________________________________________________
  else{
    //reset prevNT in case there was no forward prediction
    if(nHit==tI.m_NHitsTotal-2) tI.m_PrevNT=11;
    //Predict inside T
    if(nHit>=tI.m_NHitsV+tI.m_NHitsUT){
      //predict to next UT layer
      tI.m_PrevNT--;
      PredictStateT(tI.m_PrevNT, x, C, lastz, tI);
      //predict further if there is no hit
      while(tI.m_HasHitT[tI.m_PrevNT]==0 &&  tI.m_PrevNT>0){
        tI.m_PrevNT--;
        PredictStateT(tI.m_PrevNT, x, C, lastz, tI);
      }
    }

    //Predict to first UT layer or directly to VP
    else if(nHit==tI.m_NHitsV+tI.m_NHitsUT-1){
      //To last UT hit
      PredictStateUTT(x, C, lastz, tI);
      tI.m_PrevNUT=3;
      //predict further if there is no hit
      while(tI.m_HasHitUT[tI.m_PrevNUT]==0 &&  tI.m_PrevNUT>0){
        tI.m_PrevNUT--;
        PredictStateUT(tI.m_PrevNUT, x, C, lastz, tI);
      }
      //In case there is no UT hit, extrapolate to VP
      if(tI.m_NHitsUT==0){
        Succes &= PredictStateVUT(x, C, lastz, tI);
      }
    }

    //Predict inside UT
    else if(nHit>=tI.m_NHitsV){
      //predict to next UT layer
      tI.m_PrevNUT--;
      PredictStateUT(tI.m_PrevNUT, x, C, lastz, tI);
      //predict further if there is no hit
      while(tI.m_HasHitUT[tI.m_PrevNUT]==0 &&  tI.m_PrevNUT>0){
        tI.m_PrevNUT--;
        PredictStateUT(tI.m_PrevNUT, x, C, lastz, tI);
      }
    }

    //Predict to VP
    else if(nHit==tI.m_NHitsV-1){
      Succes &= PredictStateVUT(x, C, lastz, tI);
    }

    //Simple version for the VELO
    else if(nHit<tI.m_NHitsV-1){
      PredictStateV(nHit, x, C, lastz, tI);
    }
  }
  //backwards end___________________________________________________

  //Save information for the smoother
  if(tI.m_do_smoother){
    if(forward>0){
      tI.m_StateForwardPredicted[nHit]=x;
      tI.m_CovForwardPredicted[nHit]=C;
    }
    else{
      tI.m_StateBackwardPredicted[nHit]=x;
      tI.m_CovBackwardPredicted[nHit]=C;
    }
  }
  return Succes;
}
