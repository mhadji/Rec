#pragma once

#include <type_traits>
#include "Types.h"
#include "Scheduler.h"
#include "VectorConfiguration.h"

// ------------------
// Array constructors
// ------------------

namespace Tr {

namespace TrackVectorFit {

struct ArrayGen {
  template<size_t W>
  static inline constexpr uint16_t mask () {
    return (W==16 ? 0xFFFF : (W==8 ? 0xFF : (W==4 ? 0x0F : 0x03)));
  }

  template<size_t W, std::size_t... Is>
  static constexpr inline std::array<TRACKVECTORFIT_PRECISION, 5*W> InitialState_helper (
    const std::array<Sch::Item, W>& nodes,
    std::index_sequence<Is...>
  ) {
    return {
      nodes[Is].node->m_nodeParameters.m_referenceVector[0]...,
      nodes[Is].node->m_nodeParameters.m_referenceVector[1]...,
      nodes[Is].node->m_nodeParameters.m_referenceVector[2]...,
      nodes[Is].node->m_nodeParameters.m_referenceVector[3]...,
      nodes[Is].node->m_nodeParameters.m_referenceVector[4]...
    };
  }

  template<size_t W>
  static constexpr inline std::array<TRACKVECTORFIT_PRECISION, 5*W> InitialState (
    const std::array<Sch::Item, W>& nodes
  ) {
    return InitialState_helper<W>(nodes, std::make_index_sequence<W>{});
  }
  
  static constexpr inline std::array<TRACKVECTORFIT_PRECISION, 15> InitialCovariance_values () {
    return {
      400,
      0, 400,
      0, 0, 0.01,
      0, 0, 0, 0.01,
      0, 0, 0, 0, 0.0001
    };
  }

  static constexpr inline std::array<TRACKVECTORFIT_PRECISION, 15> InitialCovariance_values (
    const size_t
  ) {
    return InitialCovariance_values();
  }

  template<size_t W, std::size_t... Is>
  static constexpr inline std::array<TRACKVECTORFIT_PRECISION, 15*W> InitialCovariance_helper (
    std::index_sequence<Is...>
  ) {
    return {
      InitialCovariance_values(Is)[0]...,
      InitialCovariance_values(Is)[1]...,
      InitialCovariance_values(Is)[2]...,
      InitialCovariance_values(Is)[3]...,
      InitialCovariance_values(Is)[4]...,
      InitialCovariance_values(Is)[5]...,
      InitialCovariance_values(Is)[6]...,
      InitialCovariance_values(Is)[7]...,
      InitialCovariance_values(Is)[8]...,
      InitialCovariance_values(Is)[9]...,
      InitialCovariance_values(Is)[10]...,
      InitialCovariance_values(Is)[11]...,
      InitialCovariance_values(Is)[12]...,
      InitialCovariance_values(Is)[13]...,
      InitialCovariance_values(Is)[14]...
    };
  }

  template<size_t W>
  static constexpr inline std::array<TRACKVECTORFIT_PRECISION, 15*W> InitialCovariance () {
    return InitialCovariance_helper<W>(std::make_index_sequence<W>{});
  }
};

}

}
