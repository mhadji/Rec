#pragma once

#include "../Types.h"
#include "Predict.h"
#include "Update.h"

namespace Tr {

namespace TrackVectorFit {

namespace Scalar {

/**
 * @brief      Fits an initial node, whose covariance is passed by parameter
 */
template<class T>
inline void fit (
  Node& node,
  const std::array<TRACKVECTORFIT_PRECISION, 15>& covariance
) {
  initialize<T>(node, covariance);
  update<T>(node);
}

/**
 * @brief      Fits a node
 */
template<class T>
inline void fit (
  Node& node,
  const Node& prevnode
) {
  predict<T>(node, prevnode);
  update<T>(node);
}

}

}

}
