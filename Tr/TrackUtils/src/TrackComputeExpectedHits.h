#ifndef _TrackComputeExpectedHits_H_
#define _TrackComputeExpectedHits_H_

/** @class TrackComputeExpectedHits TrackComputeExpectedHits.h
 *
 *
 *  @author S. Hansmann-Menzemer
 *  @date   20.07.2009
 */

#include "GaudiAlg/GaudiAlgorithm.h"
#include <string>

struct IHitExpectation;
struct IVeloExpectation;

class TrackComputeExpectedHits final : public GaudiAlgorithm {

public:

  // Constructors and destructor
  TrackComputeExpectedHits(const std::string& name, ISvcLocator* pSvcLocator);

  StatusCode initialize() override;
  StatusCode execute() override;

private:

  std::string m_inputLocation;

  IVeloExpectation* m_veloExpectation = nullptr;
  IHitExpectation* m_ttExpectation = nullptr;
  IHitExpectation* m_itExpectation = nullptr;
  IHitExpectation* m_otExpectation = nullptr;

};

#endif
