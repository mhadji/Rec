#pragma once

// Include files
// -------------
// from STD
#include <algorithm>
#include <exception>
#include <unordered_map>
#include <array>
#include <memory>
#include <cassert>
#include <type_traits>
#include <cstddef>

// TrackEvent
#include "Event/Track.h"
#include "Event/Node.h"
#include "Event/TrackFunctor.h"
#include "Event/KalmanFitResult.h"

// LHCbKernel
#include "Kernel/HitPattern.h"
#include "Kernel/LHCbID.h"

// Range v3
#include <range/v3/view.hpp>
#include <range/v3/range_for.hpp>

// local

// Include files
// -------------
// from Gaudi
#include "GaudiAlg/MergingTransformer.h"
#if DEBUGHISTOGRAMS
#include "GaudiAlg/GaudiHistoAlg.h"
#endif
#include "GaudiKernel/ToolHandle.h"

// from TrackInterfaces
#include "TrackInterfaces/ITrackCloneFinder.h"
#include "TrackInterfaces/ITrackFitter.h"
#include "TrackInterfaces/ITrackStateInit.h"
#include "TrackInterfaces/IGhostProbability.h"
#include "TrackKernel/TrackCloneData.h"

#include "TrackVectorFit/Types.h"
#include "Kernel/VectorSOAStore.h"

namespace  {

  /// structure to save some data for each track
  class TrackData : public LHCb::TrackCloneData<>
  {
  private:
    bool m_isAccepted;
    double m_qOverP;
    LHCb::Track::FitStatus m_fitStatus;
    enum { Clone = 1 };

  public:
    /// constructor
    TrackData (std::unique_ptr<LHCb::Track> tr) :
      TrackCloneData<>(std::move(tr)),
      m_qOverP(track().firstState().qOverP()),
      m_fitStatus(track().fitStatus())
    { }
    /// return q/p (or what it was at construction time)
    double qOverP () const { return m_qOverP; }
    LHCb::Track::FitStatus previousStatus () const { return m_fitStatus; }
    bool cloneFlag () const { return userFlags() & Clone; }
    void setCloneFlag () { setUserFlags(userFlags() | Clone); }
    bool isAccepted () const { return m_isAccepted; }
    void setAccepted (const bool& isAccepted) { m_isAccepted = isAccepted; }

    std::vector<std::reference_wrapper<TrackData>> clones;
  };
}

#if DEBUGHISTOGRAMS
typedef GaudiHistoAlg TrackBestTrackCreatorBase;
#else
typedef GaudiAlgorithm TrackBestTrackCreatorBase;
#endif


/** @brief kill clones among tracks in input container and Kalman-fit survivors
 *
 * @author Wouter Hulsbergen
 * - initial release
 *
 * @author Manuel Schiller
 * @date 2014-11-18
 * - simplify, C++11 cleanups, use BloomFilter based TrackCloneData
 * @date 2014-12-09
 * - add code to use ancestor information on tracks to deal with obvious clones
 */
class TrackBestTrackCreator final :
  public Gaudi::Functional::MergingTransformer<LHCb::Tracks(const Gaudi::Functional::vector_of_const_<LHCb::Tracks> &ranges),
                                               Gaudi::Functional::Traits::BaseClass_t<TrackBestTrackCreatorBase>> {
public:
  /// Standard constructor
  TrackBestTrackCreator(const std::string& name, ISvcLocator* pSvcLocator);

  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode finalize  () override;    ///< Algorithm finalization
  LHCb::Tracks
    operator()(const Gaudi::Functional::vector_of_const_<LHCb::Tracks> &ranges) const override;

private:
  ToolHandle<ITrackStateInit> m_stateinittool{"TrackStateInitTool", this};
  ToolHandle<ITrackFitter> m_fitter{"TrackMasterFitter", this};
  ToolHandle<IGhostProbability> m_ghostTool{"Run2GhostId", this};

  Gaudi::Property<double> m_maxOverlapFracVelo{this, "MaxOverlapFracVelo", 0.5};
  Gaudi::Property<double> m_maxOverlapFracT{this, "MaxOverlapFracT", 0.5};
  Gaudi::Property<double> m_maxOverlapFracTT{this, "MaxOverlapFracTT", 0.35, "essentially: max 1 common hit"};
  Gaudi::Property<double> m_minLongLongDeltaQoP{this, "MinLongLongDeltaQoP", -1};
  Gaudi::Property<double> m_minLongDownstreamDeltaQoP{this, "MinLongDownstreamDeltaQoP", 5e-6};
  Gaudi::Property<double> m_maxChi2DoF{this, "MaxChi2DoF", 3};
  Gaudi::Property<double> m_maxChi2DoFVelo{this, "MaxChi2DoFVelo", 999};
  Gaudi::Property<double> m_maxChi2DoFT{this, "MaxChi2DoFT", 999};
  Gaudi::Property<double> m_maxChi2DoFMatchAndTT{this, "MaxChi2DoFMatchTT", 999};
  Gaudi::Property<double> m_maxGhostProb{this, "MaxGhostProb", 99999};
  Gaudi::Property<bool> m_fitTracks{this, "FitTracks", true, "fit the tracks using the Kalman filter"};
  Gaudi::Property<bool> m_initTrackStates{this, "InitTrackStates", true, "initialise track states using m_stateinittool"};
  Gaudi::Property<bool> m_addGhostProb{this, "AddGhostProb", false, "Add the Ghost Probability to a track"};
  Gaudi::Property<bool> m_useAncestorInfo{this, "UseAncestorInfo", true, "use ancestor information to identify obvious clones"};
  Gaudi::Property<bool> m_doNotRefit{this, "DoNotRefit", false, "Do not refit already fitted tracks"};
  Gaudi::Property<unsigned> m_batchSize{this, "BatchSize", 50, "size of batches (if processing in batches)"};

  bool m_debugLevel;
  mutable std::atomic<int> m_nGhosts{0};

  /// Process sequentially or in batches
  bool m_processInBatches;

protected:
  /// are tracks clones in VeloR and VeloPhi
  bool veloClones (const TrackData&, const TrackData&) const;
  /// are tracks clones in VeloR or VeloPhi
  bool veloOrClones (const TrackData&, const TrackData&) const;
  /// are tracks clones in T
  bool TClones (const TrackData&, const TrackData&) const;
  /// are tracks clones in TT
  bool TTClones (const TrackData&, const TrackData&) const;

  void fitAndSelect (
    const std::vector<std::reference_wrapper<TrackData>>::iterator& tracksBegin,
    const std::vector<std::reference_wrapper<TrackData>>::iterator& tracksEnd
  ) const;

  void fitAndUpdateCounters (
    const std::vector<std::reference_wrapper<TrackData>>::iterator& tracksBegin,
    const std::vector<std::reference_wrapper<TrackData>>::iterator& tracksEnd
  ) const;

  /// check if tracks pointed to by their TrackData objects are clones
  bool areClones (const TrackData& it, const TrackData& jt) const;

  /// mapping between original track and the index of its copy
  typedef std::pair<const LHCb::Track*, size_t> CopyMapEntry;
  typedef std::vector<CopyMapEntry> CopyMap;
};
