// Include files
#include <algorithm>
// from TrackEvent
#include "Event/Track.h"

// local
#include "TrackFromDST.h"

namespace {

std::pair<std::string,std::vector<LHCb::Track::History>>
make( const std::string& s, const std::initializer_list<LHCb::Track::History>& l )
{
    return { s, l };
}

// Map between track history flags and output containers
// ------------------------------------------------------------------
static const std::array< std::pair<std::string, std::vector<LHCb::Track::History>> ,6> s_map = {
    make( LHCb::TrackLocation::Seed,   { LHCb::Track::History::PatSeeding,
                                         LHCb::Track::History::TsaTrack,
                                         LHCb::Track::History::PrSeeding } ),
    make( LHCb::TrackLocation::Velo,   { LHCb::Track::History::PatVelo       ,
                                         LHCb::Track::History::PatFastVelo } ),
    make( LHCb::TrackLocation::VeloTT, { LHCb::Track::History::PatVeloTT     ,
                                         LHCb::Track::History::PrVeloUT } ),
    make( LHCb::TrackLocation::Forward,{ LHCb::Track::History::PatForward    ,
                                         LHCb::Track::History::PrForward } ),
    make( LHCb::TrackLocation::Match,  { LHCb::Track::History::PatMatch      ,
                                         LHCb::Track::History::PrMatch } ),
    make( LHCb::TrackLocation::Downstream, { LHCb::Track::History::PatDownstream,
                                         LHCb::Track::History::PatKShort, // in for backward compatiblity, PatDownstream tracks were called PatKShort tracks before
                                         LHCb::Track::History::PrDownstream  } )
};

}
//-----------------------------------------------------------------------------
// Implementation file for class : TrackFromDST
//
// 2006-09-18 : Eduardo Rodrigues
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TrackFromDST )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TrackFromDST::TrackFromDST( const std::string& name,
                            ISvcLocator* pSvcLocator)
  : GaudiAlgorithm ( name , pSvcLocator )
{
  declareProperty( "TracksInContainer",
                   m_tracksInContainer = LHCb::TrackLocation::Default );
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode TrackFromDST::execute()
{
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  // Get all the tracks from in input container
  // ------------------------------------------
  const LHCb::Tracks* inTracks = get<LHCb::Tracks>( m_tracksInContainer );
  if ( msgLevel(MSG::DEBUG) ) debug() << "# of Tracks in input container \""
                              << m_tracksInContainer
                              << "\" = " << inTracks -> size() << endmsg;
  std::vector<LHCb::Track*> work{ inTracks->begin(), inTracks->end() }; // make a copy so we partition them...
  auto first = work.begin(), last = work.end();
  for (const auto& item : s_map ) {
      // partition the tracks according to their History flag
      auto pivot = std::stable_partition( first, last,
                                          [&](const LHCb::Track* t) {
          auto i = std::find( item.second.begin(), item.second.end(), t->history() );
          return i != item.second.end();
      });
      // clone them to their corresponding containers
      auto out = std::make_unique<LHCb::Tracks>();
      std::for_each( first, pivot, [&]( const LHCb::Track* t ) {
          out->add( t->cloneWithKey() );
      });
      // put container on the TES
      put( out.release(), item.first );
      if ( msgLevel(MSG::DEBUG) ) debug()
          << "Stored " << std::distance(first, pivot)
          << " tracks in " << item.first << endmsg;

      // and go to the next range of tracks...
      first = pivot;
  }
  if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) {
      std::for_each(first,last,[&](const LHCb::Track* t) {
          debug() << "Invalid track type " << t->history() << endmsg;
      });
  }
  return StatusCode::SUCCESS;
}
