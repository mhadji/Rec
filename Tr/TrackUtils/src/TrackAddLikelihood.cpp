// track interfaces
#include "Event/Track.h"

#include "TrackInterfaces/ITrackManipulator.h"
#include "TrackAddLikelihood.h"
#include "GaudiKernel/ToStream.h"

using namespace LHCb;

DECLARE_COMPONENT( TrackAddLikelihood )

TrackAddLikelihood::TrackAddLikelihood(const std::string& name,
                       ISvcLocator* pSvcLocator):
  GaudiAlgorithm(name, pSvcLocator)
{
 declareProperty("inputLocation", m_input );
 declareProperty("LikelihoodTool", m_likelihoodToolName = "TrackLikelihood");
 declareProperty("types", m_types = { Track::History::PatVelo, Track::History::PatVeloTT,
                                      Track::History::PatForward, Track::History::TrackMatching,
                                      Track::History::PatMatch, Track::History::PatSeeding,
                                      Track::History::PatDownstream, Track::History::TsaTrack,
                                      Track::History::PatVeloGeneral, Track::History::PatFastVelo } );
}

StatusCode TrackAddLikelihood::initialize() {
  std::transform( m_types.begin(), m_types.end(),
                  std::inserter(m_toolMap, m_toolMap.end() ),
                  [&](unsigned int t) {
    Track::History type = Track::History(t);
    auto name = Gaudi::Utils::toString(type)+"_likTool";
    return std::make_pair(type, tool<ITrackManipulator>(m_likelihoodToolName,
                                              name, this ) );
  });
  return StatusCode::SUCCESS;
}

StatusCode TrackAddLikelihood::execute(){
  for (const auto& track : *m_input.get() ) {
    unsigned int type = track->history();
    auto iter = m_toolMap.find(type);
    if (iter == m_toolMap.end()) {
      // @FIXME : do we really want to skip all other tracks?
      return Warning("Likelihood not calculated: Unknown track type",
                     StatusCode::SUCCESS );
    }
    iter->second->execute(*track).ignore();
  }
  return StatusCode::SUCCESS;
}
