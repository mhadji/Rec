// Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// track interfaces
#include "Event/State.h"

#include "TrackPrepareVelo.h"

using namespace LHCb;
using namespace Gaudi::Units;
using namespace Gaudi;

DECLARE_COMPONENT( TrackPrepareVelo )

TrackPrepareVelo::TrackPrepareVelo(const std::string& name,
                                   ISvcLocator* pSvcLocator)
  : GaudiAlgorithm(name, pSvcLocator)
  , m_ignoreBest(false)
{
  // constructor
  declareProperty( "inputLocation", m_inputLocation = TrackLocation::Velo);
  declareProperty( "outputLocation", m_outputLocation = "/Event/Rec/Track/PreparedVelo");
  declareProperty( "bestLocation", m_bestLocation = TrackLocation::Default);
  declareProperty( "ptVelo", m_ptVelo = 400.*MeV);
  declareProperty( "reverseCharge", m_reverseCharge = false );

}

StatusCode TrackPrepareVelo::initialize(){
  m_ignoreBest = m_bestLocation.empty();
  return StatusCode::SUCCESS;
}

StatusCode TrackPrepareVelo::execute(){

  Tracks* inCont = get<Tracks>(m_inputLocation);
  Tracks* bestCont = 0 ;
  if ( !m_ignoreBest ) bestCont = get<Tracks>(m_bestLocation);
  Tracks* outCont = new Tracks();

  // loop
  for(auto track: *inCont){
    bool takeTrack = false;
    if (m_ignoreBest == false) takeTrack = used(track,bestCont);
    if ( takeTrack == false){
      Track* aTrack = track->clone();
      int firstStrip(-1);
      for( auto id : aTrack->lhcbIDs() ){
	if( id.isVelo() ) {
	  firstStrip = id.veloID().strip();
	  break;
	}
      }
      if( firstStrip == -1 ){
	return Warning("Setting can not set random q/p for non-velo track");
      }
      int charge;
      firstStrip % 2  == 0 ? charge = -1 : charge = 1;
      if( m_reverseCharge ) charge *= -1; // flip charge assignment if requested
      prepare(aTrack, charge );
      outCont->insert(aTrack);
    }
  } // iterT

  put(outCont,m_outputLocation);

  return StatusCode::SUCCESS;
}

bool TrackPrepareVelo::used(const Track* aTrack, const Tracks* bestCont) const
{
  // if the track has already been flagged as a clone then do not use it
  if ( aTrack -> checkFlag(Track::Flags::Clone) ) return true;
  // check if velo track is used.
  bool found = false;
  Tracks::const_iterator iterT =  bestCont->begin();
  while ((iterT != bestCont->end())&&(found == false)){
    const SmartRefVector<LHCb::Track>& parents = (*iterT)->ancestors();
    for ( SmartRefVector<LHCb::Track>::const_iterator iterP = parents.begin();
          iterP != parents.end(); ++iterP) {
      const Track* testTrack = *iterP;
      if ( testTrack == aTrack) found = true;
    } // iterP
    ++iterT;
  } // iterT

  return found;
}

void TrackPrepareVelo::prepare(Track* aTrack, const int charge) const{
  // set q/p and error in all of the existing states
  const std::vector< LHCb::State * > states = aTrack->states();
  std::vector< LHCb::State * >::const_iterator iState;
  for ( iState = states.begin() ; iState != states.end() ; ++iState ){
    TrackVector& vec = (*iState)->stateVector();
    double slope2 = std::max(vec(2)*vec(2) + vec(3)*vec(3), 1e-20);
    double curv = charge * sqrt( slope2 ) / (m_ptVelo * sqrt( 1. + slope2 ));
    (*iState)->setQOverP(curv);
    (*iState)->setErrQOverP2(1e-6);
  }
}
