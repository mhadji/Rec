#ifndef _TrackAddLikelihood_H_
#define _TrackAddLikelihood_H_

/** @class TrackAddLikelihood TrackAddLikelihood.h
 *
 *  Copy a container of tracks. By default do not copy tracks that failed the fit
 *
 *  @author M.Needham
 *  @date   30/05/2006
 */

#include "GaudiAlg/GaudiAlgorithm.h"
#include <string>
#include <vector>
#include <map>

struct ITrackManipulator;
class TrackAddLikelihood final : public GaudiAlgorithm {

public:

  // Constructors and destructor
  TrackAddLikelihood(const std::string& name, ISvcLocator* pSvcLocator);

  StatusCode initialize() override;
  StatusCode execute() override;

private:

  DataObjectHandle<LHCb::Tracks> m_input{ LHCb::TrackLocation::Default, Gaudi::DataHandle::Reader, this };
  std::vector<unsigned int> m_types;
  std::string  m_likelihoodToolName;
  std::map<unsigned int, const ITrackManipulator*> m_toolMap;

};

#endif
