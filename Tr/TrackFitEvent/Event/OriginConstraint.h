#ifndef TRACKFITEVENT_ORIGINCONSTRAINT_H
#define TRACKFITEVENT_ORIGINCONSTRAINT_H

// Include files
#include "Event/Measurement.h"

#ifdef _WIN32
// Avoid conflict of Windows macro with std::max
  #ifndef NOMINMAX
    #define NOMINMAX
  #endif
#endif

#include <ostream>

// Forward declarations

namespace LHCb
{

  // Forward declarations
  class ChiSquare ;
  class State ;

  class OriginConstraint : public LHCb::Measurement
  {
  public:
    OriginConstraint( const Gaudi::XYZPoint& point,
		      const Gaudi::SymMatrix3x3& covariance ) ;

    /// Default Destructor
    ~OriginConstraint() {}

    /// Clone the OTMeasurement
    LHCb::Measurement* clone() const override { return new OriginConstraint(*this) ; }

    /// filter this constraint. returns the chi2
    LHCb::ChiSquare filter( LHCb::State& state,
			    const Gaudi::TrackVector& reference) const ;

  private:
    Gaudi::XYZPoint m_origin ;
    Gaudi::SymMatrix3x3 m_weight ; // weight matrix (inverse covariance)
  };
}
#endif
