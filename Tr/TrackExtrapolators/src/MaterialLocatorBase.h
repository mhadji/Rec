#ifndef TRACKEXTRAPOLATORS_TRACKMATERIALINTERSECTORBASE_H
#define TRACKEXTRAPOLATORS_TRACKMATERIALINTERSECTORBASE_H

// Include files
// -------------
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/SystemOfUnits.h"

// from TrackInterfaces
#include "TrackInterfaces/IMaterialLocator.h"
#include "TrackInterfaces/IStateCorrectionTool.h"

#include "DetDesc/ITransportSvc.h"

/** @class MaterialLocatorBase MaterialLocatorBase.h
 *
 *  A MaterialLocatorBase is a base class implementing methods
 *  from the IMaterialLocatorBase interface.
 *
 *  @author Wouter Hulsbergen
 *  @date   12/05/2007
 */

class MaterialLocatorBase : public extends<GaudiTool, IMaterialLocator>
{
public:

  /// Constructor
  MaterialLocatorBase( const std::string& type,
                       const std::string& name,
                       const IInterface* parent );

  /// intialize
  StatusCode initialize() override;

  using IMaterialLocator::intersect;
  using IMaterialLocator::intersect_r;

  // Create an instance of the accelerator cache
  ranges::v3::any createCache() const override
  {
    return m_tSvc->createCache();
  }

  /// Intersect a line with volumes in the geometry
  size_t intersect( const Gaudi::XYZPoint& p,
                    const Gaudi::XYZVector& v,
                    Intersections& intersepts) const override
  {
    return intersect_r( p, v, intersepts, m_accelCache );
  }

  /// Intersect a trajectory with volumes in the geometry
  size_t intersect( const LHCb::ZTrajectory& traj,
                    Intersections& intersepts ) const override
  {
    return intersect_r( traj, intersepts, m_accelCache );
  }

  /// Intersect a trajectory interpolated between two statevectors with volumes in the geometry
  size_t intersect( const LHCb::StateVector& origin,
                    const LHCb::StateVector& target,
                    Intersections& intersepts ) const override
  {
    return intersect_r( origin, target, intersepts, m_accelCache );
  }

  /// Intersect a line with volumes in the geometry
  size_t intersect_r( const Gaudi::XYZPoint& p,
                      const Gaudi::XYZVector& v,
                      Intersections& intersepts,
                      ranges::v3::any& accelCache ) const override;

  /// Intersect a trajectory with volumes in the geometry
  size_t intersect_r( const LHCb::ZTrajectory& traj,
                      Intersections& intersepts,
                      ranges::v3::any& accelCache ) const override;

  /// Intersect a trajectory interpolated between two statevectors with volumes in the geometry
  size_t intersect_r( const LHCb::StateVector& origin,
                      const LHCb::StateVector& target,
                      Intersections& intersepts,
                      ranges::v3::any& accelCache ) const override;

  void applyMaterialCorrections( LHCb::State& stateAtTarget,
                                 const IMaterialLocator::Intersections& intersepts,
                                 double zorigin,
                                 const LHCb::Tr::PID pid,
                                 bool applyScatteringCorrection,
                                 bool applyEnergyLossCorrection ) const override;
private:

  Gaudi::Property<std::string> m_dedxtoolname
  { this, "GeneralDedxToolName" , "StateDetailedBetheBlochEnergyCorrectionTool" };
  Gaudi::Property<std::string> m_msCorrectionToolName
  { this, "StateMSCorrectionToolName" , "StateThickMSCorrectionTool" };

  size_t m_maxNumIntervals             = 20;
  double m_maxDeviation                = 5*Gaudi::Units::cm;
  double m_maxDeviationAtRefstates     = 2*Gaudi::Units::mm;
  double m_maxDeviationAtVeloRefstates = 0.5*Gaudi::Units::mm;

  IStateCorrectionTool* m_scatteringTool = nullptr;
  IStateCorrectionTool* m_dedxtool       = nullptr;

  ToolHandle<const IStateCorrectionTool> m_elecdedxtool
  { "StateElectronEnergyCorrectionTool" };

protected:

  /// Transport service
  ServiceHandle<ITransportSvc> m_tSvc;

  /** Local accelerator cache. Should eventually be removed so
   *  only the re-entrant versions are available */
  mutable ranges::v3::any m_accelCache;

};

#endif // TRACKEXTRAPOLATORS_TRACKMATERIALINTERSECTORBASE_H
