#ifndef TRACKINTERFACES_IMEASUREMENTPROVIDER_H
#define TRACKINTERFACES_IMEASUREMENTPROVIDER_H 1

// Include files
// -------------
// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include <vector>
#include "boost/range/iterator_range.hpp"

// Forward declarations
namespace LHCb {
 class LHCbID;
 class Track;
 class Measurement;
 class StateVector;
 class ZTrajectory ;
}


/** @class IMeasurementProvider IMeasurementProvider.h TrackInterfaces/IMeasurementProvider.h
 *
 *  Interface for the measurement provider tool
 *
 *  @author Jose Hernando
 *  @author Eduardo Rodrigues
 *  @date   2005-06-28
 */
struct IMeasurementProvider : extend_interfaces<IAlgTool> {

  DeclareInterfaceID(  IMeasurementProvider, 2, 0 );

  /** Load (=create) all the Measurements from the list of LHCbIDs
   *  on the input Track
   */
  virtual StatusCode load( LHCb::Track& track ) const = 0;

  /** Construct a Measurement of the type of the input LHCbID
   *  Note: this method is not for general use. A user should preferably call
   *  the "load( Track& track )" method to load=create "in one go" all the
   *  Measurements from the list of LHCbIDs on the Track.
   *  This method is in fact called internally by "load( Track& track )".
   *  @return Pointer the the Measurement created
   *  @param  id:  input LHCbID
   *  @param  localY: creates y trajectory for muon, if true
   */
  virtual LHCb::Measurement* measurement( const LHCb::LHCbID& id,
					  bool localY = false ) const = 0;

  /** Construct a measurement with a statevector. This takes care that
      things like errors depending on track angle are correctly
      set. */

  /** Construct a measurement with a reference trajectory. This takes care that
      things like errors depending on track angle are correctly set. */
  virtual LHCb::Measurement* measurement( const LHCb::LHCbID& id,
                                          const LHCb::ZTrajectory& reftraj,
                                          bool localY = false ) const = 0;

  /** create measurements for a set of LHCbIDs **/
  using range_of_ids = boost::iterator_range<typename std::vector<LHCb::LHCbID>::iterator>;
  virtual void addToMeasurements( range_of_ids,
                                  std::vector<LHCb::Measurement*>&,
                                  const LHCb::ZTrajectory& ) const = 0 ;
};
#endif // TRACKINTERFACES_IMEASUREMENTPROVIDER_H
