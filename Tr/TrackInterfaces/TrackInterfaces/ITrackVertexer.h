#ifndef TRACKINTERFACES_ITRACKVERTEXER_H
#define TRACKINTERFACES_ITRACKVERTEXER_H 1

// Include files
// from STL
#include <vector>
#include <memory>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

namespace LHCb {
  class TwoProngVertex ;
  class Track ;
  class State ;
  class RecVertex ;
}


/** @class ITrackVertexer ITrackVertexer.h TrackInterfaces/ITrackVertexer.h
 *
 *  @author Wouter HULSBERGEN
 *  @date   2007-11-07
 *
 */
struct ITrackVertexer : extend_interfaces<IAlgTool> {

  DeclareInterfaceID(ITrackVertexer, 2, 0 );
  typedef std::vector<const LHCb::State*> StateContainer ;
  typedef std::vector<const LHCb::Track*> TrackContainer ;


  virtual std::unique_ptr<LHCb::TwoProngVertex> fit(const LHCb::State& stateA, const LHCb::State& stateB) const = 0 ;
  virtual std::unique_ptr<LHCb::RecVertex> fit(const StateContainer& states) const = 0 ;
  virtual std::unique_ptr<LHCb::RecVertex> fit(const TrackContainer& tracks) const = 0 ;
  virtual bool computeDecayLength(const LHCb::TwoProngVertex& vertex,
				  const LHCb::RecVertex& pv,
				  double& chi2,double& decaylength,double& decaylengtherr) const = 0 ;

  /// Return the ip chi2 for a track (uses stateprovider, not good for
  /// HLT: better call routine below with track->firstState())
  virtual double ipchi2( const LHCb::Track& track, const LHCb::RecVertex& pv) const = 0 ;

  /// Return the ip chi2 for a track state
  virtual double ipchi2( const LHCb::State& state, const LHCb::RecVertex& pv) const = 0 ;
};
#endif // TRACKINTERFACES_ITRACKVERTEXER_H
