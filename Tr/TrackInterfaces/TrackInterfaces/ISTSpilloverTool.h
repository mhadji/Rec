#ifndef TRACKINTERFACES_ISTSPILLOVERTOOL_H
#define TRACKINTERFACES_ISTSPILLOVERTOOL_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

// Forward declarations
namespace LHCb
{
  class Node;
}


/** @class ISTSpilloverTool ISTSpilloverTool.h TrackInterfaces/ISTSpilloverTool.h
 *
 *
 *  @author Vincenzo Battista
 *  @date   2015-08-13
 */
struct ISTSpilloverTool : extend_interfaces<IAlgTool> {
  DeclareInterfaceID(ISTSpilloverTool, 2, 0 );

  /// Return PDF(ADC,costheta|central)/PDF(ADC|spillover) starting from a track node
  virtual double pdfRatio( const LHCb::Node* node ) const = 0;


};
#endif // TRACKINTERFACES_ISTSPILLOVERTOOL_H
