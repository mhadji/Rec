#ifndef TRACKINTERFACES_IMATERIALLOCATOR_H
#define TRACKINTERFACES_IMATERIALLOCATOR_H

// Include files
// -------------
// from Gaudi
#include "GaudiKernel/IAlgTool.h"

// from LHCbKernel
#include "Event/TrackTypes.h"
#include "Kernel/TrackDefaultParticles.h"

// others
#include "DetDesc/ILVolume.h"

// range v3
#include <range/v3/utility/any.hpp>


// forwarded
namespace LHCb
{
  class State ;
  class StateVector ;
  class ZTrajectory ;
}

/** @class IMaterialLocatorLocator
 *
 *  Interface for tools that locate materil intersections on a trajectory
 *
 *  @author Wouter Hulsbergen
 *  @date   2006-05-16
 */

struct IMaterialLocator : extend_interfaces<IAlgTool>
{
  /// interface ID
  DeclareInterfaceID( IMaterialLocator, 2, 0 );

  /// embedded class representing intersection
  struct Intersection final
  {
    double z1;
    double z2;
    double tx;
    double ty;
    const Material* material;
  };

  /// container of intersections
  typedef std::vector<Intersection> Intersections ;

  /// Create an instance of the accelerator cache
  virtual ranges::v3::any createCache() const = 0 ;

  /// Intersect a line with volumes in the geometry
  virtual size_t intersect( const Gaudi::XYZPoint& p,
                            const Gaudi::XYZVector& v,
			    ILVolume::Intersections& intersepts ) const =  0 ;

  /// Intersect a line with volumes in the geometry
  virtual size_t intersect( const Gaudi::XYZPoint& p,
                            const Gaudi::XYZVector& v,
			    Intersections& intersepts) const = 0  ;

  /// Intersect a trajectory with volumes in the geometry
  virtual size_t intersect( const LHCb::ZTrajectory& traj,
                            Intersections& intersepts ) const = 0;

  /// Intersect a trajectory interpolated between two statevectors with volumes in the geometry
  virtual size_t intersect( const LHCb::StateVector& origin,
                            const LHCb::StateVector& target,
			    Intersections& intersepts ) const = 0 ;

  /// Intersect a line with volumes in the geometry
  virtual size_t intersect_r( const Gaudi::XYZPoint& p,
                              const Gaudi::XYZVector& v,
                              ILVolume::Intersections& intersepts,
                              ranges::v3::any& accelCache ) const =  0 ;

  /// Intersect a line with volumes in the geometry
  virtual size_t intersect_r( const Gaudi::XYZPoint& p,
                              const Gaudi::XYZVector& v,
                              Intersections& intersepts,
                              ranges::v3::any& accelCache ) const = 0  ;

  /// Intersect a trajectory with volumes in the geometry
  virtual size_t intersect_r( const LHCb::ZTrajectory& traj,
                              Intersections& intersepts,
                              ranges::v3::any& accelCache ) const = 0;

  /// Intersect a trajectory interpolated between two statevectors with volumes in the geometry
  virtual size_t intersect_r( const LHCb::StateVector& origin,
                              const LHCb::StateVector& target,
                              Intersections& intersepts,
                              ranges::v3::any& accelCache ) const = 0 ;

  /// Apply material corrections using material in intersepts
  virtual void applyMaterialCorrections( LHCb::State& stateAtTarget,
					 const Intersections& intersepts,
					 double zorigin,
					 const LHCb::Tr::PID pid,
					 bool applyScatteringCorrection = true,
					 bool applyELossCorrection = true ) const = 0 ;

} ;

#endif
