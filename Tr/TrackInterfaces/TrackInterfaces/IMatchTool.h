#ifndef IMATCHTOOL_H
#define IMATCHTOOL_H 1

// Include files

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

// Event model
#include "Event/Track.h"

/** @class IMatchTool IMatchTool.h TrackInterfaces/IMatchTool.h
 *
 *  @author Johannes Albrecht
 *  @date   2008-04-25
 */
struct IMatchTool : extend_interfaces<IAlgTool> {

  DeclareInterfaceID( IMatchTool, 2, 0 );

  /// match velo tracks with seed tracks, output in matchs
  virtual StatusCode match(const LHCb::Tracks& velos,
                           const LHCb::Tracks& seeds,
                           LHCb::Tracks& matchs )=0;

  /// for trigger: match a single velo with seed track,
  /// output: output track and match chi2,
  /// SUCCESS if chi2 below cut, FAILURE if not
  virtual StatusCode matchSingle(const LHCb::Track& velo,
                                 const LHCb::Track& seed ,
                                 LHCb::Track& output,
                                 double& chi2)=0;

};
#endif // IMATCHTOOL_H
