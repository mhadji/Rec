#ifndef _LongTrackReferenceCreator_H
#define _LongTrackReferenceCreator_H

/** @class LongTrackReferenceCreator LongTrackReferenceCreator.h
 *
 * Implementation of TrackCaloMatch tool
 * see interface header for description
 *
 *  @author M.Needham
 *  @date   30/12/2005
 */

#include "GaudiAlg/GaudiTool.h"

#include "TrackInterfaces/ITrackManipulator.h"

#include <string>

namespace LHCb{
  class Track;
  class State;
  class Measurement;
}

struct ITrackExtrapolator;
class IMagneticFieldSvc;
struct ITrajPoca;

class LongTrackReferenceCreator: public extends<GaudiTool, ITrackManipulator>  {

public:

  /** constructer */
  using base_class::base_class;

  /** intialize */
  StatusCode initialize() override;

  /** add reference info to the track */
  StatusCode execute(LHCb::Track& aTrack) const override;

private:
  ITrackExtrapolator* m_extrapolator = nullptr;
};

#endif
