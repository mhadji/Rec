/** @class FTMeasurementProvider FTMeasurementProvider.cpp
 *
 * Implementation of FTMeasurementProvider
 * see interface header for description
 *
 *  @author Wouter Hulsbergen
 *  @date   30/12/2005
 */

#include "TrackInterfaces/IMeasurementProvider.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/AnyDataHandle.h"
#include "GaudiKernel/ToolHandle.h"

#include "TrackInterfaces/ITrackExtrapolator.h"
#include "Event/FTMeasurement.h"
#include "Event/StateVector.h"
#include "FTDet/DeFTDetector.h"
#include "Event/TrackParameters.h"
#include "TrackKernel/TrackTraj.h"
#include "Event/FTLiteCluster.h"

class FTMeasurementProvider final : public extends< GaudiTool,
                                                    IMeasurementProvider >
{
public:

  /// constructor
  FTMeasurementProvider(const std::string& type,
                         const std::string& name,
                         const IInterface* parent);

  StatusCode initialize() override;
  LHCb::Measurement* measurement( const LHCb::LHCbID& id,
                                  bool localY=false ) const override;
  LHCb::Measurement* measurement( const LHCb::LHCbID& id,
                                  const LHCb::ZTrajectory& refvector,
                                  bool localY=false) const override;
  inline LHCb::FTMeasurement* ftmeasurement( const LHCb::LHCbID& id ) const  ;

  const FastClusterContainer<LHCb::FTLiteCluster,int>* clusters() const;

  void addToMeasurements( range_of_ids ids,
                          std::vector<LHCb::Measurement*>& measurements,
                          const LHCb::ZTrajectory& reftraj) const override ;

  StatusCode load( LHCb::Track&  ) const override {
    return Error( "sorry, MeasurementProviderBase::load not implemented" );
  }

private:
  const DeFTDetector* m_det = nullptr;
  AnyDataHandle<FastClusterContainer<LHCb::FTLiteCluster,int>> m_clustersDh { LHCb::FTLiteClusterLocation::Default,
                                                                              Gaudi::DataHandle::Reader, this };
} ;

//=============================================================================
// Declare to tool factory
//=============================================================================

DECLARE_COMPONENT( FTMeasurementProvider )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
FTMeasurementProvider::FTMeasurementProvider( const std::string& type,
                                              const std::string& name,
                                              const IInterface* parent )
  :  base_class( type, name , parent )
{
  declareProperty("ClusterLocation",m_clustersDh);
}


//-----------------------------------------------------------------------------
/// Initialize
//-----------------------------------------------------------------------------

StatusCode FTMeasurementProvider::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if( sc.isFailure() ) { return Error( "Failed to initialize!", sc ); }

  // Retrieve the detector element
  m_det = getDet<DeFTDetector>( DeFTDetectorLocation::Default ) ;

  return sc;
}

//-----------------------------------------------------------------------------
/// Load clusters from the TES
//-----------------------------------------------------------------------------

const FastClusterContainer<LHCb::FTLiteCluster,int>* FTMeasurementProvider::clusters() const
{
  return m_clustersDh.get();
}

//-----------------------------------------------------------------------------
/// Create a measurement
//-----------------------------------------------------------------------------
LHCb::FTMeasurement* FTMeasurementProvider::ftmeasurement( const LHCb::LHCbID& id ) const {

  if( !id.isFT() ) {
    error() << "Not an FT measurement" << endmsg ;
    return nullptr;
  }
  /// The clusters are sorted, so we can use a binary search (lower bound search)
  /// to find the element corresponding to the channel ID
  const auto& c = *clusters();
  auto itH = std::lower_bound( c.begin(),  c.end(), id.ftID(),
                      [](const LHCb::FTLiteCluster clus, const LHCb::FTChannelID id){
                        return clus.channelID() < id;
                      });
  return ( itH != c.end() ) ?  new LHCb::FTMeasurement( (*itH), *m_det ) : nullptr;
}

//-----------------------------------------------------------------------------
/// Return the measurement
//-----------------------------------------------------------------------------
LHCb::Measurement* FTMeasurementProvider::measurement( const LHCb::LHCbID& id,
                                                       bool /*localY*/ ) const {
  return ftmeasurement(id) ;
}

//-----------------------------------------------------------------------------
/// Create a measurement with statevector. For now very inefficient.
//-----------------------------------------------------------------------------

LHCb::Measurement* FTMeasurementProvider::measurement( const LHCb::LHCbID& id,
                                                       const LHCb::ZTrajectory& /* reftraj */,
                                                       bool /*localY*/ ) const {
  // default implementation
  return ftmeasurement(id) ;
}

//-----------------------------------------------------------------------------
/// Create measurements for list of LHCbIDs
//-----------------------------------------------------------------------------

void FTMeasurementProvider::addToMeasurements( range_of_ids ids,
                                               std::vector<LHCb::Measurement*>& measurements,
                                               const LHCb::ZTrajectory& reftraj) const
{
  std::transform( begin(ids), end(ids), std::back_inserter(measurements),
                  [&](const LHCb::LHCbID& id)
                  { return measurement(id,reftraj,false) ; });
}
