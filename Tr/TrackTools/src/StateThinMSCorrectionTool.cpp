// Include files
// -------------
#include "vdt/log.h"

// from DetDesc
#include "DetDesc/Material.h"

// from TrackEvent
#include "Event/TrackParameters.h"

// local
#include "StateThinMSCorrectionTool.h"

using namespace Gaudi::Units;

//-----------------------------------------------------------------------------
// Implementation file for class : StateThinMSCorrectionTool
//
// 2006-08-21 : Eduardo Rodrigues
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( StateThinMSCorrectionTool )

//=============================================================================
// Correct a State for multiple scattering
// in the approximation of a thin scatter
//=============================================================================
void StateThinMSCorrectionTool::correctState( LHCb::State& state,
                                              const Material* material,
                                              ranges::v3::any& /*cache*/,
                                              double wallThickness,
                                              bool,
                                              double  ) const
{
  const double t          = wallThickness / material -> radiationLength();
  const double norm2      = 1.0 + std::pow(state.tx(),2) + std::pow(state.ty(),2);
  double scatLength = 0.;
  if ( t > TrackParameters::lowTolerance ) {
    const double radThick = sqrt(norm2) * t;
    scatLength = radThick*std::pow( TrackParameters::moliereFactor *
                                 (1.+0.038*vdt::fast_log(radThick)),2 );
  }

  // protect against zero momentum
  const double p = std::max( state.p(), 1.0*MeV );

  const double norm2cnoise = norm2 * m_msff2 * scatLength / std::pow(p,2);

  // update covariance matrix C = C + Q
  // multiple scattering covariance matrix Q only has three elements...
  Gaudi::TrackSymMatrix& tC = state.covariance();
  tC(2,2) += norm2cnoise * ( 1. + std::pow(state.tx(),2) );
  tC(3,3) += norm2cnoise * ( 1. + std::pow(state.ty(),2) );
  tC(3,2) += norm2cnoise * state.tx() * state.ty();
}

//=============================================================================
