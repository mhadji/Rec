
// Include files

#include "Event/OTMeasurement.h"
#include "Event/StateVector.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "TrajOTProjector.h"

class TrajOTCosmicsProjector : public TrajOTProjector
{
public:

  /// Standard constructor
  using TrajOTProjector::TrajOTProjector;

  StatusCode initialize() override;

  using TrajOTProjector::project;

protected:
  TrackProjector::InternalProjectResult
    internal_project(const LHCb::StateVector& state,
                     const LHCb::Measurement& meas) const override;

private:
  Gaudi::Property<double> m_tofReferenceZ { this, "TofReferenceZ", 12.8*Gaudi::Units::m } ;
  Gaudi::Property<bool> m_fitEventT0 { this, "FitEventT0", true } ;
  Gaudi::Property<bool> m_useConstantDriftVelocity { this, "UseConstantDriftVelocity", true } ;
};

//----------------------------------------------------------------------------

DECLARE_COMPONENT( TrajOTCosmicsProjector )

StatusCode TrajOTCosmicsProjector::initialize()
{
  StatusCode sc = TrajOTProjector::initialize() ;
  info() << "Fit event t0 = " << m_fitEventT0.value() << endmsg ;
  info() << "Use const v_drift = " << m_useConstantDriftVelocity.value() << endmsg ;
  return sc ;
}

//-----------------------------------------------------------------------------
/// Project a state onto a measurement
//-----------------------------------------------------------------------------
TrackProjector::InternalProjectResult
TrajOTCosmicsProjector::internal_project (const LHCb::StateVector& statevector,
                                          const LHCb::Measurement& genMeas) const {
  // (Decided to catch this particlar project call such that I do no
  // need to introduce new virtual function hooks in default
  // projector.)

  // check that we ar dealing with an OTMeasurement
  if (!genMeas.checkType(LHCb::Measurement::OT)) {
    throw StatusCode::FAILURE;
  }
  const LHCb::OTMeasurement& meas = dynamic_cast<const LHCb::OTMeasurement&>(genMeas) ;

  // compute the tof correction relative to a reference z.
  double L0 = (meas.z() - m_tofReferenceZ)*std::sqrt( 1 + statevector.tx()*statevector.tx() + statevector.ty()*statevector.ty()) ;
  bool forward = statevector.ty() < 0 ;
  double tof = (forward ? 1 : -1) * L0/Gaudi::Units::c_light ;
  // should we subtract a reference time-of-flight?
  //   tof -= fabs( m_tofReferenceZ - meas.z() ) / Gaudi::Units::c_light ;
  // add this to the measurement, including the phase. we'll use the
  // qop-entry of the statevector to store the phase.
  double eventt0 = statevector.parameters()[4] ;
  // ugly const-cast to update the measurement's time-of-flight
  (const_cast< LHCb::OTMeasurement&>(meas)).setDeltaTimeOfFlight( (float) (tof + eventt0) ) ;
  // need to test ambiguity before calling projector!-(
  // call the standard projector (which uses the time-of-flight)
  auto result = TrajOTProjector::internal_project( statevector, meas ) ;
  bool usedrifttime =
    ( meas.driftTimeStrategy() == LHCb::OTMeasurement::FitTime ||
      meas.driftTimeStrategy() == LHCb::OTMeasurement::FitDistance ) ;

  // update the projection matrix with the derivative to event-t0.
  if( usedrifttime && m_fitEventT0 ) {
    if ( fitDriftTime() ) {
      result.H(0,4) = 1 ;
    } else {
      double vdrift = meas.module().rtRelation().drdt() ;
      if( !m_useConstantDriftVelocity ) {
        // get the drift velocity. for the linearization it is best to
        // have something as closest to the truth as possible. that's not
        // the drift time. so, instead, we compute the inverse:
        double dtdr = meas.module().rtRelation().dtdr( std::abs(result.doca) ) ;
        vdrift = 1/dtdr ;
      }
      // now we just need to get the sign right. I think that H is MINUS
      // the derivative of the residual in Fruhwirth's languague
      result.H(0,4) = meas.ambiguity() * vdrift ;
    }
  }

  return result;
}
