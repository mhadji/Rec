#include "Map.h"

const TrackMonitorMaps::TypeMap& TrackMonitorMaps::typeDescription()
{
  static const TrackMonitorMaps::TypeMap s_map = { {"Velo",LHCb::Track::Types::Velo},
                                                   {"VeloR",LHCb::Track::Types::VeloR},
                                                   {"Long",LHCb::Track::Types::Long},
                                                   {"Upstream",LHCb::Track::Types::Upstream},
                                                   {"Downstream",LHCb::Track::Types::Downstream},
                                                   {"Ttrack",LHCb::Track::Types::Ttrack},
                                                   {"Muon",LHCb::Track::Types::Muon} };
  return s_map;
}

const TrackMonitorMaps::InfoHistMap& TrackMonitorMaps::infoHistDescription()
{
   static const  InfoHistMap s_map = { {LHCb::Track::AdditionalInfo::FitTChi2,HistoRange("8", 0., 100.)},
                                       {LHCb::Track::AdditionalInfo::FitTNDoF,HistoRange("9", 0., 50.)},
                                       {LHCb::Track::AdditionalInfo::FitVeloChi2,HistoRange("10", 0., 100.)},
                                       {LHCb::Track::AdditionalInfo::FitVeloNDoF,HistoRange("11", 0., 50.)},
                                       {LHCb::Track::AdditionalInfo::FitMatchChi2,HistoRange("12", 0., 100.)} };
   return s_map;
}
