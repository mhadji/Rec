// Include files
// -------------

// From Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// Local
#include "PatPV3D.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PatPV3D
//
// 2004-02-17 : Eduardo Rodrigues
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( PatPV3D )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PatPV3D::PatPV3D(const std::string& name,
                 ISvcLocator* pSvcLocator) :
MultiTransformerFilter(name , pSvcLocator,
                       KeyValue{"InputTracks", LHCb::TrackLocation::Default},
                       {KeyValue("OutputVerticesName", LHCb::RecVertexLocation::Velo3D),
                        KeyValue("PrimaryVertexLocation", "")}) {
  declareProperty( "RefitPV", m_refitpv = false ) ;
}

//=============================================================================
// Initialisation. Check parameters
//=============================================================================
StatusCode PatPV3D::initialize() {

  StatusCode sc = MultiTransformerFilter::initialize();
  if (!sc) return sc;

  debug() << "==> Initialise" << endmsg;

  // Access PVOfflineTool
  m_pvsfit = tool<IPVOfflineTool>("PVOfflineTool",this);
  if(!m_pvsfit) {
    err() << "Unable to retrieve the PVOfflineTool" << endmsg;
    return  StatusCode::FAILURE;
  }
  return StatusCode::SUCCESS;

}

//=============================================================================
// Main execution
//=============================================================================
std::tuple<bool, LHCb::RecVertices,LHCb::PrimaryVertices>
PatPV3D::operator()(const LHCb::Tracks& inputTracks) const {

  MsgStream msg(msgSvc(), name());
  bool isDebug = msg.level() <= MSG::DEBUG;

  if (isDebug) {
    debug() << "==> Execute" << endmsg;
  }

  LHCb::RecVertices outputRecVertices;
  LHCb::PrimaryVertices outputPrimaryVertices;
  std::vector<LHCb::RecVertex> rvts;
  bool filter = false;
  StatusCode scfit = m_pvsfit->reconstructMultiPV(inputTracks, rvts);
  if (scfit == StatusCode::SUCCESS) {
    filter = !rvts.empty();
    for (const auto& iv : rvts) {
      LHCb::RecVertex* vertex = new LHCb::RecVertex(iv);
      vertex->setTechnique(LHCb::RecVertex::Primary);
      outputRecVertices.insert(vertex);
    }
    for (const auto& iv : rvts) {
      LHCb::PrimaryVertex* vertex = new LHCb::PrimaryVertex(iv,m_refitpv);
      outputPrimaryVertices.insert(vertex);
    }
  } else {
    Warning("reconstructMultiPV failed!",scfit).ignore();
  }

  return std::make_tuple(filter, std::move(outputRecVertices), std::move(outputPrimaryVertices));
}
