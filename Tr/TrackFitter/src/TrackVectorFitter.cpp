#include "TrackVectorFitter.h"

#include "TrackVectorFit/vector/TrackExtrapolator.h"
// #include <valgrind/callgrind.h>
// #include "ittnotify.h"

DECLARE_COMPONENT( TrackVectorFitter )

namespace Tr {
namespace TrackVectorFitter {

thread_local Tr::TrackVectorFit::TrackVectorFit fitter;

}
}

/**
 * @brief Standard constructor.
 */
TrackVectorFitter::TrackVectorFitter (
  const std::string& type,
  const std::string& name,
  const IInterface* parent
  ) : base_class(type, name, parent),
  m_extrapolator("TrackMasterExtrapolator", this),
  m_veloExtrapolator("TrackLinearExtrapolator", this),
  m_measProvider("MeasurementProvider", this),
  m_materialLocator("DetailedMaterialLocator", this),
  m_projectorSelector("TrackProjectorSelector", this)
{
  declareInterface<ITrackFitter>(this);

  declareProperty("Extrapolator", m_extrapolator);
  declareProperty("VeloExtrapolator", m_veloExtrapolator);
  declareProperty("MeasProvider", m_measProvider);
  declareProperty("MaterialLocator", m_materialLocator);
  declareProperty("Projector", m_projectorSelector);

  // Code profiling
  // m_firstCall = true;
}

/**
 * @brief Initializes all tools and debug level.
 */
StatusCode TrackVectorFitter::initialize () {  
  // Add the ability to disable the RungeKuttaExtrapolator
  std::string prefix ("TrackParabolicExtrapolator");
  m_rungeKuttaExtrapolatorEnabled = m_extrapolator.typeAndName().compare(0, prefix.size(), prefix);
  info() << "Selected extrapolator " << m_extrapolator.typeAndName() << endmsg;

  // Note: Do we really need to specify the pid, or is it always 211 for the fit?
  m_magneticFieldService = service(m_fieldSvcName, true);
  m_magneticFieldGrid = m_magneticFieldService->fieldGrid();

  // Count the number of hits of each type
  m_minNumHits = {m_minNumVeloRHits, m_minNumVeloPhiHits, m_minNumTTHits, m_minNumTHits, m_minNumMuonHits};

  if (base_class::initialize().isFailure() ||
    m_projectorSelector.retrieve().isFailure() ||
    m_measProvider.retrieve().isFailure() ||
    m_extrapolator.retrieve().isFailure() ||
    m_veloExtrapolator.retrieve().isFailure() ||
    m_materialLocator.retrieve().isFailure()) {

    return StatusCode::FAILURE;
  }

  if (msgLevel(MSG::DEBUG)) {
    debug() << " " << endmsg
      << "============ TrackVectorFitter Settings ===========" << endmsg
      << ((m_upstream) ? " Upstream fit" : " Downstream fit") << endmsg
      << " Number of fit iterations: " << m_numFitIter << endmsg
      << " Max " << m_numOutlierIter << " outliers removed with outliers"
      << " at chi2 > " << m_chi2Outliers << endmsg
      << " State z positions at: " << endmsg
      << ((m_stateAtBeamLine) ? " beam line," : "") << " first/last measurement"
      << (m_addDefaultRefNodes ? ", default reference positions" : "" )  << endmsg
      << "==================================================" << endmsg;
  }

  return StatusCode::SUCCESS;
}

/**
 * @brief Releases all tools and finalizes.
 */
StatusCode TrackVectorFitter::finalize () {
  m_extrapolator.release().ignore();
  m_veloExtrapolator.release().ignore();
  m_projectorSelector.release().ignore();
  m_measProvider.release().ignore();
  m_materialLocator.release().ignore();

  return base_class::finalize();
}

/**
 * @brief Performs the Kalman Filter fit over the desired track.
 */
StatusCode TrackVectorFitter::operator() (
  LHCb::Track& track,
  const LHCb::Tr::PID& pid
) const {
  std::vector<std::reference_wrapper<LHCb::Track>> tracks;
  tracks.push_back(track);

  // Invoke entrypoint with list of tracks
  return operator()(tracks, pid);
}

/**
 * @brief Performs the Kalman Filter over all designated tracks.
 */
StatusCode TrackVectorFitter::operator() (
  std::vector<std::reference_wrapper<LHCb::Track>>& tracks,
  const LHCb::Tr::PID& pid
) const {
  // Helper function to erase failed tracks from the reference list
  StatusCode sc (StatusCode::SUCCESS);
  auto eraseFailed = [&sc] (std::list<std::reference_wrapper<Tr::TrackVectorFit::Track>>& tfitting) {
    for (auto it=tfitting.begin(); it!=tfitting.end(); ) {
      Tr::TrackVectorFit::Track& t = *it;
      if (t.track().checkFitStatus(LHCb::Track::FitStatus::FitFailed)) {
        it = tfitting.erase(it);
        sc = StatusCode(StatusCode::FAILURE);
      } else {
        ++it;
      }
    }
  };

  // Prints out all nodes
  auto printNodes = [&] (Tr::TrackVectorFit::Track& t) {
    debug() << "Track #" << t.m_index << endmsg;
    auto& nodes = t.nodes();
    for (const Tr::TrackVectorFit::Node& node : nodes) {
      debug() << node << endmsg;
    }
  };

  // Code profiling
  // if (!m_firstCall) {
  //   CALLGRIND_TOGGLE_COLLECT;
  //   __itt_resume();
  // }
  
  Tr::TrackVectorFitter::fitter.reset(msgLevel(MSG::DEBUG), msgLevel(MSG::VERBOSE));

  // Generate the Tr::TrackVectorFit::Tracks
  std::list<Tr::TrackVectorFit::Track> tList;

  // Work with a list of fitting tracks
  std::list<std::reference_wrapper<Tr::TrackVectorFit::Track>> tOutliers;
  std::list<std::reference_wrapper<Tr::TrackVectorFit::Track>> tAccepted;

  unsigned trackNumber = 0;
  std::for_each(tracks.begin(), tracks.end(), [&] (LHCb::Track& track) {
    // We start on an unknown status
    track.setFitStatus(LHCb::Track::FitStatus::FitStatusUnknown);

    // Generate FitResult objects
    generateFitResult(track);

    // Make the nodes from the measurements
    if (track.fitResult()->nodes().empty() || m_makeNodes) {
      // Generate the nodes
      if (makeNodes(track, pid).isFailure()) {
        track.fitResult()->clearNodes();
        track.setFitStatus(LHCb::Track::FitStatus::FitFailed);
      }
    }

    if (!track.checkFitStatus(LHCb::Track::FitStatus::FitFailed)) {
      // Generate the Tr::TrackVectorFit::Track and Tr::TrackVectorFit::Nodes
      tList.emplace_back(track, trackNumber++);
    }
  });

  // Work with a list of fitting tracks
  std::list<std::reference_wrapper<Tr::TrackVectorFit::Track>> tConvergence (tList.begin(), tList.end());

  // First iteration
  unsigned tSize = tConvergence.size();

  // Run the vector scheduler
  Tr::TrackVectorFitter::fitter.initializeBasePointers<false>(tConvergence);

  std::for_each(tConvergence.begin(), tConvergence.end(), [&] (Tr::TrackVectorFit::Track& t) {
    // Populate the reference vectors from the defined states
    populateRefVectors(t);

    // Project measurements, update residuals and materials
    // for the first time
    projectAndUpdate(t, pid);
  });

  updateTransport(Tr::TrackVectorFitter::fitter.scheduler());

  std::for_each(tConvergence.begin(), tConvergence.end(), [&] (Tr::TrackVectorFit::Track& t) {
    // Set nTrackParameters
    setNTrackParameters(t);
  });

  Tr::TrackVectorFitter::fitter.smoothFit<false>(tConvergence);
  eraseFailed(tConvergence);

  if (msgLevel(MSG::DEBUG)) {
    debug() << "Convergence iteration #0" << endmsg;
    std::for_each(tConvergence.begin(), tConvergence.end(), [&] (Tr::TrackVectorFit::Track& t) {
      printNodes(t);
    });
  }

  // Iterate the track fit for linearisation. Be careful with chi2
  // convergence here: The first iteration might not be using OT
  // drifttimes in which case the chi2 can actually go up in the 2nd
  // iteration.
  for (unsigned i=1; i<m_numFitIter; ++i) {
    const bool sameSize = (tSize == tConvergence.size());
    tSize = tConvergence.size();

    if (!sameSize) {
      // Run the vector scheduler
      Tr::TrackVectorFitter::fitter.initializeBasePointers<true>(
        tConvergence,
        !(m_updateMaterial && m_applyMaterialCorrections)
      );
    } else {
      // Even though we don't initialize the scheduler and pointers,
      // we still need to copy the smoothed state into the ref vectors
      std::for_each(tConvergence.begin(), tConvergence.end(), [&] (Tr::TrackVectorFit::Track& t) {
        t.updateRefVectors();
      });
    }

    std::for_each(tConvergence.begin(), tConvergence.end(), [&] (Tr::TrackVectorFit::Track& t) {
      projectAndUpdate(t, pid, m_updateMaterial);
      // Note: projectAndUpdate may flaw the track (set its fit status to FitFailed).
      //       We proceed with those nevertheless, otherwise we screw up the scheduler.
      //       We will check them after the fit anyway.
    });

    updateTransport(Tr::TrackVectorFitter::fitter.scheduler());

    std::for_each(tConvergence.begin(), tConvergence.end(), [&] (Tr::TrackVectorFit::Track& t) {
      // Set nTrackParameters
      setNTrackParameters(t);

      t.savePrefit();
      t.saveChi2();
    });

    Tr::TrackVectorFitter::fitter.smoothFit<false>(tConvergence);

    if (msgLevel(MSG::DEBUG)) {
      debug() << "Convergence iteration #" << i << endmsg;
      std::for_each(tConvergence.begin(), tConvergence.end(), [&] (Tr::TrackVectorFit::Track& t) {
        printNodes(t);
      });
    }

    for (auto it=tConvergence.begin(); it!=tConvergence.end(); ) {
      Tr::TrackVectorFit::Track& t = *it;

      if (t.track().checkFitStatus(LHCb::Track::FitStatus::FitFailed)) {
        it = tConvergence.erase(it);
      } else {
        // Require at least 3 iterations, because of the OT prefit.
        const bool converged = !t.wasPrefit()
          && i>0
          && t.chi2Difference() < (m_maxDeltaChi2Converged * t.ndof());

        if (converged) {
          // TODO - Is this useful at all?
          t.setIterationsToConverge(i+1);

          tOutliers.push_back(*it);
          it = tConvergence.erase(it);
        } else {
          ++it;
        }
      }
    }

    if (tConvergence.empty()) {
      break;
    }
  }

  // Add leftovers
  std::for_each(tConvergence.begin(), tConvergence.end(), [&] (Tr::TrackVectorFit::Track& t) {
    tOutliers.push_back(t);
  });

  if (m_numOutlierIter.value() == 0) {
    // In case we are not doing outlier removal, just populate the tracks.
    // We are done here!
    populateTracks(tOutliers);
  }
  else {
    // Calculate number of iterations for Outlier Removal
    for (auto it=tOutliers.begin(); it!=tOutliers.end(); ) {
      Tr::TrackVectorFit::Track& t = *it;
      const unsigned nOutliers = t.track().fitResult()->nOutliers();
      if (nOutliers >= m_numOutlierIter) {
        tAccepted.push_back(*it);
        it = tOutliers.erase(it);
      } else {
        t.saveNumberOfOutlierIterations(m_numOutlierIter - nOutliers);
        ++it;
      }
    }

    // Outlier removal
    for (unsigned i=0; i<m_numOutlierIter; ++i) {
      // Remove outlier, and exit condition from outlier removal
      for (auto it=tOutliers.begin(); it!=tOutliers.end(); ) {
        Tr::TrackVectorFit::Track& t = *it;
        bool outlierRemoved = false;
        if (t.numberOfOutlierIterations() > i && t.ndof() > 1) {
          // Attempt to remove the worst outlier
          outlierRemoved = removeWorstOutlier(t);
        }
        if ( !outlierRemoved ) {
          // If we did not manage to remove an outlier, accept this track
          tAccepted.push_back(*it);
          it = tOutliers.erase(it);
        } else {
          ++it;
        }
      }

      // In the outlier removal iterations,
      // we always need to initialize the base pointers,
      // as the track size varies
      Tr::TrackVectorFitter::fitter.initializeBasePointers<true>(
        tOutliers,
        !(m_updateMaterial && m_applyMaterialCorrections)
      );

      std::for_each(tOutliers.begin(), tOutliers.end(), [&] (Tr::TrackVectorFit::Track& t) {
        projectAndUpdate(t, pid, m_updateMaterial);
      });

      updateTransport(Tr::TrackVectorFitter::fitter.scheduler());

      std::for_each(tOutliers.begin(), tOutliers.end(), [&] (Tr::TrackVectorFit::Track& t) {
        // Set nTrackParameters
        setNTrackParameters(t);
      });

      Tr::TrackVectorFitter::fitter.smoothFit<true>(tOutliers);

      if (msgLevel(MSG::DEBUG)) {
        debug() << "Outlier removal iteration #" << i << endmsg;
        std::for_each(tOutliers.begin(), tOutliers.end(), [&] (Tr::TrackVectorFit::Track& t) {
          printNodes(t);
        });
      }

      eraseFailed(tOutliers);
    }

    // Add leftovers
    std::for_each(tOutliers.begin(), tOutliers.end(), [&] (Tr::TrackVectorFit::Track& t) {
      tAccepted.push_back(t);
    });

    // Populate tracks
    populateTracks(tAccepted);
  }

  // Code profiling
  // if (!m_firstCall) {
  //   CALLGRIND_TOGGLE_COLLECT;
  //   __itt_pause();
  // }
  // m_firstCall = false;
  // CALLGRIND_DUMP_STATS;

  return sc;
}

/**
 * @brief      Populates the tracks with the fitted information.
 */
void TrackVectorFitter::populateTracks (
  std::list<std::reference_wrapper<Tr::TrackVectorFit::Track>>& tracks
) const {
  std::for_each(tracks.begin(), tracks.end(), [&] (Tr::TrackVectorFit::Track& t) {
    // Populate nodes with states, covariances, etc.
    Tr::TrackVectorFitter::fitter.populateNodes(t);

    // Determine states
    determineStates(t);

    // Add chi2 info
    t.calculateChi2Types();

    // Success!
    t.track().setFitStatus(LHCb::Track::FitStatus::Fitted);
  });

  // TODO - Is this used at all?
  if (m_fillExtraInfo) {
    std::for_each(tracks.begin(), tracks.end(), [&] (Tr::TrackVectorFit::Track& t) {
      fillExtraInfo(t);
    });
  }
}

void TrackVectorFitter::generateFitResult (LHCb::Track& track) const {
  if (track.fitResult() == nullptr) {
    track.setFitResult(new LHCb::TrackFitResult());
  }
}

/**
 * @brief Determines the z position of the closest approach
 *        to the beam line by linear extrapolation.
 */
double TrackVectorFitter::closestToBeamLine (const LHCb::State& state) const {
  const Gaudi::TrackVector& vec = state.stateVector();
  auto z = state.z();

  // Check on division by zero (track parallel to beam line!)
  if (vec[2] != 0 || vec[3] != 0) {
    z -= (vec[0]*vec[2] + vec[1]*vec[3]) / (vec[2]*vec[2] + vec[3]*vec[3]);
  }

  // Don't go outside the sensible volume
  // TODO - Make -100 * Gaudi::Units::cm a variable with some meaningful name
  return std::min(std::max(z, -100 * Gaudi::Units::cm), StateParameters::ZBegRich2);
}

/**
 * @brief Creates the nodes from the measurements.
 */
StatusCode TrackVectorFitter::makeNodes (LHCb::Track& track, const LHCb::Tr::PID& pid) const {
  auto& fitResult = *(track.fitResult());

  // TODO - Is this needed?
  fitResult.clearNodes();

  // Clear the measurements if asked for
  if (m_makeMeasurements) {
    fitResult.clearMeasurements();
  }

  if (msgLevel(MSG::DEBUG)) {
    debug() << "Track before making nodes: " << track << endmsg;
  }

  if (initializeRefStates(track, pid).isFailure()) {
    return Warning("Problems setting reference info", StatusCode::FAILURE, 1);
  }

  // Check if it is needed to populate the track with measurements
  if (track.checkPatRecStatus(LHCb::Track::PatRecStatus::PatRecIDs) || fitResult.measurements().empty()) {
    if (m_measProvider->load(track).isFailure()) {
      return Error("Unable to load measurements!", StatusCode::FAILURE);
    }
    track.setPatRecStatus(LHCb::Track::PatRecStatus::PatRecMeas);
    if (msgLevel(MSG::DEBUG)) {
      debug() << "# LHCbIDs, Measurements = " << track.nLHCbIDs()
              << ", " << fitResult.nMeasurements() << endmsg;
    }
  }

  // check that there are sufficient measurements. in fact, one is
  // enough for the fit not to fail
  if (fitResult.measurements().empty()) {
    return Warning("No measurements on track", StatusCode::FAILURE, 0);
  }

  // Create the nodes for the measurements.
  const auto& measurements = fitResult.measurements();
  LHCb::TrackFitResult::NodeContainer& nodes = fitResult.nodes();
  nodes.reserve(measurements.size() + 6);
  std::transform(measurements.rbegin(), measurements.rend(),
    std::back_inserter(nodes),
    [] (LHCb::Measurement* m) {
      return new Tr::TrackVectorFit::FitNode(m);
    }
  );

  // Sort the nodes in z
  const bool backward = track.checkFlag(LHCb::Track::Flags::Backward);
  const bool upstream = (m_upstream && !backward) || (!m_upstream && backward);
  if (upstream) {
    std::sort(nodes.begin(), nodes.end(), TrackFunctor::decreasingByZ());
  } else {
    std::sort(nodes.begin(), nodes.end(), TrackFunctor::increasingByZ());
  }

  return StatusCode::SUCCESS;
}

/**
 * @brief Populates the reference vectors of the track nodes.
 */
void TrackVectorFitter::populateRefVectors (Tr::TrackVectorFit::Track& t) const {
  // Set the reference using a TrackTraj
  LHCb::TrackTraj::StateContainer states;
  states.insert(states.end(), t.track().states().begin(), t.track().states().end());
  LHCb::TrackTraj tracktraj(states);

  // Populate the ref vectors inside the Tr::TrackVectorFit::Node
  std::for_each(t.nodes().begin(), t.nodes().end(), [&] (Tr::TrackVectorFit::Node& n) {
    n.setRefVector(tracktraj.stateVector(n.node().z()).parameters());
  });
}

/**
 * @brief Projects references, and optionally updates material corrections and transport.
 */
void TrackVectorFitter::projectAndUpdate (
  Tr::TrackVectorFit::Track& t,
  const LHCb::Tr::PID& pid,
  const bool& doUpdateMaterialCorrections
) const {
  // update the projections. need to be done every time ref is updated
  projectReference(t);
  // return failureInfo("Problem projecting reference");

  // add all the noise, if required
  if (doUpdateMaterialCorrections && m_applyMaterialCorrections) {
    updateMaterialCorrections(t, pid);
  }
}

/**
 * @brief Initializes the reference states of the nodes.
 * @details Given existing states on the track, this tool adds states at fixed
 *          z-positions along the track. If a track state already exists
 *          sufficiently close to the desired state, it will not add the state.
 */
StatusCode TrackVectorFitter::initializeRefStates (LHCb::Track& track, const LHCb::Tr::PID& pid) const
{
  if (msgLevel(MSG::DEBUG)) {
    debug() << "TrackVectorFitter::initializeRefStates" << endmsg;
  }

  StatusCode sc = StatusCode::SUCCESS;

  // first fix the momentum of states on the track. need to make sure this works for Velo-TT as well.
  if (track.states().empty()) {
    sc = Error("Track has no state! Can not fit.", StatusCode::FAILURE);
  } else {
    // first need to make sure all states already on track have
    // reasonable momentum. still needs to check that this works for
    // velo-TT
    const LHCb::State* stateAtT = track.stateAt(LHCb::State::AtT);
    const LHCb::State& refstate = stateAtT ? *stateAtT :
      *(track.checkFlag(LHCb::Track::Flags::Backward) ? track.states().front() : track.states().back());
    for (auto* state : track.states()) {
      const_cast<LHCb::State*>(state)->setQOverP( refstate.qOverP() );
    }

    // collect the z-positions where we want the states
    boost::container::static_vector<double, 4> zpositions;
    if (track.hasT()) {
      zpositions.push_back(StateParameters::ZBegT);
      zpositions.push_back(StateParameters::ZEndT);
    }
    if (track.hasTT() || (track.hasT() && track.hasVelo())) {
      zpositions.push_back(StateParameters::ZEndTT);
    }
    if (track.hasVelo()) {
      zpositions.push_back(StateParameters::ZEndVelo);
    }

    // the following container is going to hold pairs of 'desired'
    // z-positionds and actual states. the reason for the gymnastics
    // is that we always want to propagate from the closest availlable
    // state, but then recursively. this will make the parabolic
    // approximation reasonably accurate.
    typedef std::pair<double, const LHCb::State*> ZPosWithState;
    std::vector<ZPosWithState> states;
    states.reserve(track.states().size());

    // we first add the states we already have
    std::transform(track.states().begin(), track.states().end(),
                   std::back_inserter(states),
                   [] (const LHCb::State* s) { return std::make_pair(s->z(),s); });

    // now add the other z-positions, provided nothing close exists
    const double maxDistance = 50*Gaudi::Units::cm;
    for (const auto& z : zpositions) {
      const bool not_found = std::none_of(states.begin(), states.end(), [&] (const ZPosWithState& s) {
          return std::abs(z - s.first) < maxDistance;
      });
      if (not_found) {
        states.emplace_back(z, nullptr);
      }
    }

    std::sort(states.begin(), states.end(), [] (decltype(states[0])& s0, decltype(states[0])& s1) {
      return s0.first < s1.first;
    });

    // create the states in between
    const ITrackExtrapolator* extrap = extrapolator(track.type());
    LHCb::Track::StateContainer newstates;
    for (auto it = states.begin(); it != states.end() && sc.isSuccess(); ++it) {
      if (it->second) continue;

      // find the nearest existing state to it
      auto best= states.end();
      for (auto jt = states.begin(); jt != states.end(); ++jt) {
        if (it != jt
          && jt->second
          && (best==states.end() || std::abs(jt->first - it->first) < std::abs(best->first - it->first))) {
          best = jt;
        }
      }

      assert(best != states.end());

      // move from that state to this iterator, using the extrapolator and filling all states in between.
      int direction = best > it ? -1 : +1;
      LHCb::StateVector statevec (best->second->stateVector(), best->second->z());
      for( auto jt = best+direction; jt != it+direction && sc.isSuccess(); jt += direction) {
        sc = extrap->propagate(statevec, jt->first, 0, pid);
        if (sc.isFailure()) {
          Warning("initializeRefStates() fails in propagating state",StatusCode::FAILURE).ignore();
          if (msgLevel(MSG::DEBUG)) {
            debug() << "Problem propagating state: " << statevec << " to z= " << jt->first << endmsg;
          }
        } else {
          newstates.push_back(new LHCb::State(statevec));
          jt->second = newstates.back();
        }
      }

    }

    // Finally, copy the new states to the track.
    if (sc.isSuccess()) {
      track.addToStates(newstates);
    } else {
      // TODO - sigh...
      for (LHCb::State* state : newstates) {
        delete state;
      }
    }
  }
  return sc;
}

/**
 * @brief Updates the measurements before a refit.
 * @details Projects reference projects a reference parameter of a node (smoothed state).
 */
void TrackVectorFitter::projectReference (Tr::TrackVectorFit::Track& t) const
{
  for (auto& n : t.nodes()) {
    auto& node = n.node();
    // get the projector object, for the kind of measurement we have
    // ITrackProjectorSelector m_projectorSelector
    ITrackProjector* projector = m_projectorSelector->projector(node.measurement());

    // if the pointer is empty, we couldn't get the projector,
    // and issue an error
    if (!projector) {
      // sc = Warning("Could not get projector for measurement", StatusCode::FAILURE, 0);
      t.track().setFitStatus(LHCb::Track::FitStatus::FitFailed);
      if (msgLevel(MSG::DEBUG)) {
        debug() << "could not get projector for measurement" << endmsg;
      }
      break;
    }

    if (projector->projectReference(n).isFailure()) {
      // Warning("Unable to project statevector", sc, 0).ignore();
      t.track().setFitStatus(LHCb::Track::FitStatus::FitFailed);
      if (msgLevel(MSG::DEBUG)) {
        debug() << "Unable to project this statevector: " << n.get<Tr::TrackVectorFit::Op::NodeParameters, Tr::TrackVectorFit::Op::ReferenceVector>() << endmsg;
      }
      break;
    }
  }
}

void TrackVectorFitter::updateMaterialCorrections (
  Tr::TrackVectorFit::Track& t,
  const LHCb::Tr::PID& pid
) const {
  if (msgLevel(MSG::DEBUG)) {
    debug() << "TrackVectorFitter::updateMaterialCorrections" << endmsg;
  }

  // the noise in each node is the noise in the propagation between
  // the previous node and this node.

  // first collect all volumes on the track. The advantages of collecting them all at once
  // is that it is much faster. (Less call to TransportSvc.)
  auto& track = t.track();
  auto& nodes = t.nodes();

  if (nodes.size() > 1) {
    // only apply energyloss correction for tracks that traverse magnet
    const bool applyenergyloss = m_applyEnergyLossCorrections && track.hasT() && (track.hasVelo() || track.hasTT());

    // if only velo, or magnet off, use a fixed momentum based on pt.
    auto scatteringMomentum = track.firstState().p();
    if (m_scatteringPt>0 && !track.hasT() && !track.hasTT()) {
      const auto tx     = track.firstState().tx();
      const auto ty     = track.firstState().ty();
      const auto slope2 = tx*tx + ty*ty;
      const auto tanth  = std::max(std::sqrt(slope2 / (1+slope2)), 1e-4);
      scatteringMomentum = m_scatteringPt / tanth;
    }

    // set some limits for the momentum used for scattering
    scatteringMomentum = std::min(scatteringMomentum, m_maxMomentumForScattering.value());
    scatteringMomentum = std::max(scatteringMomentum, m_minMomentumForScattering.value());

    // if m_scatteringP is set, use it
    if (m_scatteringP > 0) {
      scatteringMomentum = m_scatteringP;
    }

    track.fitResult()->setPScatter(scatteringMomentum);

    if (msgLevel(MSG::DEBUG)) {
      debug() << "scattering momentum: " << scatteringMomentum << endmsg;
    }

    // this is farily tricky now: we want to use TracjTraj, but we
    // cannot create it directly from the nodes, because that would
    // trigger the filter!

    // TODO - This is not the case anymore. Room for optimization?

    // note that this is the same traj as used in setting the first
    // ref. cannot we just save it somewhere?
    LHCb::TrackTraj::StateContainer states;
    states.insert( states.end(), track.states().begin(), track.states().end() );
    LHCb::TrackTraj tracktraj(states);
    auto zmin = nodes.front().node().z();
    auto zmax = nodes.back().node().z();
    if (zmin > zmax) {
      std::swap(zmin, zmax);
    }
    tracktraj.setRange(zmin, zmax);

    IMaterialLocator::Intersections intersections;
    // make sure we have the space we need in intersections so we don't need to
    // reallocate (offline, I've seen tracks with more than 670 intersections
    // in 100 events; we stay a bit above that to be on the safe side - and we
    // don't mind the occasional reallocate if it's a rare track that has even
    // more intersections)
    intersections.reserve(1024);
    m_materialLocator->intersect(tracktraj, intersections);

    // now we need to redistribute the result between the nodes. the first node cannot have any noise.
    auto zorigin = nodes.front().node().z();
    for (auto inode = (nodes.begin() + 1); inode!=nodes.end(); ++inode) {
      auto& n = *inode;

      auto ztarget = n.node().z();
      LHCb::State state (
        (Gaudi::TrackVector) n.get<Tr::TrackVectorFit::Op::NodeParameters, Tr::TrackVectorFit::Op::ReferenceVector>(),
        Gaudi::TrackSymMatrix(),
        n.node().z(),
        n.node().state().location()
      );
      state.setQOverP(1.0 / scatteringMomentum);

      m_materialLocator->applyMaterialCorrections(state, intersections, zorigin, pid, true, applyenergyloss);
      auto deltaE = 1.0 / state.qOverP() - scatteringMomentum;

      n.setNoiseMatrix(state.covariance());
      n.setDeltaEnergy(deltaE);

      zorigin = ztarget;
    }
  }
}

/**
 * @brief      Sets the propagation between the previous node and this one.
 *
 * @param      t     { parameter_description }
 */
void TrackVectorFitter::updateTransport (
  std::list<Tr::TrackVectorFit::Sch::Blueprint<Tr::TrackVectorFit::vector_width()>>& scheduler
) const {
  Tr::TrackVectorFit::Mem::Iterator forwardTransportIterator (Tr::TrackVectorFitter::fitter.m_transportForwardStore);
  Tr::TrackVectorFit::Mem::Iterator backwardTransportIterator (Tr::TrackVectorFitter::fitter.m_transportBackwardStore);
  Tr::TrackVectorFit::Mem::Iterator forwardNodeIterator (Tr::TrackVectorFitter::fitter.m_forwardNodeIterator);
  TRACKVECTORFIT_PRECISION* currentNodeParametersPointer = nullptr;
  TRACKVECTORFIT_PRECISION* currentBackwardTransportPointer = nullptr;

  for (auto& s : scheduler) {
    const auto& in = s.in;
    const auto& action = s.action;
    auto& pool = s.pool;

    // Fetch required pointers
    auto* previousNodeParametersPointer = currentNodeParametersPointer;
    auto* previousBackwardTransportPointer = currentBackwardTransportPointer;
    auto* forwardTransportMatrixPointer = forwardTransportIterator.nextVector();
    currentNodeParametersPointer = forwardNodeIterator.nextVector();
    currentBackwardTransportPointer = backwardTransportIterator.nextVector();

    const bool attempt_parallel = action.all() && !(in.all());
    bool is_parallel_velo = attempt_parallel;
    bool is_parallel_short = attempt_parallel;

    if (attempt_parallel) {
      // Check if we have a parallel velo or parallel parabolic fit to do
      for (unsigned i=0; i<Tr::TrackVectorFit::vector_width(); ++i) {
        if (!in[i]) {
          const auto& type = ((pool[i].track)->track()).type();
          const auto& prevnode = *(pool[i].node - 1);
          const auto& n  = *(pool[i].node);
          const auto& refZ = prevnode.node().z();
          const auto& z = n.node().z();
          const auto distance = std::abs(z - refZ);

          is_parallel_velo &= (type == LHCb::Track::Velo || type == LHCb::Track::VeloR);
          is_parallel_short &= !m_rungeKuttaExtrapolatorEnabled || (distance < 100.0*Gaudi::Units::mm);
        }
      }
    }

    if (is_parallel_velo) {
      if (msgLevel(MSG::VERBOSE)) {
        verbose() << "Parallel VELO updateTransport: " << pool << endmsg;
        for (unsigned i=0; i<Tr::TrackVectorFit::vector_width(); ++i) {
          verbose() << *(pool[i].node) << endmsg;
        }
      }

      // Create views
      Tr::TrackVectorFit::Mem::View::NodeParameters currentNodeParameters (currentNodeParametersPointer);
      Tr::TrackVectorFit::Mem::View::NodeParameters previousNodeParameters (previousNodeParametersPointer);

      Tr::TrackVectorFit::Vector::Extrapolator<Tr::TrackVectorFit::vector_width()>::velo(
        pool,
        in,
        m_minMomentumForELossCorr.value(),
        previousNodeParameters.m_referenceVector.m_basePointer,
        forwardTransportMatrixPointer,
        currentNodeParameters.m_transportVector.m_basePointer,
        previousBackwardTransportPointer
      );
      
      if (msgLevel(MSG::VERBOSE)) {
        verbose() << "Calculated nodes:" << endmsg;
        for (unsigned i=0; i<Tr::TrackVectorFit::vector_width(); ++i) {
          verbose() << *(pool[i].node) << endmsg;
        }
      }
    } else if (is_parallel_short) {
      if (msgLevel(MSG::VERBOSE)) {
        verbose() << "Parallel parabolic updateTransport: " << pool << endmsg;
        for (unsigned i=0; i<Tr::TrackVectorFit::vector_width(); ++i) {
          verbose() << *(pool[i].node) << endmsg;
        }
      }
      
      // Create views
      Tr::TrackVectorFit::Mem::View::NodeParameters currentNodeParameters (currentNodeParametersPointer);
      Tr::TrackVectorFit::Mem::View::NodeParameters previousNodeParameters (previousNodeParametersPointer);

      Tr::TrackVectorFit::Vector::Extrapolator<Tr::TrackVectorFit::vector_width()>::parabolic(
        pool,
        in,
        m_minMomentumForELossCorr.value(),
        m_magneticFieldGrid,
        m_extrapolator->usesGridInterpolation(),
        previousNodeParameters.m_referenceVector.m_basePointer,
        forwardTransportMatrixPointer,
        currentNodeParameters.m_transportVector.m_basePointer,
        previousBackwardTransportPointer
      );
      
      if (msgLevel(MSG::VERBOSE)) {
        verbose() << "Calculated nodes:" << endmsg;
        for (unsigned i=0; i<Tr::TrackVectorFit::vector_width(); ++i) {
          verbose() << *(pool[i].node) << endmsg;
        }
      }
    } else {
      // Scalar execution
      for (unsigned i=0; i<Tr::TrackVectorFit::vector_width(); ++i) {
        if (!in[i] && action[i]) {
          auto& track = (pool[i].track)->track();
          auto& prevnode = *(pool[i].node - 1);
          auto& n  = *(pool[i].node);

          const Gaudi::TrackVector refVector = (Gaudi::TrackVector) prevnode.get<Tr::TrackVectorFit::Op::NodeParameters, Tr::TrackVectorFit::Op::ReferenceVector>();
          const auto& refZ = prevnode.node().z();

          const ITrackExtrapolator* extrap = extrapolator(track.type());
          Gaudi::TrackMatrix tm = ROOT::Math::SMatrixIdentity();

          const auto& z = n.node().z();
          LHCb::StateVector stateVector {refVector, refZ};

          if (msgLevel(MSG::VERBOSE)) {
            verbose() << "Propagating node " << n << endmsg;
          }

          // This is the meat of it
          // https://gitlab.cern.ch/lhcb/Rec/blob/c16c198e90c549334b83fbdcd3cd4c036f5f7f05/Tr/TrackExtrapolators/src/TrackExtrapolator.cpp
          if (extrap->propagate(stateVector, z, &tm).isFailure()) {
            track.setFitStatus(LHCb::Track::FitStatus::FitFailed);
            if (msgLevel(MSG::DEBUG)) {
              debug() << "unable to propagate reference vector from z=" << refZ
                      << " to " << z
                      << "; track type = " << track.type()
                      << ": vec = " << refVector << endmsg;
            }
            break;
          }

          // correct for energy loss
          const auto& dE = n.deltaEnergy();
          if (std::abs(stateVector.qOverP()) > LHCb::Math::lowTolerance) {
            const auto charge = stateVector.qOverP() > 0 ? 1. :  -1.;
            const auto momnew = std::max(m_minMomentumForELossCorr.value(), std::abs(1/stateVector.qOverP()) + dE);
            if (std::abs(momnew) > m_minMomentumForELossCorr) {
              stateVector.setQOverP(charge/momnew);
            }
          }

          // calculate the 'transport vector' (need to replace that)
          const Gaudi::TrackVector tv = stateVector.parameters() - tm * refVector;
          n.setTransportVector(tv);
          n.setTransportMatrix(tm);
          prevnode.calculateAndSetInverseTransportMatrix(tm);

          if (msgLevel(MSG::VERBOSE)) {
            verbose() << "Calculated node " << n << endmsg;
          }
        }
      }
    }
  }
}

void TrackVectorFitter::setNTrackParameters (Tr::TrackVectorFit::Track& t) const {
  if (m_useSeedStateErrors) {
    // we need to do this until we can properly deal with the seed state
    // dcampora: what does this mean?
    t.setNTrackParameters(0);
  } else {
    auto& nodes = t.nodes();
    for (unsigned i=1; i<nodes.size(); ++i) {
      // m_basePointer[(col*(col+1)/2 + row) * vector_width()];
      // with row=2, col=4 => element 12
      const auto& tm = nodes[i].get<Tr::TrackVectorFit::Op::Forward, Tr::TrackVectorFit::Op::TransportMatrix>();
      if (tm.m_basePointer && std::abs(tm[12]) != 0) {
        t.setNTrackParameters(5);
        return;
      }
    }

    t.setNTrackParameters(4);
  }
}

/**
 * @brief      Removes the worse outlier from track t.
 * @details    Loop over the nodes and find the one with the
 *             highest chi2 > m_chi2Outliers, provided there is
 *             enough hits of this type left.
 */
bool TrackVectorFitter::removeWorstOutlier (Tr::TrackVectorFit::Track& t) const {
  // return false if outlier chi2 cut < 0
  if (m_chi2Outliers.value() < 0.0) {
    return false;
  }

  std::array<size_t, 5> numHits = {0,0,0,0,0};
  for (auto& n : t.nodes()) {
    if (n.node().type() == LHCb::Node::HitOnTrack) {
      assert(hitType(n.node().measurement().type()) < 5);
      ++numHits[hitType(n.node().measurement().type())];
    }
  }

  using NodeWithChi2 = std::pair<std::reference_wrapper<Tr::TrackVectorFit::Node>, double>;
  std::vector<NodeWithChi2> nodesWithChi2UL;
  nodesWithChi2UL.reserve(t.nodes().size());

  const TRACKVECTORFIT_PRECISION trackChi2 = t.chi2();

  if (msgLevel(MSG::DEBUG)){
    debug() << "removeWorstOutlier: total chi2 " << trackChi2 << endmsg;
  }

  for (auto it=t.nodes().begin(); it!=t.nodes().end(); ++it) {
    Tr::TrackVectorFit::Node& n = *it;
    if (n.node().hasMeasurement() && n.node().type() == LHCb::Node::HitOnTrack) {
      const unsigned type = hitType(n.node().measurement().type());
      assert(type < numHits.size());
      if (numHits[type] > m_minNumHits[type]) {
        // Find out the chi2 contribution of this node
        auto chi2Contribution = trackChi2;
        if (it != t.nodes().begin())   { chi2Contribution -= (it-1)->get<Tr::TrackVectorFit::Op::Forward, Tr::TrackVectorFit::Op::Chi2>(); }
        if ((it+1) != t.nodes().end()) { chi2Contribution -= (it+1)->get<Tr::TrackVectorFit::Op::Backward, Tr::TrackVectorFit::Op::Chi2>(); }

        if (msgLevel(MSG::DEBUG)){
          debug() << "node LHCbID " << n.node().measurement().lhcbID().channelID()
            << " chi2Contribution " << chi2Contribution << " (" << trackChi2 << " - "
            << ((it != t.nodes().begin()) ? (it-1)->get<Tr::TrackVectorFit::Op::Forward, Tr::TrackVectorFit::Op::Chi2>() : 0) << " - "
            << (((it+1) != t.nodes().end()) ? (it+1)->get<Tr::TrackVectorFit::Op::Backward, Tr::TrackVectorFit::Op::Chi2>() : 0) << ")" << endmsg;
        }

        if (chi2Contribution > m_chi2Outliers.value()) {
          nodesWithChi2UL.emplace_back(n, chi2Contribution);
        }
      }
    }
  }

  // Sort by decreasing chi2
  std::sort(nodesWithChi2UL.begin(), nodesWithChi2UL.end(),
    [] (const NodeWithChi2& n0, const NodeWithChi2& n1) {
    return n0.second > n1.second;
  });

  if (msgLevel(MSG::DEBUG)) {
    debug() << "All bad measurements, ordered: " << endmsg;
    for (const NodeWithChi2& node : nodesWithChi2UL) {
      Tr::TrackVectorFit::Node& n = node.first;
      debug() << "Measurement of type " << n.node().measurement().type()
              << " LHCbID " << n.node().measurement().lhcbID().channelID()
              << " at z " << n.node().z()
              << " with chi2Contribution " << node.second
              << " and chi2 " << n.smoothChi2()
              << endmsg;
    }
  }

  if (!nodesWithChi2UL.empty() && nodesWithChi2UL.front().second < m_chi2Outliers.value()) {
    return false;
  }

  auto worstChi2 = m_chi2Outliers.value();
  auto worstNodeIt = nodesWithChi2UL.end();
  for (auto it=nodesWithChi2UL.begin(); it!=nodesWithChi2UL.end(); ++it) {
    if (it->second > worstChi2) {
      Tr::TrackVectorFit::Node& n = it->first;
      const auto chi2 = n.smoothChi2();
      if (chi2 > worstChi2) {
        worstChi2 = chi2;
        worstNodeIt = it;
      }
    }
  }

  if (worstNodeIt == nodesWithChi2UL.end()) {
    if (msgLevel(MSG::DEBUG)) {
      debug() << "Outlier not found" << endmsg;
    }

    return false;
  }

  if (msgLevel(MSG::DEBUG)) {
    Tr::TrackVectorFit::Node& n = worstNodeIt->first;
    debug() << "Measurement of type " << n.node().measurement().type()
      << " LHCbID " << n.node().measurement().lhcbID().channelID()
      << " at z " << n.node().z()
      << " with chi2 " << worstChi2
      << " removed." << endmsg;
  }

  // Mark the node as an outlier
  worstNodeIt->first.get().setOutlier();

  return true;
}

bool TrackVectorFitter::removeWorstOutlierSimplified (Tr::TrackVectorFit::Track& t) const {
  // return false if outlier chi2 cut < 0
  if (m_chi2Outliers < 0.0) {
    return false;
  }

  std::array<size_t, 5> numHits = {0,0,0,0,0};
  for (auto& n : t.nodes()) {
    if (n.node().type() == LHCb::Node::HitOnTrack) {
      assert(hitType(n.node().measurement().type()) < 5);
      ++numHits[hitType(n.node().measurement().type())];
    }
  }

  // Perhaps the chi2 logic can be simplified
  std::vector<std::reference_wrapper<Tr::TrackVectorFit::Node>> badNodes;
  for (auto& n : t.nodes()) {
    if (n.node().hasMeasurement() && n.node().type() == LHCb::Node::HitOnTrack) {
      const unsigned type = hitType(n.node().measurement().type());
      assert(type < numHits.size());
      if (numHits[type] > m_minNumHits[type]) {
        // Add this node if its chi2 is over the threshold
        if (n.smoothChi2() > m_chi2Outliers) {
          badNodes.push_back(n);
        }
      }
    }
  }

  // If we didn't find any bad nodes, return false
  if (badNodes.empty()) {
    return false;
  }

  std::sort(badNodes.begin(), badNodes.end(), [] (const Tr::TrackVectorFit::Node& n0, const Tr::TrackVectorFit::Node& n1) {
    return n0.smoothChi2() > n1.smoothChi2();
  });

  ((Tr::TrackVectorFit::Node) badNodes[0]).setOutlier();

  return true;
}

/**
 * @brief Set the states of the track.
 */
void TrackVectorFitter::determineStates (Tr::TrackVectorFit::Track& t) const {
  auto& track = t.track();

  // Clean the non-fitted states in the track!
  track.clearStates();

  LHCb::TrackFitResult::NodeContainer& nodes = t.track().fitResult()->nodes();

  auto hasMeasurement = [] (const LHCb::Node* node) { return node->hasMeasurement(); };

  // Add the state at the first and last measurement position
  auto inode = std::find_if(nodes.cbegin(), nodes.cend(), hasMeasurement);
  auto* firstMeasurementNode = (inode!=nodes.cend()? *inode : nullptr);
  auto jnode = std::find_if(nodes.crbegin(), nodes.crend(), hasMeasurement);
  auto* lastMeasurementNode = (jnode!=nodes.crend()? *jnode : nullptr);

  bool upstream = nodes.front()->z() > nodes.back()->z();
  bool reversed =
    (upstream && !track.checkFlag(LHCb::Track::Flags::Backward)) ||
    (!upstream && track.checkFlag(LHCb::Track::Flags::Backward));

  // This state is not filtered for a forward only fit.
  if (m_addDefaultRefNodes) {
    LHCb::State firststate = firstMeasurementNode->state();
    firststate.setLocation(reversed ? LHCb::State::LastMeasurement : LHCb::State::FirstMeasurement);
    track.addToStates(firststate);
  }

  // This state is always filtered
  LHCb::State laststate = lastMeasurementNode->state();
  laststate.setLocation(reversed ? LHCb::State::FirstMeasurement : LHCb::State::LastMeasurement);
  track.addToStates(laststate);

  // Add the states at the reference positions
  for (const auto& node : nodes) {
    if (node->type() == LHCb::Node::Reference) {
      track.addToStates(node->state());
    }
  }

  if (msgLevel(MSG::DEBUG)) {
    debug() << "Track " << track.key() << " has " << track.nStates()
            << " states after fit\n  at z = ";

    for (const auto& s : track.states()) {
      debug() << s->z() << ", ";
    }

    debug() << track.fitResult()->nActiveMeasurements()
            << " measurements used for the fit (out of "
            << track.nLHCbIDs() << ")." << endmsg;
  }
}

/**
 * @brief Add info from fitter as extrainfo to track.
 */
void TrackVectorFitter::fillExtraInfo (Tr::TrackVectorFit::Track& t) const {
  // Note: There was the following TODO here.
  //       Rather, extend LHCb::Track with whatever we need.
  //
  // Old TODO: migrate clients to use KalmanFitResult directly. Then
  //           remove the extrainfo field.

  // Clean up the track info
  auto& track = t.track();
  track.eraseInfo(LHCb::Track::AdditionalInfo::FitVeloChi2);
  track.eraseInfo(LHCb::Track::AdditionalInfo::FitVeloNDoF);
  track.eraseInfo(LHCb::Track::AdditionalInfo::FitTChi2);
  track.eraseInfo(LHCb::Track::AdditionalInfo::FitTNDoF);
  track.eraseInfo(LHCb::Track::AdditionalInfo::FitMatchChi2);
  track.eraseInfo(LHCb::Track::AdditionalInfo::FitFracUsedOTTimes);

  if (track.hasT()) {
    track.addInfo(LHCb::Track::AdditionalInfo::FitTChi2, track.fitResult()->chi2Downstream().chi2());
    track.addInfo(LHCb::Track::AdditionalInfo::FitTNDoF, track.fitResult()->chi2Downstream().nDoF());
    const unsigned& nOTMeas = track.fitResult()->nMeasurements(LHCb::Measurement::OT);
    if (nOTMeas > 0) {
      track.addInfo(LHCb::Track::AdditionalInfo::FitFracUsedOTTimes, nActiveOTTimes(t) / ((float) nOTMeas));
    }
  }

  if (track.hasVelo()) {
    track.addInfo(LHCb::Track::AdditionalInfo::FitVeloChi2, track.fitResult()->chi2Velo().chi2());
    track.addInfo(LHCb::Track::AdditionalInfo::FitVeloNDoF, track.fitResult()->chi2Velo().nDoF());
    if (track.hasT()) {
      track.addInfo(LHCb::Track::AdditionalInfo::FitMatchChi2, track.fitResult()->chi2Match().chi2());
    }
  }
}
