#include "STDet/DeSTDetector.h"
#include "PatVeloTTHybridFit.h"
#include "TfKernel/RecoFuncs.h"
#include "TfKernel/STHit.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PatVeloTTHybridFit
//
// 2017-03-20 : Michel De Cian, michel.de.cian@cern.ch
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( PatVeloTTHybridFit )
//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PatVeloTTHybridFit::PatVeloTTHybridFit(const std::string& type,
                       const std::string& name,
                       const IInterface* parent)
  : base_class(type, name, parent)
{
  declareInterface<IPatVeloTTFit>(this);
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode PatVeloTTHybridFit::initialize(){
  StatusCode sc = GaudiTool::initialize(); // must be executed first
  if (sc.isFailure()) return sc;  // error printed already by GaudiTool

  m_PatTTMagnetTool = tool<PatTTMagnetTool>( "PatTTMagnetTool","PatTTMagnetTool");

  m_ttDet = getDet<DeSTDetector>(DeSTDetLocation::TT);

  return StatusCode::SUCCESS;
}
//=============================================================================
// Fit the track, starting with the LHCbIDs 
//=============================================================================
StatusCode PatVeloTTHybridFit::fitVTT( LHCb::Track& track ) const {

  //Save some variables
  const LHCb::State& state = *track.stateAt(LHCb::State::EndVelo) ;
  
  const float xVelo    = state.x();
  const float yVelo    = state.y();
  const float zVelo    = state.z();
  const float txVelo   = state.tx();
  const float tyVelo   = state.ty();
  const float zMidTT   = m_PatTTMagnetTool->zMidTT();
  const float yAtMidTT = yVelo + tyVelo*(zMidTT-zVelo);
  const float sigmaVeloSlope = 0.10*Gaudi::Units::mrad;
  const float invSigmaVeloSlope = 1.0/sigmaVeloSlope;
  const float zKink = 1780.0;
  
  //Save some variables
  const float yAt0 = yVelo + tyVelo*(0. - zVelo);
  const float xMid = xVelo + txVelo*(zKink-zVelo);
  float wb = sigmaVeloSlope*(zKink - zVelo);
  wb=1./(wb*wb);
  const float invKinkVeloDist = 1/(zKink-zVelo);

  const LHCb::STLiteCluster::FastContainer*  stLiteContainer =
    get<LHCb::STLiteCluster::FastContainer>(LHCb::STLiteClusterLocation::TTClusters);

  // compute an upper limit on the size of hits and sthits so we
  // can reserve enough space, and hence guarantee that pointers
  // to their elements do not get invalidated
  const auto& ids = track.lhcbIDs();
  const int nTTHits = std::count_if( ids.begin(), ids.end(), [](const LHCb::LHCbID& id) { return id.detectorType()==LHCb::LHCbID::TT; } );
  std::vector< PatTTHit > hits ; hits.reserve(nTTHits); 
  std::vector< PatTTHit* > pHits ; pHits.reserve(nTTHits); 
  std::vector< Tf::STHit > sthits ; sthits.reserve(nTTHits);
  
  // loop over LHCbIDs, extract TT-station measurements
  for( const auto& id : track.lhcbIDs() ) {
    if(  id.detectorType()!=LHCb::LHCbID::TT ) continue ;
    const LHCb::STChannelID stChan = id.stID() ;
    const DeSTSector* stSector = m_ttDet->findSector( stChan );
    if(!stSector) return Error( "No sector found for TT hit!" );

    auto iclus =  stLiteContainer->find< LHCb::STLiteCluster::findPolicy >( stChan ) ;
    if( iclus == stLiteContainer->end() ) {
      return Error( "Cannot find lite cluster!" );
    }
    
    sthits.emplace_back( *stSector, *iclus ) ;
    Tf::STHit* sthit = &sthits.back();
    hits.emplace_back(  *sthit );
    PatTTHit* patTTHit = &hits.back();
    updateTTHitForTrack(patTTHit,yAt0, tyVelo);
    pHits.push_back(patTTHit);
  }

  // -- Fit the hits
  // -- wb(0), zKink(1), zMidTT(2), xMid(3), invKinkVeloDist(4), invsigmaVeloSlope(5), xVelo(6), txVelo(7)
  const std::array<float,8> vars = { wb, zKink, zMidTT, xMid, invKinkVeloDist, invSigmaVeloSlope, xVelo, txVelo };
  // -- qop, chi2, xTT, txTT
  std::array<float,3> params = { 0.0, 0.0, 0.0 };
  finalFit( pHits, vars, params);
  
  const float zOrigin = zVelo-yVelo/tyVelo;
  const float bdl     = m_PatTTMagnetTool->bdlIntegral(tyVelo,zOrigin,zVelo);
  const float qpxz2p  =-1*vdt::fast_isqrt(1.+tyVelo*tyVelo)/bdl*3.3356/Gaudi::Units::GeV;

  const float qop     = params[0]*qpxz2p;
  const float xTT     = params[1];
  const float txTT    = params[2];
  
  // set q/p in all of the existing states
  const std::vector< LHCb::State * > states = track.states();
  for (auto& state : states) state->setQOverP(qop);

  //== Add a new state...
  LHCb::State temp;
  temp.setLocation( LHCb::State::AtTT );
  temp.setState( xTT,
                 yAtMidTT,
                 zMidTT,
                 txTT,
                 tyVelo,
                 qop );

  track.addToStates( std::move(temp) );

  return StatusCode::SUCCESS;
}

//=============================================================================
// -- Final fit, using overlap hits
//=============================================================================
void PatVeloTTHybridFit::finalFit( const std::vector<PatTTHit*>& theHits, 
                                   const std::array<float,8>& vars, 
                                   std::array<float,3>& params ) const{
  
  // -- vars are:
  // -- wb(0), zKink(1), zMidTT(2), xMid(3), invKinkVeloDist(4), invsigmaVeloSlope(5), xVelo(6), txVelo(7)
  const float zDiff = 0.001*(vars[1]-vars[2]);
  float mat[3] = { vars[0], vars[0]*zDiff, vars[0]*zDiff*zDiff };
  float rhs[2] = { vars[0]*vars[3], vars[0]*vars[3]*zDiff };
  
  // -- Scale the z-component, to not run into numerical problems
  // -- with floats
  for ( auto hit : theHits){
    
    const float ui = hit->x();
    const float ci = hit->hit()->cosT();
    const float dz = 0.001*(hit->z() - vars[2]);
    const float wi = hit->hit()->weight();
    
    mat[0] += wi * ci;
    mat[1] += wi * ci * dz;
    mat[2] += wi * ci * dz * dz;
    rhs[0] += wi * ui;
    rhs[1] += wi * ui * dz;
  }
  
  ROOT::Math::CholeskyDecomp<float, 2> decomp(mat);
  if( UNLIKELY(!decomp)) {
    return;
  } else {
    decomp.Solve(rhs);
  }
  
  const float xSlopeTTFit = 0.001*rhs[1];
  const float xTTFit = rhs[0];
  
  // new VELO slope x
  const float xb = xTTFit+xSlopeTTFit*(vars[1]-vars[2]);
  const float xSlopeVeloFit = (xb-vars[6])*vars[4];
  
  // calculate q/p
  const float sinInX  = xSlopeVeloFit*vdt::fast_isqrt(1.+xSlopeVeloFit*xSlopeVeloFit);
  const float sinOutX = xSlopeTTFit*vdt::fast_isqrt(1.+xSlopeTTFit*xSlopeTTFit);
  const float qp = (sinInX-sinOutX);
  
  params = { qp, xTTFit,xSlopeTTFit };
    
}
