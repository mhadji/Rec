CMAKE_MINIMUM_REQUIRED(VERSION 2.8.5)

#---------------------------------------------------------------
# Load macros and functions for Gaudi-based projects
find_package(GaudiProject)
#---------------------------------------------------------------

# Declare project name and version
gaudi_project(Rec v22r1
              USE Lbcom v21r1
              DATA TCK/HltTCK
                   ChargedProtoANNPIDParam VERSION v1r*
                   TMVAWeights)
