// Include files

// local
#include "RecSummaryAlg.h"

//-----------------------------------------------------------------------------
// Implementation file for class : RecSummaryAlg
//
// 2011-01-19 : Chris Jones
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( RecSummaryAlg )

//=============================================================================
// Initialize
//=============================================================================
StatusCode RecSummaryAlg::initialize()
{
  const StatusCode sc = GaudiAlgorithm::initialize();
  if ( sc.isFailure() ) return sc;

  std::vector<std::string> tmpDets;
  tmpDets.reserve(m_dets.size());
  std::sort(m_dets.begin(),m_dets.end());
  std::sort(m_knownDets.begin(),m_knownDets.end());
  std::set_intersection( m_dets.begin(), m_dets.end(),
                         m_knownDets.begin(), m_knownDets.end(),
                         std::back_inserter(tmpDets) );
  m_dets = std::move(tmpDets);

  return sc;
}


//=============================================================================
// Main execution
//=============================================================================
StatusCode RecSummaryAlg::execute()
{

  // Create a new summary object and save to the TES
  auto * summary = new LHCb::RecSummary();
  put( summary, m_summaryLoc );

  // can we add tracking info ?
  const auto * tracks = getIfExists<LHCb::Tracks>(m_trackLoc);
  if ( tracks && !m_split )
  {

    // Save total number of tracks
    summary->addInfo( LHCb::RecSummary::nTracks, tracks->size() );

    // Count each track type
    int nLong(0), nDownstream(0), nUpstream(0), nT(0), nBack(0);
    for ( const auto * tk : *tracks )
    {
      if      ( tk->type() == LHCb::Track::Types::Long       ) { ++nLong; }
      else if ( tk->type() == LHCb::Track::Types::Downstream ) { ++nDownstream; }
      else if ( tk->type() == LHCb::Track::Types::Upstream   ) { ++nUpstream; }
      else if ( tk->type() == LHCb::Track::Types::Ttrack     ) { ++nT; }
      else if ( tk->type() == LHCb::Track::Types::Velo &&
                tk->checkFlag(LHCb::Track::Flags::Backward)  ) { ++nBack; }
    }
    const int nVelo = countVeloTracks()->nObj(tracks);

    // Save track info by type to summary
    summary->addInfo( LHCb::RecSummary::nLongTracks,       nLong );
    summary->addInfo( LHCb::RecSummary::nDownstreamTracks, nDownstream );
    summary->addInfo( LHCb::RecSummary::nUpstreamTracks,   nUpstream );
    summary->addInfo( LHCb::RecSummary::nVeloTracks,       nVelo );
    summary->addInfo( LHCb::RecSummary::nBackTracks,       nBack );
    summary->addInfo( LHCb::RecSummary::nTTracks,          nT );

    // Save total number of tracks
    summary->addInfo( LHCb::RecSummary::nTracks, tracks->size() );

    // -- Read in RecSummary filled in TrackBestTrackCreator and add it to the other track quantities
    auto * rTS = getIfExists<LHCb::RecSummary>(evtSvc(),LHCb::RecSummaryLocation::Track);
    if( rTS ){
      summary->addInfo( LHCb::RecSummary::nGhosts, rTS->info(LHCb::RecSummary::nGhosts,-1) );
    }


  }
  else if( m_split )
  {
    // Count each track type
    // can we add long tracking info ?
    const LHCb::Tracks * tracks_long = getIfExists<LHCb::Tracks>(m_trackLongLoc);
    const int nLong =
      ( !tracks_long ? 0 :
        std::count_if( tracks_long->begin(), tracks_long->end(),
                       []( const auto * tk )
      { return tk->type() == LHCb::Track::Types::Long; } ) );
    // can we add downstream tracking info ?
    const LHCb::Tracks * tracks_down = getIfExists<LHCb::Tracks>(m_trackDownLoc);
    const int nDownstream =
      ( !tracks_down ? 0 :
        std::count_if( tracks_down->begin(), tracks_down->end(),
                       []( const auto * tk )
      { return tk->type() == LHCb::Track::Types::Downstream; } ) );
    // Save track info by type to summary
    summary->addInfo( LHCb::RecSummary::nLongTracks,       nLong );
    summary->addInfo( LHCb::RecSummary::nDownstreamTracks, nDownstream );
    // Save total number of tracks
    summary->addInfo( LHCb::RecSummary::nTracks, nLong + nDownstream );
  }
  else
  {
    Warning( "No tracks available at '"+m_trackLoc+"'" ).ignore();
  }

  // PVs
  addSizeSummary<LHCb::RecVertices>( summary, LHCb::RecSummary::nPVs, m_pvLoc );

  // Loop over the sub-detectors
  for ( const auto & det : m_dets )
  {
    if      ( "RICH1" == det || "RICH1PMT" == det )
    {
      summary->addInfo( LHCb::RecSummary::nRich1Hits, nRichHits(Rich::Rich1) );
    }
    else if ( "RICH2" == det || "RICH2PMT" == det)
    {
      summary->addInfo( LHCb::RecSummary::nRich2Hits, nRichHits(Rich::Rich2) );
    }
    else if ( "VELO" == det )
    {
      addSizeSummary<LHCb::VeloLiteCluster::VeloLiteClusters, LHCb::VeloClusters>
        ( summary, LHCb::RecSummary::nVeloClusters, m_veloLoc );
    }
    else if ( "TT" == det )
    {
      addSizeSummary<LHCb::STLiteCluster::STLiteClusters, LHCb::STClusters>
        ( summary, LHCb::RecSummary::nTTClusters, m_ttLoc );
    }
    else if ( "IT" == det )
    {
      addSizeSummary<LHCb::STLiteCluster::STLiteClusters, LHCb::STClusters>
        ( summary, LHCb::RecSummary::nITClusters, m_itLoc );
    }
    else if ( "OT" == det )
    {
      summary->addInfo( LHCb::RecSummary::nOTClusters, otTool()->totalNumberOfHits() );
    }
    else if ( "SPD" == det )
    {
      addSizeSummary<LHCb::CaloDigits>( summary, LHCb::RecSummary::nSPDhits, m_spdLoc );
    }
    else if ( "MUON" == det )
    {
      // Muon Tracks
      addSizeSummary<LHCb::Tracks>( summary, LHCb::RecSummary::nMuonTracks, m_muonTracksLoc );

      // Muon Coords
      const LHCb::MuonCoords * coords = getIfExists<LHCb::MuonCoords>(m_muonCoordsLoc);
      if ( coords )
      {
        // Count by stations
        std::map<int,unsigned int> mCount;
        for ( const auto * C : *coords ) { ++mCount[C->key().station()]; }
        // Save to summary
        if ( mCount[0]>0 ) { summary->addInfo( LHCb::RecSummary::nMuonCoordsS0, mCount[0] ); }
        if ( mCount[1]>0 ) { summary->addInfo( LHCb::RecSummary::nMuonCoordsS1, mCount[1] ); }
        if ( mCount[2]>0 ) { summary->addInfo( LHCb::RecSummary::nMuonCoordsS2, mCount[2] ); }
        if ( mCount[3]>0 ) { summary->addInfo( LHCb::RecSummary::nMuonCoordsS3, mCount[3] ); }
        if ( mCount[4]>0 ) { summary->addInfo( LHCb::RecSummary::nMuonCoordsS4, mCount[4] ); }
      }
      else
      {
        Warning( "No MuonCoords available at '"+m_muonCoordsLoc+"'" ).ignore();
      }
    }
    //Upgrade detectors
    else if ( "VP" == det )
    {
      addSizeSummary<LHCb::VPClusters>( summary, LHCb::RecSummary::nVeloClusters, m_vpLoc );
    }
    else if ( "UT" == det )
    {
      addSizeSummary<LHCb::STClusters>( summary, LHCb::RecSummary::nUTClusters, m_utLoc );
    }
    else if ( "FT" == det )
    {
      typedef FastClusterContainer<LHCb::FTLiteCluster,int> FTLiteClusters;
      addSizeSummary<FTLiteClusters>( summary, LHCb::RecSummary::nFTClusters, m_ftLoc );
    }

    else
    {
      Warning( "Unknown detector '" + det + "'" ).ignore();
    }

  }

  if ( msgLevel(MSG::DEBUG) ) { debug() << *summary << endmsg; }

  return StatusCode::SUCCESS;
}

//=============================================================================
