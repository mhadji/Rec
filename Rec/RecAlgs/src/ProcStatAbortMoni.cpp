// local
#include "ProcStatAbortMoni.h"

//-----------------------------------------------------------------------------
// Implementation file for class : ProcStatAbortMoni
//
// 2010-07-16 : Chris Jones
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( ProcStatAbortMoni )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
ProcStatAbortMoni::ProcStatAbortMoni( const std::string& name,
                                      ISvcLocator* pSvcLocator )
  : Consumer ( name, pSvcLocator,
               { "ProcStatusLocation",  LHCb::ProcStatusLocation::Default } )
{}

//=============================================================================
// Initialization
//=============================================================================
StatusCode ProcStatAbortMoni::initialize()
{
  StatusCode sc = Consumer::initialize();
  if ( sc.isFailure() ) return sc;

  // book the histo
  m_h = bookProfile1D( "aborts", "Processing Abort Rates (%)",
                       -0.5, m_subSystems.size()-0.5,
                       m_subSystems.size() );
  // Set the bin labels
  const bool ok = Gaudi::Utils::Histos::setBinLabels( m_h, m_subSystems );

  // return
  return ok ? sc : StatusCode::FAILURE ;
}

//=============================================================================
// Main execution
//=============================================================================
void ProcStatAbortMoni::operator()(const LHCb::ProcStatus& proc) const
{
  // Loop over sub-systems and fill plot
  int index(0);
  for ( const auto& sys : m_subSystems ) {
    ++index;
    if ( sys == "Overall"  ) {
      m_h->fill( index, proc.aborted() ? 100.0 : 0.0 );
    } else {
      m_h->fill( index, proc.subSystemAbort(sys) ? 100.0 : 0.0 );
    }
  }
  // Debug printout if aborted
  if ( proc.aborted() && msgLevel(MSG::DEBUG) ) { debug() << proc << endmsg; }
}

//=============================================================================
