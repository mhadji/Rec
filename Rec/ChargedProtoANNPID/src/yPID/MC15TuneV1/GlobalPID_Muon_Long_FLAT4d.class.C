// Class: ReadElectron_Long_TMVA

#include "../../yPIDIClassifierReader.h"

#include <vector>
#include <cmath>
#include <string>
#include <iostream>
#include <memory>

// Include values transformations
#include "preprocessing/preprocessing.h"

// Include Keras model
#include "decision_train/dt_pid.h"


///////////////////////////////////// ReadMuon_Long_DNN ////////////////////////////////////////

class ReadMuon_Long_FLAT4d final : public yPIDIClassifierReader {

public:

  std::vector<std::string> inputVars;
  IronTransformer iron;
  std::unique_ptr<DT_PID> model = std::make_unique<DT_PID>();
  // List of vars
  std::vector<std::string> needed_vars = {
                "MuonNShared", "MuonIsLooseMuon", "MuonIsMuon", "MuonBkgLL", "MuonMuLL",
                "TrackFitVeloChi2", "TrackFitVeloNDoF", "TrackFitMatchChi2",
                "TrackGhostProbability", "TrackP", "TrackChi2PerDof", "TrackFitTChi2",
                "TrackPt", "TrackNumDof", "TrackFitTNDoF", "TrackDOCA", "InAccSpd",
                "InAccPrs", "InAccBrem", "InAccEcal", "InAccHcal", "InAccMuon",
                "CombDLLmu", "CombDLLpi", "CombDLLp", "CombDLLe", "CombDLLk",
                "RichAboveMuThres", "RichAboveElThres", "RichAbovePiThres",
                "RichAboveKaThres", "RichAbovePrThres", "RichUsedR1Gas", "RichUsedR2Gas",
                "RichDLLbt", "RichDLLpi", "RichDLLe", "RichDLLp", "RichDLLmu", "RichDLLk",
                "CaloBremMatch", "CaloElectronMatch", "CaloTrMatch", "CaloTrajectoryL",
                "CaloChargedSpd", "CaloChargedPrs", "CaloChargedEcal", "CaloNeutralSpd",
                "CaloNeutralPrs", "CaloNeutralEcal", "CaloSpdE", "CaloPrsE", "CaloEcalE",
                "CaloHcalE", "EcalPIDmu", "HcalPIDmu", "PrsPIDe", "BremPIDe", "EcalPIDe",
                "HcalPIDe"};

  // constructor
  ReadMuon_Long_FLAT4d( const std::vector<std::string>& theInputVars )
    : yPIDIClassifierReader()
  {
	std::vector<std::string> vars(theInputVars.begin(), theInputVars.end());
	inputVars = vars;

	// Get path to a root directory with models
	const std::string paramEnv = "CHARGEDPROTOANNPIDPARAMROOT";
	std::string paramRoot = std::string(getenv(paramEnv.c_str()));
	
	// Load Iron Transformer
	string filename = paramRoot + "/data/MC15TuneFLAT4dV1/iron_transform.txt";
	iron.read_tranform(filename);

	// Load DT model
	string model_path = paramRoot + "/data/MC15TuneFLAT4dV1/dt_pid_model.json";
	model->init_from_json_file(model_path);
  }


  double GetMvaValue( const std::vector<double>& inputValues ) const override;

private:

  // method-specific destructor
  //void //Clear();

  // input variable transformation

};



////////////////////////////////////////////////////////////////////////////////////////////////////

inline double ReadMuon_Long_FLAT4d::GetMvaValue( const std::vector<double>& inputValues ) const
{
	std::vector<double> transfromed_inputValues(inputValues.begin(), inputValues.end());
	std::vector<std::string> transfromed_inputVars(inputVars.begin(), inputVars.end());
	// std::vector<double> transfromed_inputValues = inputValues;
	// std::vector<std::string> transfromed_inputVars = inputVars;
	
	// Preprocessing
	preprocess(transfromed_inputValues, transfromed_inputVars, iron);
	transfromed_inputValues = select_features(transfromed_inputValues, transfromed_inputVars, needed_vars);
	
	// Get predictions
	std::vector<float> vals(transfromed_inputValues.begin(), transfromed_inputValues.end());
	float prediction = model->prdict_score_for_class_single_example(2, vals);
	// 0: "Ghost", 1: "Electron", 2: "Muon", 3: "Pion", 4: "Kaon", 5: "Proton"
	
	// Retunr prediction for a particle
	double retval = (double) prediction;
	
	//double retval = 0.2;
	return retval;
}
