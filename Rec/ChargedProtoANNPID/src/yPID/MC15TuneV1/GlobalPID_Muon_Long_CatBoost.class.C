// Class: ReadElectron_Long_TMVA

#include "../../yPIDIClassifierReader.h"

#include <vector>
#include <cmath>
#include <string>
#include <iostream>
#include <memory>

// Include values transformations
#include "preprocessing/preprocessing.h"

// Include Keras model
#include "cat_boost/lhcb_classifier.h"


///////////////////////////////////////////// DNN //////////////////////////////////////////////////

class ReadMuon_Long_CatBoost final : public yPIDIClassifierReader {

public:

  std::vector<std::string> inputVars;
  IronTransformer iron;
  std::unique_ptr<LHCbClassifier> model = std::make_unique<LHCbClassifier>();
  // List of vars
  std::vector<std::string> needed_vars = {
	   "MuonNShared", "MuonIsLooseMuon", "MuonIsMuon", "MuonBkgLL",
       "MuonMuLL", "TrackFitVeloChi2", "TrackFitVeloNDoF",
       "TrackFitMatchChi2", "TrackGhostProbability", "TrackP",
       "TrackChi2PerDof", "TrackFitTChi2", "TrackPt", "TrackNumDof",
       "TrackFitTNDoF", "TrackDOCA", "InAccSpd", "InAccPrs", "InAccBrem",
       "InAccEcal", "InAccHcal", "InAccMuon", "CombDLLmu", "CombDLLpi",
       "CombDLLp", "CombDLLe", "CombDLLk", "RichAboveMuThres",
       "RichAboveElThres", "RichAbovePiThres", "RichAboveKaThres",
       "RichAbovePrThres", "RichUsedR1Gas", "RichUsedR2Gas", "RichDLLbt",
       "RichDLLpi", "RichDLLe", "RichDLLp", "RichDLLmu", "RichDLLk",
       "CaloBremMatch", "CaloElectronMatch", "CaloTrMatch",
       "CaloTrajectoryL", "CaloChargedSpd", "CaloChargedPrs",
       "CaloChargedEcal", "CaloNeutralSpd", "CaloNeutralPrs",
       "CaloNeutralEcal", "CaloSpdE", "CaloPrsE", "CaloEcalE", "CaloHcalE",
       "EcalPIDmu", "HcalPIDmu", "PrsPIDe", "BremPIDe", "EcalPIDe",
       "HcalPIDe", "RichDLLpi_LL", "RichDLLe_LL", "RichDLLp_LL",
       "RichDLLmu_LL", "RichDLLk_LL", "CombDLLmu_LL", "CombDLLpi_LL",
       "CombDLLp_LL", "CombDLLe_LL", "CombDLLk_LL", "acc_cum_sum_3",
       "acc_cum_sum_5", "RichAboveSumPiKaElMuTHres",
       "RichAboveSumKaPrTHres", "RichUsedGas", "SpdCaloNeutralAcc",
       "SpdCaloChargedAcc", "SpdCaloChargedNeutral", "CaloSumSpdPrsE",
       "CaloSumPIDmu"};

  // constructor
  ReadMuon_Long_CatBoost( const std::vector<std::string>& theInputVars )
    : yPIDIClassifierReader()
  {
	std::vector<std::string> vars(theInputVars.begin(), theInputVars.end());
	inputVars = vars;
	  
	// Get path to a root directory with models
	const std::string paramEnv = "CHARGEDPROTOANNPIDPARAMROOT";
	std::string paramRoot = std::string(getenv(paramEnv.c_str()));
	  
	// Load Iron Transformer
	string filename = paramRoot + "/data/MC15TuneCatBoostV1/iron_transform.txt";
	iron.read_tranform(filename);
	
	// Load Keras DNN model
	std::string model_path = paramRoot + "/data/MC15TuneCatBoostV1/model_lhcb_muon";
	model->init_from_file(model_path);
  }


  double GetMvaValue( const std::vector<double>& inputValues ) const override;

private:

  // method-specific destructor
  //void //Clear();

  // input variable transformation

};



////////////////////////////////////////////////////////////////////////////////////////////////////

inline double ReadMuon_Long_CatBoost::GetMvaValue( const std::vector<double>& inputValues ) const
{
	std::vector<double> transfromed_inputValues(inputValues.begin(), inputValues.end());
	std::vector<std::string> transfromed_inputVars(inputVars.begin(), inputVars.end());
	// std::vector<double> transfromed_inputValues = inputValues;
	// std::vector<std::string> transfromed_inputVars = inputVars;
	
	// Preprocessing
	preprocess(transfromed_inputValues, transfromed_inputVars, iron);
	transfromed_inputValues = select_features(transfromed_inputValues, transfromed_inputVars, needed_vars);
	
	// Get predictions
	std::vector<float> vals(transfromed_inputValues.begin(), transfromed_inputValues.end());
	float prediction = model->predict_score_for_class_single_example(0, vals); 
	//0: "Ghost", 1: "Electron", 2: "Muon", 3: "Pion", 4: "Kaon", 5: "Proton"
	
	// Retunr prediction for a particle
	double retval = (double) prediction;
	//0: "Ghost", 1: "Electron", 2: "Muon", 3: "Pion", 4: "Kaon", 5: "Proton"
	
	//double retval = 0.2;
	return retval;
}
