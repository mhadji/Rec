#pragma once

#include "cern_model.fbs.h"

#include <array>
#include <vector>
#include <functional>
#include <iterator>
#include <fstream>
#include <string>
#include <exception>
#include <cmath>

class LHCbClassifier {
public:
    LHCbClassifier() {}

    explicit LHCbClassifier(const std::string& filename) {
        init_from_file(filename);
    }

    template <typename T>
    static inline T sigmoid(T val) {
        return 1 / (1 + exp(-val));
    }

    template <int N_CLASSES, typename T, typename ResultType = double>
    std::array<T, N_CLASSES> predict_scores_single_example(const std::vector<T>& example) const {
        assert(N_CLASSES == Trees[0].size());
        std::vector<char> binFeatures = BinarizeFeatures(example);
        std::array<T, N_CLASSES> result = {0};
        for (size_t treeId = 0; treeId < BinaryTrees.size(); ++treeId) {
            const auto& tree = BinaryTrees[treeId];
            int index = 0;
            for (size_t depth = 0; depth < tree.size(); ++depth) {
                index |= binFeatures[tree[depth]] << depth;
            }
            for (int resultId = 0; resultId < N_CLASSES; ++resultId) {
                result[resultId] += Trees[treeId][resultId][index];
            }
        }
        for (size_t i = 0; i < N_CLASSES; ++i) {
            result[i] = sigmoid(result[i]);
        }
        return result;
    }

    template <typename T, typename ResultType = double>
    ResultType predict_score_for_class_single_example(const unsigned int classId, const std::vector<T>& example) const {
        assert(classId < Trees[0].size());
        std::vector<char> binFeatures = BinarizeFeatures(example);
        ResultType result(0);
        for (size_t treeId = 0; treeId < BinaryTrees.size(); ++treeId) {
            const auto& tree = BinaryTrees[treeId];
            int index = 0;
            for (size_t depth = 0; depth < tree.size(); ++depth) {
                index |= binFeatures[tree[depth]] << depth;
            }
            result += Trees[treeId][classId][index];
        }
        return sigmoid(result);
    }
    void init_from_file(const std::string& filename) {
        std::vector<char> data;
        std::ifstream file(filename, std::ios::binary);
        data.clear();
        data.assign((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
        {
            flatbuffers::Verifier verifier((unsigned char*)&data[0], data.size());
            if (!NCatBoost::VerifyTCERNClassifierBuffer(verifier)) {
                throw std::runtime_error("failed to load model");
            }
        }
        auto flatbufMxNet = NCatBoost::GetTCERNClassifier(&data[0]);
        UsedBinaryFeaturesCount = 0;
        FloatFeatureCount = 0;
        for (const auto& ff : *flatbufMxNet->FloatFeatures()) {
            UsedFloatFeatures.emplace_back();
            UsedFloatFeatures.back().FeatureIndex = ff->Index();
            if (ff->Index() > FloatFeatureCount) {
                FloatFeatureCount = ff->Index();
            }
            UsedFloatFeatures.back().Borders.assign(ff->Borders()->begin(), ff->Borders()->end());
            UsedBinaryFeaturesCount += ff->Borders()->size();
        }
        FloatFeatureCount += 1; // convert index to count
        for (const auto& ftree : *flatbufMxNet->Trees()) {
            BinaryTrees.emplace_back();
            BinaryTrees.back().assign(ftree->Conditions()->begin(), ftree->Conditions()->end());
            const int leafCount = 1 << BinaryTrees.back().size();
            std::vector<std::vector<double>> treeVals;
            for (size_t i = 0; i < ftree->LeafValues()->size(); i += leafCount) {
                treeVals.emplace_back();
                treeVals.back().assign(ftree->LeafValues()->begin() + i, ftree->LeafValues()->begin() + i + leafCount);
            }
            Trees.emplace_back(std::move(treeVals));
        }
    }
private:
    template<typename T>
    inline std::vector<char> BinarizeFeatures(const std::vector<T>& features) const {
        if (features.size() > FloatFeatureCount) {
            // insert warning or assert here
        }
        std::vector<char> result(UsedBinaryFeaturesCount);
        size_t currendBinIndex = 0;
        for (const auto& floatFeature : UsedFloatFeatures) {
            const auto val = features[floatFeature.FeatureIndex];
            for (const auto& border : floatFeature.Borders) {
                result[currendBinIndex] =  val > border;
                ++currendBinIndex;
            }
        }
        return result;
    }
private:
    struct TFloatFeature {
        int FeatureIndex = -1;
        std::vector<float> Borders;
    };
    size_t UsedBinaryFeaturesCount = 0;
    size_t FloatFeatureCount = 0;
    std::vector<TFloatFeature> UsedFloatFeatures;
    std::vector<std::vector<int>> BinaryTrees;
    std::vector<std::vector<std::vector<double>>> Trees;
};