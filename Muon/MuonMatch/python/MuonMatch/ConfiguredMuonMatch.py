"""
 =============================================================
 Class to configure MuonMatch tools and algos. To import do
 'from MuonMatch import ConfiguredMuonMatch'
 =============================================================
"""

__author__ = 'Miguel Ramos Pernas'
__email__  = 'miguel.ramos.pernas@cern.ch'


from Gaudi.Configuration import *
from Configurables import ( GaudiSequencer,
                            MuonMatchVelo, MuonMatchVeloUT )
from GaudiKernel import SystemOfUnits as Units


class ConfiguredMuonMatchSequence:

    def __init__( self, ):
        """
        Initialization of the class
        """
        log.debug("# INITIALIZING")
        
    def configureMatchVelo( self, tool ):
        """
        Configure the MatchVelo tool
        """
        log.debug("# CONFIGURING MATCHVELO TOOL")

        tool.DoTiming = True
        
        tool.MinMomentum = 6*Units.MeV

    def configureMatchVeloUT( self, tool ):
        """
        Configure the MatchVeloUT tool
        """
        log.debug("# CONFIGURING MATCHVELOUT TOOL")

        tool.DoTiming = True

        tool.MinMomentum = 3*Units.MeV

    def getSeq( self ):
        """
        Return the sequences for each muon-matching tool
        """
        log.debug("# APPLYING GENERAL MUONMATCH CONFIGURATION")
        seq = GaudiSequencer("MuonMatchSeq")
        
        velomatch   = MuonMatchVelo()
        self.configureMatchVelo(velomatch)
        seq.Members.append(velomatch)

        veloutmatch = MuonMatchVeloUT()
        self.configureMatchVeloUT(veloutmatch)
        seq.Members.append(veloutmatch)
        
        return seq
