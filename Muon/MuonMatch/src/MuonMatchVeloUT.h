#ifndef MUONMATCH_VELOUT_H
#define MUONMATCH_VELOUT_H 1

// from MuonMatch
#include "MuonMatchBase.h"
#include "MuonMatch/Candidate.h"
#include "MuonMatch/Parsers.h"

class MuonMatchVeloUT;
namespace {
  typedef std::pair<double, double>
  (MuonMatchVeloUT::*getHit)( const CommonMuonHit&, bool) const;
}

/** @class MuonMatchVeloUT MuonMatchVeloUT.h
 *
 *  @author Miguel Ramos Pernas, Roel Aaij, Vasileios Syropoulos
 *  @date   2010-12-02
 *
 *  @author Miguel Ramos Pernas
 *  @date   2017-10-05
 */

class MuonMatchVeloUT : public MuonMatchBase {

public:

  enum { nStations = 5u, nRegions = 4u };
  
  /// Inherit constructor
  using MuonMatchBase::MuonMatchBase;

  virtual void addHits( Candidate& seed ) const override;
  
  virtual Candidates findSeeds( const LHCb::Track& seed, const unsigned int seedStation ) const override;

  virtual void fitCandidate( Candidate& seed ) const override;

  StatusCode initialize() override;

  void match( const LHCb::Track &seed, LHCb::Track::Vector& matches ) const override;

private:

  using DoubleProp = Gaudi::Property<double>;
  using UIntProp   = Gaudi::Property<unsigned int>;
  using BoolProp   = Gaudi::Property<bool>;
  
  // Properties for Velo seeds
  DoubleProp m_z0 {this, "MagnetPlaneParA", +5.412*Gaudi::Units::m};
  DoubleProp m_z1 {this, "MagnetPlaneParB", -3.567*Gaudi::Units::m};
  DoubleProp m_c0 {this, "MagnetCorParA",   +25.17*Gaudi::Units::mm};
  DoubleProp m_c1 {this, "MagnetCorParB",   -701.5*Gaudi::Units::mm};
  
  DoubleProp m_maxPt {this, "MaxPt", 1000*Gaudi::Units::MeV};

  Gaudi::Property<WindowMap> m_window {this, "Window", {
     
      {1u, {100*Gaudi::Units::mm, 200*Gaudi::Units::mm}},
	{2u, {200*Gaudi::Units::mm, 400*Gaudi::Units::mm}},
	  {3u, {300*Gaudi::Units::mm, 500*Gaudi::Units::mm}},
	    {4u, {400*Gaudi::Units::mm, 600*Gaudi::Units::mm}}
    }
  };

  // General properties
  DoubleProp m_maxChi2DoFX {this, "MaxChi2DoFX", 20};
  UIntProp m_maxMissed {this, "MaxMissedHits", 1};
  BoolProp m_setQOverP {this, "SeqQOverP", false};
  BoolProp m_fitY {this, "FitY", false};

  /** Corrections for errors should be of the form:
   *    {'x'  : [a, b, c, d], 'y'  : [e, f, g, h],
   *     'mx' : [c0, c1, c2], 'my' : [c0, c1, c2]}
   *  where:
   *   - Corrections on the error are applied by multiplying 2 * d{x,y} with the
   *     correction for M2-M5,
   *   - mx and my are coefficients of a polynomial in 1/p for the magnet hit,
   *     that gives the error on the hit in the magnet.
   *  x, y, mx and my must be present. x and y must contain 4 values, mx and my three.
   */
  Gaudi::Property<CorrectMap> m_errCorrect {this, "ErrorCorrections", {
   
      {"mx", {1., 0., 0.}},
	{"my", {1., 0., 0.}},
	  {"x", {1., 1., 1., 1.}},
	    {"y", {1., 1., 1., 1.}}
    }
  };

  /// Muon Detector
  DeMuonDetector* m_det = nullptr;

  /// Data from detector description and conditions
  doubles m_padSizeX;
  doubles m_padSizeY;
  
  /// Calculate the chi2 for na given candidate
  double calcChi2y( const Candidate& candidate ) const;

  /// The magnet hit is corrected when it is created, so don't shift or scale later.
  inline double correction( CorrectMap::key_type key, unsigned int idx ) const
  {
    return m_errCorrect.value().at(key).at(idx);
  }
  
  /** Calculate straight line fit using linear-least-squares
   *  Code inspired by numerical recipes, section 15.2
   */
  CandidateFitResult fit( const getHit hitFun,
			  const CommonMuonHit *magnetHit,
			  const SharedCommonConstMuonHits &hits ) const;

  /// Correction in "x"
  std::pair<double, double> hitx( const CommonMuonHit& hit, bool correct ) const {

    unsigned int i = (hit.tile().key() == 0 ? 0 : hit.station());

    double v = hit.x();

    double corr = 2*hit.dx()*(correct ? correction({"x"}, i) : 1.);

    return std::make_pair(v, corr);
  }

  /// Correction in "y"
  std::pair<double, double> hity( const CommonMuonHit& hit, bool correct ) const {

    unsigned int i = (hit.tile().key() == 0 ? 0 : hit.station());

    double v = hit.y();

    double corr = 2*hit.dy()*(correct ? correction({"y"}, i) : 1.);

    return std::make_pair(v, corr);
  }

  /// Make the residual, error and pull plots
  void plotResidual(const getHit hitFun,
		    const CommonMuonHit* hit,
		    const CommonMuonHit* magnetHit,
		    const CandidateFitResult& info,
		    std::string title) const;

};

#endif // MUONMATCH_VELOUT_H
