find_path(flatbuffers_INCLUDE_DIR NAMES flatbuffers/flatbuffers.h
          HINTS ${flatbuffers_home}/include/)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(flatbuffers DEFAULT_MSG flatbuffers_INCLUDE_DIR)

mark_as_advanced(flatbuffers_FOUND flatbuffers_INCLUDE_DIR)
set(flatbuffers_INCLUDE_DIRS ${flatbuffers_INCLUDE_DIR})
