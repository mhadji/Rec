#ifndef COMMONMUONSTATION_H_
#define COMMONMUONSTATION_H_

#include <vector>

#include "GaudiKernel/Range.h"
#include "MuonDet/DeMuonDetector.h"

#include "MuonID/CommonMuonHit.h"

/** @class CommonMuonStation CommonMuonStation.h
 *  Class to hold regions of hits and provide some geometry information
 *
 *  Used to be Hlt/Hlt1Muons/Hlt1MuonStation.
 *
 *  @author Roel Aaij
 *  @author Kevin Dungs
 *  @date   2015-01-03
 */
class CommonMuonStation final {
public:

  CommonMuonStation() = default;
  CommonMuonStation(DeMuonDetector* det, unsigned int station,
                    std::vector<double> regions,
                    CommonMuonHits hits);
  CommonMuonStation(DeMuonDetector* det, unsigned int station,
                    std::vector<double> regions);

  enum { nRegionsY = 7u };

  double z() const { return m_z; }

  unsigned int station() const { return m_station; }

  CommonMuonHitRange hits(double xmin, unsigned int region) const;
  CommonMuonHitRange hits(double xmin, double xmax, unsigned int region) const;

  CommonMuonHitRange hits(unsigned int region) const;

  unsigned int nRegions() const {
    return (m_xboundaries.size() - 1) * nRegionsY;
  }

  // TODO: generate range of id which overlap (xMin,xMax,yMin,yMax)...
  bool overlaps(unsigned id, double xmin, double xmax, double ymin_,
                double ymax) const {
    auto j = id % nRegionsY;
    auto i = id / nRegionsY;
    auto y = ymin() + j * dy();
    return !(xmax < m_xboundaries[i] || xmin > m_xboundaries[i + 1] ||
             ymin_ > y + dy() || ymax < y);
  }

private:
  friend class CommonMuonHitManager;

  void setHits(CommonMuonHits&& hts);


  inline double dy() const { return 2 * m_ymax / nRegionsY; }
  inline double ymin() const { return -m_ymax; }

  std::vector<double> m_xboundaries;
  CommonMuonHits m_hits;

  // NOTE: Iterators are not invalidated after move, although it has
  // been proposed, there is no explicit requirement from the
  // standard. Default move constructors "should" therefore work.
  // See: http://www.open-std.org/JTC1/SC22/WG21/docs/lwg-active.html#2321
  std::vector<typename CommonMuonHits::iterator> m_index;  // indices into m_hits

  double m_z = 0.;
  double m_ymax = 0.;
  unsigned int m_station = 0;
};
#endif  // COMMONMUONSTATION_H_
