#ifndef MUONHITHANDLER_H
#define MUONHITHANDLER_H

#include <array>
#include <vector>
#include <string>

#include "Event/MuonCoord.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonID/CommonMuonHit.h"
#include "MuonID/CommonMuonStation.h"

namespace MuonHitHandlerLocation {
    const std::string Default = "Muon/MuonHits";
}

class MuonHitHandler final {
public:

   MuonHitHandler(std::array<CommonMuonStation, 5> stations)
     : m_stations(std::move(stations)) {}

   //access hit
   CommonMuonHitRange hits(double xmin, unsigned int station,
                                   unsigned int region)const{
      return m_stations[station].hits(xmin, region);
   }
   CommonMuonHitRange hits(double xmin, double xmax,
                                   unsigned int station, unsigned int region)const{
      return m_stations[station].hits(xmin, xmax, region);
   }

   unsigned int nRegions(unsigned int station) const{
      return m_stations[station].nRegions();
   };

   const CommonMuonStation& station(unsigned int id) const{
      return m_stations[id];
   };

private:

   //data object stored in TES and to get from algorithms using muon coords
   std::array<CommonMuonStation, 5> m_stations;

};


#endif // MUONHITHANDLER_H
