#ifndef ICOMMONMUONTOOL_H_
#define ICOMMONMUONTOOL_H_

#include "GaudiKernel/IAlgTool.h"
#include "MuonID/CommonMuonHit.h"
#include "MuonID/MuonHitHandler.h"


struct ICommonMuonTool : extend_interfaces<IAlgTool> {
  DeclareInterfaceID(ICommonMuonTool, 2, 0);

  using MuonTrackExtrapolation = std::vector<std::pair<double, double>>;
  using MuonTrackOccupancies = std::vector<unsigned>;

  virtual MuonTrackExtrapolation extrapolateTrack(const LHCb::Track&) const  = 0;
  virtual bool preSelection(const LHCb::Track&) const noexcept = 0;
  virtual bool inAcceptance(const MuonTrackExtrapolation&) const noexcept = 0;
  virtual std::tuple<CommonConstMuonHits, MuonTrackOccupancies> hitsAndOccupancies(
    const LHCb::Track&, const MuonTrackExtrapolation&, const MuonHitHandler& hitHandler) const = 0;
  virtual std::tuple<CommonConstMuonHits, MuonTrackOccupancies>extractCrossed(
    const CommonConstMuonHits&) const noexcept  = 0;
  virtual bool isMuon(const MuonTrackOccupancies&, double) const noexcept  = 0;
  virtual bool isMuonLoose(const MuonTrackOccupancies&, double) const noexcept = 0;
  virtual std::pair<double, double>foi(int, int, double) const noexcept  = 0;
  virtual unsigned int getFirstUsedStation() const noexcept = 0;
};

#endif  // ICOMMONMUONTOOL_H_
