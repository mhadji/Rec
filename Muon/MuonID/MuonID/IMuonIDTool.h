#ifndef MUONID_IMUONIDTOOL_H
#define MUONID_IMUONIDTOOL_H 1

// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include "boost/utility/string_ref.hpp"



namespace LHCb {
  class MuonPID;
  class Track;
}


/** @class IMuonIDTool IMuonIDTool.h MuonID/IMuonIDTool.h
 *
 *  interface for generic MuonID tool
 *
 *  @author Giacomo Graziani
 *  @date   2015-11-10
 */
struct IMuonIDTool : extend_interfaces<IAlgTool> {

  DeclareInterfaceID( IMuonIDTool, 3, 0 );

  virtual StatusCode eventInitialize() { return StatusCode::SUCCESS;}

  virtual LHCb::MuonPID* getMuonID(const LHCb::Track* track) =0;

  enum class ID_d {  mediancs ,maxIso ,matchSigma ,iso ,time ,dtime };
  enum class ID_i {  match, clusize, matchedStations, matchedViews, MaxSharedHits };

  virtual double muonIDPropertyD(const LHCb::Track* , ID_d, int =-1) = 0;
  virtual int muonIDPropertyI(const LHCb::Track* , ID_i, int =-1) = 0;

  // backwards compatibile, but deprecated...
  [[deprecated("please replace the argument with enum eg. replace \"maxIso\" with IMuonIDTool::ID_d::maxIso")]]
  double muonIDPropertyD(const LHCb::Track* t, boost::string_ref prop, int s=-1) {
     if (prop == "mediancs") return muonIDPropertyD(t,ID_d::mediancs,s);
     if (prop == "maxIso") return muonIDPropertyD(t,ID_d::maxIso,s);
     if (prop == "matchSigma") return muonIDPropertyD( t, ID_d::matchSigma, s);
     if (prop == "iso") return muonIDPropertyD(t, ID_d::iso, s);
     if (prop == "time") return muonIDPropertyD(t, ID_d::time, s);
     if (prop == "dtime") return muonIDPropertyD(t, ID_d::dtime, s);
     return -9999.;
  }

  [[deprecated("please replace the argument with enum eg. replace \"match\" with IMuonIDTool::ID_i::match")]]
  int muonIDPropertyI(const LHCb::Track* t, boost::string_ref prop, int s=-1) {
     if (prop == "match") return muonIDPropertyI(t, ID_i::match, s);
     if (prop == "clusize") return muonIDPropertyI(t, ID_i::clusize, s);
     if (prop == "matchedStations") return muonIDPropertyI(t, ID_i::matchedStations, s);
     if (prop == "matchedViews")    return muonIDPropertyI(t, ID_i::matchedViews, s);
     if (prop == "MaxSharedHits")   return muonIDPropertyI(t, ID_i::MaxSharedHits, s);
     return -9999;
  };
};
#endif // MUONID_IMUONIDTOOL_H
