#ifndef INSHARED_H 
#define INSHARED_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include "Event/Track.h"



/** @class INShared INShared.h
 *  
 *
 *  @author Xabier Cid Vidal
 *  @date   2011-03-17
 */
struct INShared : extend_interfaces<IAlgTool> {

  // Return the interface ID
  DeclareInterfaceID(INShared, 2, 0 );
  virtual StatusCode getNShared(const LHCb::Track& muTrack, int& nshared)=0;
  virtual void fillNShared()=0;


};
#endif // INSHARED_H
