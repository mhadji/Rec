#ifndef MUONIDALGLITE_H
#define MUONIDALGLITE_H

#include <string>

#include "Event/MuonPID.h"
#include "Event/Track.h"

#include "GaudiAlg/Transformer.h"

#include "MuonID/ICommonMuonTool.h"
#include "DLLMuonTool.h"
#include "MakeMuonTool.h"
#include "MuonID/IMVATool.h"
#include "MuonID/MuonHitHandler.h"

/** @class MuonIDAlgLite MuonIDAlgLite.h
 * A lightweight version of MuonIDAlg that uses the CommonMuonTool.
 *
 * @author Kevin Dungs
 * @date 2015-01-06
 */
class MuonIDAlgLite final
   : public Gaudi::Functional::MultiTransformer<std::tuple<LHCb::MuonPIDs, LHCb::Tracks>
                                                (const LHCb::Tracks&, const MuonHitHandler&)> {
 public:
  /// Standard constructor
  MuonIDAlgLite(const std::string &name, ISvcLocator *pSvcLocator);

  /// initialization
  StatusCode initialize() override;
  std::tuple<LHCb::MuonPIDs,LHCb::Tracks> operator()(const LHCb::Tracks& tracks,
                                                     const MuonHitHandler& hitHandler) const override;

  /* Number of MUON stations to use. In Run II there are 5 stations, 4
  used in the software. In Run III there will be 4 stations. So in
  foreseeable future you want this = 4. Unfortunately, the 4 stations are 
  hardcoded into some algorithms so this should be a const.
  */
  static const unsigned int num_active_stations = 4;

 private:
  // Helper functions
  bool isGoodOfflineTrack(const LHCb::Track &) const;

  std::vector<CommonConstMuonHits> m_muonVector;
  LHCb::Track makeMuonTrack(const LHCb::MuonPID &) const;

  // TODO: This has to go
  //--------------------
  // flag to use DLL:
  //--------------------
  // 4 -- binned tanh(distance) with closest hit(Muon) + integral of Landau
  // fit(NonMuon).
  // 5 -- binned tanh(distance) with closest hit(Muon) + integral. New tunning
  // with runI data
  //-----------------------------
  Gaudi::Property<int> m_dllFlag {this, "DLL_flag", 4};

  // Members
  ICommonMuonTool *muonTool_ = nullptr;
  DLLMuonTool *DLLTool_ = nullptr;
  MakeMuonTool *makeMuonTool_ = nullptr;
  IMVATool *MVATool_ = nullptr;

  Gaudi::Property<bool> useTTrack_ {this, "useTTrack", false};
  Gaudi::Property<bool> runDLL_ {this, "runDLL", true};
  Gaudi::Property<bool> runMVA_ {this, "runMVA", true};

  Gaudi::Property<std::string> tesPathInputTracks_ 
    {this, "TracksLocation", ""};

  Gaudi::Property<std::string> tesPathOutputMuonPid_ 
    {this, "MuonIDLocation", LHCb::MuonPIDLocation::Default};

  Gaudi::Property<std::string> tesPathOutputMuonTracks_ 
    {this, "MuonTrackLocation", LHCb::TrackLocation::Muon};

  Gaudi::Property<std::vector<std::string>> tesPathsInputTracks_ 
    {this, "TracksLocations", {LHCb::TrackLocation::Default}};

};

#endif  // MUONIDALGLITE_H
