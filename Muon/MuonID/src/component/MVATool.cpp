/** Implementation of MVATool
 *
 * 2017-05-29: Ricardo Vazquez Gomez
 */
#include "MVATool.h"
#define SQRT3 1.7320508075688772 

DECLARE_COMPONENT(MVATool)

/*
 * Contructor
 * Also declares an interface to that the tool can be obtained in Gaudi via
 * tool<...>(...)
 */
MVATool::MVATool(const std::string& type, const std::string& name, const IInterface* parent)
    : base_class(type, name, parent){
  declareInterface<IMVATool>(this);
}

/*
 * Initialises the tool.
 */
auto MVATool::initialize() -> StatusCode {
  auto sc = GaudiTool::initialize();
  if (sc.isFailure()) {
    return sc;
  }

  muonTool_ = tool<ICommonMuonTool>("CommonMuonTool");
  // get first used station
  firstUsedStation = muonTool_->getFirstUsedStation();

  // Load station information from detector description
  m_det = getDet<DeMuonDetector>(DeMuonLocation::Default);
  nStations = m_det->stations();
  for (unsigned s = 0; s != nStations; ++s) {
    m_stationZ.push_back( m_det->getStationZ(s) );
  }

  // Input variable names for MVA with 20 variables
  std::vector<std::string> inputVars={"DTm2","DTm3","DTm4","DTm5","T1m2","T1m3","T1m4","T1m5",
                                      "Cross2","Cross3","Cross4","Cross5",
                                      "RXdm2_ms","RXdm3_ms","RXdm4_ms","RXdm5_ms",
                                      "RYdm2_ms","RYdm3_ms","RYdm4_ms","RYdm5_ms"};//,"bNShared"};
  if (m_runTMVA) {
      m_TMVA_BDT = std::make_unique<
	  ReadBDTD_mva_pion_300_5_25_05_2_masscut_withPreweight_NoNshared_NewMS>(inputVars);
  }
  if (m_runCatBoost) {
      try {
	  if (m_useFastCatBoost) {
	      m_FastCatBoostModel = \
		  std::make_unique<NCatboostStandalone::TOwningEvaluator>(m_CatBoostModelPath);
	  } else {
	      m_CatBoostModel = ModelCalcerWrapper(m_CatBoostModelPath);
	  }
      } catch (const std::runtime_error& e) {
	  error() << "Failed to load CatBoost model. Likely cases are " \
		  << "a malformed or missing file. Model file: " << m_CatBoostModelPath \
		  << std::endl;
	  error() << "Message from Catboost:"<< std::endl;
	  error() << e.what();
	  error() << endmsg;
	  return StatusCode::FAILURE;
      }
  }
  return sc;
}

MVATool::MVAResponses MVATool::calcBDT(
  const LHCb::Track& muTrack, const CommonConstMuonHits &hits) const noexcept {
  //double m_nshared = muTrack.info(307,-10); // 307 is the key allocated for the MuonNShared value. -10 is the value in case it does not exist.
  //const std::vector<LHCb::LHCbID>& ids = muTrack.lhcbIDs();
  //if(msgLevel(MSG::DEBUG)) debug() << "This track contains = " << ids.size() << " hits" << endmsg;
  std::vector<LHCb::LHCbID>::const_iterator idFromTrack;

  // linear extrapolation without using extrapolators
  const auto extrapolation = muonTool_->extrapolateTrack(muTrack); 

  // initialise values to the default values expected by the BDT
  std::vector<double> times, dts, cross, resX, resY, minDist, distSeedHit;

  for( unsigned int st = 0; st != nStations; ++st ){
    times.push_back(-10000.);
    dts.push_back(-10000.);
    cross.push_back(0.);
    resX.push_back(-10000.);
    resY.push_back(-10000.);
    minDist.push_back(1e10);
    distSeedHit.push_back(1e6);
  }

  // common factor for the multiple scattering error. Taken from formula.
  double commonFactor = 13.6/muTrack.p()/sqrt(6.);

  // Get closests hits to track
  std::vector<LHCb::LHCbID> closestHits(nStations);
  CommonConstMuonHits::iterator ih;
  for_each(std::begin(hits), std::end(hits), [&](const CommonMuonHit* ih) {
    const LHCb::LHCbID id = ih->tile();
    unsigned s = id.muonID().station();
    distSeedHit[s] = (ih->x() - extrapolation[s].first)*(ih->x() - extrapolation[s].first) +
                     (ih->y() - extrapolation[s].second)*(ih->y() - extrapolation[s].second);
    if(distSeedHit[s] < minDist[s]) {
      minDist[s] = distSeedHit[s];
      closestHits[s] = id;
    }
  });

//  for( const LHCb::LHCbID &idFromTrack: ids){
  for( unsigned int st = firstUsedStation; st != nStations; ++st ){
    LHCb::LHCbID idFromTrack = closestHits[st];
    for_each(begin(hits), end(hits), [&](const CommonMuonHit* ih) {
      const LHCb::LHCbID idFromHit = ih->tile();
      if (idFromHit == idFromTrack) {
        const unsigned int s = idFromTrack.muonID().station(); 
        times[s] = ih->time();
        dts[s] = ih->deltaTime();
        (ih->uncrossed()==0) ? cross[s] = 2. : cross[s] = ih->uncrossed();
        
        // compute the Error due to the RMS taking into account the whole traversed material. Common error for X and Y.
        double travDist = sqrt((m_stationZ[s]-m_stationZ[0])*(m_stationZ[s]-m_stationZ[0])+
                               (extrapolation[s].first-extrapolation[0].first)*(extrapolation[s].first-extrapolation[0].first)+
                               (extrapolation[s].second-extrapolation[0].second)*(extrapolation[s].second-extrapolation[0].second));
        double errMS = commonFactor*travDist*sqrt(travDist/17.58);
        if(std::abs(extrapolation[s].first-ih->x())!=2000){
          resX[s] = (extrapolation[s].first-ih->x())/sqrt(((ih->dx())/SQRT3)*((ih->dx())/SQRT3)+errMS*errMS); 
        }
        if(std::abs(extrapolation[s].second-ih->y())!=2000){
          resY[s] = (extrapolation[s].second-ih->y())/sqrt(((ih->dy())/SQRT3)*((ih->dy())/SQRT3)+errMS*errMS);
        }
      }
    });
  }

  /*
  std::vector<double> Input = {m_rxdm2,m_rxdm3,m_rxdm4,m_rxdm5,m_rydm2,m_rydm3,m_rydm4,m_rydm5,m_dtm2,m_dtm3,m_dtm4,m_dtm5,
                               m_t1m2,m_t1m3,m_t1m4,m_t1m5,m_crossm2,m_crossm3,m_crossm4,m_crossm5,
                               m_exdm2_ms,m_exdm3_ms,m_exdm4_ms,m_exdm5_ms,m_eydm2_ms,m_eydm3_ms,m_eydm4_ms,m_eydm5_ms,
                               m_rxdm2_ms,m_rxdm3_ms,m_rxdm4_ms,m_rxdm5_ms,m_rydm2_ms,m_rydm3_ms,m_rydm4_ms,m_rydm5_ms,m_nshared};
  */

  const std::vector<double> Input = {dts[0+firstUsedStation],dts[1+firstUsedStation],dts[2+firstUsedStation],dts[3+firstUsedStation],
                               times[0+firstUsedStation],times[1+firstUsedStation],times[2+firstUsedStation],times[3+firstUsedStation],
                               cross[0+firstUsedStation],cross[1+firstUsedStation],cross[2+firstUsedStation],cross[3+firstUsedStation],
                               resX[0+firstUsedStation],resX[1+firstUsedStation],resX[2+firstUsedStation],resX[3+firstUsedStation],
                               resY[0+firstUsedStation],resY[1+firstUsedStation],resY[2+firstUsedStation],resY[3+firstUsedStation]};//,m_nshared};

  if(msgLevel(MSG::DEBUG)) debug() << "Input vars to the BDT: " << dts[0+firstUsedStation] << ", " << dts[1+firstUsedStation] << ", " << dts[2+firstUsedStation] << ", " << dts[3+firstUsedStation] << ", " << times[0+firstUsedStation] << ", " << times[1+firstUsedStation] << ", " << times[2+firstUsedStation] << ", " << times[3+firstUsedStation] << ", " << cross[0+firstUsedStation] << ", " << cross[1+firstUsedStation] << ", " << cross[2+firstUsedStation] << ", " << cross[3+firstUsedStation] << ", " << resX[0+firstUsedStation] << ", " << resX[1+firstUsedStation] << ", " << resX[2+firstUsedStation] << ", " << resX[3+firstUsedStation] << ", " << resY[0+firstUsedStation] << ", " << resY[1+firstUsedStation] << ", " << resY[2+firstUsedStation] << ", " << resY[3+firstUsedStation] << endmsg;

  MVAResponses predictions;
  if (m_runCatBoost) {
      const std::vector<float> InputFloat(Input.begin(), Input.end());  
      if (m_useFastCatBoost) {
	  predictions.CatBoost = \
	      m_FastCatBoostModel->Apply(InputFloat,
					 NCatboostStandalone::EPredictionType::RawValue);
      } else {
	  predictions.CatBoost = m_CatBoostModel.CalcFlat(InputFloat);
      }
  }
  if (m_runTMVA) {
      predictions.TMVA = m_TMVA_BDT->GetMvaValue(Input);
  }
  if (msgLevel(MSG::DEBUG)) {
      debug() << "TMVA = " << predictions.TMVA << std::endl \
	      << "CatBoost = " << predictions.CatBoost << endmsg;
  }
  return predictions;
}
