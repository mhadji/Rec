#ifndef DLLMUONTOOL_H_
#define DLLMUONTOOL_H_

#include <array>
#include <string>
#include <utility>
#include <vector>
#include <map>
#include <boost/range.hpp>

#include "Event/MuonPID.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiTool.h"
#include "MuonDet/DeMuonDetector.h"

#include "MuonID/ICommonMuonTool.h"

#include "TMath.h"
#include "Math/ProbFuncMathCore.h"

static const InterfaceID IID_DLLMuonTool("DLLMuonTool", 1, 0);

/** @class DLLMuonTool DLLMuonTool.h
 * A tool that provides functionality for all steps in muon identification that
 *are only used offline.: e.g: DLL
 *
 * It is designed to have no state associated with the event. The only members
 *are related to conditions (such as detector information or FoI).
 *
 * @author Ricardo Vazquez Gomez
 * @date 2015-01-12
 */
class DLLMuonTool final : public GaudiTool {
 public:
  static const InterfaceID& interfaceID() { return IID_DLLMuonTool; }

  using MuonTrackExtrapolation =
      std::vector<std::pair<double, double>>;
  DLLMuonTool(const std::string& type, const std::string& name,
              const IInterface* parent);
  ~DLLMuonTool() override = default;
  StatusCode initialize() override;
  bool calcNShared(LHCb::MuonPID*, LHCb::MuonPIDs*,
                   const CommonConstMuonHits&, 
                   const MuonTrackExtrapolation&,
                   std::map<LHCb::MuonPID*, double>*,
                   std::map<LHCb::MuonPID*, CommonConstMuonHits>*) const;

  double calcDist(const MuonTrackExtrapolation&,
                  const CommonConstMuonHits&) const;
  unsigned GetPbin(const double, const unsigned) const;
  int getRegionFromPosition(const MuonTrackExtrapolation&) const;
  int findTrackRegion(const MuonTrackExtrapolation&) const;
  std::vector<int> findTrackRegions(const MuonTrackExtrapolation&) const;
  double calc_closestDist(const LHCb::Track&, const MuonTrackExtrapolation&,
                          const CommonConstMuonHits&,
                          const std::vector<unsigned>&) const;
  std::vector<double> loadNonMuLandauParam(const MuonTrackExtrapolation&);
  double calc_ProbMu_tanh(double, unsigned, unsigned) const;
  double calc_ProbNonMu_tanh(double, unsigned, unsigned) const;
  double calc_ProbNonMu(double, const std::vector<double>&) const;
  std::tuple<double, double, double> calcMuonLL_tanhdist_landau(
      const LHCb::Track&, const MuonTrackExtrapolation&, 
      const CommonConstMuonHits&, const std::vector<unsigned>&) const;
  std::tuple<double, double, double> calcMuonLL_tanhdist(
      const LHCb::Track&, const MuonTrackExtrapolation&,
      CommonConstMuonHits&, std::vector<unsigned>&) const;

  StatusCode calcLandauNorm();
  double calcNorm_nmu(double*);
  // compare the coordinates of two MuonPIDs
  bool compareHits(const CommonConstMuonHits&, 
                   const CommonConstMuonHits&) const;
private:
  // some hardcoded parameters. Need to be fixed
  Gaudi::Property<bool> m_OverrideDB {this, "OverrideDB", false,
        "Ignore MuonID info from conditions database."};
  Gaudi::Property<double> preSelMomentum_ {this, "PreSelMomentum"};
  Gaudi::Property<std::vector<double>> momentumCuts_ {this, "MomentumCuts"};
  Gaudi::Property<double> foiFactor_ {this, "FOIfactor", 1.};

  // TODO(kazeevn) Do we really need so many properties?
  // Same parameters for muons and non muons but different names
  // Muons
  Gaudi::Property<std::vector<double>> m_MupBinsR1 {this, "MupBinsR1"};
  Gaudi::Property<std::vector<double>> m_MupBinsR2 {this, "MupBinsR2"};
  Gaudi::Property<std::vector<double>> m_MupBinsR3 {this, "MupBinsR3"};
  Gaudi::Property<std::vector<double>> m_MupBinsR4 {this, "MupBinsR4"};

  // Landau parameters Non-Muons - Region 1-2-3-4:
  Gaudi::Property<std::vector<double>> m_NonMuLanParR1 {
    this, "NonMuLandauParameterR1"};
  Gaudi::Property<std::vector<double>> m_NonMuLanParR2 {
    this, "NonMuLandauParameterR2"};
  Gaudi::Property<std::vector<double>> m_NonMuLanParR3 {
    this, "NonMuLandauParameterR3"};
  Gaudi::Property<std::vector<double>> m_NonMuLanParR4 {
    this, "NonMuLandauParameterR4"};

  // hyperbolic tangent mapping of distances:
  typedef std::vector<std::vector<double>*> vectorOfVectors;
  
  // tanh scale factors for muons necessary for
  Gaudi::Property<std::vector<double>> m_tanhScaleFactorsMuonR1 {
    this, "tanhScaleFactorsMuonR1"};
  Gaudi::Property<std::vector<double>> m_tanhScaleFactorsMuonR2 {
    this, "tanhScaleFactorsMuonR2"};
  Gaudi::Property<std::vector<double>> m_tanhScaleFactorsMuonR3 {
    this, "tanhScaleFactorsMuonR3"};
  Gaudi::Property<std::vector<double>> m_tanhScaleFactorsMuonR4 {
    this, "tanhScaleFactorsMuonR4"};
  vectorOfVectors m_tanhScaleFactorsMuon;
  
  // hyperbolic tangent mapping of distances:
  // tanh scale factors for non-muons
  Gaudi::Property<std::vector<double>> m_tanhScaleFactorsNonMuonR1 {this, "tanhScaleFactorsNonMuonR1"};
  Gaudi::Property<std::vector<double>> m_tanhScaleFactorsNonMuonR2 {this, "tanhScaleFactorsNonMuonR2"};
  Gaudi::Property<std::vector<double>> m_tanhScaleFactorsNonMuonR3 {this, "tanhScaleFactorsNonMuonR3"};
  Gaudi::Property<std::vector<double>> m_tanhScaleFactorsNonMuonR4 {this, "tanhScaleFactorsNonMuonR4"};
  vectorOfVectors m_tanhScaleFactorsNonMuon;

  // tanh(dist2) histograms contents for muons  // DLLflag = 3 and 4
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR1_1 {this, "tanhCumulHistoMuonR1_1"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR1_2 {this, "tanhCumulHistoMuonR1_2"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR1_3 {this, "tanhCumulHistoMuonR1_3"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR1_4 {this, "tanhCumulHistoMuonR1_4"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR1_5 {this, "tanhCumulHistoMuonR1_5"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR1_6 {this, "tanhCumulHistoMuonR1_6"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR1_7 {this, "tanhCumulHistoMuonR1_7"};
  vectorOfVectors m_tanhCumulHistoMuonR1;

  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR2_1 {this, "tanhCumulHistoMuonR2_1"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR2_2 {this, "tanhCumulHistoMuonR2_2"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR2_3 {this, "tanhCumulHistoMuonR2_3"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR2_4 {this, "tanhCumulHistoMuonR2_4"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR2_5 {this, "tanhCumulHistoMuonR2_5"};
  vectorOfVectors m_tanhCumulHistoMuonR2;

  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR3_1 {this, "tanhCumulHistoMuonR3_1"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR3_2 {this, "tanhCumulHistoMuonR3_2"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR3_3 {this, "tanhCumulHistoMuonR3_3"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR3_4 {this, "tanhCumulHistoMuonR3_4"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR3_5 {this, "tanhCumulHistoMuonR3_5"};
  vectorOfVectors m_tanhCumulHistoMuonR3;

  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR4_1 {this, "tanhCumulHistoMuonR4_1"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR4_2 {this, "tanhCumulHistoMuonR4_2"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR4_3 {this, "tanhCumulHistoMuonR4_3"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR4_4 {this, "tanhCumulHistoMuonR4_4"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoMuonR4_5 {this, "tanhCumulHistoMuonR4_5"};
  vectorOfVectors m_tanhCumulHistoMuonR4;

  std::vector<vectorOfVectors*> m_tanhCumulHistoMuon;

  // tanh(dist2) histograms contents for non-muons // DLLflag = 3
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR1_1 {this, "tanhCumulHistoNonMuonR1_1"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR1_2 {this, "tanhCumulHistoNonMuonR1_2"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR1_3 {this, "tanhCumulHistoNonMuonR1_3"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR1_4 {this, "tanhCumulHistoNonMuonR1_4"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR1_5 {this, "tanhCumulHistoNonMuonR1_5"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR1_6 {this, "tanhCumulHistoNonMuonR1_6"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR1_7 {this, "tanhCumulHistoNonMuonR1_7"};
  vectorOfVectors m_tanhCumulHistoNonMuonR1;

  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR2_1 {this, "tanhCumulHistoNonMuonR2_1"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR2_2 {this, "tanhCumulHistoNonMuonR2_2"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR2_3 {this, "tanhCumulHistoNonMuonR2_3"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR2_4 {this, "tanhCumulHistoNonMuonR2_4"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR2_5 {this, "tanhCumulHistoNonMuonR2_5"};
  vectorOfVectors m_tanhCumulHistoNonMuonR2;

  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR3_1 {this, "tanhCumulHistoNonMuonR3_1"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR3_2 {this, "tanhCumulHistoNonMuonR3_2"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR3_3 {this, "tanhCumulHistoNonMuonR3_3"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR3_4 {this, "tanhCumulHistoNonMuonR3_4"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR3_5 {this, "tanhCumulHistoNonMuonR3_5"};
  vectorOfVectors m_tanhCumulHistoNonMuonR3;

  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR4_1 {this, "tanhCumulHistoNonMuonR4_1"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR4_2 {this, "tanhCumulHistoNonMuonR4_2"};
  Gaudi::Property<std::vector<double>> m_tanhCumulHistoNonMuonR4_3 {this, "tanhCumulHistoNonMuonR4_3"};
  vectorOfVectors m_tanhCumulHistoNonMuonR4;

  std::vector<vectorOfVectors*> m_tanhCumulHistoNonMuon;

  double Fdist[5];
  double small_dist[5];
  double closest_region[5];
  double closest_x[5];
  double closest_y[5];

  DeMuonDetector* m_muonDet = nullptr; 
  ICommonMuonTool* m_muonTool = nullptr;
  
  unsigned int iM2, iM3, iM4, iM5;
  unsigned m_stationsCount, m_regionsCount;
  // This is a feature of the algorithm, so a const
  static constexpr unsigned int s_usedStations = 4;
};

#endif  // DLLMUONTOOL_H_
