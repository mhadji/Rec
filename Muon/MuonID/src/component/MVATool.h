#ifndef MVATOOL_H
#define MVATOOL_H 1

#include <array>
#include <string>
#include <utility>
#include <vector>
#include <map>
#include <memory>

#include "Event/MuonPID.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiTool.h"

#include "MuonID/IMVATool.h"  // Interface

#include "MuonID/ICommonMuonTool.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "MuonDet/DeMuonDetector.h"

#include "wrapped_calcer.h"
#include "evaluator.h"

#include "weights/TMVAClassification_BDTD_mva_pion_300_5_25_05_2_masscut_withPreweight_NoNshared_NewMS.class.C"

/** @class MVATool MVATool.h
 * A tool that provides an MVA for muon identification 
 *
 * @author Ricardo Vazquez Gomez
 * @date 2017-05-29
 */

class MVATool final : public extends1<GaudiTool, IMVATool> {
 public:
  MVATool(const std::string& type, const std::string& name, 
          const IInterface* parent);
  virtual ~MVATool() override = default;
  
  virtual auto initialize() -> StatusCode override;
  virtual auto calcBDT(
      const LHCb::Track&, const CommonConstMuonHits&) const noexcept -> MVAResponses override;
  // TODO(kazeevn) this probably can be replaced with
  // Gaudi::Utils::getProperty
  // 1. Can someone please show me how to get the actual bool value?
  // 2. Will this be fast enough to call on every track?
  virtual inline bool getRunTMVA() const noexcept override {return m_runTMVA;}
  virtual inline bool getRunCatBoost() const noexcept override {return m_runCatBoost;}
  unsigned int nStations;

 private:
  Gaudi::Property<bool> m_runTMVA{this, "RunTMVA", true};
  Gaudi::Property<bool> m_runCatBoost{this, "RunCatBoost", true};
  // There are two libaries that allow for running CatBoost models
  // standalone_evaluator and model_interface
  // As of LCG v92 the standalone_evaluator is faster
  Gaudi::Property<bool> m_useFastCatBoost{this, "UseCatboostStandaloneEvaluator", true};
  Gaudi::Property<std::string> m_CatBoostModelPath{this, "CatBoostModelPath",
	  System::getEnv("TMVAWEIGHTSROOT") + "/data/MuID/MuID-Run2-MC-570-v1.cb"};

  std::unique_ptr<ReadBDTD_mva_pion_300_5_25_05_2_masscut_withPreweight_NoNshared_NewMS> m_TMVA_BDT;
  ModelCalcerWrapper m_CatBoostModel;
  std::unique_ptr<NCatboostStandalone::TOwningEvaluator> m_FastCatBoostModel;
  DeMuonDetector* m_det;
  ICommonMuonTool *muonTool_;
  std::vector<double> m_stationZ;
  unsigned int firstUsedStation;
};
#endif  // MVATOOL_H


