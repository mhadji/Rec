#ifndef COMPONENT_MUONKALMANMATCHTOOL_H
#define COMPONENT_MUONKALMANMATCHTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "MuonID/IMuonMatchTool.h"            // Interface
#include "Kernel/LHCbID.h"
#include "Kernel/TrackDefaultParticles.h"

class DeMuonDetector;
class TLinearFitter;
struct ITrackExtrapolator;
class ITrackFitter;

namespace LHCb {
  class State;
}

/** @class MuonKalmanMatchTool MuonKalmanMatchTool.h component/MuonKalmanMatchTool.h
 *
 *  /brief muon match tool based on Kalman filter through Muon stations
 *
 *  the tool extends the track Kalman filter through muon stations.
 *
 *  input hits are given through a vector of TrackMuMatch, containing a CommonMuonHit and related track extrapolation position
 *  optionally, a second list of "low-quality" hits (uncrossed or from large clusters) can be given if no matches are found in the first list
 *
 *  the fit is repeated iteratively, excluding muon hits outside OutlierCut number of sigma from the refitted track
 *
 *  use run() to perform the calculation, and then:
 *    getChisquare() to get the fit chi2 (using only best muon hit per station)
 *    getListofCommonMuonHits() or getListofMuonTiles() to get the associated muon hits (all or best per station)
 *
 *  @author Giacomo Graziani
 *  @date   2015-11-11
 */
class MuonKalmanMatchTool : public GaudiTool, virtual public IMuonMatchTool {
public:
  /// Standard constructor
  MuonKalmanMatchTool( const std::string& type,
                     const std::string& name,
                     const IInterface* parent);

  StatusCode initialize() override;

  StatusCode run(const LHCb::Track* track,
                 std::vector<TrackMuMatch>* bestMatches,
                 std::vector<TrackMuMatch>* spareMatches=NULL) override;
  void getListofCommonMuonHits(CommonConstMuonHits& matchedMuonHits, int station=-1, bool onlybest=true) override;
  void getListofMuonTiles(std::vector<LHCb::MuonTileID>& matchedTiles, int station=-1, bool onlybest=true) override;
  double getChisquare(int& ndof) const override;
  MuonMatchType getMatch(int station) const override;
  double getMatchSigma(int station) const override;

private:
  DeMuonDetector*  m_mudet = nullptr;
  ITrackExtrapolator* m_extrapolator = nullptr;
  ITrackFitter* m_trackFitter = nullptr;

  double matchStateToMuonHit(const CommonMuonHit* hit, LHCb::State &StState, double& sigmax, double& sigmay );
  void muOutlierRemoval(LHCb::Track * mutrack, LHCb::LHCbID id);

  LHCb::Tr::PID theMuon = LHCb::Tr::PID::Muon();
  const LHCb::Track* m_lasttrack = nullptr;
  std::vector< TrackMuMatch > * m_lastMatchesBest = nullptr;
  std::vector< TrackMuMatch > * m_lastMatchesSpare = nullptr;

  int m_missedStations = 0;

  // vectors for the algorithm
  std::vector< LHCb::LHCbID > m_bestMatchedTile;
  std::vector< double > m_bestPM;

  // output
  std::vector<bool> m_hitOnStation;
  CommonConstMuonHits m_matchedHits;
  CommonConstMuonHits m_bestmatchedHits;
  double m_muchi2 = -1.;
  double m_mymuchi2 = -1.;
  int m_mudof = 1;
  int m_mymudof = 1;

  // options
  Gaudi::Property<bool> m_OnlyCrossedHits 
    {this, "OnlyCrossedHits", false, 
    "use only crossed hits"};

  Gaudi::Property<float> m_maxDeltaCut 
    {this, "MaxmatchDisplacement", 5, 
    "keep all muon matches below these number of delta-chi"};

  Gaudi::Property<float> m_maxDeltaCutStation 
    {this, "MaxmatchBestDisplacement", 8, 
    "keep best muon match per station below these number of delta-chi"};

  Gaudi::Property<float> m_outlierCut 
    {this, "OutlierCut", 5, 
    "minimal number of sigma (x or y) for outlier removal after track fit"};

  Gaudi::Property<int> m_maxMatches 
    {this, "MaxMatches", 100, 
    "do not consider tracks with more than these matches in M2-M5"};

};
#endif // COMPONENT_MUONKALMANMATCHTOOL_H
