package RecSys
version           v21r0

branches cmt doc tests

#=======================================================
# LHCb reconstruction packages
#=======================================================

# Calorimeter                         # Maintainer
use CaloMoniDst * Calo # Olivier Deschamps
use CaloPIDs * Calo # Victor Egorychev, Vanya Belyaev
use CaloReco * Calo # Olivier Deschamps
use CaloTools * Calo # Olivier Deschamps

# Hlt
use HltMonitors * Hlt # Roel Aaij

# Muon
use MuonID * Muon # Erica Polycarpo, Miriam Gandelman
use MuonInterfaces * Muon # Giovanni Passaleva
use MuonPIDChecker * Muon # Erica Polycarpo, Miriam Gandelman
use MuonTools * Muon # Alessia Satta
use MuonTrackAlign * Muon # Silvia Pozzi
use MuonTrackMonitor * Muon # Giacomo Graziani
use MuonTrackRec * Muon # Giacomo Graziani

# Pr packages: Track Pattern Recognition for upgrade detectors
use PrAlgorithms * Pr # Yasmine Amhis, Manuel Schiller
use PrFitParams * Pr # Yasmine Amhis
use PrKernel * Pr # Manuel Schiller
use PrMCTools * Pr # Manuel Schiller
use PrPixel * Pr # Tim Head, Heinrich Schindler
use PrUtils * Pr # Thomas Nikodem
use PrVeloUT * Pr # Jianchun Wang

# Non sub-detector specific components
use ChargedProtoANNPID * Rec # Chris Jones
use GlobalReco * Rec # Chris Jones, Olivier Deschamps
use LumiAlgs * Rec # Rosen Matev
use RecAlgs * Rec # Marco Cattaneo
use RecConf * Rec # Marco Cattaneo
use RecInterfaces * Rec # Chris Jones

# Rich
use RichAlignment * Rich # Chris Jones, Antonis Papanestis
use RichENNRingFinder * Rich # Chris Jones
use RichGlobalPID * Rich # Chris Jones
use RichHPDImageAnalysis * Rich # Thomas Blake, Chris Jones
use RichIFBAnalysis * Rich # Ross Young, Chris Jones
use RichParticleSearch * Rich # Matt Coombes
use RichPIDMerge * Rich # Chris Jones
use RichRecAlgorithms * Rich # Chris Jones
use RichRecEvent * Rich # Chris Jones
use RichRecBase * Rich # Chris Jones
use RichRecUtils * Rich # Chris Jones
use RichRecInterfaces * Rich # Chris Jones
use RichRecMCAlgorithms * Rich # Chris Jones
use RichRecMCTools * Rich # Chris Jones
use RichRecMonitors * Rich # Chris Jones
use RichRecPhotonTools * Rich # Chris Jones
use RichRecQC * Rich # Chris Jones
use RichRecStereoTools * Rich # Chris Jones
use RichRecSys * Rich # Chris Jones
use RichRecTemplateRings * Rich # Sajan Easo
use RichRecTools * Rich # Chris Jones
use RichRecTrackTools * Rich # Chris Jones

# Tf packages: Track Pattern Recognition
use FastVelo * Tf # David Hutchcroft
use PatAlgorithms * Tf # Michel De Cian
use PatKernel * Tf # Michel De Cian
use PatVelo * Tf # David Hutchcroft
use PatVeloTT * Tf # Mariusz Witek
use TfKernel * Tf # Michel De Cian
use TfTools * Tf # Michel De Cian
use TrackSys * Tf # Michel De Cian
use TsaAlgorithms * Tf # Matt Needham
use TsaKernel * Tf # Matt Needham

# Tr packages: Track Fit & Pattern Reco Validation with MonteCarlo
use PatChecker * Tr # David Hutchcroft
use PatFitParams * Tr # Michel De Cian
use TrackAssociators * Tr # Michel De Cian, Edwin Bos
use TrackCheckers * Tr # Michel De Cian
use TrackExtrapolators * Tr # Michel De Cian
use TrackFitEvent * Tr # Michel De Cian
use TrackFitter * Tr # Wouter Hulsbergen
use TrackIdealPR * Tr # Michel De Cian
use TrackInterfaces * Tr # Wouter Hulsbergen
use TrackKernel * Tr # Wouter Hulsbergen
use TrackMCTools * Tr # Michel De Cian
use TrackMonitors * Tr # Wouter Hulsbergen, Stephanie Menzemer
use TrackProjectors * Tr # Wouter Hulsbergen
use TrackTools * Tr # Wouter Hulsbergen
use TrackUtils * Tr # Michel De Cian

# Primary Vertex Finder
use PatPV * Tr # Mariusz Witek
use FastPV * Tf # Pawel Jalocha

# Velo
use VeloRecMonitors * Velo # Malcom John

# Declare this as a container package
apply_pattern container_package

# Allow the generation of QMTest summary
apply_pattern QMTestSummarize

# Allow the generation of the SAM QMTest
apply_pattern QMTest
