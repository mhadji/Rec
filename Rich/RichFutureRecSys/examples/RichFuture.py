
###############################################################
# Job options file
###############################################################

from Gaudi.Configuration import *
from GaudiConfig.ControlFlow import seq
from Configurables import CondDB, LHCbApp, GaudiSequencer

# Fetch required data from file
from Configurables import Gaudi__Hive__FetchDataFromFile as FetchDataFromFile
fetcher = FetchDataFromFile('FetchDSTData')
fetcher.DataKeys = ['Trigger/RawEvent','Rich/RawEvent',
                    'pRec/Track/Best','pMC/Particles']

# First various raw event decodings

from Configurables import createODIN
odinDecode = createODIN( "ODINFutureDecode" )

from Configurables import Rich__Future__RawBankDecoder as RichDecoder
richDecode = RichDecoder( "RichFutureDecode" )

# Explicitly unpack the Tracks

#from Configurables import UnpackTrackFunctional
#tkUnpack = UnpackTrackFunctional("UnpackTracks")
from Configurables import UnpackTrack
tkUnpack = UnpackTrack("UnpackTracks")

# Filter the tracks by type

from Configurables import Rich__Future__Rec__TrackFilter as TrackFilter
tkFilt = TrackFilter("TrackTypeFilter")

# Now get the RICH sequences

photoRecoType = "Quartic"
#photoRecoType = "CKEstiFromRadius"

# Preload the geometry during initialise. Useful for timing studies.
preloadGeom = True
#preloadGeom = False

# Work around for some tracking tools the RICH uses that doesn't seem
# to be thread safe yet. Does not matter for normal execution, but if
# using Gaudi Hive set this to true. Changes how the RICH works to
# a not completely ideal mode, but avoids the problem.
#workAroundTrackTools = False
workAroundTrackTools = True

# Input tracks
tkLocs = { "Long" : tkFilt.OutLongTracksLocation,
           "Down" : tkFilt.OutDownTracksLocation,
           "Up"   : tkFilt.OutUpTracksLocation }
#tkLocs = { "All"  : "Rec/Track/Best" }
#tkLocs = { "Long" : tkFilt.OutLongTracksLocation }
#tkLocs = { "Up" : tkFilt.OutUpTracksLocation }

# Output PID
pidLocs =  { "Long" : "Rec/Rich/LongPIDs",
             "Down" : "Rec/Rich/DownPIDs",
             "Up"   : "Rec/Rich/UpPIDs" }
#pidLocs = { "All" : "Rec/Rich/PIDs" } 
#pidLocs = { "Long" : "Rec/Rich/LongPIDs" }
#pidLocs = { "Up" : "Rec/Rich/UpPIDs" }

# Merged output
finalPIDLoc = "Rec/Rich/PIDs"

# DataType
#dType = "2016"
dType = "Upgrade"

# Online Brunel mode.
#online = True
online = False

# The reconstruction
from RichFutureRecSys.ConfiguredRichReco import RichRecoSequence
RichRec = RichRecoSequence( dataType              = dType,
                            onlineBrunelMode      = online,
                            photonReco            = photoRecoType,
                            preloadGeometry       = preloadGeom,
                            makeTkToolsThreadSafe = workAroundTrackTools,
                            inputTrackLocations   = tkLocs,
                            outputPIDLocations    = pidLocs,
                            mergedOutputPIDLocation = finalPIDLoc )

# Monitoring
from RichFutureRecMonitors.ConfiguredRecoMonitors import RichRecMonitors
RichMoni = RichRecMonitors( inputTrackLocations = tkLocs,
                            onlineBrunelMode    = online,
                            outputPIDLocations  = pidLocs )

# Explicitly unpack the MCParticles
from Configurables import UnpackMCParticle
mcPUnpack = UnpackMCParticle()
# Unpack the MC digit summaries
from Configurables import DataPacking__Unpack_LHCb__MCRichDigitSummaryPacker_ as RichSumUnPack
mcRiSumUnP = RichSumUnPack("RichMCSummaryUnpack")

# MC Checking
from RichFutureRecMonitors.ConfiguredRecoMonitors import RichRecCheckers
RichCheck = RichRecCheckers( inputTrackLocations = tkLocs,
                             onlineBrunelMode    = online,
                             outputPIDLocations  = pidLocs )

# The final sequence to run
all = GaudiSequencer( "All", Members = [ fetcher, odinDecode, richDecode, 
                                         tkUnpack, tkFilt, RichRec ] )

# Uncomment to enable monitoring
#all.Members += [ RichMoni ]
#all.Members += [ mcPUnpack, mcRiSumUnP, RichCheck ]

all.MeasureTime = True

ApplicationMgr( TopAlg = [all],
                EvtMax = 50000,      # events to be processed (default is 10)
                #EvtSel = 'NONE', # do not use any event input
                ExtSvc = ['ToolSvc', 'AuditorSvc' ],
                AuditAlgorithms = True )

EventSelector().PrintFreq = 100
#LHCbApp().SkipEvents = 5416

CondDB() # Just to initialise
LHCbApp()

# Timestamps in messages
#LHCbApp().TimeStamp = True

msgSvc = getConfigurable("MessageSvc")
#msgSvc.setVerbose += [ "DeRichGasRadiator" ]
#msgSvc.setVerbose += [ "DeRichSystem" ]
#msgSvc.setVerbose += [ "DeRichHPD" ]
msgSvc.Format = "% F%40W%S%7W%R%T %0W%M"
from Configurables import SequencerTimerTool
SequencerTimerTool("ToolSvc.SequencerTimerTool").NameSize = 40

 # ROOT persistency for histograms
ApplicationMgr().HistogramPersistency = "ROOT"
from Configurables import RootHistCnv__PersSvc
RootHistCnv__PersSvc('RootHistCnv').ForceAlphaIds = True
HistogramPersistencySvc().OutputFile = "RichFuture.root"

# Auditors
AuditorSvc().Auditors += [ "FPEAuditor" ]
from Configurables import FPEAuditor
FPEAuditor().ActivateAt = ["Execute"]
#AuditorSvc().Auditors += [ "NameAuditor" ]
