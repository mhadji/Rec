
###############################################################
# Job options file
#==============================================================

from Gaudi.Configuration import *
from GaudiConfig.ControlFlow import seq
from Configurables import CondDB, LHCbApp, GaudiSequencer

# Fetch required data from file
from Configurables import Gaudi__Hive__FetchDataFromFile as FetchDataFromFile
fetcher = FetchDataFromFile('FetchDSTData')
fetcher.DataKeys = ['Rich/RawEvent','DAQ/RawEvent']

from Configurables import Rich__Future__RawBankDecoder as RichDecoder
richDecode = RichDecoder( "RichFutureDecode" )

# The final sequence to run
all = GaudiSequencer( "All", Members = [ fetcher, richDecode ] )
all.MeasureTime = True

#nEvnts = 999999999
nEvnts = 50000

ApplicationMgr( TopAlg = [all],
                EvtMax = nEvnts,      # events to be processed (default is 10)
                #EvtSel = 'NONE', # do not use any event input
                ExtSvc = ['ToolSvc', 'AuditorSvc' ],
                AuditAlgorithms = True )

EventSelector().PrintFreq = 1000
#LHCbApp().SkipEvents = 1278

# Just to initialise
CondDB()
LHCbApp()

# Timestamps in messages
LHCbApp().TimeStamp = True

msgSvc = getConfigurable("MessageSvc")
#msgSvc.setVerbose += [ "DeRichGasRadiator" ]
#msgSvc.setVerbose += [ "DeRichSystem" ]
#msgSvc.setVerbose += [ "DeRichHPD" ]
msgSvc.Format = "% F%40W%S%7W%R%T %0W%M"
from Configurables import SequencerTimerTool
SequencerTimerTool("ToolSvc.SequencerTimerTool").NameSize = 40

 # ROOT persistency for histograms
ApplicationMgr().HistogramPersistency = "ROOT"
from Configurables import RootHistCnv__PersSvc
RootHistCnv__PersSvc('RootHistCnv').ForceAlphaIds = True
HistogramPersistencySvc().OutputFile = "RichDecode.root"
#AuditorSvc().Auditors += [ "FPEAuditor" ]
#from Configurables import FPEAuditor
#FPEAuditor().ActivateAt = ["Execute"]
