
#pragma once

// STL
#include <tuple>
#include <array>
#include <iomanip>

// Gaudi
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/PhysicalConstants.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Event Model
#include "RichFutureRecEvent/RichRecPhotonYields.h"
#include "RichFutureRecEvent/RichRecMassHypoRings.h"
#include "RichFutureRecEvent/RichRecGeomEfficiencies.h"

// Utils
#include "RichUtils/ZipRange.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {

      // Use the functional framework
      using namespace Gaudi::Functional;

      namespace
      {
        /// Output data type
        using OutData = std::tuple<PhotonYields::Vector,PhotonSpectra::Vector>;
      }

      /** @class SignalPhotonYields RichSignalPhotonYields.h
       *
       *  Computes the emitted photon yield data from Track Segments.
       *
       *  @author Chris Jones
       *  @date   2016-09-30
       */
      class SignalPhotonYields final :
        public MultiTransformer< OutData( const PhotonYields::Vector&,
                                          const PhotonSpectra::Vector&,
                                          const GeomEffs::Vector& ),
                                 Traits::BaseClass_t<AlgBase> >
      {

      public:

        /// Standard constructor
        SignalPhotonYields( const std::string& name,
                            ISvcLocator* pSvcLocator );

      public:

        /// Algorithm execution via transform
        OutData operator()( const PhotonYields::Vector& detYields,
                            const PhotonSpectra::Vector& detSpectra,
                            const GeomEffs::Vector& geomEffs ) const override;

      };

    }
  }
}
