
#pragma once

// Gaudi
#include "GaudiAlg/Transformer.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Event Model
#include "RichFutureRecEvent/RichRecSpacePoints.h"

// Utils
#include "RichUtils/RichTrackSegment.h"
#include "RichFutureUtils/RichGeomPhoton.h"

// Interfaces
#include "RichFutureInterfaces/IRichRayTracing.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {

      // Use the functional framework
      using namespace Gaudi::Functional;

      /** @class RayTraceTrackGlobalPoints RichRayTraceTrackGlobalPoints.h
       *
       *  Ray traces the track direction through the RICH system to the
       *  HPD panels and stores the results in global coordinates.
       *
       *  @author Chris Jones
       *  @date   2016-09-30
       */

      class RayTraceTrackGlobalPoints final :
        public Transformer< SegmentPanelSpacePoints::Vector( const LHCb::RichTrackSegment::Vector& ),
                            Traits::BaseClass_t<AlgBase> >
      {

      public:

        /// Standard constructor
        RayTraceTrackGlobalPoints( const std::string& name,
                                   ISvcLocator* pSvcLocator );

        /// Initialization after creation
        virtual StatusCode initialize() override;

      public:

        /// Functional operator
        SegmentPanelSpacePoints::Vector
          operator()( const LHCb::RichTrackSegment::Vector& segments ) const override;

      private:

        /// Ray tracing tool
        ToolHandle<const IRayTracing> m_rayTrace
        { "Rich::Future::RayTracing/RayTracing", this };

        /// Cached trace modes for each radiator
        RadiatorArray<LHCb::RichTraceMode> m_traceMode = {{}};

        /// Cached forced trace modes for each radiator
        RadiatorArray<LHCb::RichTraceMode> m_fTraceMode = {{}};

      };

    }
  }
}
