
// Array properties
//#include "GaudiKernel/ParsersFactory.h"
//#include "GaudiKernel/StdArrayAsProperty.h"

// local
#include "RichDetectablePhotonYields.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

//=============================================================================

DetectablePhotonYields::DetectablePhotonYields( const std::string& name,
                                                ISvcLocator* pSvcLocator )
  : MultiTransformer ( name, pSvcLocator,
                       { KeyValue{ "TrackSegmentsLocation",           LHCb::RichTrackSegmentLocation::Default },
                         KeyValue{ "EmittedSpectraLocation",          PhotonSpectraLocation::Emitted },
                         KeyValue{ "MassHypothesisRingsLocation",     MassHypoRingsLocation::Emitted } },
                       { KeyValue{ "DetectablePhotonYieldLocation",   PhotonYieldsLocation::Detectable },
                         KeyValue{ "DetectablePhotonSpectraLocation", PhotonSpectraLocation::Detectable } } )
{ 
  m_riches.fill(nullptr);
  m_qWinZSize.fill(0);
  // debug
  //setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================

StatusCode DetectablePhotonYields::initialize()
{
  // Sets up various tools and services
  auto sc = MultiTransformer::initialize();
  if ( !sc ) return sc;

  // Rich1 and Rich2
  m_riches[Rich::Rich1] = getDet<DeRich1>( DeRichLocations::Rich1 );
  m_riches[Rich::Rich2] = getDet<DeRich2>( DeRichLocations::Rich2 );

  // Do we have HPDs or PMTs
  const bool PmtActivate = m_riches[Rich::Rich1]->RichPhotoDetConfig() == Rich::PMTConfig;

  // Quartz window eff
  const auto qEff = m_riches[Rich::Rich1]->param<double>( "HPDQuartzWindowEff" );

  // Digitisation pedestal loss
  const auto pLos =
    ( PmtActivate && m_riches[Rich::Rich1]->exists("PMTPedestalDigiEff") ?
      m_riches[Rich::Rich1]->param<double>("PMTPedestalDigiEff") :
      m_riches[Rich::Rich1]->param<double>("HPDPedestalDigiEff") );

  // store cached value
  m_qEffPedLoss = qEff * pLos;

  // Quartz window params
  m_qWinZSize[Rich::Rich1] = 
    m_riches[Rich::Rich1]->param<double>("Rich1GasQuartzWindowThickness"); 
  m_qWinZSize[Rich::Rich2] = 
    m_riches[Rich::Rich2]->param<double>("Rich2GasQuartzWindowThickness"); 
  
  // return
  return sc;
}

//=============================================================================

OutData
DetectablePhotonYields::operator()( const LHCb::RichTrackSegment::Vector& segments,
                                    const PhotonSpectra::Vector& emittedSpectra,
                                    const MassHypoRingsVector& massRings ) const
{
  // Scalar type to work with
  using ScType = PhotonYields::Type;

  // make the data to return
  OutData data;
  auto & yieldV   = std::get<PhotonYields::Vector>(data);
  auto & spectraV = std::get<PhotonSpectra::Vector>(data);
  // reserve sizes
  yieldV.reserve  ( segments.size() );
  spectraV.reserve( segments.size() );

  // local counts
  PDCount pdCount; 
  MirrorCount primMirrCount, secMirrCount;
  // reserve a reasonable starting size.
  // A single track will only interactive with a few PDs or mirrors.
  pdCount.reserve(3);
  primMirrCount.reserve(3);
  secMirrCount.reserve(3);

  // Loop over input data
  for ( const auto && data : Ranges::ConstZip(segments,emittedSpectra,massRings) )
  {
    const auto & segment     = std::get<0>(data);
    const auto & emitSpectra = std::get<1>(data);
    const auto & rings       = std::get<2>(data);

    // Create the detectable photon spectra, using the same energy range 
    // as the emitted spectra
    spectraV.emplace_back( emitSpectra.minEnergy(), emitSpectra.maxEnergy() );
    auto & detSpectra = spectraV.back();

    // create the yield data
    yieldV.emplace_back( );
    auto & yields = yieldV.back();

    // which RICH
    const auto rich = segment.rich();

    // Loop over PID types
    for ( const auto id : activeParticles() )
    {
      // the signal
      ScType signal = 0;

      // Skip rings with no hits
      //_ri_debug << "  -> " << id << " #Rings=" << rings[id].size() << endmsg;
      if ( !rings[id].empty() && Rich::BelowThreshold != id )
      {

        // Collect the signal info for this ring
        unsigned int totalInPD{0};
        pdCount.clear();
        primMirrCount.clear();
        secMirrCount.clear();
        for ( const auto& P : rings[id] )
        {
          if ( RayTracedCKRingPoint::InHPDTube == P.acceptance() )
          {
            // Mirrors
            const auto pM = P.primaryMirror();
            const auto sM = P.secondaryMirror();
            // PD
            const auto pd = P.photonDetector();
            if ( pM && sM && pd ) 
            {
              // Count accepted points
              ++totalInPD;
              // Count PDs hit by this ring
              auto iPD = std::find_if( pdCount.begin(), pdCount.end(),
                                       [&pd]( const auto & i )
                                       { return i.first == pd; } );
              if ( iPD == pdCount.end() ) { pdCount.emplace_back(pd,1); }
              else                        { ++iPD->second; }
              // Count primary mirrors
              auto iPM = std::find_if( primMirrCount.begin(), primMirrCount.end(),
                                       [&pM]( const auto & i )
                                       { return i.first == pM; } );
              if ( iPM == primMirrCount.end() ) { primMirrCount.emplace_back(pM,1); }
              else                              { ++iPM->second; }
              // Count secondary mirrors
              auto iSM = std::find_if( secMirrCount.begin(), secMirrCount.end(),
                                       [&sM]( const auto & i )
                                       { return i.first == sM; } );
              if ( iSM == secMirrCount.end() ) { secMirrCount.emplace_back(sM,1); }
              else                             { ++iSM->second; }
            }
            else { Warning("Problem with mirror or PD pointers!").ignore(); }
          }
        }
        
        // Any hits in acceptance ?
        if ( totalInPD > 0 )
        {

          //_ri_debug << std::setprecision(9) << " totalInPD = " <<  totalInPD << endmsg;

          // loop over the energy bins
          for ( unsigned int iEnBin = 0; iEnBin < emitSpectra.energyBins(); ++iEnBin )
          {

            // bin energy
            const auto energy = emitSpectra.binEnergy(iEnBin);

            // Get weighted average PD Q.E.
            ScType pdQEEff(0);
            if ( !pdCount.empty() )
            {
              for ( const auto& PD : pdCount )
              {
                // add up Q.E. eff
                pdQEEff += (ScType)(PD.second) * (*((PD.first)->pdQuantumEff()))[energy*Gaudi::Units::eV]/100 ;
              }
              // normalise the result
              pdQEEff /= (ScType)(totalInPD);
            }
            else
            {
              pdQEEff = 1;
              Warning( "No PDs found -> Assuming Av. PD Q.E. of 1", StatusCode::SUCCESS ).ignore();
            }
            //_ri_debug << std::setprecision(9) << "   -> pdQEEff : En=" << energy << " Eff=" << pdQEEff << endmsg;

            // Weighted primary mirror reflectivity
            ScType primMirrRefl(0);
            if ( !primMirrCount.empty() )
            {
              for ( const auto& PM : primMirrCount )
              {
                // add up mirror refl.
                primMirrRefl +=
                  (ScType)(PM.second) * (*(PM.first->reflectivity()))[energy*Gaudi::Units::eV];
              }
              // normalise the result
              primMirrRefl /= (ScType)(totalInPD);
            }
            else
            {
              primMirrRefl = 1;
              Warning( "No primary mirrors found -> Assuming Av. reflectivity of 1", 
                       StatusCode::SUCCESS ).ignore();
            }
            //_ri_debug << std::setprecision(9) << "   -> primMirrRefl : En=" << energy << " Eff=" << primMirrRefl << endmsg;

            // Weighted secondary mirror reflectivity
            ScType secMirrRefl(0);
            if ( !secMirrCount.empty() )
            {
              for ( const auto& SM : secMirrCount )
              {
                // add up mirror refl.
                secMirrRefl +=
                  (ScType)(SM.second) * (*(SM.first->reflectivity()))[energy*Gaudi::Units::eV];
              }
              // normalise the result
              secMirrRefl /= (ScType)(totalInPD);
            }
            else
            {
              secMirrRefl = 1;
              Warning( "No secondary mirrors found -> Assuming Av. reflectivity of 1", 
                       StatusCode::SUCCESS ).ignore();
            }
            //_ri_debug << std::setprecision(9) << "   -> secMirrRefl : En=" << energy << " Eff=" << secMirrRefl << endmsg;

            // The total efficiency
            const auto totEff = m_qEffPedLoss * pdQEEff * primMirrRefl * secMirrRefl;

            // Scale the distribution in this bin by the above efficiencies
            auto sig = ( (emitSpectra.energyDist(id))[iEnBin] * totEff );
            
            // The Quartz window efficiency
            sig *= ( energy <= 0 ? 0 :
                     Rich::Maths::fast_exp( -m_qWinZSize[rich] /
                                            (*(m_riches[rich]->gasWinAbsLength()))[energy*Gaudi::Units::eV] ) );

            //_ri_debug << std::setprecision(9) << "    -> Final Eff = " << totEff << " | DetSignal = " << sig << endmsg;

            // Save to the output spectra for this bin
            (detSpectra.energyDist(id))[iEnBin] = sig;

            // update the overal detectable signal
            signal += sig;

          } // loop over energy bins

        } // >0 PDs
        //else { _ri_debug << "    -> No ray traced hits in acceptance" << endmsg; }

      } // Not below threshold

      // save the yield for this hypo
      yields.setData(id,signal);
      //_ri_debug << std::setprecision(9) << "Final DetSignal " << id << " " << yields[id] << endmsg;
      
    } // loop over PID types

  }

  // return the new data
  return data;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( DetectablePhotonYields )

//=============================================================================
