
// local
#include "RichEmittedPhotonYields.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

//=============================================================================

EmittedPhotonYields::EmittedPhotonYields( const std::string& name,
                                          ISvcLocator* pSvcLocator )
  : MultiTransformer ( name, pSvcLocator,
                       { KeyValue{ "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default } },
                       { KeyValue{ "EmittedPhotonYieldLocation", PhotonYieldsLocation::Emitted },
                         KeyValue{ "EmittedPhotonSpectraLocation", PhotonSpectraLocation::Emitted } } )
{
  //setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================

StatusCode EmittedPhotonYields::initialize()
{
  // Sets up various tools and services
  auto sc = MultiTransformer::initialize();
  if ( !sc ) return sc;

  // Get Rich1 Detector element
  m_Rich1DE = getDet<DeRich1>( DeRichLocations::Rich1 );

  // load tools
  sc = sc && m_detParams.retrieve();

  // register for UMS updates
  //updMgrSvc()->registerCondition( this, m_Rich1DE, &SellmeirFunc::umsUpdate );
  // force first updates
  //sc = updMgrSvc()->update(this);
  // CRJ : UMS does not work because Rich1 has no conditions (yet)
  sc = sc && umsUpdate();

  // return
  return sc;
}

//=============================================================================

OutData
EmittedPhotonYields::operator()( const LHCb::RichTrackSegment::Vector& segments ) const
{
  // make the data to return
  OutData data;
  auto & yieldV   = std::get<PhotonYields::Vector>(data);
  auto & spectraV = std::get<PhotonSpectra::Vector>(data);
  
  // reserve sizes
  yieldV.reserve  ( segments.size() );
  spectraV.reserve( segments.size() );

  // Loop over segments
  for ( const auto & segment : segments )
  {

    // Which radiator
    const auto rad = segment.radiator();

    // photon energies for this rad
    const auto & en = m_photEnergies[rad];

    // Create the emitted photon spectra
    spectraV.emplace_back( en.first, en.second );
    auto & spectra = spectraV.back();

    // create the yield data
    yieldV.emplace_back( );
    auto & yields = yieldV.back();

    // Some parameters of the segment
    const auto momentum2  = segment.bestMomentum().Mag2();
    const auto length     = segment.pathLength();

    // energy bin size
    const auto enBinSize = spectra.binSize();
    
    // Loop over PID types
    for ( const auto id : activeParticles() )
    {
      // the signal value
      PhotonYields::Type signal = 0;

      // Skip below threshold
      if ( Rich::BelowThreshold != id )
      {

        // Compute some segment ID dependent parameters
        const auto Esq         = momentum2 + m_particleMassSq[id];
        const auto invBetaSqA  = 37.0 * ( momentum2>0 ? Esq/momentum2 : 0 );
        const auto invGammaSqE = ( Esq>0 ? m_particleMassSq[id]/Esq : 0 ) * enBinSize;

        //_ri_verbo << std::setprecision(9) 
        //          << id << " Esq " << Esq << " invBetaSqA " << invBetaSqA
        //          << " invGammaSqE " << invGammaSqE << endmsg;
          
        // loop over the energy bins
        for ( unsigned int iEnBin = 0; iEnBin < spectra.energyBins(); ++iEnBin )
        {

          // Compute number of photons
          auto nPhot = invBetaSqA * ( (m_paraWDiff[rad])[iEnBin] - invGammaSqE );

          // correct for wavelength independant transmission coeff. in aerogel
          if ( UNLIKELY( Rich::Aerogel == rad ) )
          {
            // get the radiator intersections
            const auto & radInts = segment.radIntersections();
            
            // normalise over each intersection
            PhotonYields::Type totPlength(0), waveIndepTrans(0);
            if ( !radInts.empty() )
            {
              // average energy for this range
              const auto avEn = spectra.binEnergy(iEnBin) * Gaudi::Units::eV;
              
              // average over all intersections
              for ( const auto& R : radInts )
              {
                const auto pLen = R.pathLength();
                const auto absL = R.radiator()->absorption()->value(avEn);
                totPlength       += pLen;
                waveIndepTrans   += pLen * Rich::Maths::fast_exp( -pLen / absL );
              }
              if ( totPlength>0 ) { waveIndepTrans /= totPlength; }
              
              // scale the expected photon yield
              nPhot *= waveIndepTrans;
            }
          }

          // bin signal
          const auto binSignal = length * ( nPhot > 0 ? nPhot : 0.0 );
          //_ri_verbo << std::setprecision(9) 
          //          << " bin " << iEnBin << " nPhot " << nPhot 
          //          << " binSignal " << binSignal << endmsg;

          // save in the spectra object
          (spectra.energyDist(id))[iEnBin] = binSignal;
          
          // Add to the total signal
          signal += binSignal;
	  
        } // energy bins
        
      } // above threshold type

      // save the yield for this hypo
      //_ri_verbo << std::setprecision(9) << "final signal " << signal << endmsg;
      yields.setData(id,signal);

    }
    
  }

  return data;
}

//=============================================================================

StatusCode EmittedPhotonYields::umsUpdate()
{
  _ri_debug << "Sellmeir parameter update triggered" << endmsg;

  // Cache photon energies
  for ( const auto rad : Rich::radiators() )
  {
    m_photEnergies[rad] = std::make_pair( m_detParams.get()->minPhotonEnergy(rad),
                                          m_detParams.get()->maxPhotonEnergy(rad) );
  }

  // Retrieve square of particle masses
  for ( const auto pid : Rich::particles() )
  {
    m_particleMassSq[pid] = richPartProps()->massSq(pid);
  }

  // Load radiator sellmeir parameters from XML
  const RadiatorArray<double> selF1 =
    { m_Rich1DE->param<double>("SellAgelF1Param"),
      m_Rich1DE->param<double>("SellC4F10F1Param"),
      m_Rich1DE->param<double>("SellCF4F1Param") };
  const RadiatorArray<double> selF2 =
    { m_Rich1DE->param<double>("SellAgelF2Param"),
      m_Rich1DE->param<double>("SellC4F10F2Param"),
      m_Rich1DE->param<double>("SellCF4F2Param") };
  const RadiatorArray<double> selE1 =
    { m_Rich1DE->param<double>("SellAgelE1Param"),
      m_Rich1DE->param<double>("SellC4F10E1Param"),
      m_Rich1DE->param<double>("SellCF4E1Param") };
  const RadiatorArray<double> selE2 = 
    { m_Rich1DE->param<double>("SellAgelE2Param"),
      m_Rich1DE->param<double>("SellC4F10E2Param"),
      m_Rich1DE->param<double>("SellCF4E2Param") };
  const RadiatorArray<double> molW  =   
    { 0,
      m_Rich1DE->param<double>("GasMolWeightC4F10Param"),
      m_Rich1DE->param<double>("GasMolWeightCF4Param") };
  const RadiatorArray<double> rho   =
    { 0,
      m_Rich1DE->param<double>("RhoEffectiveSellC4F10Param"),
      m_Rich1DE->param<double>("RhoEffectiveSellCF4Param") };
  const auto selLorGasFac = m_Rich1DE->param<double>("SellLorGasFacParam");

  // data caches
  RadiatorArray<double> RXSPscale = {{}};
  RadiatorArray<double> RXSMscale = {{}};
  RadiatorArray<double> REP       = {{}};
  RadiatorArray<double> REM       = {{}};
  RadiatorArray<double> X         = {{}};

  // Initialise the calculations and cache as much as possible for efficiency
  for ( const auto rad : Rich::radiators() )
  {
    const bool isAero = ( Rich::Aerogel == rad );
    const auto RC = ( isAero ? 1.0 : selLorGasFac * rho[rad] / molW[rad] );
    const auto RF = selF1[rad] + selF2[rad];
    const auto RE02 = ( selF1[rad]*selE2[rad]*selE2[rad] +
                        selF2[rad]*selE1[rad]*selE1[rad] ) / RF;
    const auto RE = ( selE2[rad]*selE2[rad] +
                      selE1[rad]*selE1[rad] ) / RF;
    const auto RG = ( selE1[rad]*selE1[rad] *
                      selE2[rad]*selE2[rad] ) / (RF*RE02);
    const auto RH = RE02/RF;
    const auto RM = RE + (2.0*RC);
    const auto RS = RG + (2.0*RC);
    const auto RT = std::sqrt( 0.25*RM*RM - RH*RS );
    const auto RXSP = std::sqrt( (RM/2.0 + RT)/RH );
    const auto RXSM = std::sqrt( (RM/2.0 - RT)/RH );
    REP[rad] = std::sqrt(RE02) * RXSP;
    REM[rad] = std::sqrt(RE02) * RXSM;
    RXSPscale[rad] = (RXSP - 1.0/RXSP);
    RXSMscale[rad] = (RXSM - 1.0/RXSM);
    X[rad] = (3.0*RC*std::sqrt(RE02)/(4.0*RT));
  }

  // Cache the paraW diff calculation for each energy bin

  // lambda function for the paraW calculation
  auto paraW = 
    ( [&RXSPscale,&RXSMscale,&REP,&REM,&X]
      ( const Rich::RadiatorType rad, const double energy )
      {
        const auto A = ( RXSPscale[rad] *
                         Rich::Maths::fast_log( (REP[rad]+energy)/(REP[rad]-energy) ) );
        const auto B = ( RXSMscale[rad] *
                         Rich::Maths::fast_log( (REM[rad]+energy)/(REM[rad]-energy) ) );
        return X[rad] * ( A - B );
      } );

  // Loop over radiators
  for ( const auto rad : Rich::radiators() )
  {
    // photon energies for this rad
    const auto & en = m_photEnergies[rad];
    
    // temporary photon spectra object
    PhotonSpectra spectra( en.first, en.second );
    
    // loop over the energy bins
    for ( unsigned int iEnBin = 0; iEnBin < spectra.energyBins(); ++iEnBin )
    {
      // cache the paraW(rad,topEn) - paraW(rad,botEn) factor
      const auto botEn = spectra.binEnergyLowerEdge(iEnBin);
      const auto topEn = spectra.binEnergyUpperEdge(iEnBin);
      (m_paraWDiff[rad])[iEnBin] = paraW(rad,topEn) - paraW(rad,botEn);
    }
  } 

  return StatusCode::SUCCESS;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( EmittedPhotonYields )

//=============================================================================
