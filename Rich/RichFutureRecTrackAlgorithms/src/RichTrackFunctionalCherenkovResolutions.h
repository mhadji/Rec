
#pragma once

// STL
#include <string>
#include <algorithm>

// Gaudi Kernel
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/SystemOfUnits.h"

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// Base class
//#include "RichFutureRecBase/RichRecHistoAlgBase.h"
#include "RichFutureRecBase/RichRecAlgBase.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecMassHypoRings.h"

// Utils
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"
#include "RichUtils/RichGeomFunctions.h"
#include "RichUtils/FastMaths.h"

// Interfaces
#include "RichInterfaces/IRichRefractiveIndex.h"
#include "RichInterfaces/IRichDetParameters.h"

// DetDesc
#include "DetDesc/ITransportSvc.h"
#include "DetDesc/TransportSvcException.h"

namespace Rich
{
  namespace Future
  {

    // Use the functional framework
    using namespace Gaudi::Functional;

    namespace Rec
    {

      /** @class TrackFunctionalCherenkovResolutions RichTrackFunctionalCherenkovResolutions.h
       *
       *  Computes the expected Cherenkov resolutions for the given track segments
       *
       *  @author Chris Jones
       *  @date   2016-09-30
       */
      class TrackFunctionalCherenkovResolutions final :
        public Transformer< CherenkovResolutions::Vector( const LHCb::RichTrackSegment::Vector&,
                                                          const CherenkovAngles::Vector&,
                                                          const MassHypoRingsVector& ),
                            Traits::BaseClass_t<AlgBase> >
      {

      public:

        /// Standard constructor
        TrackFunctionalCherenkovResolutions( const std::string& name,
                                             ISvcLocator* pSvcLocator );

        /// Initialization after creation
        StatusCode initialize() override;

      public:

        /// Algorithm execution via transform
        CherenkovResolutions::Vector
          operator()( const LHCb::RichTrackSegment::Vector& segments,
                      const CherenkovAngles::Vector& ckAngles,
                      const MassHypoRingsVector& massRings ) const override;

      private:

        /// Access the alternate geometry if required
        inline IGeometryInfo * altGeom() const
        {
          return ( m_altGeom ? m_altGeom->geometry() : nullptr );
        }

      private:

        /// Pointer to general refractive index tool
        ToolHandle<const IRefractiveIndex> m_refIndex
        { "Rich::Future::TabulatedRefractiveIndex/RefIndex", this };

        /// Detector parameters tool
        ToolHandle<const IDetParameters> m_detParams
        { "Rich::Future::DetParameters/DetParams:PUBLIC", this };

        /// Transport Service
        ITransportSvc * m_transSvc = nullptr;

        /// Overall scale factors for each radiator
        Gaudi::Property< RadiatorArray<double> > m_scale
        { this, "ScaleFactor", { 1.0,    1.0,    1.0    } };

        /// Radiation length / unit length for each radiator
        RadiatorArray<double> m_radLenPerUnitL = {{ 0, 3.46137e-05, 1.14196e-05 }};

        /// Flag to turn on the full treatment of MS using the TS
        Gaudi::Property< RadiatorArray<bool> > m_useTSForMS
        { this, "UseTSForMultScat", { true, false, false } };

        /// Flag to turn on the caching of the radiation length parameter
        Gaudi::Property< RadiatorArray<bool> > m_cacheRadLenP
        { this, "CacheRadLenParam", { false, true, true } };

        /// RICH PD contributions to CK theta resolution
        Gaudi::Property< RadiatorArray<double> > m_pdErr 
        { this, "PDErrors", { 0.0005, 0.0006, 0.0002 } };

        /// RICH PD reference areas (mm^2).
        Gaudi::Property< RadiatorArray<double> > m_pdRefArea 
        { this, "PDRefAreas", { 5.76, 5.76, 5.76 } };

        /// Flag to turn on the full PD area treatment per radiator
        Gaudi::Property< RadiatorArray<bool> > m_fullPDAreaTreatment
        { this, "FullPDAreaTreatment", { false, false, false } };

        /// Absolute max CK theta resolution per radiator
        Gaudi::Property< RadiatorArray<float> > m_maxRes
        { this, "MaxCKThetaRes", { 0.003,  0.0025, 0.001 } };

        /** Flag to turn on the use of an alternative geometry for
         *  the radiation length calculation */
        Gaudi::Property<bool> m_useAltGeom { this, "UseAltGeom", false };

        /// The location of the fast geometry to use
        Gaudi::Property<std::string> m_altGeomLoc 
        { this, "AltGeomLoc", "/dd/TrackfitGeometry/Structure/LHCb" };

        /// Pointer to the DetElm for the alternate geometry
        IDetectorElement * m_altGeom = nullptr;

      private: // parameters

        /// Scattering coefficent. should be used with p in GeV
        const double m_scatt = 13.6e-03;

      };

    }
  }
}
