
# Returns a configured monitoring (MC free) sequence.
def RichRecMonitors( GroupName = "", # Optional name given to this group.

                     #===========================================================
                     # General Data processing options
                     #===========================================================
                     
                     # The data type
                     dataType = "",
                     
                     # Flag to indicate we are running in 'Online Brunel' mode
                     onlineBrunelMode = False,

                     # radiators to process
                     radiators = ["Rich1Gas","Rich2Gas"],
                     
                     #===========================================================
                     # Input / Output options
                     #===========================================================
                     
                     # Dict of input track locations with name key
                     inputTrackLocations = { "All" : "Rec/Track/Best" },
                     
                     # Dict of output PID locations. Name keys must match above.
                     outputPIDLocations  = { "All" : "Rec/Rich/PIDs"  },

                     #===========================================================
                     # Monitoring options
                     #===========================================================

                     # The histogram set to use
                     histograms = "OfflineFull",

                     # Dictionary setting the histograms for each set
                     monitors = { "Expert" : [ "DecodingErrors", "RecoStats",
                                               "RichHits", "PixelClusters", 
                                               "RichMaterial",
                                               "TrackSelEff", "PhotonCherenkovAngles" ],
                                  "OfflineFull" : [ "DecodingErrors", "RecoStats",
                                                    "RichHits", "PixelClusters", 
                                                    "TrackSelEff", "PhotonCherenkovAngles" ],
                                  "OfflineExpress" : [ "DecodingErrors", "RecoStats",
                                                       "RichHits", "PixelClusters", 
                                                       "TrackSelEff", "PhotonCherenkovAngles" ],
                                  "Online" : [ "DecodingErrors", "RecoStats",
                                               "RichHits", "PixelClusters", 
                                               "TrackSelEff", "PhotonCherenkovAngles" ],
                                  "None"           : [ ]
                                  },

                     #===========================================================
                     # Technical options
                     #===========================================================

                     # Turn on CPU time measurements
                     MeasureTime = True

                     ) :

    from Configurables import GaudiSequencer
    from RichFutureRecSys.ConfiguredRichReco import RichTESTrackMap, makeRichAlg, CheckConfig

    # Check the configuration
    CheckConfig( inputTrackLocations, outputPIDLocations )

    # Check histogram set is known
    if histograms not in monitors.keys() :
        raise ValueError("Unknown histogram set "+histograms)

    # Dictionary of general algorithm properties
    algprops = { }

    # RICH Particle properties
    ppTool = "Rich::Future::ParticleProperties/RichPartProp"+GroupName+":PUBLIC"
    algprops["ParticlePropertiesTool"] = ppTool

    # Units
    GeV = 1000

    # The complete sequence
    all = GaudiSequencer( "RichMoni"+GroupName, MeasureTime = MeasureTime )

    # First track independent monitors
    
    # Decoding errors
    if "DecodingErrors" in monitors[histograms] :
        from Configurables import Rich__Future__Rec__Moni__DecodingErrors as DecodingErrors
        decodeErrs = makeRichAlg( DecodingErrors, "RichDecodingErrors"+GroupName, algprops )
        all.Members += [decodeErrs]

    # Rich Hits
    if "RichHits" in monitors[histograms] :
        from Configurables import Rich__Future__Rec__Moni__DetectorHits as DetectorHits
        richHits = makeRichAlg( DetectorHits, "RichRecPixelQC"+GroupName, algprops )
        all.Members += [richHits]

    # clusters
    if "PixelClusters" in monitors[histograms] :
        from Configurables import Rich__Future__SmartIDClustering as RichClustering
        from Configurables import Rich__Future__Rec__Moni__PixelClusters as ClusterMoni
        # Custom cluster location for monitor. Need to run clustering here as it
        # might not be enabled in the reco itself
        clusLoc = "Rec/Rich/PixelClusters/Monitoring"
        # The clustering
        clustering = makeRichAlg( RichClustering, "RichFutureMoniClustering"+GroupName, algprops )
        clustering.ApplyPixelClustering = (True,True)
        clustering.RichPixelClustersLocation = clusLoc
        # The monitoring
        clusMoni = makeRichAlg( ClusterMoni, "RichRecPixelClusters"+GroupName, algprops )
        clusMoni.RichPixelClustersLocation = clusLoc 
        # Add to the sequence
        all.Members += [ clustering, clusMoni ]

    # Loop over tracks
    for tktype,trackLocation in inputTrackLocations.iteritems() :

        # name for this sequence
        name = tktype + GroupName

        # Sequence for all moniotors for this track type
        tkSeq = GaudiSequencer( "RichMoni"+name, MeasureTime = MeasureTime )
        all.Members += [tkSeq]

        # ==================================================================
        # Intermediary data locations. 
        # Eventually should be handled by the framework
        # ==================================================================
        locs = RichTESTrackMap(name)

        # Basic reco stats.
        if "RecoStats" in monitors[histograms] :
            from Configurables import Rich__Future__Rec__Moni__SIMDRecoStats as RecoStats
            recoStats = makeRichAlg( RecoStats, "RichRecoStats"+name, algprops )
            # Inputs
            recoStats.TrackSegmentsLocation   = locs["TrackSegmentsLocation"]
            recoStats.TrackToSegmentsLocation = locs["SelectedTrackToSegmentsLocation"]
            recoStats.CherenkovPhotonLocation = locs["CherenkovPhotonLocation"]
            # Add to sequence
            tkSeq.Members += [recoStats] 

        if "TrackSelEff" in monitors[histograms] :
            from Configurables import Rich__Future__Rec__Moni__TrackSelEff as TrackSelEff
            tkSelEff = makeRichAlg( TrackSelEff, "Ri"+tktype+"TrkEff"+GroupName, algprops )
            # Inputs
            tkSelEff.TracksLocation   = trackLocation
            tkSelEff.RichPIDsLocation = outputPIDLocations[tktype]
            # Add to sequence
            tkSeq.Members += [tkSelEff] 

        if "PhotonCherenkovAngles" in monitors[histograms] :
            
            from Configurables import Rich__Future__Rec__Moni__SIMDPhotonCherenkovAngles as PhotAngles
            from Configurables import TrackSelector
            
            # Standard monitor
            ckAngles = makeRichAlg( PhotAngles, "RiCKRes"+name, algprops )
            # Inputs
            ckAngles.TracksLocation           = trackLocation
            ckAngles.TrackSegmentsLocation    = locs["TrackSegmentsLocation"]
            ckAngles.CherenkovPhotonLocation  = locs["CherenkovPhotonLocation"]
            ckAngles.CherenkovAnglesLocation  = locs["SignalCherenkovAnglesLocation"]
            ckAngles.SummaryTracksLocation    = locs["SummaryTracksLocation"]
            ckAngles.PhotonToParentsLocation  = locs["PhotonToParentsLocation"]
            # Options
            if onlineBrunelMode : 
                # Open up the CK res plot range, for the Wide photon selection
                ckAngles.CKResHistoRange = ( 0.05, 0.008, 0.004 )

            # Monitor with tight tracking cuts
            ckAnglesT = makeRichAlg( PhotAngles, "RiCKRes"+name+"Tight", algprops )
             # Inputs
            ckAnglesT.TracksLocation           = trackLocation
            ckAnglesT.TrackSegmentsLocation    = locs["TrackSegmentsLocation"]
            ckAnglesT.CherenkovPhotonLocation  = locs["CherenkovPhotonLocation"]
            ckAnglesT.CherenkovAnglesLocation  = locs["SignalCherenkovAnglesLocation"]
            ckAnglesT.SummaryTracksLocation    = locs["SummaryTracksLocation"]
            ckAnglesT.PhotonToParentsLocation  = locs["PhotonToParentsLocation"]
            # Options
            if onlineBrunelMode :
                # Clone settings from the nominal monitor
                ckAnglesT.CKResHistoRange           = ckAngles.CKResHistoRange
            # Tracks selection
            ckAnglesT.addTool(TrackSelector)
            ckAnglesT.TrackSelector.MinPCut         = 10*GeV
            ckAnglesT.TrackSelector.MinPtCut        = 0.5*GeV
            ckAnglesT.TrackSelector.MaxChi2Cut      = 2
            ckAnglesT.TrackSelector.MaxGhostProbCut = 0.1

            # Add to sequence
            tkSeq.Members += [ckAngles,ckAnglesT]

        if "RichMaterial" in monitors[histograms] :

            from Configurables import Rich__Future__Rec__Moni__TrackRadiatorMaterial as TkMaterial
            
            tkMat = makeRichAlg( TkMaterial, "RiTkMaterial"+name, algprops )
            # Inputs
            tkMat.TrackSegmentsLocation = locs["TrackSegmentsLocation"]

            # Add to sequence
            tkSeq.Members += [tkMat]

    # return the full monitoring sequence
    return all

# Returns a configured MC checking sequence.
def RichRecCheckers( GroupName = "", # Optional name given to this group.

                     #===========================================================
                     # General Data processing options
                     #===========================================================
                     
                     # radiators to process
                     radiators = ["Rich1Gas","Rich2Gas"],

                     # Flag to indicate we are running in 'Online Brunel' mode
                     onlineBrunelMode = False,

                     #===========================================================
                     # Input / Output options
                     #===========================================================
                     
                     # Dict of input track locations with name key
                     inputTrackLocations = { "All" : "Rec/Track/Best" },
                     
                     # Dict of output PID locations. Name keys must match above.
                     outputPIDLocations  = { "All" : "Rec/Rich/PIDs"  },

                     #===========================================================
                     # Monitoring options
                     #===========================================================

                     # The histogram set to use
                     histograms = "OfflineFull",

                     # List of monitors to run
                     checkers = { "Expert"         : ["PIDPerformance","PhotonCherenkovAngles"],
                                  "OfflineFull"    : ["PIDPerformance","PhotonCherenkovAngles"],
                                  "OfflineExpress" : ["PIDPerformance","PhotonCherenkovAngles"],
                                  "Online"         : ["PIDPerformance","PhotonCherenkovAngles"],
                                  "None"           : [ ]
                                  },

                     # Momentum selections for performance plots
                     momentumCuts = { "2to100"  : [ 2, 100  ],
                                      "2to10"   : [ 2, 10   ],
                                      "10to70"  : [ 10, 70  ],
                                      "70to100" : [ 70, 100 ] },

                     #===========================================================
                     # Techical options
                     #===========================================================
                     
                     # Turn on CPU time measurements
                     MeasureTime = True
                     
                     ) :

    from Configurables import GaudiSequencer
    from RichFutureRecSys.ConfiguredRichReco import ( RichTESTrackMap, RichTESPixelMap,
                                                      makeRichAlg, CheckConfig )

    # Check the configuration
    CheckConfig( inputTrackLocations, outputPIDLocations )

    # Check histogram set is known
    if histograms not in checkers.keys() :
        raise ValueError("Unknown histogram set "+histograms)

    # Units
    GeV = 1000

    # Dictionary of general algorithm properties
    algprops = { }

    # RICH Particle properties
    ppTool = "Rich::Future::ParticleProperties/RichPartProp"+GroupName+":PUBLIC"
    algprops["ParticlePropertiesTool"] = ppTool

    # Radiators ( Aerogel not supported here )
    radsF = ( False, "Rich1Gas" in radiators, "Rich2Gas" in radiators )
    algprops["Radiators"] = radsF
    # Detectors
    algprops["Detectors"] = ( radsF[0] or radsF[1], radsF[2] )

    # Name for pixel locations
    pixname = ""
    if algprops["Detectors"][0] : pixname += "RICH1"
    if algprops["Detectors"][1] : pixname += "RICH2"

    # Pixel TES locations
    cLocs = RichTESPixelMap(pixname)

    # The complete sequence
    all = GaudiSequencer( "RichCheck"+GroupName, MeasureTime = MeasureTime )

    # Make the Track->MCparticle relations table
    from Configurables import Rich__Future__MC__TrackToMCParticleRelations as TkToMCPRels
    tkMCPRels = makeRichAlg( TkToMCPRels, "RichFutureTKToMCPsRels"+GroupName, algprops )
    all.Members += [tkMCPRels]

    # Loop over tracks
    for tktype,trackLocation in inputTrackLocations.iteritems() :

        # name for this sequence
        name = tktype + GroupName
        
        # Sequence for all moniotors for this track type
        tkSeq = GaudiSequencer( "RichCheck"+name, MeasureTime = MeasureTime )
        all.Members += [tkSeq]

        # ==================================================================
        # Intermediary data locations. 
        # Eventually should be handled by the framework
        # ==================================================================
        locs = RichTESTrackMap(name)

        if "PIDPerformance" in checkers[histograms] :

            pidSeq = GaudiSequencer( "RichPIDCheck"+name, MeasureTime = MeasureTime )
            tkSeq.Members += [pidSeq]

            from Configurables import Rich__Future__Rec__MC__Moni__PIDQC as PIDQC
            from Configurables import TrackSelector

            for cutname,cuts in momentumCuts.iteritems() :

                pidQC = makeRichAlg( PIDQC, "RichPIDMon"+name+cutname, algprops )
                # Inputs
                pidQC.TracksLocation   = trackLocation
                pidQC.RichPIDsLocation = outputPIDLocations[tktype]
                pidQC.TrackToMCParticlesRelations = tkMCPRels.TrackToMCParticlesRelations
                # Cuts
                pidQC.addTool(TrackSelector)
                pidQC.TrackSelector.MinPCut = cuts[0]*GeV
                pidQC.TrackSelector.MaxPCut = cuts[1]*GeV
                # Add to sequence
                pidSeq.Members += [pidQC]

        if "PhotonCherenkovAngles" in checkers[histograms] :

            from Configurables import Rich__Future__Rec__MC__Moni__SIMDPhotonCherenkovAngles as MCCKAngles

            # Standard monitor
            mcAngs = makeRichAlg( MCCKAngles, "RiCKMCRes"+name, algprops )
            # Inputs
            mcAngs.TrackSegmentsLocation          = locs["TrackSegmentsLocation"]
            mcAngs.CherenkovPhotonLocation        = locs["CherenkovPhotonLocation"]
            mcAngs.CherenkovAnglesLocation        = locs["SignalCherenkovAnglesLocation"]
            mcAngs.SummaryTracksLocation          = locs["SummaryTracksLocation"]
            mcAngs.PhotonToParentsLocation        = locs["PhotonToParentsLocation"]
            mcAngs.TrackToMCParticlesRelations    = tkMCPRels.TrackToMCParticlesRelations
            mcAngs.RichPixelClustersLocation      = cLocs["RichPixelClustersLocation"]
            mcAngs.RichSIMDPixelSummariesLocation = cLocs["RichSIMDPixelSummariesLocation"]
            mcAngs.TracksLocation                 = trackLocation
            # Options
            if onlineBrunelMode : 
                # Open up the CK res plot range, for the Wide photon selection
                mcAngs.CKResHistoRange = ( 0.05, 0.008, 0.004 )
            # Add to sequence
            tkSeq.Members += [mcAngs]

            # Tight monitor
            mcAngsT = makeRichAlg( MCCKAngles, "RiCKMCRes"+name+"Tight", algprops )
            # Inputs
            mcAngsT.TrackSegmentsLocation          = locs["TrackSegmentsLocation"]
            mcAngsT.CherenkovPhotonLocation        = locs["CherenkovPhotonLocation"]
            mcAngsT.CherenkovAnglesLocation        = locs["SignalCherenkovAnglesLocation"]
            mcAngsT.SummaryTracksLocation          = locs["SummaryTracksLocation"]
            mcAngsT.PhotonToParentsLocation        = locs["PhotonToParentsLocation"]
            mcAngsT.TrackToMCParticlesRelations    = tkMCPRels.TrackToMCParticlesRelations
            mcAngsT.RichPixelClustersLocation      = cLocs["RichPixelClustersLocation"]
            mcAngsT.RichSIMDPixelSummariesLocation = cLocs["RichSIMDPixelSummariesLocation"]
            mcAngsT.TracksLocation                 = trackLocation
            # Options
            if onlineBrunelMode : 
                # Open up the CK res plot range, for the Wide photon selection
                mcAngsT.CKResHistoRange = mcAngs.CKResHistoRange
            # Tracks selection
            mcAngsT.addTool(TrackSelector)
            mcAngsT.TrackSelector.MinPCut         = 10*GeV
            mcAngsT.TrackSelector.MinPtCut        = 0.5*GeV
            mcAngsT.TrackSelector.MaxChi2Cut      = 2
            mcAngsT.TrackSelector.MaxGhostProbCut = 0.1
            # Add to sequence
            tkSeq.Members += [mcAngsT]

    # return the full monitoring sequence
    return all
