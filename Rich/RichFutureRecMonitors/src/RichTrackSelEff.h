
#pragma once

// STD
#include <mutex>
#include <algorithm>

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Event model
#include "Event/Track.h"
#include "Event/RichPID.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {
      namespace Moni
      {
        
        // Use the functional framework
        using namespace Gaudi::Functional;
        
        /** @class TrackSelEff RichTrackSelEff.h
         *
         *  Monitors the efficiency of the RICH track selection and processing.
         *  I.e. the efficiency for which a RichPID object is produced.
         *
         *  @author Chris Jones
         *  @date   2016-12-12
         */
        
        class TrackSelEff final
          : public Consumer< void( const LHCb::Track::Selection&,
                                   const LHCb::RichPIDs& ),
                             Traits::BaseClass_t<HistoAlgBase> >
        {
        
        public:
          
          /// Standard constructor
          TrackSelEff( const std::string& name, ISvcLocator* pSvcLocator );

        public:
          
          /// Functional operator
          void operator()( const LHCb::Track::Selection& tracks,
                           const LHCb::RichPIDs& pids ) const override;

        protected:
          
          /// Pre-Book all histograms
          StatusCode prebookHistograms() override;
          
        private:
          
          // Book the histos for the given track class
          void prebookHistograms( const std::string & tkClass );

          // track plots
          void trackPlots( const std::string & tag );

          /// Fill track plots for given class tag
          void fillTrackPlots( const LHCb::Track * track,
                               const bool sel,
                               const std::string & tkClass ) const;
          
        private:
          
          /// mutex lock
          mutable std::mutex m_updateLock;
          
        };
      
      }
    }
  }
}
