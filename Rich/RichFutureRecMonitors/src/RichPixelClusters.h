
#pragma once

// STD
#include <mutex>

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Rich Utils
#include "RichUtils/RichPixelCluster.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {
      namespace Moni
      {
        
        // Use the functional framework
        using namespace Gaudi::Functional;
        
        /** @class PixelClusters RichPixelClusters.h
         *
         *  Monitors the RICH pixel clusters.
         *
         *  @author Chris Jones
         *  @date   2016-12-06
         */
        
        class PixelClusters final
          : public Consumer< void( const Rich::PDPixelCluster::Vector& ),
                             Traits::BaseClass_t<HistoAlgBase> >
        {
        
        public:
          
          /// Standard constructor
          PixelClusters( const std::string& name, ISvcLocator* pSvcLocator );

        public:
          
          /// Functional operator
          void operator()( const Rich::PDPixelCluster::Vector& clusters ) const override;

        protected:
          
          /// Pre-Book all histograms
          StatusCode prebookHistograms() override;

        private:

          /// mutex lock
          mutable std::mutex m_updateLock;
          
        };
      
      }
    }
  }
}
