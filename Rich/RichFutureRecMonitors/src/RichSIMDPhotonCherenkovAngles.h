
#pragma once

// STD
#include <algorithm>
#include <sstream>

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiKernel/PhysicalConstants.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Event Model
#include "RichFutureRecEvent/RichSummaryEventData.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"

// Rich Utils
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"
#include "RichUtils/RichHPDIdentifier.h"

// FPE exception protection
#include "Kernel/FPEGuard.h"

// Track selector
#include "TrackInterfaces/ITrackSelector.h"

// RichDet
#include "RichDet/DeRichSystem.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {
      namespace Moni
      {
        
        // Use the functional framework
        using namespace Gaudi::Functional;
        
        /** @class SIMDPhotonCherenkovAngles RichSIMDPhotonCherenkovAngles.h
         *
         *  Monitors the reconstructed cherenkov angles.
         *
         *  @author Chris Jones
         *  @date   2016-12-12
         */
        
        class SIMDPhotonCherenkovAngles final
          : public Consumer< void( const LHCb::Track::Selection&,
                                   const Summary::Track::Vector&,
                                   const Relations::PhotonToParents::Vector&,
                                   const LHCb::RichTrackSegment::Vector&,
                                   const CherenkovAngles::Vector&,
                                   const SIMDCherenkovPhoton::Vector& ),
                             Traits::BaseClass_t<HistoAlgBase> >
        {
        
        public:
          
          /// Standard constructor
          SIMDPhotonCherenkovAngles( const std::string& name, ISvcLocator* pSvcLocator );

          /// Initialize
          StatusCode initialize() override;

        public:
          
          /// Functional operator
          void operator()( const LHCb::Track::Selection& tracks,
                           const Summary::Track::Vector& sumTracks,
                           const Relations::PhotonToParents::Vector& photToSegPix,
                           const LHCb::RichTrackSegment::Vector& segments,
                           const CherenkovAngles::Vector& expTkCKThetas,
                           const SIMDCherenkovPhoton::Vector& photons ) const override;

        protected:
          
          /// Pre-Book all histograms
          StatusCode prebookHistograms() override;

        private:

          /// Get the per PD resolution histogram ID
          inline std::string pdResPlotID( const LHCb::RichSmartID hpd ) const
          {
            const Rich::DAQ::HPDIdentifier hid(hpd);
            std::ostringstream id;
            id << "PDs/pd-" << hid.number();
            return id.str();
          }
          
        private:

          /// Pointer to RICH system detector element
          const DeRichSystem * m_RichSys = nullptr;

          /// Which radiators to monitor
          Gaudi::Property< RadiatorArray<bool> > m_rads 
          { this, "Radiators", { false, true, true } };

          /// minimum beta value for tracks
          Gaudi::Property< RadiatorArray<double> > m_minBeta 
          { this, "MinBeta", { 0.9999,  0.9999,  0.9999 } };

          /// maximum beta value for tracks
          Gaudi::Property< RadiatorArray<double> > m_maxBeta
          { this, "MaxBeta", { 999.99,  999.99,  999.99 } };

          /// Min theta limit for histos for each rad
          Gaudi::Property< RadiatorArray<double> > m_ckThetaMin 
          { this, "ChThetaRecHistoLimitMin", { 0.150,   0.030,   0.010  } };
          
          /// Max theta limit for histos for each rad
          Gaudi::Property< RadiatorArray<double> > m_ckThetaMax 
          { this, "ChThetaRecHistoLimitMax", { 0.325,   0.065,   0.036  } };

          /// Histogram ranges for CK resolution plots
          Gaudi::Property< RadiatorArray<double> > m_ckResRange
          { this, "CKResHistoRange", { 0.025,   0.005,   0.0025 } };

          /// Enable the per PD resolution plots, for the given radiators
          Gaudi::Property< RadiatorArray<bool> > m_pdResPlots
          { this, "EnablePerPDPlots", { false, false, false } };

          /** Track selector.
           *  Longer term should get rid of this and pre-filter the input data instead.
           */
          ToolHandle<const ITrackSelector> m_tkSel{ "TrackSelector", this };

        };
      
      }
    }
  }
}
