
// local
#include "RichDetectorHits.h"

using namespace Rich::Future::Rec::Moni;

//-----------------------------------------------------------------------------
// Implementation file for class : DetectorHits
//
// 2016-12-06 : Chris Jones
//-----------------------------------------------------------------------------

DetectorHits::DetectorHits( const std::string& name, ISvcLocator* pSvcLocator )
  : Consumer( name, pSvcLocator,
              KeyValue{ "DecodedDataLocation", Rich::DAQ::L1MapLocation::Default } )
{ }

//-----------------------------------------------------------------------------

StatusCode DetectorHits::prebookHistograms()
{
  StatusCode sc = StatusCode::SUCCESS;
  for ( const auto rich : Rich::detectors() )
  {
    sc = sc && richHisto1D( Rich::HistogramID("nTotalPixsPerPD",rich),
                            "Average overall PD occupancy (nHits>0)",
                            0.5, 150.5, 150 );
    sc = sc && richHisto1D( Rich::HistogramID("nTotalPixs",rich), 
                            "Overall occupancy (nHits>0)", 0, m_maxPixels, nBins1D() );
    sc = sc && richHisto1D( Rich::HistogramID("nActivePDs",rich), 
                            "# Active PDs (nHits>0)", -0.5, 300.5, 301 );
  }

  return sc;
}

//-----------------------------------------------------------------------------

void DetectorHits::operator()( const Rich::DAQ::L1Map& data ) const
{

  // Count active PDs in each RICH
  DetectorArray<unsigned long long> activePDs = {{}};
  // Count hits in each RICH
  DetectorArray<unsigned long long> richHits = {{}};

  // the lock
  std::lock_guard<std::mutex> lock(m_updateLock);

  // Loop over L1 boards
  for ( const auto & L1 : data )
  {
    // loop over ingresses for this L1 board
    for ( const auto & In : L1.second )
    {
      // Loop over HPDs in this ingress
      for ( const auto & PD : In.second.pdData() )
      {

        // PD ID
        const auto pd = PD.second.pdID();
        if ( pd.isValid() && !PD.second.header().inhibit() )
        {

          // Vector of SmartIDs
          const auto & rawIDs = PD.second.smartIDs();
          // RICH
          const auto rich = pd.rich();

          // Do we have any hits
          if ( !rawIDs.empty() ) 
          { 
            // Fill average HPD occ plot
            richHisto1D( Rich::HistogramID("nTotalPixsPerPD",rich) ) -> fill(rawIDs.size());
            // count active PDs
            ++activePDs[rich];
            // count hits
            richHits[rich] += rawIDs.size();
          }

        }

      }
    }
  }

  // Loop over RICHes
  for ( const auto rich : Rich::detectors() )
  {
    // Fill active PD plots
    richHisto1D( Rich::HistogramID("nActivePDs",rich) )->fill( activePDs[rich] );
    // Fill RICH hits plots
    if ( richHits[rich] > 0 )
    {
      richHisto1D(Rich::HistogramID("nTotalPixs",rich)) -> fill ( richHits[rich] );
    }
  }

}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( DetectorHits )

//-----------------------------------------------------------------------------
