
// local
#include "RichPixelClusterGlobalPositions.h"

using namespace Rich::Future::Rec;

//-----------------------------------------------------------------------------
// Implementation file for class : RichPixelClusterGlobalPositions
//
// 2016-09-30 : Chris Jones
//-----------------------------------------------------------------------------

PixelClusterGlobalPositions::
PixelClusterGlobalPositions( const std::string& name, 
                             ISvcLocator* pSvcLocator )
  : Transformer ( name, pSvcLocator,
                  { KeyValue{ "RichPixelClustersLocation", Rich::PDPixelClusterLocation::Default } },
                  { KeyValue{ "RichPixelGlobalPositionsLocation", SpacePointLocation::PixelsGlobal } } )
{
  declareProperty( "SmartIDTool", m_idTool );
  //setProperty( "OutputLevel", MSG::DEBUG );
}

//=============================================================================

StatusCode PixelClusterGlobalPositions::initialize()
{
  auto sc = Transformer::initialize();
  if ( !sc ) return sc;

  // load tools
  sc = sc && m_idTool.retrieve();

  return sc;
}

//=============================================================================

SpacePointVector 
PixelClusterGlobalPositions::operator()( const Rich::PDPixelCluster::Vector& clusters ) const
{
  // the container to return
  SpacePointVector points;
  points.reserve( clusters.size() );

  // Iterate over the clusters and create the global space point info for each
  for ( const auto & clus : clusters )
  {
    // This should be made cleaner sometime ...
    points.emplace_back( );
    const auto ok = m_idTool.get()->globalPosition( clus, points.back() );
    if ( UNLIKELY(!ok) )
    { _ri_debug << "Failed to compute global position for " << clus << endmsg; }
    else
    { _ri_debug << clus.primaryID() << " " << points.back() << endmsg; }
  }

  // return the final space points
  return points;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PixelClusterGlobalPositions )

//=============================================================================
