
// local
#include "RichSIMDSummaryPixels.h"

using namespace Rich::Future::Rec;

//-----------------------------------------------------------------------------

SIMDSummaryPixels::SIMDSummaryPixels( const std::string& name, 
                                      ISvcLocator* pSvcLocator )
  : Transformer ( name, pSvcLocator,
                  { KeyValue{ "RichPixelClustersLocation",    Rich::PDPixelClusterLocation::Default },
                    KeyValue{ "RichPixelGlobalPositionsLocation", SpacePointLocation::PixelsGlobal },
                    KeyValue{ "RichPixelLocalPositionsLocation",  SpacePointLocation::PixelsLocal } },
                  { KeyValue{ "RichSIMDPixelSummariesLocation",  SIMDPixelSummariesLocation::Default } } )
{
  //setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================

SIMDPixelSummaries
SIMDSummaryPixels::operator()( const Rich::PDPixelCluster::Vector& clusters,
                               const SpacePointVector& gPoints,
                               const SpacePointVector& lPoints ) const
{
  // View range for pixel data
  const auto allPixels = Ranges::ConstZip( clusters, gPoints, lPoints );
  
  // Pixel Summaries
  SIMDPixelSummaries summaries;
  summaries.reserve( ( allPixels.size() / SIMDPixel::SIMDFP::Size ) + 4 );
  
  // last RICH
  Rich::DetectorType lastRich { Rich::InvalidDetector };
  // last side
  Rich::Side         lastSide { Rich::InvalidSide     };

  // Working SIMD values
  SIMDPixel::SIMDFP gX(0), gY(0), gZ(0); // global positions
  SIMDPixel::SIMDFP lX(0), lY(0), lZ(0); // local positions
  SIMDPixel::SIMDFP effArea(0);          // effective area
  SIMDPixel::ScIndex scClusIn(-1);       // indices to original clusters
  SIMDPixel::Mask mask { false };        // selection mask

  // SIMD index
  std::size_t index = 0;

  // Working Smart IDs
  SIMDPixel::SmartIDs smartIDs;

  // Functor to save an SIMD pixel
  auto savePix = [&]() 
    { 
      summaries.emplace_back( lastRich, lastSide, smartIDs,
                              SIMDPixel::Point(gX,gY,gZ),
                              SIMDPixel::Point(lX,lY,lZ),
                              effArea, scClusIn, mask ); 
      // reset
      index = 0;
    };

  // Functor to add padding info at the end of an incomplete SIMD pixel
  auto addPadding = [&]()
    {
      // fill what has not been set with padding values
      for ( std::size_t i = index; i < SIMDPixel::SIMDFP::Size; ++i )
      {
        effArea[i]    = 0.0;
        gX[i] = lX[i] = 0.0;
        gY[i] = lY[i] = 0.0;
        // Set default z to values roughly right for each RICH, to
        // avoid precision issues in the photon reco later on.
        gZ[i] = lZ[i] = ( Rich::Rich1 == lastRich ? 1600 : 10500 );
        scClusIn[i]   = -1;
        mask[i]       = false;
        smartIDs[i]   = LHCb::RichSmartID();
      }
    };

  // Loop over all pixels
  // Note relying on fact they are sorted here.
  std::size_t clusIn(0);
  for ( auto data = allPixels.begin(); data != allPixels.end(); ++data, ++clusIn )
  {
    // load the data from the tuple
    const auto & cluster = std::get<0>(*data);
    const auto & gloPos  = std::get<1>(*data);
    const auto & locPos  = std::get<2>(*data);
    
    // which RICH and side
    const auto rich = cluster.rich();
    const auto side = cluster.panel();

    // If first time, set RICH and panel
    if ( UNLIKELY( lastRich == Rich::InvalidDetector ) )
    {
      lastRich = rich;
      lastSide = side;
    }
    
    // If different RICH or side, save current data
    if ( UNLIKELY( rich != lastRich || side != lastSide ) )
    {
      // add padding if needed
      addPadding();
      // Save the current info
      savePix();
      // update RICH and panel
      lastRich = rich;
      lastSide = side;
    }

    // Update global position
    gX[index] = gloPos.X();
    gY[index] = gloPos.Y();
    gZ[index] = gloPos.Z();
    // Update local position
    lX[index] = locPos.X();
    lY[index] = locPos.Y();
    lZ[index] = locPos.Z();
    // effective area
    effArea[index] = cluster.dePD()->effectivePixelArea();
    // SmartID
    smartIDs[index] = cluster.primaryID();
    // scalar cluster index
    scClusIn[index] = clusIn;
    // set mask OK for this pixel
    mask[index] = true;
    
    // If this is the last index, push to container and start again
    if ( UNLIKELY( SIMDPixel::SIMDFP::Size-1 == index ) )
    {
      // save an entry
      savePix();
    }
    else
    {
      // increment index for next scalar pixel
      ++index;
    }
 
  }

  // Save last one if needed
  if ( 0 != index ) 
  {
    // add padding if needed
    addPadding();
    // Save the current info
    savePix(); 
  }

  // Update pixel ranges.
  // Note we are storing indices not iterators as these remain valid 
  // under container relocations. Conversion to ranges happens on access.

  // RICH1 top -> bottom boundary
  auto itR1FirstBot =
    std::find_if( summaries.begin(), summaries.end(),
                  []( const auto & p )
                  { return ( Rich::bottom == p.side() ); } );
  // RICH1 -> RICH2 boundary
  auto itFirstR2 = 
    std::find_if( itR1FirstBot, summaries.end(),
                  []( const auto & p )
                  { return ( Rich::Rich2 == p.rich() ); } );
  // RICH2 left -> right boundary
  auto itR2FirstRight =
    std::find_if( itFirstR2, summaries.end(),
                  []( const auto & p )
                  { return ( Rich::right == p.side() ); } );

  const std::size_t first        = 0;
  const std::size_t r2start      = itFirstR2       - summaries.begin();
  const std::size_t r1botstart   = itR1FirstBot    - summaries.begin();
  const std::size_t r2rightstart = itR2FirstRight  - summaries.begin();
  const std::size_t end          = summaries.end() - summaries.begin();

  summaries.setRange( Rich::Rich1, first,   r2start );
  summaries.setRange( Rich::Rich2, r2start, end     );

  summaries.setRange( Rich::Rich1, Rich::top,    first,  r1botstart );
  summaries.setRange( Rich::Rich1, Rich::bottom, r1botstart,r2start );

  summaries.setRange( Rich::Rich2, Rich::left,   r2start, r2rightstart );
  summaries.setRange( Rich::Rich2, Rich::right,  r2rightstart, end     );

  // Some final printout
  if ( msgLevel(MSG::VERBOSE) )
  {
    verbose() << "All pixels " << summaries.size() << endmsg;
    for ( const auto & p : summaries ) { verbose() << " " << p << endmsg; }
    std::size_t rangeSum = 0;
    for ( const auto rich : { Rich::Rich1, Rich::Rich2 } )
    {
      for ( const auto side : { Rich::firstSide, Rich::secondSide } )
      {
        const auto rPixs = summaries.range(rich,side);
        verbose() << rich << " " << Rich::text(rich,side) 
                  << " pixels " << rPixs.size() << endmsg;
        for ( const auto & p : rPixs ) { verbose() << " " << p << endmsg; }
        rangeSum += rPixs.size();
      }
    }
    verbose() << "Total SIMD pixels = " << summaries.size() << " == "
              << rangeSum << endmsg;
  }

  // return
  return summaries;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SIMDSummaryPixels )

//=============================================================================
