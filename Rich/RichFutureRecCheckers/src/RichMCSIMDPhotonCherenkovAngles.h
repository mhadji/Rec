
#pragma once

// STD
#include <algorithm>

// Gaudi
#include "GaudiUtils/Aida2ROOT.h"
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiKernel/PhysicalConstants.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Event Model
#include "RichFutureRecEvent/RichSummaryEventData.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"

// Rich Utils
#include "RichUtils/ZipRange.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/RichPixelCluster.h"

// Event model
#include "Event/MCRichDigitSummary.h"

// Relations
#include "RichFutureMCUtils/RichRecMCHelper.h"

// FPE exception protection
#include "Kernel/FPEGuard.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {
      namespace MC
      {
        namespace Moni
        {
        
        // Use the functional framework
        using namespace Gaudi::Functional;
        
        /** @class SIMDPhotonCherenkovAngles RichSIMDPhotonCherenkovAngles.h
         *
         *  Monitors the reconstructed cherenkov angles.
         *
         *  @author Chris Jones
         *  @date   2016-12-12
         */
          
          class SIMDPhotonCherenkovAngles final
            : public Consumer< void( const Summary::Track::Vector&,
                                     const LHCb::Track::Selection&,
                                     const SIMDPixelSummaries&,
                                     const Rich::PDPixelCluster::Vector&,
                                     const Relations::PhotonToParents::Vector&,
                                     const LHCb::RichTrackSegment::Vector&,
                                     const CherenkovAngles::Vector&,
                                     const SIMDCherenkovPhoton::Vector&,
                                     const Rich::Future::MC::Relations::TkToMCPRels&,
                                     const LHCb::MCRichDigitSummarys& ),
                               Traits::BaseClass_t<HistoAlgBase> >
          {
          
          public:
            
            /// Standard constructor
            SIMDPhotonCherenkovAngles( const std::string& name, ISvcLocator* pSvcLocator );
            
          public:
            
            /// Functional operator
            void operator()( const Summary::Track::Vector& sumTracks,
                             const LHCb::Track::Selection& tracks,
                             const SIMDPixelSummaries& pixels,
                             const Rich::PDPixelCluster::Vector& clusters,
                             const Relations::PhotonToParents::Vector& photToSegPix,
                             const LHCb::RichTrackSegment::Vector& segments,
                             const CherenkovAngles::Vector& expTkCKThetas,
                             const SIMDCherenkovPhoton::Vector& photons,
                             const Rich::Future::MC::Relations::TkToMCPRels& tkrels,
                             const LHCb::MCRichDigitSummarys & digitSums ) const override;
            
          protected:
            
            /// Pre-Book all histograms
            StatusCode prebookHistograms() override;
            
          private:
            
            /// Which radiators to monitor
            Gaudi::Property< RadiatorArray<bool> > m_rads 
            { this, "Radiators", { false, true, true } };
            
            /// minimum beta value for tracks
            Gaudi::Property< RadiatorArray<double> > m_minBeta 
            { this, "MinBeta", { 0.9999,  0.9999,  0.9999 } };
            
            /// maximum beta value for tracks
            Gaudi::Property< RadiatorArray<double> > m_maxBeta
            { this, "MaxBeta", { 999.99,  999.99,  999.99 } };
            
            /// Min theta limit for histos for each rad
            Gaudi::Property< RadiatorArray<double> > m_ckThetaMin 
            { this, "ChThetaRecHistoLimitMin", { 0.150,   0.030,   0.010  } };
            
            /// Max theta limit for histos for each rad
            Gaudi::Property< RadiatorArray<double> > m_ckThetaMax 
            { this, "ChThetaRecHistoLimitMax", { 0.325,   0.065,   0.036  } };
            
            /// Histogram ranges for CK resolution plots
            Gaudi::Property< RadiatorArray<double> > m_ckResRange
            { this, "CKResHistoRange", { 0.025,   0.005,   0.0025 } };

            /// Option to skip electrons
            Gaudi::Property<bool> m_skipElectrons 
            { this, "SkipElectrons", false };
            
          };

        }      
      }
    }
  }
}
  
