
//-----------------------------------------------------------------------------
/** @file RichRecBase.h
 *
 *  Header file for RICH reconstruction base class : Rich::Rec::CommonBase
 *
 *  @author Chris Jones    Christopher.Rob.Jones@cern.ch
 *  @date   2005-08-26
 */
//-----------------------------------------------------------------------------

#pragma once

// Gaudi
#include "GaudiKernel/StatusCode.h"

// Interfaces
#include "RichInterfaces/IRichParticleProperties.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {

      //-----------------------------------------------------------------------------
      /** @class CommonBase RichRecBase.h RichRecBase/RichRecBase.h
       *
       *  Base class containing common RICH reconstruction functionality
       *
       *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
       *  @date   2005-08-26
       */
      //-----------------------------------------------------------------------------

      template <class PBASE>
      class CommonBase
      {

      public:

        /// Standard constructor
        CommonBase( PBASE * base = nullptr );

      protected:

        /// Intialise common RICH Reco
        StatusCode initialiseRichReco();

        /// Finalise common RICH Reco
        StatusCode finaliseRichReco();

      private:

        /// Const access to derived class
        inline const PBASE * base() const noexcept { return m_base; }

        /// Non-const access to derived class
        inline       PBASE * base()       noexcept { return m_base; }

      protected:

        /// Access the particle properties tool
        inline const IParticleProperties * richPartProps() const noexcept 
        { return m_richPartProp.get(); }

        /// Access the list of active Particle ID types to consider
        inline const Rich::Particles& activeParticles() const noexcept 
        { return m_pidTypes; }

        /// The lightest active mass hypothesis
        inline Rich::ParticleIDType lightestActiveHypo() const noexcept
        { return m_pidTypes.front(); }

        /// The heaviest active mass hypothesis
        inline Rich::ParticleIDType heaviestActiveHypo() const noexcept
        { return m_pidTypes.back(); }

      private:

        /// Particle ID types to consider
        Rich::Particles m_pidTypes;

        /// Pointer to derived class
        PBASE * m_base = nullptr; 

        /// Pointer to RichParticleProperties interface
        ToolHandle<const IParticleProperties> m_richPartProp;

      };

    }
  } 
}
