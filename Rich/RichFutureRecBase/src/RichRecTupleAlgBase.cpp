
//-----------------------------------------------------------------------------
/** @file RichRecTupleAlgBase.cpp
 *
 *  Implementation file for RICH reconstruction monitor
 *  algorithm base class : RichRecTupleAlgBase
 *
 *  @author Chris Jones    Christopher.Rob.Jones@cern.ch
 *  @date   2005/01/13
 */
//-----------------------------------------------------------------------------

// local
#include "RichFutureRecBase/RichRecTupleAlgBase.h"

// ============================================================================
// Force creation of templated class
#include "RichRecBase.icpp"
template class Rich::Future::Rec::CommonBase<Rich::Future::TupleAlgBase> ;
// ============================================================================

// ============================================================================
// Standard constructor
// ============================================================================
Rich::Future::Rec::TupleAlgBase::TupleAlgBase( const std::string& name,
                                       ISvcLocator* pSvcLocator )
  : Rich::Future::TupleAlgBase           ( name, pSvcLocator ),
    Rich::Future::Rec::CommonBase<Rich::Future::TupleAlgBase> ( this )
{
}
// ============================================================================

// ============================================================================
// Initialise
// ============================================================================
StatusCode Rich::Future::Rec::TupleAlgBase::initialize()
{
  // Initialise base class
  const StatusCode sc = Rich::Future::TupleAlgBase::initialize();
  if ( sc.isFailure() )
    return Error( "Failed to initialise Rich::TupleAlgBase", sc );

  // Common initialisation
  return initialiseRichReco();
}
// ============================================================================

// ============================================================================
// Main execute method
// ============================================================================
StatusCode Rich::Future::Rec::TupleAlgBase::execute()
{
  // All algorithms should re-implement this method
  return Error ( "Default Rich::RecTupleAlgBase::execute() called !!" );
}
// ============================================================================

// ============================================================================
// Finalize
// ============================================================================
StatusCode Rich::Future::Rec::TupleAlgBase::finalize()
{
  // Common finalisation
  const StatusCode sc = finaliseRichReco();
  if ( sc.isFailure() ) return Error( "Failed to finalise Rich::RecBase", sc );

  // Finalize base class
  return Rich::Future::TupleAlgBase::finalize();
}
// ============================================================================
