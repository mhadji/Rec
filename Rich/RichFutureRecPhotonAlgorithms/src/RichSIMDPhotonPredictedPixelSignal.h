
#pragma once

// STL
#include <iomanip>
#include <limits>

// Gaudi (must be first)
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// Rec Event
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecPhotonYields.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"

// Rich Utils
#include "RichUtils/ZipRange.h"
#include "RichUtils/FastMaths.h"

// Detector Description
#include "RichDet/DeRich1.h"
#include "RichDet/DeRich2.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {

      // Use the functional framework
      using namespace Gaudi::Functional;

      // Shortcut to output data
      namespace { using OutData = SIMDPhotonSignals::Vector; }

      /** @class SIMDPhotonPredictedPixelSignal RichSIMDPhotonPredictedPixelSignal.h
       *
       *  Computes the expected pixel signals for photon candidates.
       *
       *  @author Chris Jones
       *  @date   2016-10-19
       */
      class SIMDPhotonPredictedPixelSignal final :
        public Transformer< OutData( const SIMDPixelSummaries&,
                                     const SIMDCherenkovPhoton::Vector&,
                                     const Relations::PhotonToParents::Vector&,
                                     const LHCb::RichTrackSegment::Vector&,
                                     const CherenkovAngles::Vector&,
                                     const CherenkovResolutions::Vector&,
                                     const PhotonYields::Vector& ),
                            Traits::BaseClass_t<AlgBase> >
      {
        
      public:

        /// Standard constructor
        SIMDPhotonPredictedPixelSignal( const std::string& name,
                                        ISvcLocator* pSvcLocator );

        /// Initialization of the tool after creation
        StatusCode initialize() override;

      public:

        /// Functional operator
        OutData operator()( const SIMDPixelSummaries& pixels,
                            const SIMDCherenkovPhoton::Vector& photons,
                            const Relations::PhotonToParents::Vector& photRels,
                            const LHCb::RichTrackSegment::Vector& segments,
                            const CherenkovAngles::Vector& ckAngles,
                            const CherenkovResolutions::Vector& ckRes,
                            const PhotonYields::Vector& photYields ) const override;

      private:

        /// Scalar type
        using FP      = SIMDCherenkovPhoton::FP;
        /// SIMD type
        using SIMDFP  = SIMDCherenkovPhoton::SIMDFP;

      private:

        /// The exponential function
        inline SIMDFP myexp( const SIMDFP & x ) const noexcept 
        {
          // Fast VDT like function
          return Rich::Maths::fast_exp(x);
          // Approximation
          //return Rich::Maths::Approx::approx_exp(x);
          // Even faster approximation
          //return Rich::Maths::Approx::vapprox_exp(x);
        }
        
      private:

        /// Cache of particle types excluding 'below threshold'
        Rich::Particles m_hypos;

        /// Cache SIMD minimum argument value for the probability value
        SIMDFP m_minArgSIMD = SIMDFP::Zero();

        /// Cache exp(min arg)
        SIMDFP m_expMinArg  = SIMDFP::Zero();

        /// Cache SIMD minimum cut value for photon probability
        RadiatorArray<SIMDFP> m_minPhotonProbSIMD = {{}};

        /// cached SIMD scale factor / RoC^2
        DetectorArray<SIMDFP> m_factor = {{}};

        /// cached min exp(arg) factor
        RadiatorArray<SIMDFP> m_minExpArgF = {{}};

      private:

        /// The minimum expected track Cherenkov angle to be considered 'Above Threshold'
        Gaudi::Property< RadiatorArray<float> > m_minExpCKT
        { this, "MinExpTrackCKTheta", { 0.0f, 0.0f, 0.0f }, // was { 1e-6f, 1e-6f, 1e-6f }
            "The minimum expected track Cherenkov angle for each radiator (Aero/R1Gas/R2Gas)" };

        /// The minimum cut value for photon probability
        Gaudi::Property< RadiatorArray<float> > m_minPhotonProb
        { this, "MinPhotonProbability", { 1e-15f, 1e-15f, 1e-15f },
          "The minimum allowed photon probability values for each radiator (Aero/R1Gas/R2Gas)" };

        /// Scale factors
        Gaudi::Property< DetectorArray<float> > m_scaleFactor
        { this, "ScaleFactor", { 4.0f, 4.0f } };

        /// The minimum argument value for the probability value
        Gaudi::Property<float> m_minArg { this, "MinExpArg", -80.0f };
 
      };

    }
  }
}
