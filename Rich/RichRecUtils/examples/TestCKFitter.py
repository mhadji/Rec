from GaudiPython import gbl
from ROOT import *

hfile = TFile('/usera/jonesc/NFS/data/Savesets/06/01/Brunel-175727-20160601T080814-EOR.root')

fitter = gbl.Rich.Rec.CKResolutionFitter()

# Reduced range
#print fitter.params().RichFitMin 
#fitter.params().RichFitMin = array.array( 'f', [-0.0048, -0.0024] )
#fitter.params().RichFitMax = array.array( 'f', [0.0046,  0.00225] )

fitter.params().RichFitTypes[0].clear()
fitter.params().RichFitTypes[0].push_back("Hyperbolic")
fitter.params().RichFitTypes[0].push_back("FreeNPol")
fitter.params().RichFitTypes[1] = fitter.params().RichFitTypes[0]

canvas = TCanvas("CKFit","CKFit",1200,900)

gStyle.SetPadTickX(1)
gStyle.SetPadTickY(1)
gStyle.SetOptFit(101)
gStyle.SetOptStat(1110)

r1hist = hfile.Get('RICH/RiCKResLongTight/Rich1Gas/ckResAll')
r1res = fitter.fit( r1hist, gbl.Rich.Rich1Gas )

r1hist.Draw()
r1res.overallFitFunc.SetLineColor(kBlue+2)
r1res.bkgFitFunc.SetLineColor(kRed+3)
r1res.overallFitFunc.Draw("SAME")
r1res.bkgFitFunc.Draw("SAME")
canvas.Print( "RICH1-CKFit.pdf" );

r2hist = hfile.Get('RICH/RiCKResLongTight/Rich2Gas/ckResAll')
r2res = fitter.fit( r2hist, gbl.Rich.Rich2Gas )

r2hist.Draw()
r2res.overallFitFunc.SetLineColor(kBlue+2)
r2res.bkgFitFunc.SetLineColor(kRed+3)
r2res.overallFitFunc.Draw("SAME")
r2res.bkgFitFunc.Draw("SAME")
canvas.Print( "RICH2-CKFit.pdf" );
