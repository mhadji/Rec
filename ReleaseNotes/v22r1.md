2017-10-17 RecSys v22r1
===
This version uses Gaudi v29r0, LHCb v43r1 and Lbcom v21r1 (and LCG_91 with ROOT 6.10.06)

This version is released on `master` branch. The previous release on `master` branch  was `Rec v22r0`. This release contains all the changes that were made in `Rec v21r6` and `Rec v21r6p1` (released on `2017-patches` branch, see corresponding release notes), as well as the additional changes described in this file.

## Change to compiler support
**As from this release, support for gcc7 is added**  
Note that C++17 is not yet fully supported, waiting for support from ROOT  

**[MR !731] Fix gcc7 buffer overflow warning**  

**[MR !729] Fix gcc7 initialisation warning in MuonTrackAlign**  

**[MR !719] ChargedProtoANNPID - gcc7 Warning Suppression + Merge yPID libs into one**  

**[MR !714] Fix support for CPU dispatching with GCC7 on older CPUs that do not support AVX.**  
Follows LHCb!848.

**[MR !709] Fix gcc7 compilation error in DefaultVeloHitManager.h**  

**[MR !710] ChargedProtoANNPID - Fix json.hpp for gcc7**  

**[MR !708] Suppress compilation warnings from Boost headers**  

## Packaging changes
**[MR !760] Remove use of the temporary Kernel/TemplatedGenVector LHCb package**  

## Changes to configuration
**[MR !766] Change default pA/AA GEC cut from 80k to 20k Velo clusters**  

## Code optimisations
**[MR !607] Disable boost pool allocators**  


## Bug fixes and cleanups
**[MR !696] Fix Calo counter names broken by MR !627**  

**[MR !659] Fixed track checking in VeloIPResolutionMonitor**  
In case an empty set of tracks was found in the TES, the message reporting that no tracks were there was not displayed.  

**[MR !763] Fix untested StatusCodes uncovered by gaudi/Gaudi!386**  

## Monitoring changes
**[MR !747] Add additional variables to VPTrackMonitor**  


## Code modernisations
**[MR !730] Minor fixes to PrPixelModule**  
- Direct member initialisation (also fixes uninitialised variable warning from gcc7)  
- Remove empty destructor  
- Make the class final  

**[MR !723] Follow modernization of TrackEvent (LHCb!858)**  
- TrackFunctor functors no longer require a template argument to instantiate them  
- make internal VertexTrack final  
- use boost::optional<T> instead of std::pair<bool,T>  
- prefer STL algorithms over raw loops  
- prefer direct member initialization  
- fully qualify enums  

**[MR !718] Follow CaloHypo change in LHCb!852, plus some modernization**  
- update calls to CaloHypo::setPosition, following lhcb/LHCb!852  
- remove unnecessary  #include  
- prefer auto  
- avoid repeated searches in maps  
- prefer range-based for loops  
- avoid heap storage if stack will do  
- replace std::auto_ptr with std::unique_ptr    

**[MR !720] Change from class to struct in IMuonRawBuffer. Follows MR LHCb!847**  

**[MR !690] Inline functions in TMVA for PatLLT, add debug statement to TrackAssociator**  

**[MR !670] Modernize TrackMonitors**  
- replace `BOOST_FOREACH` with range-based for loops  
- replace boost::assign with (implicit) std::initializer_list  
- remove unused headers  
- prefer auto  
- avoid repeated calculations of chi2().prob()  

**[MR !671] Modernize TrackIdealPR**  
- remove explicit trivial destructors  
- prefer (implicit) std::initializer_list over boost::assign  
- make static map const  
- remove unused headers  

**[MR !672] Modernize TrackCheckers**
- remove unused headers  
- replace `BOOST_FOREACH` with ranger-based for loops  
- prefer Gaudi::Property  
- prefer inherited constructors  
- prefer implicit destructors over explicit trivial ones  
- make static maps const  
- prefer auto  
- replace repetitive code with a lambda   

**[MR !681] TrackRungeKuttaExtrapolator: Implement both `toStream` and `toString` instead of just `toStream`**  

**[MR !673] Modernize VeloRecMonitors**  
- remove unused headers  
- prefer STL algorithms over raw loops  
- replace `BOOST_FOREACH` with range-based for loops  
- prefer lambdas over bind  

**[MR !663] Adapt to strict TrajPoca interface**  
Follows LHCb!785  

**[MR !716] Modernize VeloClusterPosition**  
- Prefer implicit compiler generated destructors  
- Prefer inherited constructors  
- Prefer boost::optional over std::unique_ptr  
- Prefer Gaudi::Property<GaudiMath::Interpolation::Type> over Gaudi::Property<std::string>  
- Qualify VeloClusterPosition as final  
- Remove unneccesary finalize implementation  



## Changes to tests and documentation
**[MR !711] RichFuture examples update.**  

**[MR !679] Update test reference to match LHCb!795**  

**[v22r0.md] Fix bug in dependencies documentation**  
