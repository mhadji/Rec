#ifndef VELORECMONITORS_VELOIPRESOLUTIONMONITOR_H
#define VELORECMONITORS_VELOIPRESOLUTIONMONITOR_H 1

// Include files
// from Gaudi
#include <GaudiAlg/GaudiHistoAlg.h>
#include <GaudiAlg/Consumer.h>
#include <GaudiKernel/ToolHandle.h>

#include <TrackInterfaces/ITrackExtrapolator.h>
#include <TrackInterfaces/IPVOfflineTool.h>

#include <Event/RecVertex.h>

#include <TH1D.h>
#include <TH2D.h>

#include <string>
#include <sstream>

/** @class VeloIPResolutionMonitor VeloIPResolutionMonitor.h
 *
 *  An algorithm to monitor IP resolutions as a function of 1/PT.
 *  The IP of tracks used to make a primary vertex is taken on that primary vertex.
 *  Assuming these tracks are from prompt particles, the true IP should be 0, so the
 *  calculated IP is in fact the resolution on the IP. Histograms produced are the
 *  mean of the absolute unsigned 3D IP resolution against 1/PT,
 *  and the width of a Gaussian fitted to each 1D IP resolution against 1/PT.
 *
 *  @author Michael Thomas Alexander
 *  @author Sebastien Ponce
 */

namespace Velo {

  class VeloIPResolutionMonitor :
    public Gaudi::Functional::Consumer<void(const LHCb::Tracks&, const LHCb::RecVertices&),
                                            Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {
  public:
    /// Standard constructor
    VeloIPResolutionMonitor(const std::string& name, ISvcLocator* pSvcLocator);

    /// initialize
    StatusCode initialize() override;

    /// finalize
    StatusCode finalize() override;

    /// Algorithm execution
    void operator()(const LHCb::Tracks&, const LHCb::RecVertices&) const override;

  private:

    TH2D* m_h_IPXVsInversePT = nullptr;
    TH2D* m_h_IPYVsInversePT = nullptr;

    TH2D* m_h_IPXVsPhi = nullptr;
    TH2D* m_h_IPYVsPhi = nullptr;
    TH2D* m_h_IPXVsPhi_HighPT = nullptr;
    TH2D* m_h_IPYVsPhi_HighPT = nullptr;

    TH2D* m_h_IPXVsPhi_Eta45To50 = nullptr;
    TH2D* m_h_IPYVsPhi_Eta45To50 = nullptr;
    TH2D* m_h_IPXVsPhi_Eta25To30 = nullptr;
    TH2D* m_h_IPYVsPhi_Eta25To30 = nullptr;
    TH2D* m_h_IPXVsPhi_EtaLT0 = nullptr;
    TH2D* m_h_IPYVsPhi_EtaLT0 = nullptr;

    TH2D* m_h_IPXVsEta = nullptr;
    TH2D* m_h_IPYVsEta = nullptr;

    TH1D* m_h_TrackMultiplicity = nullptr;

    double m_InversePTMin = 0.;
    double m_InversePTMax = 3.;
    int m_nBins = 20;
    int m_nBinsPhi = 32;
    int m_nBinsEta = 50;

    unsigned int m_minPVnTracks = 26;
    float m_maxTrackChi2PerNDOF = 4.;
    float m_maxP = 500.;
    unsigned int m_minNRHits = 6;
    unsigned int m_minNTTHits = 1;

    float m_limitIntercept1D = 16.e-03F;
    float m_limitGradient1D = 26.7e-03F;
    float m_limitFactor = 10.;

    bool m_refitPVs = false;
    bool m_makePlotsVsPhiInBinsOfEta = false;

    bool m_save2DHistos = false;

    ITrackExtrapolator* m_trackExtrapolator = nullptr;
    IPVOfflineTool* m_pvtool = nullptr;

    StatusCode calculateIPs(const LHCb::RecVertex*, const LHCb::Track*,
                            double&, double&, double&, double&, double&, double&, LHCb::State&, LHCb::State&) const;
    void distance(const LHCb::RecVertex*, LHCb::State&, double&, double&, int) const;

    void rebinHisto(TH1D*, int nbins=0);
    void rebinHistos(std::vector<TH1D*>, int nbins=0);

    void getBinsFromTH2D(TH2D* h, std::string id, std::string title, std::string unit, std::vector<TH1D*>& out);
    void saveMeanAndSigmaProfiles(TH2D*);
    void saveMeanAndSigmaProfilesAndXProjection(TH2D*, std::string);

    TH2D* book2DHisto(std::string, std::string, unsigned int, double, double, unsigned int, double, double, bool);

  };
}

#endif // VELORECMONITORS_VELOIPRESOLUTIONMONITOR_H
