#ifndef CALORECO_CALOLCORRECTION_H
#define CALORECO_CALOLCORRECTION_H 1

// from STL
#include <string>
#include <cmath>

// Calo
#include "CaloInterfaces/ICaloHypoTool.h"
#include "CaloCorrectionBase.h"
#include "CaloDet/DeCalorimeter.h"

// Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// Event
#include "Event/CaloHypo.h"

/** @class CaloLCorrection CaloLCorrection.h
 *
 *
 *  @author Deschamps Olivier
 *  @date   2003-03-10
 *  revised 2010
 */

class CaloLCorrection :
  public virtual ICaloHypoTool ,
  public              CaloCorrectionBase
{
public:

  StatusCode process    ( LHCb::CaloHypo* hypo  ) const override;
  StatusCode operator() ( LHCb::CaloHypo* hypo  ) const override;

public:

  StatusCode initialize() override;
  StatusCode finalize() override;

  /** Standard constructor
   *  @see GaudiTool
   *  @see  AlgTool
   *  @param type tool type (?)
   *  @param name tool name
   *  @param parent  tool parent
   */
  CaloLCorrection ( const std::string& type   ,
                    const std::string& name   ,
                    const IInterface*  parent ) ;
};
// ============================================================================
#endif // CALORECO_CALOLCORRECTION_H
