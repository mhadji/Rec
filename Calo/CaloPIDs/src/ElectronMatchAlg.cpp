// Include files
#include <type_traits>
#include "CaloTrackMatchAlg.h"
#include "Event/CaloHypo.h"
#include "Relations/IRelation.h"
#include "Relations/RelationWeighted2D.h"

// ============================================================================
/** @class ElectronMatchAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-06-16
 */
// ============================================================================

class ElectronMatchAlg final : public CaloTrackMatchAlg {
 public:
  StatusCode execute() override;

  ElectronMatchAlg(const std::string& name, ISvcLocator* pSvc)
      : CaloTrackMatchAlg(name, pSvc) {
    using namespace LHCb::CaloAlgUtils;
    Inputs inputs = Inputs(1, CaloHypoLocation("Electrons", context()));
    _setProperty("Calos", Gaudi::Utils::toString(inputs));
    _setProperty("Output", CaloIdLocation("ElectronMatch", context()));
    _setProperty("Filter", CaloIdLocation("InEcal", context()));
    _setProperty("Tool", "CaloElectronMatch/ElectronMatch");
    _setProperty("Threshold", "10000");
    // track types:
    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Types::Long, LHCb::Track::Types::Downstream,
                                     LHCb::Track::Types::Ttrack));
  }
};

// ============================================================================

DECLARE_COMPONENT( ElectronMatchAlg )

// ============================================================================
// Standard execution of the algorithm
// ============================================================================

StatusCode ElectronMatchAlg::execute() {
  Assert(!m_tracks.empty(), "No Input tracks");
  Assert(!m_calos.empty(), "No Input Clusters");

  using Table = LHCb::RelationWeighted2D<LHCb::Track, LHCb::CaloHypo, float>;
  static_assert(std::is_base_of<LHCb::Calo2Track::ITrHypoTable2D, Table>::value,
                "Table must inherit from ITrHypoTable2D");

  // create the relation table and register it in TES
  Table* table = new Table(10 * 100);
  put(table, m_output);

  //  perform the actual jobs
  StatusCode sc =  doTheJob<LHCb::CaloHypo,Table>( table ) ;
  if (counterStat->isQuiet())
    counter(Gaudi::Utils::toString(m_tracks.value()) + "->" +
            Gaudi::Utils::toString(m_calos.value()) + "=>" + m_output) +=
      table->i_relations().size();
  return sc;
}
