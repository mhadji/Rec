// Include files
#include "CaloChi22ID.h"
#include "ToVector.h"

// ============================================================================

class ClusChi22ID final: public CaloChi22ID {
 public:
  StatusCode execute() override;

  ClusChi22ID(const std::string& name, ISvcLocator* pSvc)
      : CaloChi22ID(name, pSvc) {
    using LHCb::CaloAlgUtils::CaloIdLocation;
    _setProperty("Input", CaloIdLocation("ClusterMatch", context()));
    _setProperty("Output", CaloIdLocation("ClusChi2", context()));
    // @todo it must be in agrement with "Threshold" for PhotonMatchAlg
    _setProperty("CutOff", "1000");  //
    // track types:
    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Types::Long, LHCb::Track::Types::Ttrack,
                                     LHCb::Track::Types::Downstream));
  };
};


// ============================================================================

DECLARE_COMPONENT( ClusChi22ID )

// ============================================================================

StatusCode ClusChi22ID::execute() {
  typedef IRelationWeighted2D<LHCb::CaloCluster, LHCb::Track, float> TABLEI;
  typedef LHCb::Relation1D<LHCb::Track, float> Table;
  // check
  static_assert(
      std::is_base_of<LHCb::Calo2Track::IClusTrTable2D, TABLEI>::value,
      "TABLEI must inherit from IClusTrTable2D");

  // get the input table
  if (!exist<TABLEI>(m_input))
    return Warning(" Input missing '" + m_input + "'", StatusCode::SUCCESS);
  const TABLEI* input = get<TABLEI>(m_input);
  // create and register the output table
  Table* output = new Table(input->relations().size() + 10);
  put(output, m_output);
  // perform the actual jobs
  return doTheJob(input->inverse(), output);
}

// =============================================================================
