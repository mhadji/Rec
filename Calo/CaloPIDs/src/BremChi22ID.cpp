// Include files
#include "CaloChi22ID.h"
#include "ToVector.h"

// ============================================================================
/** @class BremChi22ID BremChi22ID.cpp
 *  The preconfigured instance of class CaloChi22ID
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================

class BremChi22ID final : public CaloChi22ID {
public:
  BremChi22ID(const std::string& name, ISvcLocator* pSvc)
      : CaloChi22ID(name, pSvc) {
    using LHCb::CaloAlgUtils::CaloIdLocation;
    _setProperty("Input", CaloIdLocation("BremMatch", context()));
    _setProperty("Output", CaloIdLocation("BremChi2", context()));
    // @todo it must be in agrement with "Threshold" for BremMatchAlg
    _setProperty("CutOff", "10000");
    // track types:
    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Velo, LHCb::Track::Long,
                                     LHCb::Track::Upstream));
  }
};

// ============================================================================

DECLARE_COMPONENT( BremChi22ID )

// ============================================================================
