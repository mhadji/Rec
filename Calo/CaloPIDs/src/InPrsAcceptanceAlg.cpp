// Include files
#include "InCaloAcceptanceAlg.h"

// ============================================================================
/** @class InPrsAcceptanceAlg InPrsAcceptanceAlg.cpp
 *  the preconfigured instance of InCaloAcceptanceAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-17
 */
// ============================================================================

struct InPrsAcceptanceAlg final : InCaloAcceptanceAlg {
  /// Standard constructor
  InPrsAcceptanceAlg(const std::string& name, ISvcLocator* pSvc)
      : InCaloAcceptanceAlg(name, pSvc) {
    using LHCb::CaloAlgUtils::CaloIdLocation;

    Gaudi::Functional::updateHandleLocation(*this, "Output",
                                            CaloIdLocation("InPrs", context()));

    _setProperty("Tool", "InPrsAcceptance/InPrs");
    // track types:
    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Types::Long, LHCb::Track::Types::Downstream,
                                     LHCb::Track::Types::Ttrack));
  }
};

// ============================================================================

DECLARE_COMPONENT( InPrsAcceptanceAlg )

// ============================================================================
