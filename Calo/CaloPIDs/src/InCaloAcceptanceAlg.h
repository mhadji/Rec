#ifndef CALOPIDS_INCALOACCEPTANCEALG_H 
#define CALOPIDS_INCALOACCEPTANCEALG_H 1

// Include files
#include "CaloTrackAlg.h"
#include "CaloUtils/Calo2Track.h"
#include "CaloUtils/CaloAlgUtils.h"
#include "Event/Track.h"
#include "GaudiAlg/MergingTransformer.h"
#include "Kernel/IInAcceptance.h"
#include "Relations/IRelation.h"
#include "Relations/IsConvertible.h"
#include "Relations/Relation1D.h"
#include "ToVector.h"

// ============================================================================
/** @class InCaloAcceptanceAlg InCaloAcceptanceAlg.h
 *
 *  the trivial algorithm to fill "InCaloAcceptance" table 
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-17
 */
// ============================================================================

using Table =  LHCb::Relation1D<LHCb::Track,bool>;
using Tracks = LHCb::Track::Container;
using TrackLists = Gaudi::Functional::vector_of_const_<LHCb::Tracks*>;

class InCaloAcceptanceAlg
    : public Gaudi::Functional::MergingTransformer<
          Table(const TrackLists&),
          Gaudi::Functional::Traits::BaseClass_t<CaloTrackAlg> >
{
  // check the proper convertability
  static_assert(std::is_base_of<LHCb::Calo2Track::ITrAccTable, Table>::value,
                "Table must inherit from ITrAccTable");

 public:
  /// algorithm initialization
  StatusCode initialize() override;

  /// C++11 non-copyable idiom
  InCaloAcceptanceAlg() = delete;
  InCaloAcceptanceAlg(const InCaloAcceptanceAlg&) = delete;
  InCaloAcceptanceAlg& operator=(const InCaloAcceptanceAlg&) = delete;

  /// algorithm execution
  Table operator()(const TrackLists&) const override;

  /// Standard constructor
  InCaloAcceptanceAlg(const std::string& name, ISvcLocator* pSvc);

  ToolHandle<IInAcceptance> m_tool = {"<NOT DEFINED>", this};
};

// ============================================================================
#endif  // CALOPIDS_INCALOACCEPTANCEALG_H
// ============================================================================
