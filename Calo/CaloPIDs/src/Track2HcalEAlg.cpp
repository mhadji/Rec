// Include files
#include "CaloTrack2IDAlg.h"

// ============================================================================
/** @class Track2HcalEAlg Track2HcalEAlg.cpp
 *  preconfigured instance of class  CaloTrack2IDAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-15
 */
class Track2HcalEAlg final : public CaloTrack2IDAlg {
 public:
  Track2HcalEAlg(const std::string& name, ISvcLocator* pSvc)
      : CaloTrack2IDAlg(name, pSvc) {
    using LHCb::CaloAlgUtils::CaloIdLocation;
    _setProperty("Output", CaloIdLocation("HcalE", context()));
    _setProperty("Filter", CaloIdLocation("InHcal", context()));
    _setProperty("Tool", "HcalEnergyForTrack/HcalE");
  }
};

// ============================================================================

DECLARE_COMPONENT( Track2HcalEAlg )

// ============================================================================
