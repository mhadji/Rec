// Includes
#include "GaudiAlg/Consumer.h"
#include "AIDA/IAxis.h"
#include "Event/Track.h"
#include "Event/MCParticle.h"
// #include "Relations/IRelation.h"
#include "Relations/Relation1D.h"
// #include "Relations/IRelationWeighted.h"
#include "Kernel/Particle2MCLinker.h"
#include "CaloUtils/Calo2Track.h"
#include "boost/lexical_cast.hpp"
#include "CaloMoniAlg.h"

// =============================================================================

namespace {
  /** extract the momentum from Track
   *  @param  track pointer to the track
   *  @return the momentum of the track
   */
  constexpr double momentum( const LHCb::Track *track ){
    return track != nullptr ? track->closestState( 0.0 ).p() : -100*Gaudi::Units::GeV;
  }

}

using Input = LHCb::Relation1D<LHCb::Track,float>;
using Inputs = LHCb::Track::Container;
using AccTable = LHCb::Relation1D<LHCb::Track,bool>;

// =============================================================================

/** @class CaloPIDsChecker CaloPIDsChecker.cpp
 *
 *  Class for monitoring the CaloPIDs quantities
 *
 *  The major properties
 *
 *  - "Input"
 *   The default value is                      ""
 *   The PID-quantity to be monitored, relation table of
 *   type IRelation<Track,float>
 *
 *  - "Inputs"
 *   The default value is :         TrackLocation::Default
 *   The list of Track containers to be monitored
 *
 *   - "Tr2MCP"
 *   The default value is :          "Rec/Relations/Tr2MCP"
 *   The location in TES the relation table of type
 *   IRelationWeighted<Track,MCParticle,double>
 *
 *    - "Particle"
 *    The default value is :         11
 *    Particle ID for "signal" hypothesis
 *
 *    - "Cut"
 *    The default value is :          0
 *    The cut on PID to be used for monitoring
 *
 *    - "Normialization"
 *    The default value is :          50 GeV
 *    The normalization factor used for histogramming
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date   2004-02-15
 */

class CaloPIDsChecker final
: public Gaudi::Functional::Consumer<void(const Input&, const Inputs&, const AccTable&),
    Gaudi::Functional::Traits::BaseClass_t<CaloMoniAlg>>
{
public:
  StatusCode initialize() override;
  StatusCode finalize() override;
  void operator()(const Input&, const Inputs&, const AccTable&) const override;

  CaloPIDsChecker( const std::string &name, ISvcLocator *isvc );

protected:
  /// transformation function for momentum
  inline double pFunc( const double value ) const
  { return tanh( value / m_pNorm );}

  /// h3=h1/h2
  StatusCode divide( AIDA::IHistogram1D *hh1, AIDA::IHistogram1D *hh2,
                     AIDA::IHistogram1D *hh3 ) const;

private:
  // Track -> MC associator
  Object2MCLinker<LHCb::Track>* m_track2MCLink = nullptr;

  Gaudi::Property<unsigned int> m_pid
    {this, "Particle", 11, "particle to be identified"};

  Gaudi::Property<float> m_cut
    {this, "Cut", 0., "cut value"};

  Gaudi::Property<float> m_pNorm
    {this, "Normalization", 50 * Gaudi::Units::GeV, "momentum normalization"};

  Gaudi::Property<std::string> m_typName
    {this, "TrackType", "ALL"};

  LHCb::Track::Types m_typ = LHCb::Track::TypeUnknown;
  bool m_checkType = true;
};

// =============================================================================

DECLARE_COMPONENT( CaloPIDsChecker )

// =============================================================================

CaloPIDsChecker::CaloPIDsChecker( const std::string &name, ISvcLocator *isvc )
: Consumer( name, isvc, {
    KeyValue{ "Input"          , "" },
    KeyValue{ "Inputs"         , LHCb::TrackLocation::Default },
    KeyValue{ "TrackAcceptance", "Rec/Calo/InAccEcal" }
})
{}

// =============================================================================

StatusCode CaloPIDsChecker::initialize(){
  StatusCode sc = Consumer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc; // error already printedby GaudiAlgorithm

  const std::string mom = "tanh(P/"
    + boost::lexical_cast<std::string>( m_pNorm / Gaudi::Units::GeV )
    + " GeV/c) ";
  const std::string cut  = "with cut 'DLL>"
    + boost::lexical_cast<std::string>( m_cut.value() ) + "' ";

  hBook1(  "1", "DLL for 'Signal'",     -5, 5 );
  hBook1(  "2", "DLL for 'Ghosts'",     -5, 5 );
  hBook1(  "3", "DLL for 'Background'", -5, 5 );

  hBook1( "11", mom +                "for 'Signal'",     0, 1.02, 51 );
  hBook1( "12", mom +                "for 'Ghosts'",     0, 1.02, 51 );
  hBook1( "13", mom +                "for 'Background'", 0, 1.02, 51 );

  hBook1( "21", mom + cut +          "for 'Signal'",     0, 1.02, 51 );
  hBook1( "22", mom + cut +          "for 'Ghosts'",     0, 1.02, 51 );
  hBook1( "23", mom + cut +          "for 'Background'", 0, 1.02, 51 );

  hBook1( "31", "Eff " + mom + cut + "for 'Signal'",     0, 1.02, 51 );
  hBook1( "32", "Eff " + mom + cut + "for 'Ghosts'",     0, 1.02, 51 );
  hBook1( "33", "Eff " + mom + cut + "for 'Background'", 0, 1.02, 51 );

  m_track2MCLink = new Object2MCLinker<LHCb::Track>( this, "", "", inputLocation<1>() );
  if( m_split ){
    Warning( "No area spliting allowed for CaloPIDsChecker").ignore();
    m_split = false;
  }

  if ( LHCb::CaloAlgUtils::toUpper(m_typName.value()) == "ALL" )
    m_checkType = false;
  else
    m_typ = LHCb::Track::TypesToType(m_typName.value());

  return StatusCode::SUCCESS;
}

// =============================================================================

StatusCode CaloPIDsChecker::finalize(){
  StatusCode sc = divide( m_h1["21"], m_h1["11"], m_h1["31"] );
  if(sc.isSuccess())sc=divide( m_h1["22"], m_h1["12"], m_h1["32"] );
  if(sc.isSuccess())sc= divide( m_h1["23"], m_h1["13"], m_h1["33"] );
  if(!sc.isSuccess())error()<<"Failed dividing histograms " << endmsg;

  if( nullptr != m_track2MCLink ) delete m_track2MCLink;
  m_track2MCLink = nullptr;

  return Consumer::finalize();
}


// =============================================================================

void CaloPIDsChecker::operator()(const Input& pidTable, const Inputs& tracks, const AccTable& accTable) const {

  // produce histos ?
  if ( !produceHistos() ) return;

  // Loop over tracks
  for( const auto& track: tracks ){

    // Abort if track type mismatched
    if( m_checkType && track->type() != m_typ ) continue;

    // Abort if track is outside acceptance
    if(accTable.relations(track).front().to() == false) continue;

    // get MC truth
    double maxWeight = 0;
    const LHCb::MCParticle* mc = m_track2MCLink->first(track, maxWeight);
    auto mcp = (LHCb::MCParticle*) mc;
    if( mc != nullptr ){
      do{
        double weight = 0;
        mc = m_track2MCLink->next(weight);
        if(weight > maxWeight){
          maxWeight = weight;
          mcp = (LHCb::MCParticle*) mc;
        }
      }while( mc != nullptr );
    }

    // retrieve calo pid information
    const auto calo = pidTable.relations( track );

    // get THE LAST pid
    const double DLL = calo.empty() ? -1000.0 : calo.back().to();
    if ( 0 == mcp )                                 hFill1("2",  DLL); //ghosts
    else if ( mcp->particleID().abspid() == m_pid ) hFill1("1",  DLL); //signal
    else                                            hFill1("3",  DLL); //background

    // evaluate the function of momentum
    const double pMom = pFunc( momentum( track ) );
    if ( 0 == mcp )                       hFill1("12", pMom); //ghosts
    else if ( mcp->particleID().abspid() == m_pid ) hFill1("11", pMom); //signal
    else                                       hFill1("13", pMom); //background

    // apply DLL cut
    if ( DLL < m_cut ) continue;
    if ( 0 == mcp )                       hFill1("22", pMom); //ghosts
    else if ( mcp->particleID().abspid() == m_pid ) hFill1("21", pMom); //signal
    else                                       hFill1("23", pMom); //background
  }
  return;
}

// =============================================================================
// Divide the histograms
// =============================================================================

StatusCode CaloPIDsChecker::divide( AIDA::IHistogram1D *hh1
                                  , AIDA::IHistogram1D *hh2
                                  , AIDA::IHistogram1D *hh3 ) const
{ if (( 0 == hh1 ) || ( 0 == hh2 ) || ( 0 == hh3 )){
    return Error( "AIDA::IHistogram1D* points to NULL" );
  }
  const IAxis &axis1 = hh1->axis();
  const IAxis &axis2 = hh2->axis();
  const IAxis &axis3 = hh3->axis();
  const int nBins = axis3.bins();
  if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
    debug() << "DIVIDE nbins " << nBins << endmsg;

  if (( axis1.bins() != nBins ) || ( axis2.bins() != nBins ))
  { return Error( "Different histogram specifications" );
  }
  double count = 0;
  for( int nBin = 0; nBin < nBins; ++nBin )
  { const double v1 = hh1->binHeight( nBin );
    const double v2 = hh2->binHeight( nBin );
    const double x  = 0.5 * ( axis3.binLowerEdge ( nBin ) +
                              axis3.binUpperEdge ( nBin ) );

// fill the histogram
    if ( 0 != v2 ) hh3->fill( x, v1/v2 );
    if ( 0 != v1) count += 1.;
  }

  const double content = hh3->sumBinHeights();
  if ( 0 != count ){
    info() << "'" << hh3->title()   << "' = "
           << (content/count)*100 << "[%]" << endmsg;
  }else{
    info() << "'" << hh1->title()
           << "' contains no filled bins - histogram ratio undefined" << endmsg;
  }

  return StatusCode::SUCCESS;
}
