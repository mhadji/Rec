// Include files
#include "LHCbMath/LineTypes.h"
#include "LHCbMath/GeomFun.h"
//
#include "CaloAlignmentNtp.h"
#include "CaloMoniUtils.h"

namespace {
    // hack to allow for tools with non-const interfaces...
    template <typename IFace>
    IFace* fixup(const ToolHandle<IFace>& iface) { return &const_cast<IFace&>(*iface); }
}

//-----------------------------------------------------------------------------
// Implementation file for class : CaloAlignmentNtp
//
// 2009-12-11 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CaloAlignmentNtp )

namespace {
  using Line = Gaudi::Math::Line<Gaudi::XYZPoint,Gaudi::XYZVector>;

  // Return distance between two calo cell.
  double distance( const LHCb::CaloCellID& id1, const LHCb::CaloCellID& id2 ){
    return sqrt(pow(double(id1.row()-id2.row()), 2)+pow(double(id1.col() - id2.col()),2));
  }
}

//==============================================================================
// Standard constructor, initializes variables
//==============================================================================

CaloAlignmentNtp::CaloAlignmentNtp( const std::string& name, ISvcLocator* pSvcLocator)
: Consumer( name, pSvcLocator, {
    KeyValue{ "VertexLoc"     , ""}, // Lecacy naming
    KeyValue{ "InputODIN"     , LHCb::ODINLocation::Default                 },
    KeyValue{ "InputDigits"   , LHCb::CaloAlgUtils::CaloDigitLocation("SPD")},
    KeyValue{ "InputTracks"   , LHCb::TrackLocation::Default                },
    KeyValue{ "InputContainer", LHCb::ProtoParticleLocation::Charged },
})
{
  // Tools
  declareProperty( "ExtrapolatorType" , m_extrapolator );
}

//==============================================================================
// Initialization
//==============================================================================

StatusCode CaloAlignmentNtp::initialize() {
  StatusCode sc = Consumer::initialize();
  if( sc.isFailure() ) return sc;
  if( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;
  if( !m_tracks.empty() )info() << "Will only look at track type(s) = " << m_tracks << endmsg;
  else info() << "Will look at any track type" << endmsg;

  // Get & retrieve & configure tools
  m_calo = getDet<DeCalorimeter>(DeCalorimeterLocation::Ecal);
  if(!( m_counterStat.retrieve()
     && m_caloElectron.retrieve()
     && m_odin.retrieve()
     && m_toSpd.retrieve()
     && m_toPrs.retrieve()
     && m_extrapolator.retrieve() )){
    error() << "Unable to retrive one of the ToolHandles" << endmsg;
    return StatusCode::FAILURE;
  }
  m_toSpd->setCalos( "Ecal", "Spd");
  m_toPrs->setCalos( "Ecal", "Prs");

  // set vertex location
  if(m_vertLoc == "")
    m_vertLoc = m_usePV3D ? LHCb::RecVertexLocation::Velo3D : LHCb::RecVertexLocation::Primary;

  //activate pairing when mass filter is on
  if(m_mas.value().first != -99999 || m_mas.value().second != +99999) m_pairing=true;

  // set vertex location
  m_vertLoc = inputLocation();  // <-- Careful with the index
  if(m_vertLoc == "")
    m_vertLoc = m_usePV3D ? LHCb::RecVertexLocation::Velo3D : LHCb::RecVertexLocation::Primary;
  updateHandleLocation( *this, "VertexLoc", m_vertLoc );

  return StatusCode::SUCCESS;
}

//==============================================================================

bool CaloAlignmentNtp::acceptTrack(const LHCb::Track* track) const {
  if( track == nullptr ) return false;
  if( m_tracks.empty() ) return true;
  const int ttype = track->type();
  return std::any_of(m_tracks.begin(), m_tracks.end(), [ttype](int itype){return itype==ttype;});
}

//==============================================================================

bool CaloAlignmentNtp::hypoProcessing(const LHCb::CaloHypo* hypo) const {
  if( hypo == nullptr ) return false;
  LHCb::CaloMomentum momentum( hypo );
  const double e  = momentum.e();
  const double et = momentum.pt();
  if( !inRange(m_et , et )) return false;
  if( !inRange(m_e  , e  )) return false;
  const double ePrs = fixup(m_toPrs)->energy( *hypo , "Prs"  );
  if( !inRange(m_prs, ePrs)) return false;
  const double iSpd = fixup(m_toSpd)->multiplicity( *hypo , "Spd");
  if( !inRange(m_spd, iSpd)) return false;
  if( m_inArea && !inArea(hypo) ) return false;
  return true;
}

//==============================================================================

bool CaloAlignmentNtp::inArea(const LHCb::CaloHypo* hypo) const {
  const auto hypoR  = position3d(hypo);
  const auto cell   = m_calo->Cell_( hypoR );
  if( !cell ){
    if(m_counterStat->isQuiet()) counter("no cell related to position") += 1;
    info() << "No valid cell related to position " << hypoR << endmsg;
    return false;
  }
  return cell->neighbors().size() == 8;
}


//==============================================================================
// Main execution
//==============================================================================

void CaloAlignmentNtp::operator()(const Vertices& verts, const ODIN& odin,
      const Digits& digits, const Tracks& tracks, const Protos& protos) const {

  if( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;
  if( m_counterStat->isQuiet() ) counter("#PV="+ Gaudi::Utils::toString(verts.size()) + " ["+inputLocation()+"]")+=1;

  // 1. Validate event
  m_odin->getTime();
  const auto nSpd       = digits.size();
  const auto nTracks    = tracks.size();
  const auto nVertices  = verts.size();
  if( !inRange( m_nSpd, nSpd      )) return;
  if( !inRange( m_nTrk, nTracks   )) return;
  if( !inRange( m_nVtx, nVertices )) return;
  if(m_counterStat->isQuiet()) counter("1-selected events") += 1;

  // Loop over protoparticles
  for( auto p = protos.begin(); protos.end() != p ; ++p ){
    const auto proto = *p;

    // 2: Validate proto
    if( !fixup(m_caloElectron)->set(proto)) continue;
    if( m_counterStat->isQuiet()) counter("2-selected protos")+=1;

    // 3: Validate track
    const auto track = proto->track();
    if( !acceptTrack( track )) continue;
    if( m_counterStat->isQuiet()) counter("3-selected tracks")+=1;

    // 4: Validate hypo
    const auto hypo = fixup(m_caloElectron)->electron();
    if( !hypoProcessing(hypo) ) continue;
    if( m_counterStat->isQuiet()) counter("4-selected hypos")+=1;

    // 5: Validate electron
    const auto eOp      = fixup(m_caloElectron)->eOverP();
    if( !inRange( m_eop   , eOp)) continue;
    if( !inRange( m_dlle  , proto->info(LHCb::ProtoParticle::additionalInfo::CombDLLe , 9999.))) continue;
    if( !inRange( m_rdlle , proto->info(LHCb::ProtoParticle::additionalInfo::RichDLLe, 9999.))) continue;
    if( !inRange( m_bMatch, proto->info(LHCb::ProtoParticle::additionalInfo::CaloBremMatch, 9999.))) continue;
    if( !inRange( m_eMatch, proto->info(LHCb::ProtoParticle::additionalInfo::CaloElectronMatch, 9999.))) continue;
    if( !inRange( m_cMatch, proto->info(LHCb::ProtoParticle::additionalInfo::CaloTrMatch, 9999.))) continue;
    if( m_counterStat->isQuiet()) counter("5-selected electrons")+=1;

    // track extrapolation
    const auto cluster  = firstCluster(hypo);
    const auto id       = cluster->seed();
    const auto hypoR    = position3d(hypo);
    const auto clusR    = position3d(cluster);
    const auto statR    = position3d(fixup(m_caloElectron)->caloState());
    const auto closR    = position3d(fixup(m_caloElectron)->closestState());
    const auto cs       = momentum(fixup(m_caloElectron)->caloState());
    const auto theta    = fixup(m_caloElectron)->caloState().momentum().Theta();
    const auto t        = momentum(track);
    const auto brem     = fixup(m_caloElectron)->bremstrahlung();
    const auto hasBrem  = (brem!=nullptr);
    if( m_brem && !hasBrem ) continue; // is Brem explicitly requested

    const Gaudi::XYZPoint cellR( m_calo->cellX(id) , m_calo->cellY(id), m_calo->cellZ(id) );// seed position

    // Bremstrahllung
    const auto bP       = momentum(fixup(m_caloElectron)->bremCaloMomentum());
    const auto bremR    = position3d(brem);
    const auto bcluster = firstCluster(brem);
    const auto bid      = bcluster->seed();
    const auto bclusR   = position3d(bcluster);
    const auto bcellR   = Gaudi::XYZPoint( m_calo->cellX(bid) , m_calo->cellY(bid), m_calo->cellZ(bid) );// seed
    const auto dist     = hasBrem ? distance(bid,id) : -1.;
    if(hasBrem){
      // brem-electron separation
      if( bid.area()==id.area() && !inRange( m_dist, dist ) ) continue;
      if( m_counterStat->isQuiet() ) counter("6-selected electron+Brem")+=1;
    }

    // perform electron pairing
    double mas = 9999.;
    if( m_pairing && m_extrapolator!=0 ){
      for( auto pp = p+1 ; protos.end() != pp ; ++pp ){
        const auto proto2 = *pp;
        if( !fixup(m_caloElectron)->set(proto2) ) continue;;
        const auto hypo2 = fixup(m_caloElectron)->electron();
        if( hypo == hypo2 ) continue;
        if( !hypoProcessing( hypo2 ) ) continue;
        const auto eOp2 = fixup(m_caloElectron)->eOverP();
        if( !inRange( m_eop, eOp2 )) continue;
        if( !inRange( m_dlle , proto2->info(LHCb::ProtoParticle::additionalInfo::CombDLLe, 0.))) continue;
        if( !inRange( m_rdlle, proto2->info(LHCb::ProtoParticle::additionalInfo::RichDLLe, 0.))) continue;
        // compute mass
        const auto t1 = proto->track();
        const auto t2 = proto2->track();
        if( !acceptTrack(t2) ) continue;
        if( t1==nullptr || t2==nullptr ) continue;
        if( -1 != t1->charge()*t2->charge()) continue;
        LHCb::State st1 = t1->firstState();
        LHCb::State st2 = t2->firstState();
        StatusCode sc = m_extrapolator->propagate(st1, 0.);
        if(sc.isFailure()) Warning("Propagation 1 failed").ignore();
        sc = m_extrapolator->propagate(st2, 0.);
        if(sc.isFailure()) Warning("Propagation 2 failed").ignore();
        const auto p1 = st1.momentum();
        const auto p2 = st2.momentum();
        double m2 = p1.R()*p2.R();
        m2 -= p1.X()*p2.X();
        m2 -= p1.Y()*p2.Y();
        m2 -= p1.Z()*p2.Z();
        m2 *= 2;
        if( m2 > 0 ){
          const double m = sqrt(m2) ;
          if( m < mas ) mas = m;
        }
      }
      // add mass
      if( !inRange( m_mas, mas ) ) continue;
    }

    // Start filling tuple
    if(m_tuple){
      auto ntp = nTuple(500, "CaloAlignment", CLID_ColumnWiseTuple);
      ntp->column( "Spd"        , fixup(m_toSpd)->multiplicity( *hypo, "Spd" ));
      ntp->column( "Prs"        , fixup(m_toPrs)->energy( *hypo , "Prs" ));
      ntp->column( "BremEleDist", dist );
      ntp->column( "TrackMatch" , proto->info(LHCb::ProtoParticle::additionalInfo::CaloTrMatch, 9999.));
      ntp->column( "ElecMatch"  , proto->info(LHCb::ProtoParticle::additionalInfo::CaloElectronMatch, 9999.));
      ntp->column( "BremMatch"  , proto->info(LHCb::ProtoParticle::additionalInfo::CaloBremMatch, 9999.));
      ntp->column( "TrajectoryL", proto->info(LHCb::ProtoParticle::additionalInfo::CaloTrajectoryL, 9999.));
      ntp->column( "VeloCharge" , proto->info(LHCb::ProtoParticle::additionalInfo::VeloCharge, -1.));
      ntp->column( "DLLe"       , proto->info(LHCb::ProtoParticle::additionalInfo::CombDLLe, 0.));
      ntp->column( "RichDLLe"   , proto->info(LHCb::ProtoParticle::additionalInfo::RichDLLe, 0.));
      // hypo info
      ntp->column("EoP"   , eOp );
      ntp->column("hypoE" , LHCb::CaloMomentum(hypo).e());
      ntp->column("hypoR" , hypoR );
      // track info
      ntp->column("Charge"        , track->charge());
      ntp->column("TrackP"        , t);
      ntp->column("TrackR"        , statR);
      ntp->column("ClosestR"      , closR);
      ntp->column("caloState"     , cs );
      ntp->column("caloStateErrX" , sqrt( fixup(m_caloElectron)->caloState().covariance()(0,0)));
      ntp->column("caloStateErrY" , sqrt( fixup(m_caloElectron)->caloState().covariance()(1,1)));
      ntp->column("caloStateErrTX", sqrt( fixup(m_caloElectron)->caloState().covariance()(2,2)));
      ntp->column("caloStateErrTY", sqrt( fixup(m_caloElectron)->caloState().covariance()(3,3)));
      ntp->column("incidence",theta);
      // cluster info
      ntp->column("id"      , id.index());
      ntp->column("ClusterE", cluster->e());
      ntp->column("ClusterR", clusR);
      ntp->column("SeedR"   , cellR);
      // brem info
      ntp->column("BremId"      , bid.index());
      ntp->column("BremP"       , bP);
      ntp->column("BremR"       , bremR);
      ntp->column("BremClusterR", bclusR);
      ntp->column("BremSeedR"   , bcellR);

      double bStX=0;
      double bStY=0;
      double bStTX=0;
      double bStTY=0;
      double bStCX=0;
      double bStCY=0;

      if( hasBrem ){
        auto bState = (LHCb::State*) track->stateAt(LHCb::State::BegRich1);
        if( bState == nullptr ){
          bState = track->states()[0];
          debug() << track->states();
          Warning("BegRich1 state does not exists - used first state", StatusCode::SUCCESS).ignore();
        }
        bStX  = sqrt( bState->covariance()(0,0) );
        bStY  = sqrt( bState->covariance()(1,1) );
        bStTX = sqrt( bState->covariance()(2,2) );
        bStTY = sqrt( bState->covariance()(3,3) ) ;
        const auto dz = bremR.Z()-bState->z();
        bStCX = bState->covariance()(0,0) +  bState->covariance()(2,2) * dz * dz +  bState->covariance()(0,2) * 2.*dz;
        bStCY = bState->covariance()(1,1) +  bState->covariance()(3,3) * dz * dz +  bState->covariance()(1,3) * 2.*dz;
        bStCX = (bStCX > 0) ? sqrt( bStCX) : 0;
        bStCY = (bStCY > 0) ? sqrt( bStCY) : 0;
      }
      ntp->column( "bStateErrX" , bStX);
      ntp->column( "bStateErrY" , bStY);
      ntp->column( "bStateErrTX", bStTX);
      ntp->column( "bStateErrTY", bStTY);
      ntp->column( "bCaloStateX", bStCX);
      ntp->column( "bCaloStateY", bStCY);

      // odin info
      ntp->column("run"        , odin.runNumber());
      ntp->column("event"      , (double) odin.eventNumber() );
      ntp->column("triggertype", odin.triggerType());
      ntp->column("nSpd"       , nSpd      );
      ntp->column("nTracks"    , nTracks   );
      ntp->column("nVertices"  , nVertices );
      if(m_pairing) ntp->column("MinMee",mas);
      ntp->write();
      if(m_counterStat->isQuiet()) counter("7-Events in ntuple")+=1;
    }

    // Histogramming
    if( !m_histo && !m_profil ) return;

    std::string prof ="";

    if(m_counterStat->isQuiet()) counter("8-Events in histos")+=1;

    const std::string area   = id.areaName() ;
    const std::string side   = (id.col() < 32) ? "C" : "A";
    const std::string level  = (id.row() < 32) ? "Bottom" : "Top";
    const std::string barea  = bid.areaName() ;
    const std::string bside  = (bid.col() < 32) ? "C" : "A";
    const std::string blevel = (bid.row() < 32) ? "Bottom" : "Top";

    const std::string charge = (track->charge() > 0 ) ? "electron" : "positron";
    std::vector<std::string> q{ "all", charge };

    std::vector<std::string> bsplit{
      "all/",
      barea+"/all/",
      barea+"/"+charge+"/all/",
      barea+"/"+bside+"/all/",
      barea+"/"+bside+"/"+charge+"/all/",
      barea+"/"+bside+"/"+ blevel +"/all/",
      barea+"/"+bside+"/"+ blevel +"/"+charge+"/all/",
      barea+"/"+blevel+"/all/",
      barea+"/"+blevel+"/"+charge+"/all/",
      barea+"/"+blevel+"/"+ bside +"/all/",
      barea+"/"+blevel+"/"+ bside +"/"+charge+"/all/",
    };

    std::vector<std::string> split{
      "all/",
      area+"/all/",
      area+"/"+charge+"/all/",
      area+"/"+side+"/all/",
      area+"/"+side+"/"+charge+"/all/",
      area+"/"+side+"/"+ level +"/all/",
      area+"/"+side+"/"+ level +"/"+charge+"/all/",
      area+"/"+level+"/all/",
      area+"/"+level+"/"+charge+"/all/",
      area+"/"+level+"/"+ side +"/all/",
      area+"/"+level+"/"+ side +"/"+charge+"/all/",
    };

    // ------ brem aligment (DeltaXX versus ThetaYY)
    const double thMin  = -0.30;
    const double thMax  = +0.30;
    const double bthX   = t.X()/t.Z();
    const double bthY   = t.Y()/t.Z();
    const double step   = (thMax-thMin)/m_thBin;
    if( hasBrem ){
      const double bdceX  = (bcellR.X() - bthX*bcellR.Z()); // relative to cell
      const double bdceY  = (bcellR.Y() - bthY*bcellR.Z());
      const double bdbX   = (bremR.X()  - bthX*bremR.Z());  // relative to hypo
      const double bdbY   = (bremR.Y()  - bthY*bremR.Z());
      const double bdclX  = (bclusR.X() - bthX*bclusR.Z()); // relative to cluster
      const double bdclY  = (bclusR.Y() - bthY*bclusR.Z());

      // extrapolate at Z = ShowerMax plane !!
      Gaudi::XYZPoint origin(0,0,0);
      Gaudi::XYZVector vector(bthX,bthY,1.);
      const Line line( origin ,vector );
      const auto plane  = m_calo->plane(CaloPlane::ShowerMax);
      const auto point  = intersection(line,plane);
      const double bdpX = (bremR.X() - bthX*point.Z());
      const double bdpY = (bremR.Y() - bthY*point.Z());

      const std::string bbinX = "bin"+Gaudi::Utils::toString( int((bthX-thMin)/step) );
      const std::string bbinY = "bin"+Gaudi::Utils::toString( int((bthY-thMin)/step) );
      std::vector<std::string> bhat{ "", bbinX, bbinY };

      plot1D(eOp,"eOp control (all)", 0.5, 2.,100);
      for( const auto label1: q ){
        const std::string base = "BremAlign/Delta/" + label1 + "/";
        if(m_histo){
          for( const auto label2: bhat ){
            plot1D( bdceX, label1+"Cell/"+label2+"/dX"   , "dX  : Cell-Brem(CellZ)"      , m_min, m_max, m_bin );
            plot1D( bdceY, label1+"Cell/"+label2+"/dY"   , "dY  : Cell-Brem(CellZ)"      , m_min, m_max, m_bin );
            plot1D( bdclX, label1+"Cluster/"+label2+"/dX", "dX  : Cluster-Brem(ClusterZ)", m_min, m_max, m_bin );
            plot1D( bdclY, label1+"Cluster/"+label2+"/dY", "dY  : Cluster-Brem(ClusterZ)", m_min, m_max, m_bin );
            plot1D( bdbX , label1+"Hypo/"+label2+"/dX"   , "dX  : Hypo-Brem(HypoZ)"      , m_min, m_max, m_bin );
            plot1D( bdbY , label1+"Hypo/"+label2+"/dY"   , "dY  : Hypo-Brem(HypoZ)"      , m_min, m_max, m_bin );
            plot1D( bdpX , label1+"HypoSM/"+label2+"/dX" , "dX  : Hypo-Brem(ShowerMax)"  , m_min, m_max, m_bin );
            plot1D( bdpY , label1+"HypoSM//"+label2+"/dY", "dY  : Hypo-Brem(ShowerMax)"  , m_min, m_max, m_bin );
          }
        }
        if(m_profil){
          profile1D( bthX, bdceX , label1+"Cell/dX.thX"   , "dX.vs.thX: Cell-Brem(CellZ)"      , thMin, thMax, m_thBin, prof, m_min, m_max );
          profile1D( bthY, bdceX , label1+"Cell/dX.thY"   , "dX.vs.thY: Cell-Brem(CellZ)"      , thMin, thMax, m_thBin, prof, m_min, m_max );
          profile1D( bthX, bdclX , label1+"Cluster/dX.thX", "dX.vs.thX: Cluster-Brem(ClusterZ)", thMin, thMax, m_thBin, prof, m_min, m_max );
          profile1D( bthY, bdclX , label1+"Cluster/dX.thY", "dX.vs.thY: Cluster-Brem(ClusterZ)", thMin, thMax, m_thBin, prof, m_min, m_max );
          profile1D( bthX, bdbX  , label1+"Hypo/dX.thX"   , "dX.vs.thX: Hypo-Brem(HypoZ)"      , thMin, thMax, m_thBin, prof, m_min, m_max );
          profile1D( bthY, bdbX  , label1+"Hypo/dX.thY"   , "dX.vs.thY: Hypo-Brem(HypoZ)"      , thMin, thMax, m_thBin, prof, m_min, m_max );
          profile1D( bthX, bdpX  , label1+"HypoSM/dX.thX" , "dX.vs.thX: Hypo-Brem(ShoweMax)"   , thMin, thMax, m_thBin, prof, m_min, m_max );
          profile1D( bthY, bdpX  , label1+"HypoSM/dX.thY" , "dX.vs.thY: Hypo-Brem(ShowerMax)"  , thMin, thMax, m_thBin, prof, m_min, m_max );
          profile1D( bthX, bdceY , label1+"Cell/dY.thX"   , "dY.vs.thX: Hypo-Brem(CellZ)"      , thMin, thMax, m_thBin, prof, m_min, m_max );
          profile1D( bthY, bdceY , label1+"Cell/dY.thY"   , "dY.vs.thY: Hypo-Brem(CellZ)"      , thMin, thMax, m_thBin, prof, m_min, m_max );
          profile1D( bthX, bdclY , label1+"Cluster/dY.thX", "dY.vs.thX: Cluster-Brem(ClusterZ)", thMin, thMax, m_thBin, prof, m_min, m_max );
          profile1D( bthY, bdclY , label1+"Cluster/dY.thY", "dY.vs.thY: Cluster-Brem(ClusterZ)", thMin, thMax, m_thBin, prof, m_min, m_max );
          profile1D( bthX, bdbY  , label1+"Hypo/dY.thX"   , "dY.vs.thX: Hypo-Brem(HypoZ)"      , thMin, thMax, m_thBin, prof, m_min, m_max );
          profile1D( bthY, bdbY  , label1+"Hypo/dY.thY"   , "dY.vs.thY: Hypo-Brem(HypoZ)"      , thMin, thMax, m_thBin, prof, m_min, m_max );
          profile1D( bthX, bdpY  , label1+"HypoSM/dY.thX" , "dY.vs.thX: Hypo-Brem(ShowerMax)"  , thMin, thMax, m_thBin, prof, m_min, m_max );
          profile1D( bthY, bdpY  , label1+"HypoSM/dY.thY" , "dY.vs.thY: Hypo-Brem(ShowerMax)"  , thMin, thMax, m_thBin, prof, m_min, m_max );
        }
      }

      // S-shape
      const auto bsize  = m_calo->cellSize(bid);
      const auto batx   = (bthX*bremR.Z()  - bcellR.X())/bsize; // brem provides the 'true' position @ HypoZ
      const auto baty   = (bthY*bremR.Z()  - bcellR.Y())/bsize;
      const auto batxSM = (bthX*point.Z()  - bcellR.X())/bsize; // brem provides the 'true' position @ ShowerMax
      const auto batySM = (bthY*point.Z()  - bcellR.Y())/bsize;
      const auto babx   = (bclusR.X()      - bcellR.X())/bsize; // e-weighted barycenter (cluster)
      const auto baby   = (bclusR.Y()      - bcellR.Y())/bsize;
      const auto bacx   = (bremR.X()       - bcellR.X())/bsize; // S-shape corrected barycenter (hypo)
      const auto bacy   = (bremR.Y()       - bcellR.Y())/bsize;

      for( const auto label: bsplit ){
        std::string base = "BremAlign/Sshape/" + label;
        if(m_histo){
          plot2D( batx  , babx, base + "Cluster/X"  , "bremX(HypoZ) / clusterX "   , -1*m_r, +1*m_r, -1*m_r, +1*m_r, m_b, m_b );
          plot2D( baty  , baby, base + "Cluster/Y"  , "bremY(HypoZ) / clusterY"    , -1*m_r, +1*m_r, -1*m_r, +1*m_r, m_b, m_b );
          plot2D( batx  , bacx, base + "Hypo/X"     , "bremX(HypoZ) / hypoX"       , -1*m_r, +1*m_r, -1*m_r, +1*m_r, m_b, m_b );
          plot2D( baty  , bacy, base + "Hypo/Y"     , "bremY(HypoZ) / hypoY"       , -1*m_r, +1*m_r, -1*m_r, +1*m_r, m_b, m_b );
          plot2D( batxSM, babx, base + "ClusterSM/X", "bremX(ShowerMax) / clusterX", -1*m_r, +1*m_r, -1*m_r, +1*m_r, m_b, m_b );
          plot2D( batySM, baby, base + "ClusterSM/Y", "bremY(ShowerMax) / clusterY", -1*m_r, +1*m_r, -1*m_r, +1*m_r, m_b, m_b );
          plot2D( batxSM, bacx, base + "HypoSM/X"   , "bremX(ShowerMax) / hypoX"   , -1*m_r, +1*m_r, -1*m_r, +1*m_r, m_b, m_b );
          plot2D( batySM, bacy, base + "HypoSM/Y"   , "bremY(ShowerMax) / hypoY"   , -1*m_r, +1*m_r, -1*m_r, +1*m_r, m_b, m_b );
        }
        if(m_profil){
          base = "BremAlign/Sshape/" + label;
          profile1D( batx  , babx, base + "Cluster/profX"  , "bremX(HypoZ) / clusterX "   , -1*m_r, +1*m_r, m_b , prof, -1*m_r, 1*m_r );
          profile1D( baty  , baby, base + "Cluster/profY"  , "bremY(HypoZ) / clusterY"    , -1*m_r, +1*m_r, m_b , prof, -1*m_r, 1*m_r );
          profile1D( batx  , bacx, base + "Hypo/profX"     , "bremX(HypoZ) / hypoX"       , -1*m_r, +1*m_r, m_b , prof, -1*m_r, 1*m_r );
          profile1D( baty  , bacy, base + "Hypo/profY"     , "bremY(HypoZ) / hypoY"       , -1*m_r, +1*m_r, m_b , prof, -1*m_r, 1*m_r );
          profile1D( batxSM, babx, base + "ClusterSM/profX", "bremX(ShowerMax) / clusterX", -1*m_r, +1*m_r, m_b , prof, -1*m_r, 1*m_r );
          profile1D( batySM, baby, base + "ClusterSM/profY", "bremY(ShowerMax) / clusterY", -1*m_r, +1*m_r, m_b , prof, -1*m_r, 1*m_r );
          profile1D( batxSM, bacx, base + "HypoSM/profX"   , "bremX(ShowerMax) / hypoX"   , -1*m_r, +1*m_r, m_b , prof, -1*m_r, 1*m_r );
          profile1D( batySM, bacy, base + "HypoSM/profY"   , "bremY(ShowerMax) / hypoY"   , -1*m_r, +1*m_r, m_b , prof, -1*m_r, 1*m_r );

          base = "BremAlign/iSshape/" + label;
          profile1D( babx, batx  , base + "Cluster/profX"  , "bremX(HypoZ) .vs. clusterX "    , -1*m_r, +1*m_r, m_b , prof, -1*m_r, 1*m_r);
          profile1D( baby, baty  , base + "Cluster/profY"  , "bremY(HypoZ) .vs. clusterY"     , -1*m_r, +1*m_r, m_b , prof, -1*m_r, 1*m_r);
          profile1D( bacx, batx  , base + "Hypo/profX"     , "bremX(HypoZ) .vs. hypoX"        , -1*m_r, +1*m_r, m_b , prof, -1*m_r, 1*m_r);
          profile1D( bacy, baty  , base + "Hypo/profY"     , "bremY(HypoZ) .vs. hypoY"        , -1*m_r, +1*m_r, m_b , prof, -1*m_r, 1*m_r);
          profile1D( babx, batxSM, base + "ClusterSM/profX", "bremX(ShowerMax) .vs. clusterX" , -1*m_r, +1*m_r, m_b , prof, -1*m_r, 1*m_r);
          profile1D( baby, batySM, base + "ClusterSM/profY", "bremY(ShowerMax) .vs. clusterY" , -1*m_r, +1*m_r, m_b , prof, -1*m_r, 1*m_r);
          profile1D( bacx, batxSM, base + "HypoSM/profX"   , "bremX(ShowerMax) .vs. hypoX"    , -1*m_r, +1*m_r, m_b , prof, -1*m_r, 1*m_r);
          profile1D( bacy, batySM, base + "HypoSM/profY"   , "bremY(ShowerMax) .vs. hypoY"    , -1*m_r, +1*m_r, m_b , prof, -1*m_r, 1*m_r);
        }
      }
    }


    // ----- track extrapolator alignment
    const double thX  = hypoR.X()/hypoR.Z();
    const double thY  = hypoR.Y()/hypoR.Z();
    const double dsX  = (hypoR.X() - statR.X()); // extrapolate @ ShowerMax plane
    const double dsY  = (hypoR.Y() - statR.Y());
    const double dcX  = (hypoR.X() - closR.X()); // extrapolate @ closest state
    const double dcY  = (hypoR.Y() - closR.Y());
    const double dceX = (cellR.X() - statR.X()); // relative to cell
    const double dceY = (cellR.Y() - statR.Y());
    const double dclX = (clusR.X() - statR.X()); // relative to cluster
    const double dclY = (clusR.Y() - statR.Y());

    // plot delta's
    const std::string binX = "bin"+Gaudi::Utils::toString( int((thX-thMin)/step) );
    const std::string binY = "bin"+Gaudi::Utils::toString( int((thY-thMin)/step) );
    const std::vector<std::string> hat{ "all", binX, binY };

    for( const auto label1: q ){
      std::string base = "ElectronAlign/Delta/" + label1 + "/";
      if(m_histo){
        for( const auto label2: hat ){
          plot1D( dceX, base+"Cell/"+label2+"/dX"   , "dX  : Cell-Track(CellZ)"       , m_min, m_max, m_bin );
          plot1D( dceY, base+"Cell/"+label2+"/dY"   , "dY  : Cell-Track(CellZ)"       , m_min, m_max, m_bin );
          plot1D( dclX, base+"Cluster/"+label2+"/dX", "dX  : Cluster-Track(ClusterZ)" , m_min, m_max, m_bin );
          plot1D( dclY, base+"Cluster/"+label2+"/dY", "dY  : Cluster-Track(ClusterZ)" , m_min, m_max, m_bin );
          plot1D( dsX , base+"HypoSM/"+label2+"/dX" , "dX  : Hypo-Track(ShowerMax)"   , m_min, m_max, m_bin );
          plot1D( dsY , base+"HypoSM/"+label2+"/dY" , "dY  : Hypo-Track(ShowerMax)"   , m_min, m_max, m_bin );
          plot1D( dcX , base+"Hypo/"+label2+"/dX"   , "dX  : Hypo-Track(HypoZ)"       , m_min, m_max, m_bin );
          plot1D( dcY , base+"Hypo/"+label2+"/dY"   , "dY  : Hypo-Track(HypoZ)"       , m_min, m_max, m_bin );
        }
      }
      if(m_profil){
        profile1D( bthX, dceX, base+"Cell/dX.thX"   , "dX.vs.thX: Cell-Track(CellZ)"      , thMin, thMax, m_thBin, prof, m_min, m_max );
        profile1D( bthY, dceX, base+"Cell/dX.thY"   , "dX.vs.thY: Cell-Track(CellZ)"      , thMin, thMax, m_thBin, prof, m_min, m_max );
        profile1D( bthX, dclX, base+"Cluster/dX.thX", "dX.vs.thX: Cluster-Track(ClusterZ)", thMin, thMax, m_thBin, prof, m_min, m_max );
        profile1D( bthY, dclX, base+"Cluster/dX.thY", "dX.vs.thY: Cluster-Track(ClusterZ)", thMin, thMax, m_thBin, prof, m_min, m_max );
        profile1D( bthX, dcX , base+"Hypo/dX.thX"   , "dX.vs.thX: Hypo-Track(HypoZ)"      , thMin, thMax, m_thBin, prof, m_min, m_max );
        profile1D( bthY, dcX , base+"Hypo/dX.thY"   , "dX.vs.thY: Hypo-Track(HypoZ)"      , thMin, thMax, m_thBin, prof, m_min, m_max );
        profile1D( bthX, dsX , base+"HypoSM/dX.thX" , "dX.vs.thX: Hypo-Track(ShowerMax)"  , thMin, thMax, m_thBin, prof, m_min, m_max );
        profile1D( bthY, dsX , base+"HypoSM/dX.thY" , "dX.vs.thY: Hypo-Track(ShowerMax)"  , thMin, thMax, m_thBin, prof, m_min, m_max );
        profile1D( bthX, dceY, base+"Cell/dY.thX"   , "dY.vs.thX: Hypo-Track(CellZ)"      , thMin, thMax, m_thBin, prof, m_min, m_max );
        profile1D( bthY, dceY, base+"Cell/dY.thY"   , "dY.vs.thY: Hypo-Track(CellZ)"      , thMin, thMax, m_thBin, prof, m_min, m_max );
        profile1D( bthX, dclY, base+"Cluster/dY.thX", "dY.vs.thX: Hypo-Track(ClusterZ)"   , thMin, thMax, m_thBin, prof, m_min, m_max );
        profile1D( bthY, dclY, base+"Cluster/dY.thY", "dY.vs.thY: Hypo-Track(ClusterZ)"   , thMin, thMax, m_thBin, prof, m_min, m_max );
        profile1D( bthX, dcY , base+"Hypo/dY.thX"   , "dY.vs.thX: Hypo-Track(HypoZ)"      , thMin, thMax, m_thBin, prof, m_min, m_max );
        profile1D( bthY, dcY , base+"Hypo/dY.thY"   , "dY.vs.thY: Hypo-Track(HypoZ)"      , thMin, thMax, m_thBin, prof, m_min, m_max );
        profile1D( bthX, dsY , base+"HypoSM/dY.thX" , "dY.vs.thX: Hypo-Track(ShowerMax)"  , thMin, thMax, m_thBin, prof, m_min, m_max );
        profile1D( bthY, dsY , base+"HypoSM/dY.thY" , "dY.vs.thY: Hypo-Track(ShowerMax)"  , thMin, thMax, m_thBin, prof, m_min, m_max );
      }
    }

    // S-shape
    const double size   = m_calo->cellSize(id);
    const double atx    = ( closR.X() - cellR.X())/size; // track propagation provides the 'true' position @ HypoZ
    const double aty    = ( closR.Y() - cellR.Y())/size;
    const double atxSM  = ( statR.X() - cellR.X())/size; // track propagation provides the 'true' position @ ShowerMax
    const double atySM  = ( statR.Y() - cellR.Y())/size;
    const double abx    = ( clusR.X() - cellR.X())/size; // e-weighted barycenter (cluster)
    const double aby    = ( clusR.Y() - cellR.Y())/size;
    const double acx    = ( hypoR.X() - cellR.X())/size; // S-shape corrected barycenter (hypo)
    const double acy    = ( hypoR.Y() - cellR.Y())/size;

    for( const auto label: split ){
      std::string base = "ElectronAlign/Sshape/" + label;
      if(m_histo){
        plot2D( atx  , abx, base + "Cluster/X"  , "trackX(bestZ) .vs. clusterX "    , -1*m_r, +1*m_r, -1*m_r, +1*m_r, m_b, m_b );
        plot2D( aty  , aby, base + "Cluster/Y"  , "trackY(bestZ) .vs. clusterY"     , -1*m_r, +1*m_r, -1*m_r, +1*m_r, m_b, m_b );
        plot2D( atx  , acx, base + "Hypo/X"     , "trackX(bestZ) .vs. hypoX"        , -1*m_r, +1*m_r, -1*m_r, +1*m_r, m_b, m_b );
        plot2D( aty  , acy, base + "Hypo/Y"     , "trackY(bestZ) .vs. hypoY"        , -1*m_r, +1*m_r, -1*m_r, +1*m_r, m_b, m_b );
        plot2D( atxSM, abx, base + "ClusterSM/X", "trackX(ShowerMax) .vs. clusterX" , -1*m_r, +1*m_r, -1*m_r, +1*m_r, m_b, m_b );
        plot2D( atySM, aby, base + "ClusterSM/Y", "trackY(ShowerMax) .vs. clusterY" , -1*m_r, +1*m_r, -1*m_r, +1*m_r, m_b, m_b );
        plot2D( atxSM, acx, base + "HypoSM/X"   , "trackX(ShowerMax) .vs. hypoX"    , -1*m_r, +1*m_r, -1*m_r, +1*m_r, m_b, m_b );
        plot2D( atySM, acy, base + "HypoSM/Y"   , "trackY(ShowerMax) .vs. hypoY"    , -1*m_r, +1*m_r, -1*m_r, +1*m_r, m_b, m_b );
      }
      if(m_profil){
        profile1D( atx  , abx, base + "Cluster/profX"   , "trackX(bestZ) .vs. clusterX "    , -1*m_r, +1*m_r, m_b, prof, -1*m_r, 1*m_r );
        profile1D( aty  , aby, base + "Cluster/profY"   , "trackY(bestZ) .vs. clusterY"     , -1*m_r, +1*m_r, m_b, prof, -1*m_r, 1*m_r );
        profile1D( atx  , acx, base + "Hypo/profX"      , "trackX(bestZ) .vs. hypoX"        , -1*m_r, +1*m_r, m_b, prof, -1*m_r, 1*m_r );
        profile1D( aty  , acy, base + "Hypo/profY"      , "trackY(bestZ) .vs. hypoY"        , -1*m_r, +1*m_r, m_b, prof, -1*m_r, 1*m_r );
        profile1D( atxSM, abx, base + "ClusterSM/profX" , "trackX(ShowerMax) .vs. clusterX" , -1*m_r, +1*m_r, m_b, prof, -1*m_r, 1*m_r );
        profile1D( atySM, aby, base + "ClusterSM/profY" , "trackY(ShowerMax) .vs. clusterY" , -1*m_r, +1*m_r, m_b, prof, -1*m_r, 1*m_r );
        profile1D( atxSM, acx, base + "HypoSM/profX"    , "trackX(ShowerMax) .vs. hypoX"    , -1*m_r, +1*m_r, m_b, prof, -1*m_r, 1*m_r );
        profile1D( atySM, acy, base + "HypoSM/profY"    , "trackY(ShowerMax) .vs. hypoY"    , -1*m_r, +1*m_r, m_b, prof, -1*m_r, 1*m_r );
        base = "ElectronAlign/iSshape/" + label;
        profile1D( abx, atx  , base + "Cluster/profX"   , "trackX(bestZ) .vs. clusterX "    , -1*m_r, +1*m_r, m_b, prof, -1*m_r, 1*m_r );
        profile1D( aby, aty  , base + "Cluster/profY"   , "trackY(bestZ) .vs. clusterY"     , -1*m_r, +1*m_r, m_b, prof, -1*m_r, 1*m_r );
        profile1D( acx, atx  , base + "Hypo/profX"      , "trackX(bestZ) .vs. hypoX"        , -1*m_r, +1*m_r, m_b, prof, -1*m_r, 1*m_r );
        profile1D( acy, aty  , base + "Hypo/profY"      , "trackY(bestZ) .vs. hypoY"        , -1*m_r, +1*m_r, m_b, prof, -1*m_r, 1*m_r );
        profile1D( abx, atxSM, base + "ClusterSM/profX" , "trackX(ShowerMax) .vs. clusterX" , -1*m_r, +1*m_r, m_b, prof, -1*m_r, 1*m_r );
        profile1D( aby, atySM, base + "ClusterSM/profY" , "trackY(ShowerMax) .vs. clusterY" , -1*m_r, +1*m_r, m_b, prof, -1*m_r, 1*m_r );
        profile1D( acx, atxSM, base + "HypoSM/profX"    , "trackX(ShowerMax) .vs. hypoX"    , -1*m_r, +1*m_r, m_b, prof, -1*m_r, 1*m_r );
        profile1D( acy, atySM, base + "HypoSM/profY"    , "trackY(ShowerMax) .vs. hypoY"    , -1*m_r, +1*m_r, m_b, prof, -1*m_r, 1*m_r );
      }
    }
  }
  return;
}

//==============================================================================
