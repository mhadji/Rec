#include "CaloEFlowBase.h"
#include "GaudiAlg/Consumer.h"

/*
  Class CaloEFlowMC

  Refactored from the original implementation to allow migration to Gaudi::Functional,
  such that this class's computation specialized in MC objects.

  Author: Chitsanu Khurewathanakul
  Data  : 2016-10-29
 */

namespace {
  using MCHits = LHCb::MCCaloHit::Container;
}

class CaloEFlowMC final
: public Gaudi::Functional::Consumer<void(const Input&, const MCHits&),
    Gaudi::Functional::Traits::BaseClass_t<CaloEFlowBase>>
{
public:
  StatusCode initialize() override;
  void operator()(const Input&, const MCHits&) const override;

  CaloEFlowMC(const std::string &name, ISvcLocator *pSvcLocator);

private:
  Gaudi::Property<std::string> m_slot {this, "Slot", "", "pile up evetn '' or Prev/ or Next/  "};
  Gaudi::Property<bool> m_mctruth {this, "MCTruth", false};

  const short int m_pidKplus = 321;
  const short int m_pidKminus = -321;
  const short int m_pidPiplus = 211;
  const short int m_pidPiminus = -211;
};

//==============================================================================

DECLARE_COMPONENT( CaloEFlowMC )

//==============================================================================

CaloEFlowMC::CaloEFlowMC( const std::string &name, ISvcLocator *pSvcLocator )
  : Consumer( name, pSvcLocator, {
      KeyValue{ "Input"      , "" },
      KeyValue{ "InputMCHits", "" },
  })
{}

//==============================================================================
// Initialization
//==============================================================================

StatusCode CaloEFlowMC::initialize() {
  StatusCode sc = Consumer::initialize();
  if ( sc.isFailure() ) return sc;

  // Configure InputMCHits, after m_slot is frozen
  // getting detector name from instance's name (so it requires the algo's name
  // to start with the detector name, e.g., EcalEFlowMon
  int index = name().find_last_of(".") +1 ; // return 0 if '.' not found --> OK !!
  auto detectorName = name().substr( index, 4 );
  if ( name().substr(index,3) == "Prs" ) detectorName = "Prs";
  if ( name().substr(index,3) == "Spd" ) detectorName = "Spd";
  auto InputMCHits = m_slot + "MC/" + detectorName + "/Hits";
  updateHandleLocation( *this, "InputMCHits", InputMCHits );

  return StatusCode::SUCCESS;
}

//==============================================================================
// Main execution
//==============================================================================

void CaloEFlowMC::operator()(const Input& digits, const MCHits& hits) const {

  // produce histos ?
  if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
    debug() << " Producing histo " << produceHistos() << endmsg;
  if ( !produceHistos() ) return;

  // get input data
  if ( digits.empty() ){
    if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
      debug() << "Empty digit found at " << inputLocation() << endmsg;
    return;
  }

  // Main execution in the CaloEFlowBase
  process_digits(digits);

  // Extra work for on MCTruth if requested
  if (!m_mctruth) return;

  if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
    debug() << "LOOKING FOR MCTRUTH" << endmsg;

  // Loop over MC hits
  for( const auto& hit: hits ){
    if ( hit == nullptr ) continue;
    const auto id = hit->cellID();
    if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
      debug() << "hit " << hit << " id " << id << endmsg;
    if(!(m_calo->valid(id) && !m_calo->isPinId( id ))) continue ;

    const double e = hit->activeE();
    const int pid = hit->particle()->particleID().pid();

    if( UNLIKELY( msgLevel(MSG::VERBOSE) ) )
      verbose() << " cellID " << id.index() << " e " << e  << endmsg;

    if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) {
      debug() << " part " << hit->particle() << endmsg;
      debug() << " partID " << pid
              << " origin vtx " << hit->particle()->originVertex()->position() << endmsg;
    }

    // Fill the histos
    if(pid==m_pidKplus) {
      if(doHisto("5")) fillCalo2D("5", id, 1., detData() + " K+ digits position 2D view");
      if(doHisto("6")) fillCalo2D("6", id, e , detData() + " K+ energy weighted - digits position 2D view");
    }
    else if(pid==m_pidKminus) {
      if(doHisto("7")) fillCalo2D("7", id, 1., detData() + " K- digits position 2D view");
      if(doHisto("8")) fillCalo2D("8", id, e , detData() + " K- energy weighted - digits position 2D view");
    }
    else if(pid==m_pidPiplus) {
      if(doHisto("9"))  fillCalo2D("9" , id, 1., detData() + " Pi+ digits position 2D view");
      if(doHisto("10")) fillCalo2D("10", id, e , detData() + " Pi+ energy weighted - digits position 2D view");
    }
    else if(pid==m_pidPiminus) {
      if(doHisto("11")) fillCalo2D("11", id, 1., detData() + " Pi- digits position 2D view");
      if(doHisto("12")) fillCalo2D("12", id, e , detData() + " Pi- energy weighted - digits position 2D view");
    }
  }
}
